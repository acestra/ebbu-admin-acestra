<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FeedDeleteCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'ebbu:feeddelete';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Used to Create a crone Job to move Old RSS .';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','250M');
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$error = "";
		$limitDay =  '-'.Config::get('services.limitday').' day';

		try {

			$today = Date('Y-m-d');
			$date = DateTime::createFromFormat('Y-m-d', $today);
			$date = date_modify($date,$limitDay);
			$date = date_format($date , 'Y-m-d');


			$EbooFeeds_last = EbooFeeds::where('article_pubdate','<=',$date)->get();		

			foreach ($EbooFeeds_last as $key => $value) {

				$EbooFeeds_change = EbooFeeds::find($value->id);
				$EbooFeedsOld = new EbooFeedsOld;
				$EbooFeedsOld->id_old = $EbooFeeds_change->id;
				$EbooFeedsOld->rss_link_id = $EbooFeeds_change->rss_link_id;
				$EbooFeedsOld->article_title = trim($EbooFeeds_change->article_title);
				$EbooFeedsOld->article_description = $EbooFeeds_change->article_description ;
				$EbooFeedsOld->article_link = $EbooFeeds_change->article_link;
				$EbooFeedsOld->clickcount = $EbooFeeds_change->clickcount;
				$EbooFeedsOld->likecount = $EbooFeeds_change->likecount;
				$EbooFeedsOld->readcount = $EbooFeeds_change->readcount;
				$EbooFeedsOld->article_pubdate = $EbooFeeds_change->article_pubdate;
				$EbooFeedsOld->session_date      = new DateTime;
				$EbooFeedsOld->status = 2;
				$EbooFeedsOld->save();
				$EbooFeeds = EbooFeeds::find($value->id);
				$EbooFeeds->delete();
			}
			
		} catch (Exception $e) {
			$error = $e;
		}
		if($error)
		{
			if (Config::get('services.mailSend.cronError')=='on') {
				Mail::send('emails.cron.error', array('firstname'=> Config::get('services.mailSend.Name'), 'error' => $error ) , function($message) {
					$message->to(Config::get('services.mailSend.Email'), Config::get('services.mailSend.Name'))->subject('Feed Delete have some problem!');
				});
			}
			$error = "";
		}
		else
		{
			if (Config::get('services.mailSend.cronComplete')=='on') {
				Mail::send('emails.cron.complete', array('firstname'=> Config::get('services.mailSend.Name') ) , function($message) {
					$message->to(Config::get('services.mailSend.Email'), Config::get('services.mailSend.Name'))->subject('Feed Delete Complete Without Error');
				});
			}
			$error = "";
		}

		Cache::flush();

		$this->line('Welcome to the user generator Feed .');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
