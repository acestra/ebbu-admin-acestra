<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FeedSyncCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'ebbu:feedsync';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Used to Create a crone Job to Synchronize RSS .';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		ini_set('display_errors', 0);
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','250M');
		//libxml_use_internal_errors(true);
		ini_set('magic_quotes_gpc', 0);
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		$error = "";
		$validatorCount = 0;
		$limitDay =  '-'.Config::get('services.limitday').' day';
		$EbooRss = EbooRss::where('status','=',1)
			//->where('id','=',688)
			->orderBy('session_date','asc')->get();

		foreach ($EbooRss as $rss_source)
		{
			$EbbuRss_change = EbooRss::find($rss_source->id);

			$rss_link = $rss_source->rss_link;
			
			$rss = new DOMDocument();
			try
			{

				$curl = curl_init();

				curl_setopt_array($curl, Array(
					CURLOPT_URL            => $rss_link,
					CURLOPT_RETURNTRANSFER => TRUE,
					CURLOPT_ENCODING       => 'UTF-8'
					));

				$data = curl_exec($curl);
				curl_close($curl);

				$xml = simplexml_load_string($data);
				
				if(isset($xml->entry)){
					$this->getXmlOptionEntry($xml,$rss_source->id);
				}else{
					$this->getXmlOptionChannel($xml,$rss_source->id);
				}
				/*print_r($articles);
              die;*/
			
			}
			catch(Exception $e){ 
				$error = $e;
				/* the data provided is not valid XML */
				//echo $e;
				Session::flash('message', 'Can`t updated feeds! '.$rss_source->id);
				/*$EbooRss = EbooRss::find($rss_source->id);
				$EbooRss->status = 2;
				$EbooRss->save();*/
			}
			
			$EbbuRss_change->session_date = new DateTime;
			$EbbuRss_change->save();
		}

		if ($validatorCount>0) {
			if (Config::get('services.mailSend.cronValidationError')=='on') {
				Mail::send('emails.cron.validate', array('firstname'=> Config::get('services.mailSend.Name'), 'count' => $validatorCount ) , function($message) {
					$message->to(Config::get('services.mailSend.Email'), Config::get('services.mailSend.Name'))->subject('Feed Insert has dublicate entry!');
				});
			}
			$validatorCount = 0;
		}
		if ($error) {
			if (Config::get('services.mailSend.cronError')=='on') {
				Mail::send('emails.cron.error', array('firstname'=> Config::get('services.mailSend.Name'), 'error' => $error ) , function($message) {
					$message->to(Config::get('services.mailSend.Email'), Config::get('services.mailSend.Name'))->subject('Feed Insert have some problem!');
				});
			}
			$error = "";
		}
		else
		{
			if (Config::get('services.mailSend.cronComplete')=='on') {
				Mail::send('emails.cron.complete', array('firstname'=> Config::get('services.mailSend.Name') ) , function($message) {
					$message->to(Config::get('services.mailSend.Email'), Config::get('services.mailSend.Name'))->subject('Feed Insert Complete Without Error');
				});
			}
			$error = "";
		}

		try {

			$today = Date('Y-m-d');
			$date = DateTime::createFromFormat('Y-m-d', $today);
			$date = date_modify($date,$limitDay);
			$date = date_format($date , 'Y-m-d');

			$EbooFeeds_last = EbooFeeds::where('article_pubdate','<=',$date)->get();		

			foreach ($EbooFeeds_last as $key => $value) {

				$EbooFeeds_change = EbooFeeds::find($value->id);
				/*$EbooFeedsOld = new EbooFeedsOld;
				$EbooFeedsOld->id_old = $EbooFeeds_change->id;
				$EbooFeedsOld->rss_link_id = $EbooFeeds_change->rss_link_id;
				$EbooFeedsOld->article_title = trim($EbooFeeds_change->article_title);
				$EbooFeedsOld->article_description = $EbooFeeds_change->article_description ;
				$EbooFeedsOld->article_link = $EbooFeeds_change->article_link;
				$EbooFeedsOld->clickcount = $EbooFeeds_change->clickcount;
				$EbooFeedsOld->likecount = $EbooFeeds_change->likecount;
				$EbooFeedsOld->readcount = $EbooFeeds_change->readcount;
				$EbooFeedsOld->article_pubdate = $EbooFeeds_change->article_pubdate;
				$EbooFeedsOld->session_date      = new DateTime;
				$EbooFeedsOld->status = 2;
				$EbooFeedsOld->save();*/
				$EbooFeeds = EbooFeeds::find($value->id);
				$EbooFeeds->delete();
			}
			
		} catch (Exception $e) {
			$error = $e;
		}
		if ($error) {
			if (Config::get('services.mailSend.cronError')=='on') {
				Mail::send('emails.cron.error', array('firstname'=> Config::get('services.mailSend.Name'), 'error' => $e ) , function($message) {
					$message->to(Config::get('services.mailSend.Email'), Config::get('services.mailSend.Name'))->subject('Feed Delete have some problem!');
				});
			}
			$error = "";
		}
		else
		{
			if (Config::get('services.mailSend.cronComplete')=='on') {
				Mail::send('emails.cron.complete', array('firstname'=> Config::get('services.mailSend.Name') ) , function($message) {
					$message->to(Config::get('services.mailSend.Email'), Config::get('services.mailSend.Name'))->subject('Feed Delete Complete Without Error');
				});
			}
			$error = "";
		}

		Cache::flush();

		$this->line('Welcome to the user generator Feed .');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}
	protected function getXmlOptionEntry($xml,$rss_source) {
		
         	foreach($xml->entry as $item)
				{
					$article_title = addslashes(trim($item->title));
					$article_pubdate = date("Y-m-d g:i", strtotime($item->published));
					$article_link = $item->link['href'];
					$EbooFeeds_find = EbooFeeds::where('article_title','=',$article_title)->get();

					if($EbooFeeds_find->count()==0){

						$rules = array(
							'article_title'       => 'required|unique:feeds',
							'article_link'       => 'required|unique:feeds',
							'article_pubdate'       => 'required'
							);
						$input_value = array(
							'article_title'       => $article_title,
							'article_link'       => $article_link,
							'article_pubdate'       => $article_pubdate
							);
						
						$validator = Validator::make($input_value , $rules);

						if ($validator->fails()) {
							$validatorCount++;
						} else {
							$EbooFeeds = new EbooFeeds;
							$yesterday = date('Y-m-d',strtotime("-10 days"));
							$formattedDate = date('Y-m-d',strtotime($article_pubdate));
							if($yesterday<=$formattedDate)
							{
								$EbooFeeds->rss_link_id = $rss_source;
								$EbooFeeds->article_title = $article_title;
								$EbooFeeds->article_description = "";
								$EbooFeeds->article_link = $article_link;
								$EbooFeeds->article_pubdate = $article_pubdate;
								$EbooFeeds->clickcount = 0;
								$EbooFeeds->likecount = 0;
								$EbooFeeds->readcount = 0; 
								$EbooFeeds->session_date      = new DateTime;
								$EbooFeeds->status  	= 1;
								$EbooFeeds->save();
							}
						}
					}
				}
	}


	protected function getXmlOptionChannel($xml) {
           $articles = $xml->channel->item;
        	foreach($articles as $item)
				{
					$article_title = addslashes(trim($item->title));
					$article_pubdate = date("Y-m-d g:i", strtotime($item->pubDate));
					$article_link = $item->link;

					$EbooFeeds_find = EbooFeeds::where('article_title','=',$article_title)->get();

					if($EbooFeeds_find->count()==0){

						$rules = array(
							'article_title'       => 'required|unique:feeds',
							'article_link'       => 'required|unique:feeds',
							'article_pubdate'       => 'required'
							);
						$input_value = array(
							'article_title'       => $article_title,
							'article_link'       => $article_link,
							'article_pubdate'       => $article_pubdate
							);
						
						$validator = Validator::make($input_value , $rules);

						if ($validator->fails()) {
							$validatorCount++;
						} else {
							$EbooFeeds = new EbooFeeds;
							$yesterday = date('Y-m-d',strtotime("-10 days"));
							$formattedDate = date('Y-m-d',strtotime($article_pubdate));
							if($yesterday<=$formattedDate)
							{
								$EbooFeeds->rss_link_id = $rss_source->id;
								$EbooFeeds->article_title = $article_title;
								$EbooFeeds->article_description = "";
								$EbooFeeds->article_link = $article_link;
								$EbooFeeds->article_pubdate = $article_pubdate;
								$EbooFeeds->clickcount = 0;
								$EbooFeeds->likecount = 0;
								$EbooFeeds->readcount = 0; 
								$EbooFeeds->session_date      = new DateTime;
								$EbooFeeds->status  	= 1;
								$EbooFeeds->save();
							}
						}
					}
				}

	}

}
