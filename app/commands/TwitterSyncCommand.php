<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TwitterSyncCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'ebbu:twittersync';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Used to Create a crone Job to Synchronize Twitter Feed.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','250M');
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$error = "";
		$validatorCount = 0;
		$dublicateCount = 0;
		$insertCount = 0;
		$limitDay =  '-'.Config::get('services.limitday').' day';
		$consumer_key =  Config::get('twitter.consumer_key');
		$consumer_secret =  Config::get('twitter.consumer_secret');
		$twitter_limit =  Config::get('twitter.twitter_limit');

		try{
			$connection = new TwitterOAuth($consumer_key, $consumer_secret);

			$yesterday = date('Y-m-d h:i:s',strtotime("-30 minutes "));
			$TwitterRss = TwitterRss::where('status','=',1)
				//->where('session_date','<',$yesterday)
				->orderBy('session_date','asc');

			$TwitterRss->count() < 150 ? $taken = $TwitterRss->count() : $taken = round($TwitterRss->count()/2);

			$TwitterRss = $TwitterRss->take($taken)->get();


			foreach ($TwitterRss as $twitter_source)
			{
				$TwitterRss_change = TwitterRss::find($twitter_source->id);
				try
				{
					$tweets = $connection->get('statuses/user_timeline', array('screen_name' => $twitter_source->twitter_handler ,'exclude_replies' => 'true', 'include_rts' => 'false', 'count' => $twitter_limit));
					if(!empty($tweets)) {
						foreach($tweets as $tweet) {
							if ($tweet->entities->urls) {
								$entities = $tweet->entities->urls[0]->expanded_url;
								$TwitterFeeds_find = TwitterFeeds::where('id_str','=',$tweet->id_str)->get();

								if($TwitterFeeds_find->count()==0){

									$tweetText = $tweet->text;
									$tweetText = preg_replace("#(http://|(www.))(([^s<]{4,68})[^s<]*)#", '', $tweetText);
									$tweetText = preg_replace('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $tweetText);
									$created_date = $tweet->created_at;

									$EbbuFeeds = EbooFeeds::where('article_title','=',$tweetText)->get();

									if($EbbuFeeds->count()==0)
									{

										$rules = array(
												'tweet_text'       => 'required|unique:twitter_feeds',
												'article_link'       => 'required|unique:twitter_feeds',
												'created_date'       => 'required'
											      );
										$input_value = array(
												'tweet_text'       => addslashes(trim($tweetText)),
												'article_link'       => $entities,
												'created_date'       => date("Y-m-d", strtotime($created_date))
												);
										$validator = Validator::make($input_value , $rules);

										if ($validator->fails()) {
											$validatorCount++;
										} else {
											$TwitterFeeds = new TwitterFeeds;

											$yesterday=date('Y-m-d',strtotime("-10 days"));
											$formattedDate = date("Y-m-d", strtotime($created_date));
											if($yesterday<=$formattedDate)
											{
												$TwitterFeeds->twitter_handler_id = $twitter_source->id;
												$TwitterFeeds->tweet_text = addslashes(trim($tweetText));
												$TwitterFeeds->article_link = $entities;
												$TwitterFeeds->id_str = $tweet->id_str;
												$TwitterFeeds->retweet_count = $tweet->retweet_count;
												$TwitterFeeds->click_count = 0;
												$TwitterFeeds->likecount = 0;
												$TwitterFeeds->readcount = 0; 
												$TwitterFeeds->created_date = date("Y-m-d h:i:s", strtotime($created_date));
												$TwitterFeeds->session_date      = new DateTime;
												$TwitterFeeds->status  	= 1;
												$TwitterFeeds->save();
											}
										}
										$insertCount++;
									}
									else
									{
										$dublicateCount++;
									}
								}
							}
						}
					}
					else
					{
						$error = "Empty";
					}
				}
				catch(Exception $e){ 
					$error = $e;
					//echo $e;
					/*
					   $TwitterRss = TwitterRss::find($twitter_source->id);
					   $TwitterRss->status = 2;
					   $TwitterRss->save();
					 */
				}
				$TwitterRss_change->session_date = new DateTime;
				$TwitterRss_change->save();
			}

		}
		catch(Exception $e){ 
			//echo $e;
			$error = $e;
		}
		if ($validatorCount>0) {
			if (Config::get('services.mailSend.cronValidationError')=='on') {
				Mail::send('emails.cron.validate', array('firstname'=> Config::get('services.mailSend.Name') , 'count' => $validatorCount ) , function($message) {
					$message->to(Config::get('services.mailSend.Email'), Config::get('services.mailSend.Name'))->subject('Twitter Feed Insert has dublicate entry!');
				});
			}
			$validatorCount = 0;
		}

		if ($error) {
			if (Config::get('services.mailSend.cronError')=='on') {
				Mail::send('emails.cron.error', array('firstname'=> Config::get('services.mailSend.Name'), 'error' => $error ) , function($message) {
					$message->to(Config::get('services.mailSend.Email'), Config::get('services.mailSend.Name'))->subject('Twitter Feed Insert have some problem!');
				});
			}
			$error = "";
		}
		else
		{
			if (Config::get('services.mailSend.cronComplete')=='on') {
				Mail::send('emails.cron.complete', array('firstname'=> Config::get('services.mailSend.Name') ) , function($message) {
					$message->to(Config::get('services.mailSend.Email'), Config::get('services.mailSend.Name'))->subject('Twitter Feed Insert Complete');
				});
			}
			$error = "";
		}

		if($dublicateCount>0)
		{
			if (Config::get('services.mailSend.cronDublicate')=='on') {
				Mail::send('emails.cron.duplicate', array('firstname'=> Config::get('services.mailSend.Name'), 'duplicate' => $dublicateCount ) , function($message) {
					$message->to(Config::get('services.mailSend.Email'), Config::get('services.mailSend.Name'))->subject('Twitter Feed Insert Complete with Duplicat');
				});
			}	
		}


		try
		{
			$today = Date('Y-m-d');
			$date = DateTime::createFromFormat('Y-m-d', $today);
			$date = date_modify($date,$limitDay);
			$date = date_format($date , 'Y-m-d');


			$TwitterFeeds_last = TwitterFeeds::where('created_date','<=',$date)->get();		

			foreach ($TwitterFeeds_last as $key => $value) {
				$TwitterFeeds_change = TwitterFeeds::find($value->id);
				$TwitterFeedsOld = new TwitterFeedsOld;
				$TwitterFeedsOld->twitter_handler_id = $TwitterFeeds_change->id;
				$TwitterFeedsOld->tweet_text = trim($TwitterFeeds_change->tweet_text);
				$TwitterFeedsOld->article_link = $TwitterFeeds_change->article_link;
				$TwitterFeedsOld->created_date = $TwitterFeeds_change->created_date;
				$TwitterFeedsOld->click_count = $TwitterFeeds_change->click_count;
				$TwitterFeedsOld->likecount = $TwitterFeeds_change->likecount;
				$TwitterFeedsOld->readcount = $TwitterFeeds_change->readcount;
				$TwitterFeedsOld->id_str = $TwitterFeeds_change->id_str;
				$TwitterFeedsOld->retweet_count = $TwitterFeeds_change->retweet_count;
				$TwitterFeedsOld->session_date      = new DateTime;
				$TwitterFeedsOld->status = 2;
				$TwitterFeedsOld->save();
				$TwitterFeeds = TwitterFeeds::find($value->id);
				$TwitterFeeds->delete();
			}

                        $EbbuFeeds = EbooFeeds::select('id','article_title',DB::raw('count(*) as total'))
                                ->groupBy('article_title')
                                ->havingRaw("total > 1")
                                ->get();

                        if($EbbuFeeds->count()>0)
                        {
                                foreach($EbbuFeeds as $EbbuFeedsDelete)
                                {
                                        $EbbuDelete = EbooFeeds::find($EbbuFeedsDelete->id);
                                        $EbbuDelete->delete();        
                                }                                     
                        }

		}
		catch(Exception $e)
		{
			$error = $e;
		}
		if ($error) {
			if (Config::get('services.mailSend.cronError')=='on') {
				Mail::send('emails.cron.error', array('firstname'=> Config::get('services.mailSend.Name'), 'error' => $error ) , function($message) {
					$message->to(Config::get('services.mailSend.Email'), Config::get('services.mailSend.Name'))->subject('Twitter Feed Delete have some problem!');
				});
			}
			$error = "";
		}
		else
		{
			if (Config::get('services.mailSend.cronComplete')=='on') {
				Mail::send('emails.cron.complete', array('firstname'=> Config::get('services.mailSend.Name')) , function($message) {
					$message->to(Config::get('services.mailSend.Email'), Config::get('services.mailSend.Name'))->subject('Twitter Feed Delete Complete without error');
				});
			}
			$error = "";
		}

		Cache::flush();

		$this->line('Welcome to the user generator Twitter Feed.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}