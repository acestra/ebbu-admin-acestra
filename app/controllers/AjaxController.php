<?php

class AjaxController extends BaseController {

    public function __construct( ) { 
    	ini_set('max_execution_time', 0); 
    	ini_set('memory_limit','250M');
    }

	public function twitterFeedsOldAjax()
	{
		//print_r($_REQUEST);
		$Feeds_return = array();
		$data = array();
		$new_skip = $_REQUEST['start']*$_REQUEST['draw'];
		$response = [
			"draw" => $_REQUEST['draw'],
			"recordsTotal" => TwitterFeeds::count(),
			"recordsFiltered" => TwitterFeeds::count(),
			'data' => []
		];
		try
		{
			$statusCode = 200;
			if (!$_REQUEST['search']['value']) {
				$TwitterFeeds = TwitterFeeds::select('id', 'twitter_handler_id','tweet_text','created_date','click_count','status');

				$check = '';
				$order = "";
				foreach($_REQUEST['order'] as $key => $value)
				{
					if($value['column']==0)
						$column = 'id';
					else if($value['column']==3)
						$column = 'tweet_text';
					else if($value['column']==4)
						$column = 'click_count';
					else if($value['column']==5)
						$column = 'created_date';
					else if($value['column']==6)
						$column = 'status';
					else
						$check = "Available";

					$order =  $value['dir'];

					if ($check!='Available') {
						$TwitterFeeds = $TwitterFeeds->orderBy( $column, $order);
					}

				}
				$TwitterFeeds = $TwitterFeeds->skip($_REQUEST['start'])->take($_REQUEST['length'])->get();

			}
			else
			{
				$TwitterFeeds = TwitterFeeds::select('id', 'twitter_handler_id','tweet_text','created_date','click_count','status')
					->where('status','=',1)
					->where('tweet_text','like', '%'.$_REQUEST['search']['value'].'%');

				$response["recordsFiltered"] = $TwitterFeeds->count();

				$TwitterFeeds = $TwitterFeeds->skip($_REQUEST['start'])->take($_REQUEST['length'])
						->get();	

				$response["recordsTotal"] = TwitterFeeds::count(); 

			}


			if($TwitterFeeds->count()>0)
			{
				foreach ($TwitterFeeds as $key => $value) {

					if( $value->status ==1) 
						$status =  "Enable"; 
					else
						$status = "Disable";


					$response['data'][] = [ $value->id,	 
					TwitterFeeds::getCategoryName($value->twitter_handler_id),
					TwitterFeeds::getPublicationName($value->twitter_handler_id),
					stripslashes($value->tweet_text),
					$value->click_count,
					date("F j, Y, g:i a" , strtotime($value->created_date) ),
					$status,
					];	
				}
			}
			else{
				$statusCode = 200;
				return Response::json($response, $statusCode);
			}

			return Response::json($response, $statusCode);
		}
		catch(Exception $e){
			//echo $e;
			$statusCode = 404;
			return Response::json($response, $statusCode);
		}
	}

	public function ebbuFeedsOldAjax()
	{
		//$EbooFeeds = EbooFeeds::select('id', 'rss_link_id','article_title','article_pubdate','clickcount','status')->get();
		$Feeds_return = array();
		$data = array();
		$new_skip = $_REQUEST['start']*$_REQUEST['draw'];
		$response = [
			"draw" => $_REQUEST['draw'],
			"recordsTotal" => EbooFeeds::count(),
			"recordsFiltered" => EbooFeeds::count(),
			'data' => []
		];
		try
		{
			$statusCode = 200;
			if (!$_REQUEST['search']['value']) {
				$EbooFeeds = EbooFeeds::select('id', 'rss_link_id','article_title','article_pubdate','clickcount','status');
				$check = '';
				$order = "";
				foreach($_REQUEST['order'] as $key => $value)
				{
					if($value['column']==0)
						$column = 'id';
					else if($value['column']==3)
						$column = 'article_title';
					else if($value['column']==4)
						$column = 'clickcount';
					else if($value['column']==5)
						$column = 'article_pubdate';
					else if($value['column']==6)
						$column = 'status';
					else
						$check = "Available";

					$order =  $value['dir'];

					if ($check!='Available') {
						$EbooFeeds = $EbooFeeds->orderBy( $column, $order);
					}

				}
				$EbooFeeds = $EbooFeeds->skip($_REQUEST['start'])->take($_REQUEST['length'])->get();
			}
			else
			{
				$EbooFeeds = EbooFeeds::select('id', 'rss_link_id','article_title','article_pubdate','clickcount','status')
					->where('status','=',1)
					->where('article_title','like', '%'.$_REQUEST['search']['value'].'%');

				$response["recordsFiltered"] = $EbooFeeds->count();

				$EbooFeeds = $EbooFeeds->skip($_REQUEST['start'])->take($_REQUEST['length'])
					->get();	

				$response["recordsTotal"] = EbooFeeds::count(); 
			}


			if($EbooFeeds->count()>0)
			{
				foreach ($EbooFeeds as $key => $value) {

					if( $value->status ==1) 
						$status =  "Enable"; 
					else
						$status = "Disable";


					$response['data'][] = [ $value->id,	 
					EbooFeeds::getCategoryName($value->rss_link_id),
					EbooFeeds::getPublicationName($value->rss_link_id),
					stripslashes($value->article_title),
					$value->clickcount,
					date("F j, Y, g:i a" , strtotime($value->article_pubdate) ),
					$status,
					];	
				}
			}
			else{
				$statusCode = 200;
				return Response::json($response, $statusCode);
			}

			return Response::json($response, $statusCode);
		}
		catch(Exception $e){
			//echo $e;
			$statusCode = 404;
			return Response::json($response, $statusCode);
		}
	}
	 public function remove_more_in_edit()
	{
		
		echo $id = Input::get('search_term_id');
		$CategorySearchTerm = SrchTermModel::find($id);;
		$CategorySearchTerm->delete();
        Cache::flush();
	}
	
	
	
}
