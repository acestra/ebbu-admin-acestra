<?php

class AppController extends BaseController {

	private $token_security ;

    public function __construct( ) { 
		header("Access-Control-Allow-Origin: *");
		header('Access-Control-Allow-Credentials: true');
    	$this->token_security = 543219588865478555;
    	$this->limitDay =  '-'.Config::get('services.limitday').' days';
    }

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function userinsert()
	{
		try{
			if($_REQUEST['security_token'] == $this->token_security)
			{
				$useremail=$_REQUEST['useremail'];
				$Users = Users::where('user_id','=',$useremail)
				->where('status','=',1)
				->get();

				if($Users->count()>0)
				{
					$responseInsert = array(
						"status"	=>'100' ,
						"message" 	=> 'success',
						"user_id" 	=> $Users[0]->id
						);	
				}
				else
				{
					$Users_new = new Users ; 
					$Users_new->user_id = $useremail;
					$Users_new->session_date = new DateTime;
					$Users_new->status = 1;
					$Users_new->save();
					$EbooUserUsage = new EbooUserUsage; 
					$EbooUserUsage->u_id = $Users_new->id;
					$EbooUserUsage->session_date = new DateTime;
					$EbooUserUsage->status = 1;
					$EbooUserUsage->save();
					$responseInsert= array(
						"status"=>'100',
						"message"=>'success',
						"user_id"=>$Users_new->id	
						);
				}

				$data=array("LoginResponse"=>$responseInsert );

				echo json_encode($data);
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function clickcount()
	{
		try{
			if($_REQUEST['security_token'] == $this->token_security)
			{
				$id = $_REQUEST['id'];
				$user_id = $_REQUEST['user_id'];
				$news_source = $_REQUEST['news_source'];

				if($news_source=='rss')
				{
					$EbooFeeds = EbooFeeds::find($id);
					$EbooFeeds->clickcount = $EbooFeeds->clickcount+1;
					$EbooFeeds->save();
				}
				else if($news_source=='twitter')
				{
					$TwitterFeeds = TwitterFeeds::find($id);
					$TwitterFeeds->click_count = $TwitterFeeds->click_count+1;
					$TwitterFeeds->save();	
				}
				$UserClickedData = new UserClickedData ;

				if (!$user_id) {
					$user_id = 0;
				}
				$UserClickedData->u_id = $user_id ;
				if($news_source=='rss')
					$UserClickedData->feeds_id = $id ;
				if($news_source=='twitter')
					$UserClickedData->tweets_id = $id ;
				$UserClickedData->session_date = new DateTime;
				$UserClickedData->status = 1 ;
				$UserClickedData->save();

				if(isset($_REQUEST['feed_conducted']) && isset($_REQUEST['search_query_id']))
				{
					$search_query_id=$_REQUEST['search_query_id'];
					$feed_conducted=$_REQUEST['feed_conducted'];

					if($feed_conducted==1 && $search_query_id!='')
					{
						$EbooUserSearch = EbooUserSearch::where('u_id','=',$user_id)
						->where('id','=',$search_query_id)
						->get();
						$EbooUserSearchs = EbooUserSearch::find($EbooUserSearch[0]->id);
						$EbooUserSearchs->articles_clicked = $EbooUserSearch[0]->articles_clicked+1;
						$EbooUserSearchs->save();
					}
				}
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function feedsearch()
	{
		$Feeds = array();
		try{
			if($_REQUEST['security_token'] == $this->token_security)
			{

				$cache_key = date('d_y_m').'_user_'.$_REQUEST['user_id'].'_tweet_'.$_REQUEST['new_tweet_skip_limit'].'_feed_'.$_REQUEST['new_rss_skip_limit'].'_search_'.$_REQUEST['q'];

				if(isset($_REQUEST['publication_id']))
					$cache_key .= '_publication_id_'.$_REQUEST['publication_id'];

				if(isset($_REQUEST['category_id']))
					$cache_key .= '_category_id_'.$_REQUEST['category_id'];

				if(Config::get('cache.driver')=='redis')
				{
					$redis = Redis::connection();
					$cache = $redis->get($cache_key);
				}
				else
				{
					$cache = Cache::has( $cache_key);
				}


				if( !$cache) {

					if(isset($_REQUEST['user_id']))
					{
						$user_id = $_REQUEST['user_id'];

					}else{
						$user_id = NULL;
					}

					$yesterday = date('Y-m-d',strtotime($this->limitDay));
					$today = date('Y-m-d');

					$shownRssArticle = $_REQUEST['shownrssarticle'];
					$shownTwitterArticle = $_REQUEST['showntwitterarticle'];

					$RSSarticle = explode(',',$shownRssArticle);
					$Twitterarticle = explode(',',$shownTwitterArticle); 

					if(isset($_REQUEST['publication_id']) && isset($_REQUEST['category_id']))
					{
						$selected_publication_id = $_REQUEST['publication_id'];
						$selected_category_id = $_REQUEST['category_id'];

					}else{
						$selected_publication_id = '0';
						$selected_category_id = '0';
					}

					$limit_count =  $_REQUEST['search_limit_count'] ; 

					$new_tweet_skip = $_REQUEST['new_tweet_skip_limit'];
					$new_rss_skip = $_REQUEST['new_rss_skip_limit'];

					$search_value = $_REQUEST['q'];

					$EbooUserSearch = new EbooUserSearch ;
					$EbooUserSearch->u_id = $user_id;
					$EbooUserSearch->search_query = $search_value;
					$EbooUserSearch->session_date = new DateTime;
					$EbooUserSearch->status = 1;
					$EbooUserSearch->save();


					$search_query_id = $EbooUserSearch->id;


					$UserClickedData = UserClickedData::where('u_id','=',$user_id)
						->where('status','=',1)
						->whereBetween('session_date', array($yesterday, $today))
						//->skip(0)->take($limit_count)
						->get();

					if($UserClickedData->count()>0)
					{
						foreach($UserClickedData as $value)
						{
							$user_view[] = $value->feeds_id; 
							$user_tweet_view[] = $value->tweets_id;
						}
					}
					else
					{
						$user_view[] = '';
						$user_tweet_view[] = '';
					}


					if($selected_publication_id!=0)
					{

						/************SELECTING THE RSS LINK ID WHICH IS SAME AS SELECTED PUBLICATION ID **********/
						/************(note: below query will always give atleast 1 row ,so no need to  checkfor mysql_num_rows***********/

						$select_feed_publication = EbooRss::where('publication_id','=',$selected_publication_id)
							->where('status','=',1)
							->get(array( 
										DB::Raw('group_concat(id) as selected_id')
								   ));

						foreach($select_feed_publication as $value_select)
						{
							$feed_publication_ids= $value_select->selected_id;
						}

						if($feed_publication_ids=='' || $feed_publication_ids==NULL )
						{
							$feed_publication_ids='0';	
						}

						$EbooFeeds = EbooFeeds::whereRaw("MATCH(article_title) AGAINST(?)",array($search_value))
							->where('status','=',1)
							->whereIn('rss_link_id', array($feed_publication_ids))
							->whereNotIn('id', array($shownRssArticle))
							->whereBetween('article_pubdate', array($yesterday, $today))
							->orderBy(DB::raw("MATCH(article_title) AGAINST('$search_value' in boolean mode)"),'DESC')
							->get();

						$select_twitter_publication = TwitterRss::where('publication_id','=',$selected_publication_id)
							->where('status','=',1)
							->get(array( 
										DB::Raw('group_concat(id) as selected_id')
								   ));

						foreach($select_twitter_publication as $value_select)
						{
							$twitter_publication_ids= $value_select->selected_id;
						}

						if($twitter_publication_ids=='' || $twitter_publication_ids==NULL )
						{
							$twitter_publication_ids='0';	
						}
						$TwitterFeeds = TwitterFeeds::whereRaw("MATCH(tweet_text) AGAINST(?)",array($search_value))
							->where('status','=',1)
							->whereIn('twitter_handler_id', array($twitter_publication_ids))
							->whereNotIn('id', array($shownTwitterArticle))
							->whereBetween('created_date', array($yesterday, $today))
							->orderBy(DB::raw("MATCH(tweet_text) AGAINST('$search_value' in boolean mode)"), 'DESC')
							->get();

					}
					else
					{
						$EbooFeeds = EbooFeeds::whereRaw("MATCH(article_title) AGAINST('?' in boolean mode)",array($search_value))
							->where('status','=',1)
							->whereNotIn('id', array($shownRssArticle))
							->whereBetween('article_pubdate', array($yesterday, $today))
							->orderBy('relevance','DESC')
							->skip($new_rss_skip)->take($limit_count)
							->select('*',DB::raw("MATCH(article_title) AGAINST('$search_value' in boolean mode) as relevance"))->get();

						$TwitterFeeds = TwitterFeeds::whereRaw("MATCH(tweet_text) AGAINST(?)",array($search_value))
							->where('status','=',1)
							->whereNotIn('id', array($shownTwitterArticle))
							->whereBetween('created_date', array($yesterday, $today))
							->orderBy('relevance2','DESC')
							->skip($new_tweet_skip)->take($limit_count)
							->select('*',DB::raw("MATCH(tweet_text) AGAINST('$search_value' in boolean mode) as relevance2"))->get();
					}

					$count_rss = $EbooFeeds->count();
					$count_tweet = $TwitterFeeds->count();

					$truth = json_decode($this->getSkipLimit($count_rss,$count_tweet));

					$new_rss_skip = $truth->count_allowed_rss;
					$new_tweet_skip = $truth->count_allowed_tweet;

					$feed = 1;
					$tweet = 1;

					if($EbooFeeds->count()>0 || $TwitterFeeds->count()>0)
					{

						$twitter = $this->getTwitterArray($TwitterFeeds,$user_tweet_view,$selected_publication_id,$selected_category_id,$new_tweet_skip);

						$Feed = $this->getFeedArray($EbooFeeds,$user_view,$selected_publication_id,$selected_category_id,$new_rss_skip);
						$Feeds = array_merge($twitter,$Feed);

						if(isset($Feeds))
						{

							if(isset($RSSarticle))
							{
								$rssarticle=implode(',',$RSSarticle);
							}else{
								$rssarticle='';
							}
							if(isset($Twitterarticle))
							{
								$tweetarticle=implode(',',$Twitterarticle);

							}else{
								$tweetarticle='';
							}	

							$data=array("Feeds"=>$Feeds , "FeedStatus"=>'success',"SearchQueryId"=>$search_query_id,"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip,"RSSarticle"=>$rssarticle,"Twitterarticle"=>$tweetarticle);
						}else{
							$data=array("FeedStatus"=>'fail');	
						}

					}else{
						$data=array("FeedStatus"=>'fail');
					}

					if(Config::get('services.appCache'))
					{
						if(Config::get('cache.driver')=='redis')
						{
							$redis->set($cache_key,json_encode($data));
							$redis->expire($cache_key,60);
						}else{
							Cache::put( $cache_key, json_encode($data), 1 );
						}	
					}

					echo json_encode($data);
				}else{
					if(Config::get('cache.driver')=='redis')
					{
						$data = $redis->get($cache_key);
					}
					else
					{
						$data = Cache::get( $cache_key );
					}

					echo $data;
				}
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function trending()
	{
		$Feeds = array();
		$screenName = "";
		$EbooFeeds = array();
		$TwitterFeeds = array();
		$twitter = "";
		$Feed = "";


		try{
			if($_REQUEST['security_token'] == $this->token_security)
			{
				$cache_key = date('d_y_m').'_user_'.$_REQUEST['user_id'].'_tweet_'.$_REQUEST['new_tweet_skip_limit'].'_feed_'.$_REQUEST['new_rss_skip_limit'];


				if(isset($_REQUEST['publication_id']))
					$cache_key .= '_publication_id_'.$_REQUEST['publication_id'];

				if(isset($_REQUEST['category_id']))
					$cache_key .= '_category_id_'.$_REQUEST['category_id'];

				$cache_key_feed = $cache_key.'_feed';
				$cache_key_data = $cache_key.'_data';

				if(isset($_REQUEST['user_id']))
				{
					$user_id=$_REQUEST['user_id'];
				}else{
					$user_id= NULL;
				}

				//$user_id = $this->getUserId($_REQUEST['user_id']);

				$yesterday=date('Y-m-d',strtotime($this->limitDay));
				$today=date('Y-m-d');				

				if(isset($_REQUEST['screenName']))
				{
					$screenName=$_REQUEST['screenName'];
				}

				if(Config::get('cache.driver')=='redis')
				{
					$redis = Redis::connection();
					$cache = $redis->get($cache_key_data);
				}
				else
				{
					$cache = Cache::has( $cache_key_data);
				}

				if(!$cache) { 

				$shownRssArticle = $_REQUEST['shownrssarticle'];
				$shownTwitterArticle = $_REQUEST['showntwitterarticle'];

				$RSSarticle = explode(',',$shownRssArticle);
				$Twitterarticle = explode(',',$shownTwitterArticle); 

				if(isset($_REQUEST['publication_id']) && isset($_REQUEST['category_id']))
				{
					$selected_publication_id = $_REQUEST['publication_id'];
					$selected_category_id = $_REQUEST['category_id'];

				}else{
					$selected_publication_id = '0';
					$selected_category_id = '0';
				}

				//$limit_skip = $_REQUEST['page_count'] * $_REQUEST['limit_count']  ;
				$new_tweet_skip=$_REQUEST['new_tweet_skip_limit'];
				$new_rss_skip=$_REQUEST['new_rss_skip_limit'];
				$limit_count =  $_REQUEST['limit_count'] ;

				$UserClickedData = UserClickedData::where('u_id','=',$user_id)
					->where('status','=',1)
					->whereBetween('session_date', array($yesterday, $today))
					->get();

				if($UserClickedData->count()>0)
				{
					foreach ($UserClickedData as $key => $value) {
						$user_view[] = $value->feeds_id; 
						$user_tweet_view[] = $value->tweets_id;
					}
				}
				else
				{
					$user_view[] = '';
					$user_tweet_view[] = '';
				}

				if($selected_publication_id!=0)
				{

					/************SELECTING THE RSS LINK ID WHICH IS SAME AS SELECTED PUBLICATION ID **********/
					/************(note: below query will always give atleast 1 row ,so no need to  checkfor mysql_num_rows***********/

					$select_feed_publication = '';
					$select_feed_publication = EbooRss::where('publication_id','=',$selected_publication_id)
						->where('status','=',1)
    						->get(array( DB::Raw('group_concat(id) as selected_id') ));

					foreach($select_feed_publication as $value_select)
					{
						$feed_publication_ids= $value_select->selected_id;
					}

					if($feed_publication_ids=='' || $feed_publication_ids==NULL )
					{
						$feed_publication_ids='0';	
					}

					$EbooFeeds = EbooFeeds::where('status','=',1)
						->whereIn('rss_link_id', array($feed_publication_ids))
						->whereNotIn('id', array($shownRssArticle))
						->whereBetween('article_pubdate', array($yesterday, $today))
						->skip($new_rss_skip)->take($limit_count)->get();

					$select_twitter_publication = TwitterRss::where('publication_id','=',$selected_publication_id)
						->where('status','=',1)
						->get(array( DB::Raw('group_concat(id) as selected_id') ));

					foreach($select_twitter_publication as $value_select)
					{
						$twitter_publication_ids = $value_select->selected_id;
					}

					if($twitter_publication_ids=='' || $twitter_publication_ids==NULL )
					{
						$twitter_publication_ids='0';	
					}

					$TwitterFeeds = TwitterFeeds::where('status','=',1)
						->whereIn('twitter_handler_id', array($twitter_publication_ids))
						->whereNotIn('id', array($shownTwitterArticle))
						->whereBetween('created_date', array($yesterday, $today))
						->skip($new_tweet_skip)->take($limit_count)->get();
				}
				else
				{

					$EbooFeeds = EbooFeeds::where('status','=',1)
						->whereNotIn('id', array($shownRssArticle))
						->whereBetween('article_pubdate', array($yesterday, $today))
						->orderBy(DB::raw('RAND()'));

					$TwitterFeeds = TwitterFeeds::where('status','=',1)
						->whereNotIn('id', array($shownTwitterArticle))
						->whereBetween('created_date', array($yesterday, $today))
						->orderBy(DB::raw('RAND()'));

					if($screenName=='mostpopular'){

						$EbooFeeds = $EbooFeeds->orderBy('clickcount','DESC')
							->skip($new_rss_skip)->take($limit_count)->get();

						$TwitterFeeds = $TwitterFeeds->orderBy('click_count','DESC')
							->skip($new_tweet_skip)->take($limit_count)->get();
					}
					else{

						$EbooFeeds = $EbooFeeds->skip($new_rss_skip)->take($limit_count)->get();

						$TwitterFeeds = $TwitterFeeds->skip($new_tweet_skip)->take($limit_count)->get();

					}

				}

				$count_rss = $EbooFeeds->count();
				$count_tweet = $TwitterFeeds->count();


				$truth = json_decode($this->getSkipLimit($count_rss,$count_tweet));

				$new_rss_skip = $truth->count_allowed_rss;
				$new_tweet_skip = $truth->count_allowed_tweet;

				$feed=1;
				$tweet=1;

				/*******************TWITTER ADDITION TO SEARCH********************/	

				if($EbooFeeds->count()>0 || $TwitterFeeds->count()>0)
				{

					$twitter = $this->getTwitterArray($TwitterFeeds,$user_tweet_view,$selected_publication_id,$selected_category_id,$new_tweet_skip);
					$Feed = $this->getFeedArray($EbooFeeds,$user_view,$selected_publication_id,$selected_category_id,$new_rss_skip);
					$Feeds = array_merge($twitter,$Feed);
					if(isset($Feeds))
					{

						if(isset($RSSarticle))
						{
							$rssarticle=implode(',',$RSSarticle);
						}else{
							$rssarticle='';
						}
						if(isset($Twitterarticle))
						{
							$tweetarticle=implode(',',$Twitterarticle);

						}else{
							$tweetarticle='';
						}	

						shuffle($Feeds);
						$data=array("Feeds"=>$Feeds, "FeedStatus"=>'success',"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip,"RSSarticle"=>$rssarticle,"Twitterarticle"=>$tweetarticle);
					}else{
						$data=array("FeedStatus"=>'fail');	
					}
				}
				else{
					$data=array("FeedStatus"=>'fail');
				}

				if(Config::get('services.appCache'))
				{
					if(Config::get('cache.driver')=='redis')
					{
						$redis->set($cache_key_feed,json_encode($Feeds));
						$redis->expire($cache_key_feed,60);
						$redis->set($cache_key_data,json_encode($data));
						$redis->expire($cache_key_data,60);
					}
					else
					{
						Cache::put( $cache_key_feed, $Feeds, 1 );
						Cache::put( $cache_key_data, json_encode($data), 1 );
					}	
				}

				echo json_encode($data);
				}
				else
				{

					$UserClickedData = UserClickedData::where('u_id','=',$user_id)
						->where('status','=',1)
						->whereBetween('session_date', array($yesterday, $today))
						->get();

					if($UserClickedData->count()>0)
					{
						foreach ($UserClickedData as $key => $value) {
							$user_view[] = $value->feeds_id; 
							$user_tweet_view[] = $value->tweets_id;
						}
					}
					else
					{
						$user_view[] = '';
						$user_tweet_view[] = '';
					}

					$new_rss_skip = '';
					$new_tweet_skip = '';
					$rssarticle = '';
					$tweetarticle = '';
					$Feeds = [];

					if(Config::get('cache.driver')=='redis')
					{
						$data_change = $redis->get($cache_key_data);
					}
					else
					{
						$data_change = Cache::get( $cache_key_data );
					}


					$data = json_decode($data_change);
					//print_r($data);
					foreach ($data as $keys => $values) {
						//print_r($values);
						if($keys=='Feeds')
						{
							foreach ($values as $key => $value) {
								//echo $value->newssource.'<br>';
								if($value->newssource=='rss')
								{
									if(in_array($value->id,$user_view))
									{
										$viewed_status = '1';
									}else{
										$viewed_status = '0';
									}
									$Feeds[] = array( "id" => $value->id,	 
											"source" => $value->source,
											"sourcename" => $value->sourcename,
											"article_link" => $value->article_link,
											"title" => trim($value->title),
											"description" => trim(preg_replace('/\s\s+/', ' ',$value->description)),
											"sourceimage" => $value->sourceimage,
											"newssource"=> $value->newssource,
											"pubdate" =>  $value->pubdate,
											"viewstatus" => $viewed_status,
											"publication_id"=>$value->publication_id,
											"category_id"=>$value->category_id
											);	
								}
								else if ($value->newssource=='twitter') {
									if(in_array($value->id,$user_tweet_view))
									{
										$viewed_status = '1';
									}else{
										$viewed_status = '0';
									}

									$Feeds[] = array( "id" => $value->id,	 
											"source" => $value->source,
											"sourcename" => $value->sourcename,
											"article_link" => $value->article_link,
											"title" => trim($value->title),
											"description" => trim(preg_replace('/\s\s+/', ' ',$value->description)),
											"sourceimage" => $value->sourceimage,
											"newssource"=> $value->newssource,
											"pubdate" =>  $value->pubdate,
											"viewstatus" => $viewed_status,
											"publication_id"=>$value->publication_id,
											"category_id"=>$value->category_id
											);	
								}
							}
						}
						if($keys == 'FeedStatus')
							$FeedStatus = $values;
						elseif($keys == 'rss_skip_limit')
							$new_rss_skip = $values;
						elseif($keys == 'twitter_skip_limit')
							$new_tweet_skip = $values;
						elseif($keys == 'RSSarticle')
							$rssarticle = $values;
						elseif($keys == 'Twitterarticle')
							$tweetarticle = $values;
					}
					$data = array("Feeds"=>$Feeds, "FeedStatus"=> $FeedStatus,"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip,"RSSarticle"=>$rssarticle,"Twitterarticle"=>$tweetarticle);
					echo json_encode($data);
				}
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function searchword()
	{
		try{
			if($_REQUEST['security_token'] == $this->token_security)
			{
				$EbooUserSearch = new EbooUserSearch;
				$EbooUserSearch->u_id = $_REQUEST['user_id'];
				$EbooUserSearch->search_query = $_REQUEST['id'];
				$EbooUserSearch->session_date = new DateTime;
				$EbooUserSearch->status = 1;
				$EbooUserSearch->save();
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function updatetime()
	{
		try{
			if($_REQUEST['security_token'] == $this->token_security)
			{
				$user_time=$_REQUEST['seconds'];
				$user_id=$_REQUEST['user_id'];
				$user_time = $user_time / 1000 ;
				$EbooUserUsage = EbooUserUsage::where('u_id','=',$user_id)->get();
				$total_user_time =  $EbooUserUsage[0]->total_usage + $user_time ; 
				$EbooUserUsage[0]->total_usage = $total_user_time ; 
				$EbooUserUsage[0]->session_date = new DateTime;
				$EbooUserUsage[0]->save();
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function feedback()
	{
		try{
			if($_REQUEST['security_token'] == $this->token_security)
			{
				$user_id = $_REQUEST['user_id'];

				//$name = $_REQUEST['name'];
				//$email_address = $_REQUEST['email_address'];
				$feedback = $_REQUEST['feedback'];
				$rating = $_REQUEST['rating'];
				$Feedback = new Feedback;
				$Feedback->u_id = $user_id;
				//$Feedback->name = $name;
				//$Feedback->email_address = $email_address;
				$Feedback->feedback = $feedback;
				$Feedback->rating = $rating;
				$Feedback->session_date = new DateTime;
				$Feedback->status = 1;
				$Feedback->save();
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}


        /**
         * Setup the layout used by the controller.
         *
         * @return void
         */

        protected function sourceview()
        {
		$Feeds = array();
                try{
                        if($_REQUEST['security_token'] == $this->token_security)
                        {
				$viewed_source=$_REQUEST['source_viewed'];
				$viewed_source_array=explode(',',$viewed_source);
				$source_skip_limit=$_REQUEST['source_limit'];

				$select_source = EbooPublication::where('status','=',1)
					->whereNotIn('id', $viewed_source_array)
					->take($source_skip_limit)
					->orderBy(DB::raw('RAND()'))
					->get();
				if($select_source->count()>0)
				{
					foreach($select_source as $Source)
					{

						$Feeds[]=array(
								"publication_id" =>$Source->id,
								"sourceimage" => htmlspecialchars($Source->publication_image,ENT_QUOTES),
								"sourcename"=>htmlspecialchars($Source->publication_name,ENT_QUOTES),
							      );

						$viewed_source_array[]=$Source->id;
					}

					$final_viewed_source=implode(',',$viewed_source_array);

					$data=array("Feeds"=>$Feeds, "FeedStatus"=>'success',"source_viewed"=>$final_viewed_source);
				}
				else{
					$data=array("FeedStatus"=>'fail');
				}
				echo json_encode($data);
                        }
                }catch(Exception $e){
                        //echo $e->getMessage();
                }
        }


	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function trendingsource()
	{
		$Feeds = array();
		$EbooFeeds = array();
		$TwitterFeeds = array();

		try{
			if($_REQUEST['security_token'] == $this->token_security)
			{
				$cache_key = date('d_y_m').'_user_'.$_REQUEST['user_id'].'_tweet_'.$_REQUEST['new_tweet_skip_limit'].'_feed_'.$_REQUEST['new_rss_skip_limit'];


				if(isset($_REQUEST['publication_id']))
					$cache_key .= '_publication_id_'.$_REQUEST['publication_id'];

				if(isset($_REQUEST['category_id']))
					$cache_key .= '_category_id_'.$_REQUEST['category_id'];

				$cache_key_feed = $cache_key.'_feed';
				$cache_key_data = $cache_key.'_data';

				if(isset($_REQUEST['user_id']))
				{
					$user_id=$_REQUEST['user_id'];
				}else{
					$user_id= NULL;
				}

				//$user_id = $this->getUserId($_REQUEST['user_id']);

				$yesterday=date('Y-m-d',strtotime($this->limitDay));
				$today=date('Y-m-d');				

				if(Config::get('cache.driver')=='redis')
				{
					$redis = Redis::connection();
					$cache = $redis->get($cache_key_data);
				}
				else
				{
					$cache = Cache::has( $cache_key_data);
				}

				if(!$cache) { 

					$shownRssArticle = $_REQUEST['shownrssarticle'];
					$shownTwitterArticle = $_REQUEST['showntwitterarticle'];

					$RSSarticle = explode(',',$shownRssArticle);
					$Twitterarticle = explode(',',$shownTwitterArticle); 

					if(isset($_REQUEST['publication_id']))
					{
						$selected_publication_id = $_REQUEST['publication_id'];
					}else{
						$selected_publication_id = '0';
					}

					//$limit_skip = $_REQUEST['page_count'] * $_REQUEST['limit_count']  ;
					$new_tweet_skip=$_REQUEST['new_tweet_skip_limit'];
					$new_rss_skip=$_REQUEST['new_rss_skip_limit'];
					$limit_count =  $_REQUEST['search_limit_count'];

					$publication_main_name = EbooPublication::getPublicationName($selected_publication_id);
					$search_value = trim($_REQUEST['q']);

					$UserClickedData = UserClickedData::where('u_id','=',$user_id)
						->where('status','=',1)
						->whereBetween('session_date', array($yesterday, $today))
						->get();

					if($UserClickedData->count()>0)
					{
						foreach ($UserClickedData as $key => $value) {
							$user_view[] = $value->feeds_id; 
							$user_tweet_view[] = $value->tweets_id;
						}
					}
					else
					{
						$user_view[] = '';
						$user_tweet_view[] = '';
					}

					if($selected_publication_id!=0)
					{

						/************SELECTING THE RSS LINK ID WHICH IS SAME AS SELECTED PUBLICATION ID **********/
						/************(note: below query will always give atleast 1 row ,so no need to  checkfor mysql_num_rows***********/

						$select_feed_publication = EbooRss::where('publication_id','=',$selected_publication_id)
							->where('status','=',1)
							->get(array(DB::Raw('group_concat(id) as selected_id')));

						foreach($select_feed_publication as $value_select)
						{
							$feed_publication_ids= $value_select->selected_id;
						}

						if($feed_publication_ids=='' || $feed_publication_ids==NULL )
						{
							$feed_publication_ids='0';	
						}

						$EbooFeeds = EbooFeeds::where('status','=',1)
							->whereIn('rss_link_id', array($feed_publication_ids))
							->whereNotIn('id', array($shownRssArticle))
							->whereBetween('article_pubdate', array($yesterday, $today))
							->orderBy(DB::raw('RAND()'))
							->skip($new_rss_skip)->take($limit_count)->get();

						$select_twitter_publication = TwitterRss::where('publication_id','=',$selected_publication_id)
							->where('status','=',1)
							->get(array(DB::Raw('group_concat(id) as selected_id')));

						foreach($select_twitter_publication as $value_select)
						{
							$twitter_publication_ids = $value_select->selected_id;
						}

						if($twitter_publication_ids=='' || $twitter_publication_ids==NULL )
						{
							$twitter_publication_ids='0';	
						}
						$TwitterFeeds = TwitterFeeds::where('status','=',1)
							->whereIn('twitter_handler_id', array($twitter_publication_ids))
							->whereNotIn('id', array($shownTwitterArticle))
							->whereBetween('created_date', array($yesterday, $today))
							->orderBy(DB::raw('RAND()'))
							->skip($new_tweet_skip)->take($limit_count)->get();
					}

					$count_rss = $EbooFeeds->count();
					$count_tweet = $TwitterFeeds->count();

					$truth = json_decode($this->getSkipLimit($count_rss,$count_tweet));

					$new_rss_skip = $truth->count_allowed_rss;
					$new_tweet_skip = $truth->count_allowed_tweet;

					$feed=1;
					$tweet=1;

					/*******************TWITTER ADDITION TO SEARCH********************/	

					if($EbooFeeds->count()>0 || $TwitterFeeds->count()>0)
					{
						$selected_category_id = 0;
						$twitter = $this->getTwitterArray($TwitterFeeds,$user_tweet_view,$selected_publication_id,$selected_category_id,$new_tweet_skip);
						$Feed = $this->getFeedArray($EbooFeeds,$user_view,$selected_publication_id,$selected_category_id,$new_rss_skip);
						$Feeds = array_merge($twitter,$Feed);
						if(isset($Feeds))
						{

							if(isset($RSSarticle))
							{
								$rssarticle=implode(',',$RSSarticle);
							}else{
								$rssarticle='';
							}
							if(isset($Twitterarticle))
							{
								$tweetarticle=implode(',',$Twitterarticle);

							}else{
								$tweetarticle='';
							}	

							shuffle($Feeds);
							$data=array("Feeds"=>$Feeds, "FeedStatus"=>'success',"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip,"RSSarticle"=>$rssarticle,"Twitterarticle"=>$tweetarticle);
							$data=array("Feeds"=>$Feeds , "FeedStatus"=>'success',"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip,"RSSarticle"=>$rssarticle,"Twitterarticle"=>$tweetarticle,"publication_main_name"=>$publication_main_name);

						}else{
							$data=array("FeedStatus"=>'fail');	
						}

						//$data=array("Feeds"=>$Feeds , "FeedStatus"=>'success',"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip);
					}
					else{
						$data=array("FeedStatus"=>'fail');
					}

					if(Config::get('services.appCache'))
					{
						if(Config::get('cache.driver')=='redis')
						{
							$redis->set($cache_key_feed,json_encode($Feeds));
							$redis->expire($cache_key_feed,60);
							$redis->set($cache_key_data,json_encode($data));
							$redis->expire($cache_key_data,60);
						}
						else
						{
							Cache::put( $cache_key_feed, $Feeds, 1 );
							Cache::put( $cache_key_data, json_encode($data), 1 );
						}	
					}

					echo json_encode($data);
				}
				else
				{
					$UserClickedData = UserClickedData::where('u_id','=',$user_id)
						->where('status','=',1)
						->whereBetween('session_date', array($yesterday, $today))
						->get();

					if($UserClickedData->count()>0)
					{
						foreach ($UserClickedData as $key => $value) {
							$user_view[] = $value->feeds_id; 
							$user_tweet_view[] = $value->tweets_id;
						}
					}
					else
					{
						$user_view[] = '';
						$user_tweet_view[] = '';
					}

					$new_rss_skip = '';
					$new_tweet_skip = '';
					$rssarticle = '';
					$tweetarticle = '';
					$Feeds = [];

					if(Config::get('cache.driver')=='redis')
					{
						$data_change = $redis->get($cache_key_data);
					}
					else
					{
						$data_change = Cache::get( $cache_key_data );
					}


					$data = json_decode($data_change);
					//print_r($data);
					foreach ($data as $keys => $values) {
						//print_r($values);
						if($keys=='Feeds')
						{
							foreach ($values as $key => $value) {
								//echo $value->newssource.'<br>';
								if($value->newssource=='rss')
								{
									if(in_array($value->id,$user_view))
									{
										$viewed_status = '1';
									}else{
										$viewed_status = '0';
									}
									$Feeds[] = array( "id" => $value->id,	 
											"source" => $value->source,
											"sourcename" => $value->sourcename,
											"article_link" => $value->article_link,
											"title" => trim($value->title),
											"description" => trim(preg_replace('/\s\s+/', ' ',$value->description)),
											"sourceimage" => $value->sourceimage,
											"newssource"=> $value->newssource,
											"pubdate" =>  $value->pubdate,
											"viewstatus" => $viewed_status,
											"publication_id"=>$value->publication_id,
											"category_id"=>$value->category_id
											);	
								}
								else if ($value->newssource=='twitter') {
									if(in_array($value->id,$user_tweet_view))
									{
										$viewed_status = '1';
									}else{
										$viewed_status = '0';
									}

									$Feeds[] = array( "id" => $value->id,	 
											"source" => $value->source,
											"sourcename" => $value->sourcename,
											"article_link" => $value->article_link,
											"title" => trim($value->title),
											"description" => trim(preg_replace('/\s\s+/', ' ',$value->description)),
											"sourceimage" => $value->sourceimage,
											"newssource"=> $value->newssource,
											"pubdate" =>  $value->pubdate,
											"viewstatus" => $viewed_status,
											"publication_id"=>$value->publication_id,
											"category_id"=>$value->category_id
											);	
								}
							}
						}
						if($keys == 'FeedStatus')
							$FeedStatus = $values;
						elseif($keys == 'rss_skip_limit')
							$new_rss_skip = $values;
						elseif($keys == 'twitter_skip_limit')
							$new_tweet_skip = $values;
						elseif($keys == 'RSSarticle')
							$rssarticle = $values;
						elseif($keys == 'Twitterarticle')
							$tweetarticle = $values;
						elseif($keys == 'publication_main_name')
							$publication_main_name = $values;

					}
					$data=array("Feeds"=>$Feeds , "FeedStatus"=>$FeedStatus,"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip,"RSSarticle"=>$rssarticle,"Twitterarticle"=>$tweetarticle,"publication_main_name"=>$publication_main_name);

					echo json_encode($data);
				}
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}



	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function adminlists()
	{
		$Feeds_return = array();
		$data = array();
		$response = [
		'admins' => []
		];
		try
		{
			if($_REQUEST['security_token'] == $this->token_security)
			{
				$cache_key = date('d_y_m').'_limit_'.$_REQUEST['limit_count'].'_new_skip_'.$_REQUEST['new_skip'];
				if( !Cache::has( $cache_key) ) {
					$statusCode = 200;

					$Admin = EbooAdmin::where('admin_user','!=','')
					->skip($_REQUEST['new_skip'])->take($_REQUEST['limit_count'])->get();

					if($Admin->count()>0)
					{
						foreach ($Admin as $key => $value) {

							$response['admins'][] = [ "id" => $value->id,	 
							"title" => $value->admin_user,
							];	

						}
					}
					else{
						$statusCode = 200;
						return Response::json($response, $statusCode);
					}
					Cache::put( $cache_key, $response, 1 );
					return Response::json($response, $statusCode);
				}
				else
				{
					$response = Cache::get( $cache_key );
					$statusCode = 200;
					return Response::json($response, $statusCode);
				}
			}
		}
		catch(Exception $e){
			echo $e;
			$statusCode = 404;
			return Response::json($response, $statusCode);
		}
	}


	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function singleadmin()
	{
		$Feeds_return = array();
		$data = array();
		$response = [
			'admins' => []
		];
		try
		{
			if($_REQUEST['security_token'] == $this->token_security)
			{
				$cache_key = 'admin_id_'.$_REQUEST['admin_id'];
				if( !Cache::has( $cache_key) ) {
					$statusCode = 200;

					$Admin = EbooAdmin::find($_REQUEST['admin_id']);

					if($Admin->count()>0)
					{
							$response['admins'][] = [ 
								"id" => $Admin->id,	 
								"title" => $Admin->admin_user,
							];
					}
					else{
						return Response::json($response, $statusCode);
					}
					Cache::put( $cache_key, $response, 1 );
					return Response::json($response, $statusCode);
				}
				else
				{
					$response = Cache::get( $cache_key );
					$statusCode = 200;
					return Response::json($response, $statusCode);
				}
			}
		}
		catch(Exception $e){
			$statusCode = 404;
			return Response::json($response, $statusCode);
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function getUserId($user_id='')
	{
		if($user_id!='')
		{
			$user_id = $user_id;

		}else{
			$user_id = NULL;
		}
		return $user_id;
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function getSkipLimit($count_rss=0,$count_tweet=0)
	{
		if($count_rss>=5 && $count_tweet>=5)
		{
			$count_allowed_rss=5;
			$count_allowed_tweet=5;
		}
		elseif($count_rss>=5 && $count_tweet<5)
		{
			$count_allowed_rss=$count_rss-$count_tweet;
			$count_allowed_tweet=$count_tweet;
		}
		elseif($count_rss<5 && $count_tweet>=5)		
		{
			$count_allowed_tweet=$count_tweet-$count_rss;
			$count_allowed_rss=$count_rss;

		}
		elseif($count_rss<5 && $count_tweet<5)
		{
			$count_allowed_rss=$count_rss;
			$count_allowed_tweet=$count_tweet;
		}
		$response = [ 
			"count_allowed_rss" => $count_allowed_rss,
			"count_allowed_tweet" => $count_allowed_tweet,
		];
		return json_encode($response);
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function getPubDate($created_date='')
	{
		if( $created_date < date('Y-m-d',strtotime('-3 days')) ) {
			$pubdate = '<img src="img/fire03.png" style="width:6px !important;" >';
		}
		else if( $created_date >=  date('Y-m-d',strtotime('-3 days')) &&  $created_date <= date('Y-m-d',strtotime('-5 days'))){
			$pubdate = '<img src="img/fire02.png" style="width:6px !important;" >';
		}
		else 
		{
			$pubdate = '<img src="img/fire01.png" style="width:6px !important;" >';	
		}
		return $pubdate;
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function getArticleTitle($article_title='')
	{
		$article_title = trim(preg_replace( "/\r|\n/", " ", $article_title));
		$title = strlen(trim($article_title))>75 ? mb_substr(trim($article_title),0,72).'...' : trim($article_title);
		return stripslashes($title);
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function getFeedArray($EbooFeeds='',$user_view='',$selected_publication_id=0,$selected_category_id=0,$new_rss_skip=0,$feed=1)
	{
		$Feeds = array();
		foreach ($EbooFeeds as $key => $value) {
			$publication_name = EbooFeeds::getPublicationName($value->rss_link_id);
			$category_name = EbooFeeds::getCategoryName($value->rss_link_id);
			$publication_image = EbooFeeds::getPublicationImage($value->rss_link_id);

			//$pubdate = $this->getPubDate($value->article_pubdate);

			$pubdate ="";

			if(in_array($value->id,$user_view))
			{
				$viewed_status='1';
			}else{
				$viewed_status='0';
			}

			$title = $this->getArticleTitle($value->article_title);

			$publication_id = EbooFeeds::getPublicationId($value->rss_link_id);
			$category_id = EbooFeeds::getCategoryId($value->rss_link_id);

			if($selected_publication_id!=0 && $selected_category_id!=0)
			{
				$Feeds[] = array( "id" => $value->id,
					"source" => htmlspecialchars($publication_name,ENT_QUOTES),
					"sourcename" => htmlspecialchars($publication_name,ENT_QUOTES),
					"article_link" => $value->article_link,
					"title" => htmlspecialchars($title,ENT_QUOTES),
					"description" => "",
					"sourceimage" => htmlspecialchars($publication_image,ENT_QUOTES),
					"newssource"=>'rss',
					"pubdate" =>  htmlspecialchars($pubdate,ENT_QUOTES),
					"viewstatus" => htmlspecialchars($viewed_status,ENT_QUOTES),
					"publication_id"=>htmlspecialchars($publication_id,ENT_QUOTES),
					"category_id"=>htmlspecialchars($category_id,ENT_QUOTES)
					);

			}
			else
			{
				$Feeds[] = array( "id" => $value->id,
					"source" => htmlspecialchars($publication_name,ENT_QUOTES),
					"sourcename" => htmlspecialchars($publication_name,ENT_QUOTES),
					"article_link" => $value->article_link,
					"title" => htmlspecialchars($title,ENT_QUOTES),
					"description" => "",
					"sourceimage" => htmlspecialchars($publication_image,ENT_QUOTES),
					"newssource"=>'rss',
					"pubdate" =>  htmlspecialchars($pubdate,ENT_QUOTES),
					"viewstatus" => htmlspecialchars($viewed_status,ENT_QUOTES),
					"publication_id"=>htmlspecialchars($publication_id,ENT_QUOTES),
					"category_id"=>htmlspecialchars($category_id,ENT_QUOTES)
					);
			}
			if ($feed>=$new_rss_skip) {
				break;
			}
			$feed++;
		}
		return $Feeds;
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function getTwitterArray($TwitterFeeds='',$user_tweet_view='',$selected_publication_id=0,$selected_category_id=0,$new_tweet_skip=0,$tweet=1)
	{
		$Feeds = array();
		foreach ($TwitterFeeds as $key => $value) {
			$TwitterRss = TwitterRss::find($value->twitter_handler_id);
			if($TwitterRss->count()>0)
			{
				$publication_name = TwitterFeeds::getPublicationName($value->twitter_handler_id);
				$category_name = TwitterFeeds::getCategoryName($value->twitter_handler_id);
				$publication_image = TwitterFeeds::getPublicationImage($value->twitter_handler_id);

							//$pubdate = $this->getPubDate($value->created_date);
				$pubdate ="";

				$publication_id_tweet = TwitterFeeds::getPublicationId($value->twitter_handler_id);
				$category_id_tweet = TwitterFeeds::getCategoryId($value->twitter_handler_id);

				if(in_array($value->id,$user_tweet_view))
				{
					$viewed_status='1';
				}else{
					$viewed_status='0';
				}
							//$article_title = trim(preg_replace( "/\r|\n/", "", preg_replace('/[^A-Za-z0-9\-]/', ' ', $value->tweet_text)));
				$title = $this->getArticleTitle($value->tweet_text);
				if($selected_publication_id!=0 && $selected_category_id!=0)
				{

					$Feeds[]=array( "id" => $value->id,	 
						"source" => htmlspecialchars($publication_name,ENT_QUOTES),
						"sourcename" => htmlspecialchars($publication_name,ENT_QUOTES),
						"article_link" => $value->article_link,
						"title" => htmlspecialchars($title,ENT_QUOTES),
						"description" => "",
						"sourceimage" => htmlspecialchars($publication_image,ENT_QUOTES),
						"newssource"=>'twitter',
						//"twitterid" => $value->id_str,
						"pubdate" =>  htmlspecialchars($pubdate,ENT_QUOTES),
						"viewstatus" => htmlspecialchars($viewed_status,ENT_QUOTES),
						"publication_id"=>htmlspecialchars($publication_id_tweet,ENT_QUOTES),
						"category_id"=>htmlspecialchars($category_id_tweet,ENT_QUOTES)
						);
				}
				else
				{
					$Feeds[]=array( "id" => $value->id,	 
						"source" => htmlspecialchars($publication_name,ENT_QUOTES),
						"sourcename" => htmlspecialchars($publication_name,ENT_QUOTES),
						"article_link" => $value->article_link,
						"title" => htmlspecialchars($title,ENT_QUOTES),
						"description" => "",
						"sourceimage" => htmlspecialchars($publication_image,ENT_QUOTES),
						"newssource"=>'twitter',
						//"twitterid" => $value->id_str,
						"pubdate" =>  htmlspecialchars($pubdate,ENT_QUOTES),
						"viewstatus" => htmlspecialchars($viewed_status,ENT_QUOTES),
						"publication_id"=>htmlspecialchars($publication_id_tweet,ENT_QUOTES),
						"category_id"=>htmlspecialchars($category_id_tweet,ENT_QUOTES)
						);
				}
			}
			if ($tweet>=$new_tweet_skip) {
				break;
			}
			$tweet++;
		}

		return $Feeds;
	}
}


