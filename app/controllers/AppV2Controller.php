<?php

class AppV2Controller extends BaseController {

	private $token_security ;

    public function __construct( ) { 
		header("Access-Control-Allow-Origin: *");
		header('Access-Control-Allow-Credentials: true');
    	$this->token_security = 543219588865478555;
    	$this->limitDay =  '-'.Config::get('services.limitday').' days';
    }

	public function userSessionVerify($userID='',$sessionCode='')
	{
		$UserSession = UserSession::where('user_id','=',$userID)
			->where('session_code','=',$sessionCode)
			->get();

		if ($UserSession->count()>0) {
				return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function userinsert()
	{

		$verification_code = rand(10000, 99999);
		$today = date('Y-m-d H:i:s');
		$mailSend = false;
		$expire_datetime = date('Y-m-d H:i:s', strtotime($today . ' + 1 day'));

		try{
			if($_REQUEST['security_token'] == $this->token_security)
			{
				$useremail = $_REQUEST['useremail'];
				$UserDetails = Users::where('user_id','=',$useremail);

                $users = $UserDetails->get();
                $userscheck = $UserDetails->first();
				
				if($users->count()>0)
				{

					if (isset($_REQUEST['source'])) {

						$session = $this->setUserSession($userscheck->id,1);
						
						$responseInsert = array(
							"status"	=>'100' ,
							"message" 	=> 'success',
							"userStatus" => 1,
							"user_id" 	=> $userscheck->id,
							"session_code" => $session
						);
					}
					else
					{
						$usersAlready = $UserDetails->where('status','=',1)->get();
						if(count($usersAlready)>0)
						{
							$foruser_id = $UserDetails->first();

							//user goes to disable 
							$userdisable = Users::where('user_id','=',$useremail)->first();
							if (isset($_REQUEST['username'])) {
								$userdisable->username = $_REQUEST['username'];
							}
							if (isset($_REQUEST['source'])) {
								$userdisable->source = $_REQUEST['source'];
							}
							else
							{
								$userdisable->source = "email";
							}
							$userdisable->status = 2;
							$userdisable->save();

							$session = $this->setUserSession($userscheck->id,$userdisable->status);

							$responseInsert = array(
								"status"	=>'101' ,
								"message" 	=> 'Pending',
								"userStatus" => $userdisable->status,
								"user_id" 	=> $userscheck->id,
								"session_code" => $session
							);
							

						}else{

							$session = $this->setUserSession($userscheck->id,$userscheck->status);

							$responseInsert = array(
								"status"	=>'101' ,
								"message" 	=> 'Pending',
								"userStatus" => $userscheck->status,
								"user_id" 	=> $userscheck->id,
								"session_code" => $session
							);
						}
						$UserVerification = UserVerifications::where('user_id','=',$userscheck->id)->first();
						if(!isset($UserVerification))
						{
							$UserVerification = new UserVerifications;
							$UserVerification->status = 1; 
							$UserVerification->user_id = $userscheck->id;
							$UserVerification->expire_date = $expire_datetime;
							$UserVerification->verification_code = $verification_code;
							$UserVerification->save();
							$mailSend = true;
						}
						else if($UserVerification->status==2&&$UserVerification->expire_date>$today)
						{
							$verification_code = $verification_code;
							$UserVerification->status = 1; 
							$UserVerification->save();
							$mailSend = true;
						}
						else if($UserVerification->status==1)
						{
							$userv = UserVerifications::find($UserVerification->id);
							$userv->verification_code = $verification_code;
							$userv->expire_date = $expire_datetime;
							$userv->status = 1;
							$userv->save();
							$verification_code = $verification_code;
							$mailSend = true;
						}
						else
						{   
							$userv = UserVerifications::find($UserVerification->id);
							$userv->verification_code = $verification_code;
							$userv->expire_date = $expire_datetime;
							$userv->status = 1;
							$userv->save();

							$verification_code = $verification_code;
							//$UserVerification->status = 1; 
							//$UserVerification->expire_date = $expire_datetime;
							//$UserVerification->save();
							$mailSend = true;
						}

						if ($mailSend==true) {
                    	//mail
							$user = array('email'=>$useremail) ;
							$data = array(
								'detail'=>'Thank you for signing up to use Ebbu. We are excited to help you discover great articles.',
								'expire_date' =>$expire_datetime,
								'code'	=> $verification_code
							);

							Mail::send('emails.user_verification', $data, function($message) use ($user)
							{
								$message->to($user['email'])->subject('User Verification Code');
							});
							$verification_code;
						}
					}
				}
				else
				{
                     //new user
					
					$Users_new = new Users; 
					$Users_new->user_id = $useremail;
					$Users_new->session_date = new DateTime;
					if (isset($_REQUEST['username'])) {
						$Users_new->username = $_REQUEST['username'];
					}
					if (isset($_REQUEST['source'])) {
						$Users_new->source = $_REQUEST['source'];
					}
					else
					{
						$Users_new->source = "email";
					}
					$Users_new->location = "";
					$Users_new->city = "";
					$Users_new->state = "";
					$Users_new->pincode = 0;
					$Users_new->country = "";
					$Users_new->hardware_info = "";
					$Users_new->software_version = "";
					$Users_new->fb_handle = "";
					$Users_new->tumblr_handle = "";
					$Users_new->linkedin_handle = "";
					$Users_new->first_usage = 0;
					$Users_new->status = 2;// default status is disable(2), after verification enable(1)
					$Users_new->save();

					$EbbuUserUsage = new EbooUserUsage; 
					$EbbuUserUsage->u_id = $Users_new->id;
					$EbbuUserUsage->session_date = new DateTime;
					$EbbuUserUsage->total_usage = 0 ; 
					$EbbuUserUsage->status = 1;
					$EbbuUserUsage->save();

					if (isset($_REQUEST['source'])) {

						$session = $this->setUserSession($Users_new->id,1);

						$responseInsert = array(
							"status"	=>'100' ,
							"message" 	=> 'success',
							"userStatus" => 1,
							"user_id" 	=> $Users_new->id,
							"session_code" => $session
						);
						
					}
					else
					{

						$session = $this->setUserSession($Users_new->id,$Users_new->status);

						$responseInsert= array(
							"status"=>'101',
							"message"=>'pending',
							"userStatus" => $Users_new->status,
							"user_id"=>$Users_new->id,
							"session_code" => $session	
						);

						

						/* Mail*/
						$verification_code = rand(10000, 99999);
						$today = date('Y-m-d H:i:s');
						$expire_datetime = date('Y-m-d H:i:s', strtotime($today . ' + 1 day'));
						$user = array('email'=>$useremail) ;
						$data = array(
							'detail'=>'Thank you signing up to use Ebbu. We are excited to help you discover great articles.',
							'expire_date' =>$expire_datetime,
							'code'	=> $verification_code
						);

						Mail::send('emails.user_verification', $data, function($message) use ($user)
						{
							$message->to($user['email'])->subject('User Verification Code');
						});
						/* store to db*/

						$UserVerification = new UserVerifications;
						$UserVerification->user_id = $Users_new->id;
						$UserVerification->verification_code = $verification_code;
						$UserVerification->expire_date = $expire_datetime;
						$UserVerification->status = 1; 
						$UserVerification->save();
						$verification_code;
					}
				}

				$data= array("LoginResponse"=>$responseInsert );

				echo json_encode($data);
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	public function setUserSession($userID='',$status=2)
	{
		$session_code = rand(100000000, 999999999);

		$UserSession = UserSession::where('user_id','=',$userID)
			->first();

		if (isset($UserSession)) {
			if ($UserSession->status!=$status) {
				$UserSessionUpdate = UserSession::find($UserSession->id);
				$UserSessionUpdate->status = $status;
				$UserSessionUpdate->save();
				//echo "<pre>";print_r($UserSessionUpdate);die;
				return $UserSessionUpdate->session_code;
			}
			else
			{
				//echo $UserSession[0]->session_code;die;
				return $UserSession->session_code;
			}
		}
		else
		{
			$UserSessionNew = new UserSession;
			$UserSessionNew->user_id = $userID;
			$UserSessionNew->session_code = $session_code;
			$UserSessionNew->status = $status;
			$UserSessionNew->save();
			return $session_code;
		}
	}

	protected function user_verification()
	{

		$useremail=$_REQUEST['useremail'];
		$today = date('Y-m-d H:i:s');

		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

		try{

			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{

				$Users = Users::where('user_id','=',$useremail)
					->get();

				if($Users->count()>0)
				{
					$user_id = $Users[0]->id;
					$verification_code = $_REQUEST['vcode'];
					$user_verification = UserVerifications::checkUserVCode($user_id,$verification_code);
					if($user_verification == TRUE)
					{   
						//user verification status goes to disable
						UserVerifications::where('user_id','=',$user_id)->update(array('status'=>2));
								 //users table status goes to enable
						$UsersUpdate = Users::where('id','=',$user_id)->update(array('status'=>1));
								 // EbooUserUsage
						EbooUserUsage::where('u_id','=',$user_id)->update(array('status'=>2));
								//verification successful

						$session = $this->setUserSession($user_id,1);

						$responseInsert = array(
							"status"	=>'100' ,
							"message" 	=> 'success',
							"userStatus" => 1,
							"user_id" 	=> $user_id,
							"session_code" => $session
							);

					}else{
								//expired code
						$verifiCode = UserVerifications::where('user_id','=',$user_id)->where('verification_code','=',$verification_code)->get();
						if(count($verifiCode)==0)
						{
							$session = $this->setUserSession($user_id,$Users[0]->status);
							$responseInsert = array(
								"status"	=>'304' ,
								"message" 	=> 'The code you entered invalid. Please try again or use a different email address.',
								"userStatus" => $Users[0]->status,
								"user_id" 	=> $user_id,
								"session_code" => $session
								);

						}else{
                    		//code has been expired so send again verify code
							$verification_code = rand(10000, 99999);

							$expire_datetime = date('Y-m-d H:i:s', strtotime($today . ' + 1 day'));
							$foruser_id = Users::where('user_id','=',$useremail)->first();
							$UserVerification = UserVerifications::where('user_id','=',$foruser_id->id)->first();


							$UserVerification->verification_code = $verification_code;
							$UserVerification->expire_date = $expire_datetime;
							$UserVerification->status = 1; 
							// default status is enable(1), after verification is goes to disable(2) 
							//print_r($UserVerification);
							$UserVerification->save();

							/* Mail*/

							$user = array('email'=>$useremail) ;
							$data = array(
								'detail'=>'Your Verification Code here',
								'expire_date' =>$expire_datetime,
								'code'	=> $verification_code
								);

							Mail::send('emails.user_verification', $data, function($message) use ($user)
							{
								$message->to($user['email'])->subject('User Verification Code');
							});

							$session = $this->setUserSession($user_id,$foruser_id->status);

							$responseInsert = array(
								"status"	=>'101' ,
								"userStatus" => $foruser_id->status,
								"message" 	=> 'Verification code expired, So Again we sent it. check mail',
								"user_id" 	=> $user_id,
								"session_code" => $session
								);
						}
					}
				}else{

					$responseInsert = array(
						"status"	=>'304' ,
						"message" 	=> 'Invalid User'
						);	
				}

				$data=array("LoginResponse"=>$responseInsert );
				echo json_encode($data);
				Cache::flush();	 
			}
		}catch(Exception $e){
			//echo $e->getMessage();
			$responseInsert = array(
						"status"	=>'500' ,
						"message" 	=> $e->getMessage()
						);
		}
	}  

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function clickcount()
	{
				if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

		try{
			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{
				$id = $_REQUEST['id'];
				$user_id = $_REQUEST['user_id'];
				$news_source = $_REQUEST['news_source'];

				if($news_source=='rss')
				{
					$EbbuFeeds = EbooFeeds::find($id);
					$EbbuFeeds->clickcount = $EbbuFeeds->clickcount+1;
					$EbbuFeeds->save();
				}
				else if($news_source=='twitter')
				{
					$TwitterFeeds = TwitterFeeds::find($id);
					$TwitterFeeds->click_count = $TwitterFeeds->click_count+1;
					$TwitterFeeds->save();	
				}
				$UserClickedData = new UserClickedData ;

				if (!$user_id) {
					$user_id = 0;
				}
				$UserClickedData->u_id = $user_id ;
				if($news_source=='rss')
					$UserClickedData->feeds_id = $id ;
				if($news_source=='twitter')
					$UserClickedData->tweets_id = $id ;
				$UserClickedData->session_date = new DateTime;
				$UserClickedData->status = 1 ;
				$UserClickedData->save();

				if(isset($_REQUEST['feed_conducted']) && isset($_REQUEST['search_query_id']))
				{
					$search_query_id=$_REQUEST['search_query_id'];
					$feed_conducted=$_REQUEST['feed_conducted'];

					if($feed_conducted==1 && $search_query_id!='')
					{
						$EbbuUserSearch = EbooUserSearch::where('u_id','=',$user_id)
						->where('id','=',$search_query_id)
						->get();
						$EbbuUserSearchs = EbooUserSearch::find($EbbuUserSearch[0]->id);
						$EbbuUserSearchs->articles_clicked = $EbbuUserSearch[0]->articles_clicked+1;
						$EbbuUserSearchs->save();
					}
				}
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function feedsearch()
	{
		$Feeds = array();

		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

		try{
			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{

				$cache_key = date('d_y_m').'_user_'.$_REQUEST['user_id'].'_tweet_'.$_REQUEST['new_tweet_skip_limit'].'_feed_'.$_REQUEST['new_rss_skip_limit'].'_search_'.$_REQUEST['q'];

				if(isset($_REQUEST['publication_id']))
					$cache_key .= '_publication_id_'.$_REQUEST['publication_id'];

				if(isset($_REQUEST['category_id']))
					$cache_key .= '_category_id_'.$_REQUEST['category_id'];

				if(Config::get('cache.driver')=='redis')
				{
					$redis = Redis::connection();
					$cache = $redis->get($cache_key);
				}
				else
				{
					$cache = Cache::has( $cache_key);
				}


				if( !$cache) {

					if(isset($_REQUEST['user_id']))
					{
						$user_id = $_REQUEST['user_id'];

					}else{
						$user_id = NULL;
					}

					$yesterday = date('Y-m-d',strtotime($this->limitDay));
					$today = date('Y-m-d');

					$shownRssArticle = $_REQUEST['shownrssarticle'];
					$shownTwitterArticle = $_REQUEST['showntwitterarticle'];

					$RSSarticle = explode(',',$shownRssArticle);
					$Twitterarticle = explode(',',$shownTwitterArticle); 

					if(isset($_REQUEST['publication_id']) && isset($_REQUEST['category_id']))
					{
						$selected_publication_id = $_REQUEST['publication_id'];
						$selected_category_id = $_REQUEST['category_id'];

					}else{
						$selected_publication_id = '0';
						$selected_category_id = '0';
					}

					$limit_count =  $_REQUEST['search_limit_count'] ; 

					$new_tweet_skip = $_REQUEST['new_tweet_skip_limit'];
					$new_rss_skip = $_REQUEST['new_rss_skip_limit'];

					$search_value = $_REQUEST['q'];

					$EbbuUserSearch = new EbooUserSearch ;
					$EbbuUserSearch->u_id = $user_id;
					$EbbuUserSearch->search_query = $search_value;
					$EbbuUserSearch->articles_clicked = 0;
					$EbbuUserSearch->session_date = new DateTime;
					$EbbuUserSearch->status = 1;
					$EbbuUserSearch->save();


					$search_query_id = $EbbuUserSearch->id;


					$UserClickedData = UserClickedData::where('u_id','=',$user_id)
						->where('status','=',1)
						->whereBetween('session_date', array($yesterday, $today))
						//->skip(0)->take($limit_count)
						->get();

					if($UserClickedData->count()>0)
					{
						foreach($UserClickedData as $value)
						{
							$user_view[] = $value->feeds_id; 
							$user_tweet_view[] = $value->tweets_id;
						}
					}
					else
					{
						$user_view[] = '';
						$user_tweet_view[] = '';
					}


					if($selected_publication_id!=0)
					{

						/************SELECTING THE RSS LINK ID WHICH IS SAME AS SELECTED PUBLICATION ID **********/
						/************(note: below query will always give atleast 1 row ,so no need to  checkfor mysql_num_rows***********/

						$select_feed_publication = EbooRss::where('publication_id','=',$selected_publication_id)
							->where('status','=',1)
							->get(array( 
										DB::Raw('group_concat(id) as selected_id')
								   ));

						foreach($select_feed_publication as $value_select)
						{
							$feed_publication_ids= $value_select->selected_id;
						}

						if($feed_publication_ids=='' || $feed_publication_ids==NULL )
						{
							$feed_publication_ids='0';	
						}

						$EbbuFeeds = EbooFeeds::whereRaw("MATCH(article_title) AGAINST(?)",array($search_value))
							->where('status','=',1)
							->whereIn('rss_link_id', array($feed_publication_ids))
							->whereNotIn('id', array($shownRssArticle))
							->whereBetween('article_pubdate', array($yesterday, $today))
							->orderBy(DB::raw("MATCH(article_title) AGAINST('$search_value' in boolean mode)"),'DESC')
							->get();

						$select_twitter_publication = TwitterRss::where('publication_id','=',$selected_publication_id)
							->where('status','=',1)
							->get(array( 
										DB::Raw('group_concat(id) as selected_id')
								   ));

						foreach($select_twitter_publication as $value_select)
						{
							$twitter_publication_ids= $value_select->selected_id;
						}

						if($twitter_publication_ids=='' || $twitter_publication_ids==NULL )
						{
							$twitter_publication_ids='0';	
						}
						$TwitterFeeds = TwitterFeeds::whereRaw("MATCH(tweet_text) AGAINST(?)",array($search_value))
							->where('status','=',1)
							->whereIn('twitter_handler_id', array($twitter_publication_ids))
							->whereNotIn('id', array($shownTwitterArticle))
							->whereBetween('created_date', array($yesterday, $today))
							->orderBy(DB::raw("MATCH(tweet_text) AGAINST('$search_value' in boolean mode)"), 'DESC')
							->get();

					}
					else
					{
						$EbbuFeeds = EbooFeeds::whereRaw("MATCH(article_title) AGAINST('?' in boolean mode)",array($search_value))
							->where('status','=',1)
							->whereNotIn('id', array($shownRssArticle))
							->whereBetween('article_pubdate', array($yesterday, $today))
							->orderBy('relevance','DESC')
							->skip($new_rss_skip)->take($limit_count)
							->select('*',DB::raw("MATCH(article_title) AGAINST('$search_value' in boolean mode) as relevance"))->get();

						$TwitterFeeds = TwitterFeeds::whereRaw("MATCH(tweet_text) AGAINST(?)",array($search_value))
							->where('status','=',1)
							->whereNotIn('id', array($shownTwitterArticle))
							->whereBetween('created_date', array($yesterday, $today))
							->orderBy('relevance2','DESC')
							->skip($new_tweet_skip)->take($limit_count)
							->select('*',DB::raw("MATCH(tweet_text) AGAINST('$search_value' in boolean mode) as relevance2"))->get();
					}

					$count_rss = $EbbuFeeds->count();
					$count_tweet = $TwitterFeeds->count();

					$truth = json_decode($this->getSkipLimit($count_rss,$count_tweet));

					$new_rss_skip = $truth->count_allowed_rss;
					$new_tweet_skip = $truth->count_allowed_tweet;

					$feed = 1;
					$tweet = 1;

					if($EbbuFeeds->count()>0 || $TwitterFeeds->count()>0)
					{
 
						$twitter = $this->getTwitterArray($TwitterFeeds,$user_tweet_view,$selected_publication_id,$selected_category_id,$new_tweet_skip);

						$Feed = $this->getFeedArray($EbbuFeeds,$user_view,$selected_publication_id,$selected_category_id,$new_rss_skip);
						$Feeds = array_merge($twitter,$Feed);

						if(isset($Feeds))
						{

							if(isset($RSSarticle))
							{
								$rssarticle=implode(',',$RSSarticle);
							}else{
								$rssarticle='';
							}
							if(isset($Twitterarticle))
							{
								$tweetarticle=implode(',',$Twitterarticle);

							}else{
								$tweetarticle='';
							}	

							$data=array("Feeds"=>$Feeds , "FeedStatus"=>'success',"SearchQueryId"=>$search_query_id,"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip,"RSSarticle"=>$rssarticle,"Twitterarticle"=>$tweetarticle);
						}else{
							$data=array("FeedStatus"=>'fail');	
						}

					}else{
						$data=array("FeedStatus"=>'fail');
					}

					if(Config::get('services.appCache'))
					{
						if(Config::get('cache.driver')=='redis')
						{
							$redis->set($cache_key,json_encode($data));
							$redis->expire($cache_key,60);
						}else{
							Cache::put( $cache_key, json_encode($data), 1 );
						}	
					}

					echo json_encode($data);
				}else{
					if(Config::get('cache.driver')=='redis')
					{
						$data = $redis->get($cache_key);
					}
					else
					{
						$data = Cache::get( $cache_key );
					}

					echo $data;
				}
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	
	protected function trending()
	{
		$Feeds = array();
		$screenName = "";
		$EbbuFeeds = array();
		$TwitterFeeds = array();
		$twitter = "";
		$Feed = "";

		//$_REQUEST['categorySearch_id'] = '1,2,3';

		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

		try{
			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{

				$cache_key = date('d_y_m').'_user_'.$_REQUEST['user_id'].'_tweet_'.$_REQUEST['new_tweet_skip_limit'].'_feed_'.$_REQUEST['new_rss_skip_limit'];


				if(isset($_REQUEST['publication_id']))
					$cache_key .= '_publication_id_'.$_REQUEST['publication_id'];

				if(isset($_REQUEST['category_id']))
					$cache_key .= '_category_id_'.$_REQUEST['category_id'];

				$cache_key_feed = $cache_key.'_feed';
				$cache_key_data = $cache_key.'_data';

				if(isset($_REQUEST['user_id']))
				{
					$user_id=$_REQUEST['user_id'];
				}else{
					$user_id= NULL;
				}

				//$user_id = $this->getUserId($_REQUEST['user_id']);

				$yesterday = date('Y-m-d',strtotime($this->limitDay));
				$today = date('Y-m-d');				

				if(isset($_REQUEST['screenName']))
				{
					$screenName=$_REQUEST['screenName'];
				}

				if(Config::get('cache.driver')=='redis')
				{
					$redis = Redis::connection();
					$cache = $redis->get($cache_key_data);
				}
				else
				{
					$cache = Cache::has( $cache_key_data);
				}

				if(!$cache) { 

				$shownRssArticle = $_REQUEST['shownrssarticle'];
				$shownTwitterArticle = $_REQUEST['showntwitterarticle'];

				$RSSarticle = explode(',',$shownRssArticle);
				$Twitterarticle = explode(',',$shownTwitterArticle);

				if(isset($_REQUEST['publication_id']) && isset($_REQUEST['category_id']))
				{
					$selected_publication_id = $_REQUEST['publication_id'];
					$selected_category_id = $_REQUEST['category_id'];

				}else{
					$selected_publication_id = '0';
					$selected_category_id = '0';
				}

				//$limit_skip = $_REQUEST['page_count'] * $_REQUEST['limit_count']  ;
				$new_tweet_skip=$_REQUEST['new_tweet_skip_limit'];
				$new_rss_skip=$_REQUEST['new_rss_skip_limit'];
				$limit_count =  $_REQUEST['limit_count'] ;

				$UserClickedData = UserClickedData::where('u_id','=',$user_id)
					->where('status','=',1)
					->whereBetween('session_date', array($yesterday, $today))
					->get();


				if($UserClickedData->count()>0)
				{
					foreach ($UserClickedData as $key => $value) {
						$user_view[] = $value->feeds_id; 
						$user_tweet_view[] = $value->tweets_id;
					}
				}
				else
				{
					$user_view[] = '';
					$user_tweet_view[] = '';
				}

				$EbbuFeeds = EbooFeeds::where('status','=',1)
					->whereNotIn('id', array($shownRssArticle))
					->whereBetween('article_pubdate', array($yesterday, $today));

				$TwitterFeeds = TwitterFeeds::where('status','=',1)
					->whereNotIn('id', array($shownTwitterArticle))
					->whereBetween('created_date', array($yesterday, $today));
					
				
				if(isset($_REQUEST['categorySearch_id']))
				{
					$cache_key_categorySearch_id = 'categorySearch_'.$_REQUEST['categorySearch_id'];

					if(Cache::has( $cache_key_categorySearch_id))
					{
						$SrchTermModel = Cache::get( $cache_key_categorySearch_id );
					}
					else
					{

						$categorySearchId = explode(',',$_REQUEST['categorySearch_id']); 
						$SrchTermModel = SrchTermModel::where('status','=',1)
							->whereIn('category_search_id',$categorySearchId)
							->select('name')
							->get();
						Cache::put( $cache_key_categorySearch_id, $SrchTermModel, 360 );
					}

					foreach ($SrchTermModel as $key => $value) {

						if ($key==0) {
							$EbbuFeeds = $EbbuFeeds->whereRaw("MATCH(article_title) AGAINST('?' in boolean mode)",array($value->name));

							$TwitterFeeds = $TwitterFeeds->whereRaw("MATCH(tweet_text) AGAINST(? in boolean mode)",array($value->name));
						}
						else
						{
							$EbbuFeeds = $EbbuFeeds->orWhereRaw("MATCH(article_title) AGAINST('?' in boolean mode)",array($value->name));

							$TwitterFeeds = $TwitterFeeds->orWhereRaw("MATCH(tweet_text) AGAINST(? in boolean mode)",array($value->name));
						}
					}
				}

					
				if(isset($_REQUEST['searchTearm']))
				{
					$categorySearchId = explode(',',$_REQUEST['searchTearm']); 

					foreach ($categorySearchId as $key => $value) {

						if ($key==0) {
							$EbbuFeeds = $EbbuFeeds->whereRaw("MATCH(article_title) AGAINST('?' in boolean mode)",array($value));

							$TwitterFeeds = $TwitterFeeds->whereRaw("MATCH(tweet_text) AGAINST(? in boolean mode)",array($value));
						}
						else
						{
							$EbbuFeeds = $EbbuFeeds->orWhereRaw("MATCH(article_title) AGAINST('?' in boolean mode)",array($value));

							$TwitterFeeds = $TwitterFeeds->orWhereRaw("MATCH(tweet_text) AGAINST(? in boolean mode)",array($value));
						}
					}
				}


				if($selected_publication_id!=0)
				{

					/************SELECTING THE RSS LINK ID WHICH IS SAME AS SELECTED PUBLICATION ID **********/
					/************(note: below query will always give atleast 1 row ,so no need to  checkfor mysql_num_rows***********/

					$select_feed_publication = '';
					$select_feed_publication = EbooRss::where('publication_id','=',$selected_publication_id)
						->where('status','=',1)
    						->get(array( DB::Raw('group_concat(id) as selected_id') ));

					foreach($select_feed_publication as $value_select)
					{
						$feed_publication_ids= $value_select->selected_id;
					}

					if($feed_publication_ids=='' || $feed_publication_ids==NULL )
					{
						$feed_publication_ids='0';	
					}

					$EbbuFeeds = $EbbuFeeds->whereIn('rss_link_id', array($feed_publication_ids));

					$select_twitter_publication = TwitterRss::where('publication_id','=',$selected_publication_id)
						->where('status','=',1)
						->get(array( DB::Raw('group_concat(id) as selected_id') ));

					foreach($select_twitter_publication as $value_select)
					{
						$twitter_publication_ids = $value_select->selected_id;
					}

					if($twitter_publication_ids=='' || $twitter_publication_ids==NULL )
					{
						$twitter_publication_ids='0';	
					}

					$TwitterFeeds = $TwitterFeeds->whereIn('twitter_handler_id', array($twitter_publication_ids));

				}
				else
				{

					$EbbuFeeds = $EbbuFeeds->orderBy(DB::raw('RAND()'));

					$TwitterFeeds = $TwitterFeeds->orderBy(DB::raw('RAND()'));

					if($screenName=='mostpopular'){

						$EbbuFeeds = $EbbuFeeds->orderBy('clickcount','DESC');

						$TwitterFeeds = $TwitterFeeds->orderBy('click_count','DESC');

					}
						
					//for checking promote skip value, if records avaialable or not
                  /* if(Cache::has('Promote_count'))
                    {
                    	$promote_total = Cache::get( 'Promote_count' );
                    }else{
                    	$promote_skipper = Promote::where('status','=',1)->get();
					    $promote_total = count($promote_skipper);
					    Cache::put( 'Promote_count',$promote_total, 1);
                    }
					
					$remain_promote = $promote_total- $new_rss_skip;*/
					$limit_counts = $_REQUEST['limit_count'];
					if($new_rss_skip > 0)
					{
		              	$new_rss_skip_count =  ($new_rss_skip/$limit_counts) * 3;
		              
			          }else{
			          	$new_rss_skip_count = 0;
			          	
			          }
			          
			          
					
				/*$Ebbupromote   =   Promote::where('status','=',1)
				      ->orderBy('weight', 'DESC')
				      ->skip($new_rss_skip_count)
				      ->take(3)->get();*/

  					$date = new DateTime;
  					$formatted_date = $date->format('Y-m-d');


  					$Ebbupromote = Promote::where('status','=',1)
  						->where('list','=',1)
  						->where('expiry_date','>=',$formatted_date)
  						->select('*',DB::raw('RAND() * weight AS w'))
  						->orderBy('w', 'DESC')
  						->take(1)->get();

/*				       $sql = "SELECT t.*, RAND() * t.weight AS w 
								FROM ebbu_promote t 
								ORDER BY w DESC
								LIMIT 1";
						$Ebbupromote =  DB::select($sql);*/

				}

				$EbbuFeeds = $EbbuFeeds
				               //->skip($new_rss_skip)->take($limit_count)
				               ->get();
				$TwitterFeeds = $TwitterFeeds
				                 //->skip($new_tweet_skip)->take($limit_count)
				                   ->get();
	
				$count_rss = $EbbuFeeds->count();
				$count_tweet = $TwitterFeeds->count();

				//$truth = json_decode($this->getSkipLimit($count_rss,$count_tweet));

				//$new_rss_skip = $truth->count_allowed_rss;
				//$new_tweet_skip = $truth->count_allowed_tweet;

				$new_rss_skip = $count_rss;
				$new_tweet_skip = $count_tweet;


				$feed=1;
				$tweet=1;

				/*******************TWITTER ADDITION TO SEARCH********************/	
				//echo "<pre>";
             	// print_r($Ebbupromote);die();
				if($EbbuFeeds->count()>0 || $TwitterFeeds->count()>0)
				{

					$twitter = $this->getTwitterArray($TwitterFeeds,$user_tweet_view,$selected_publication_id,$selected_category_id,$new_tweet_skip);
					$Feed = $this->getFeedArray($EbbuFeeds,$user_view,$selected_publication_id,$selected_category_id,$new_rss_skip);
                     // for promote
					
					if($selected_publication_id!=0)
					{
						$Feeds = array_merge($twitter,$Feed);
					}else{

						$promote = $this->getPromoteArray($Ebbupromote,$user_view,$selected_publication_id,$selected_category_id,$new_rss_skip);

						$Feeds_before = array_merge($twitter,$Feed);
						$Feeds_after  = $this->super_unique($Feeds_before,'publication_id');
						$Feeds = array_merge($Feeds_after,$promote);
						
					}

					if(isset($Feeds))
					{
						if(isset($RSSarticle))
						{
							$rssarticle = implode(',',$RSSarticle);
						}else{
							$rssarticle='';
						}
						if(isset($Twitterarticle))
						{
							$tweetarticle = implode(',',$Twitterarticle);

						}else{
							$tweetarticle = '';
						}	

						shuffle($Feeds);
						$data=array("Feeds"=>$Feeds, "FeedStatus"=>'success',"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip,"RSSarticle"=>$rssarticle,"Twitterarticle"=>$tweetarticle);
					}else{
						$data=array("FeedStatus"=>'fail');	
					}
				}
				else{
					$data=array("FeedStatus"=>'fail');
				}

				if(Config::get('services.appCache'))
				{
					if(Config::get('cache.driver')=='redis')
					{
						$redis->set($cache_key_feed,json_encode($Feeds));
						$redis->expire($cache_key_feed,60);
						$redis->set($cache_key_data,json_encode($data));
						$redis->expire($cache_key_data,60);
					}
					else
					{
						Cache::put( $cache_key_feed, $Feeds, 1 );
						Cache::put( $cache_key_data, json_encode($data), 1 );
					}	
				}

				echo json_encode($data);
				
				}
				else
				{

					$UserClickedData = UserClickedData::where('u_id','=',$user_id)
						->where('status','=',1)
						->whereBetween('session_date', array($yesterday, $today))
						->get();

					if($UserClickedData->count()>0)
					{
						foreach ($UserClickedData as $key => $value) {
							$user_view[] = $value->feeds_id; 
							$user_tweet_view[] = $value->tweets_id;
						}
					}
					else
					{
						$user_view[] = '';
						$user_tweet_view[] = '';
					}

					$new_rss_skip = '';
					$new_tweet_skip = '';
					$rssarticle = '';
					$tweetarticle = '';
					$Feeds = [];

					if(Config::get('cache.driver')=='redis')
					{
						$data_change = $redis->get($cache_key_data);
					}
					else
					{
						$data_change = Cache::get( $cache_key_data );
					}


					$data = json_decode($data_change);
					//print_r($data);
					foreach ($data as $keys => $values) {
						//print_r($values);
						if($keys=='Feeds')
						{
							foreach ($values as $key => $value) {
								//echo $value->newssource.'<br>';
								if($value->newssource=='rss')
								{
									if(in_array($value->id,$user_view))
									{
										$viewed_status = '1';
									}else{
										$viewed_status = '0';
									}
									$Feeds[] = array( "id" => $value->id,	 
											"source" => $value->source,
											"sourcename" => $value->sourcename,
											"article_link" => $value->article_link,
											"title" => trim($value->title),
											"description" => trim(preg_replace('/\s\s+/', ' ',$value->description)),
											"sourceimage" => $value->sourceimage,
											"newssource"=> $value->newssource,
											"pubdate" =>  $value->pubdate,
											"viewstatus" => $viewed_status,
											"publication_id"=>$value->publication_id,
											"category_id"=>$value->category_id
											);	
								}
								else if ($value->newssource=='twitter') {
									if(in_array($value->id,$user_tweet_view))
									{
										$viewed_status = '1';
									}else{
										$viewed_status = '0';
									}

									$Feeds[] = array( "id" => $value->id,	 
											"source" => $value->source,
											"sourcename" => $value->sourcename,
											"article_link" => $value->article_link,
											"title" => trim($value->title),
											"description" => trim(preg_replace('/\s\s+/', ' ',$value->description)),
											"sourceimage" => $value->sourceimage,
											"newssource"=> $value->newssource,
											"pubdate" =>  $value->pubdate,
											"viewstatus" => $viewed_status,
											"publication_id"=>$value->publication_id,
											"category_id"=>$value->category_id
											);	
								}
							}
						}
						if($keys == 'FeedStatus')
							$FeedStatus = $values;
						elseif($keys == 'rss_skip_limit')
							$new_rss_skip = $values;
						elseif($keys == 'twitter_skip_limit')
							$new_tweet_skip = $values;
						elseif($keys == 'RSSarticle')
							$rssarticle = $values;
						elseif($keys == 'Twitterarticle')
							$tweetarticle = $values;
					}
					$data = array("Feeds"=>$Feeds, "FeedStatus"=> $FeedStatus,"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip,"RSSarticle"=>$rssarticle,"Twitterarticle"=>$tweetarticle);
					

					echo json_encode($data);

				}
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function searchword()
	{
		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

		try{
			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{
				$EbbuUserSearch = new EbooUserSearch;
				$EbbuUserSearch->u_id = $_REQUEST['user_id'];
				$EbbuUserSearch->search_query = $_REQUEST['id'];
				$EbbuUserSearch->articles_clicked = 0;
				$EbbuUserSearch->session_date = new DateTime;
				$EbbuUserSearch->status = 1;
				$EbbuUserSearch->save();
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function updatetime()
	{
		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

		try{
			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{
				$user_time=$_REQUEST['seconds'];
				$user_id=$_REQUEST['user_id'];
				$user_time = $user_time / 1000 ;
				$EbbuUserUsage = EbooUserUsage::where('u_id','=',$user_id)->get();
				$total_user_time =  $EbbuUserUsage[0]->total_usage + $user_time ; 
				$EbbuUserUsage[0]->total_usage = $total_user_time ; 
				$EbbuUserUsage[0]->session_date = new DateTime;
				$EbbuUserUsage[0]->save();
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function feedback()
	{
		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

		try{
			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{
				$user_id = $_REQUEST['user_id'];

				//$name = $_REQUEST['name'];
				//$email_address = $_REQUEST['email_address'];
				$feedback = $_REQUEST['feedback'];
				$rating = $_REQUEST['rating'];
				$Feedback = new Feedback;
				$Feedback->u_id = $user_id;
				//$Feedback->name = $name;
				//$Feedback->email_address = $email_address;
				$Feedback->feedback = $feedback;
				$Feedback->rating = $rating;
				$Feedback->session_date = new DateTime;
				$Feedback->status = 1;
				$Feedback->save();
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}


    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */

    protected function sourceview()
    {

    	if(isset($_REQUEST['user_session']))
    	{
    		$user_session = $_REQUEST['user_session'];
    	}
    	else
    	{
    		$user_session = '';
    	}

    	$Feeds = array();
    	try{
    		if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
    		{
    			$viewed_source=$_REQUEST['source_viewed'];
    			$viewed_source_array=explode(',',$viewed_source);
    			$source_skip_limit=$_REQUEST['source_limit'];

				$select_source = EbooPublication::select("publication.id",'publication.publication_image','publication.publication_name')
								->whereIn('id', DB::table('rss')->select('publication_id')->union(DB::table('twitter_handler')->select('publication_id'))->lists('publication_id'))
			    				->where('publication.status','=',1)
			    				->whereNotIn('publication.id', $viewed_source_array)
			    				->orderBy(DB::raw('RAND()'))
			    				->get();

    			// $select_source = EbooPublication::join('rss', 'publication.id', '=', 'rss.publication_id')
    			// 	->join('twitter_handler', 'twitter_handler.publication_id', '=', 'publication.id')
    			// 	->distinct('publication.id')
    			// 	->select("publication.id",'publication.publication_image','publication.publication_name')
    			// 	->where('publication.status','=',1)
    			// 	->whereNotIn('publication.id', $viewed_source_array)
    			// 	//->take($source_skip_limit)
    			// 	->orderBy(DB::raw('RAND()'))
    			// 	->get();
				
				if($select_source->count()>0)
				{
					foreach($select_source as $Source)
					{

						$Feeds[]=array(
							"publication_id" =>$Source->id,
							"sourceimage" => htmlspecialchars($Source->publication_image,ENT_QUOTES),
							"sourcename"=>htmlspecialchars($Source->publication_name,ENT_QUOTES),
						);

						$viewed_source_array[]=$Source->id;
					}

					$final_viewed_source=implode(',',$viewed_source_array);

					$data=array("Feeds"=>$Feeds, "FeedStatus"=>'success',"source_viewed"=>$final_viewed_source);
				}
				else{
					$data=array("FeedStatus"=>'fail');
				}
				echo json_encode($data);
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}


	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function getsourceviewarray($RssFeeds)
	{

		

	}

	protected function trendingsource()
	{
		$Feeds = array();
		$EbbuFeeds = array();
		$TwitterFeeds = array();

		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

		try{
			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{
				$cache_key = date('d_y_m').'_user_'.$_REQUEST['user_id'].'_tweet_'.$_REQUEST['new_tweet_skip_limit'].'_feed_'.$_REQUEST['new_rss_skip_limit'];


				if(isset($_REQUEST['publication_id']))
					$cache_key .= '_publication_id_'.$_REQUEST['publication_id'];

				if(isset($_REQUEST['category_id']))
					$cache_key .= '_category_id_'.$_REQUEST['category_id'];

				$cache_key_feed = $cache_key.'_feed';
				$cache_key_data = $cache_key.'_data';

				if(isset($_REQUEST['user_id']))
				{
					$user_id=$_REQUEST['user_id'];
				}else{
					$user_id= NULL;
				}

				//$user_id = $this->getUserId($_REQUEST['user_id']);

				$yesterday=date('Y-m-d',strtotime($this->limitDay));
				$today=date('Y-m-d');				

				if(Config::get('cache.driver')=='redis')
				{
					$redis = Redis::connection();
					$cache = $redis->get($cache_key_data);
				}
				else
				{
					$cache = Cache::has( $cache_key_data);
				}

				if(!$cache) { 

					$shownRssArticle = $_REQUEST['shownrssarticle'];
					$shownTwitterArticle = $_REQUEST['showntwitterarticle'];

					$RSSarticle = explode(',',$shownRssArticle);
					$Twitterarticle = explode(',',$shownTwitterArticle); 

					if(isset($_REQUEST['publication_id']))
					{
						$selected_publication_id = $_REQUEST['publication_id'];
					}else{
						$selected_publication_id = '0';
					}

					//$limit_skip = $_REQUEST['page_count'] * $_REQUEST['limit_count']  ;
					$new_tweet_skip=$_REQUEST['new_tweet_skip_limit'];
					$new_rss_skip=$_REQUEST['new_rss_skip_limit'];
					$limit_count =  $_REQUEST['search_limit_count'];

					$publication_main_name = EbooPublication::getPublicationName($selected_publication_id);
					$search_value = trim($_REQUEST['q']);

					$UserClickedData = UserClickedData::where('u_id','=',$user_id)
						->where('status','=',1)
						->whereBetween('session_date', array($yesterday, $today))
						->get();

					if($UserClickedData->count()>0)
					{
						foreach ($UserClickedData as $key => $value) {
							$user_view[] = $value->feeds_id; 
							$user_tweet_view[] = $value->tweets_id;
						}
					}
					else
					{
						$user_view[] = '';
						$user_tweet_view[] = '';
					}

					if($selected_publication_id!=0)
					{

						/************SELECTING THE RSS LINK ID WHICH IS SAME AS SELECTED PUBLICATION ID **********/
						/************(note: below query will always give atleast 1 row ,so no need to  checkfor mysql_num_rows***********/

						$select_feed_publication = EbooRss::where('publication_id','=',$selected_publication_id)
							->where('status','=',1)
							->get(array(DB::Raw('group_concat(id) as selected_id')));

						foreach($select_feed_publication as $value_select)
						{
							$feed_publication_ids= $value_select->selected_id;
						}

						if($feed_publication_ids=='' || $feed_publication_ids==NULL )
						{
							$feed_publication_ids='0';	
						}

						$EbbuFeeds = EbooFeeds::where('status','=',1)
							->whereIn('rss_link_id', array($feed_publication_ids))
							->whereNotIn('id', array($shownRssArticle))
							->whereBetween('article_pubdate', array($yesterday, $today))
							->orderBy(DB::raw('RAND()'))
							->skip($new_rss_skip)->take($limit_count)->get();

						$select_twitter_publication = TwitterRss::where('publication_id','=',$selected_publication_id)
							->where('status','=',1)
							->get(array(DB::Raw('group_concat(id) as selected_id')));

						foreach($select_twitter_publication as $value_select)
						{
							$twitter_publication_ids = $value_select->selected_id;
						}

						if($twitter_publication_ids=='' || $twitter_publication_ids==NULL )
						{
							$twitter_publication_ids='0';	
						}
						$TwitterFeeds = TwitterFeeds::where('status','=',1)
							->whereIn('twitter_handler_id', array($twitter_publication_ids))
							->whereNotIn('id', array($shownTwitterArticle))
							->whereBetween('created_date', array($yesterday, $today))
							->orderBy(DB::raw('RAND()'))
							->skip($new_tweet_skip)->take($limit_count)->get();
					}

					$count_rss = $EbbuFeeds->count();
					$count_tweet = $TwitterFeeds->count();

					$truth = json_decode($this->getSkipLimit($count_rss,$count_tweet));

					$new_rss_skip = $truth->count_allowed_rss;
					$new_tweet_skip = $truth->count_allowed_tweet;

					$feed=1;
					$tweet=1;

					/*******************TWITTER ADDITION TO SEARCH********************/	

					if($EbbuFeeds->count()>0 || $TwitterFeeds->count()>0)
					{
						$selected_category_id = 0;
						$twitter = $this->getTwitterArray($TwitterFeeds,$user_tweet_view,$selected_publication_id,$selected_category_id,$new_tweet_skip);
						$Feed = $this->getFeedArray($EbbuFeeds,$user_view,$selected_publication_id,$selected_category_id,$new_rss_skip);
						$Feeds = array_merge($twitter,$Feed);
						if(isset($Feeds))
						{

							if(isset($RSSarticle))
							{
								$rssarticle=implode(',',$RSSarticle);
							}else{
								$rssarticle='';
							}
							if(isset($Twitterarticle))
							{
								$tweetarticle=implode(',',$Twitterarticle);

							}else{
								$tweetarticle='';
							}	

							shuffle($Feeds);
							$data=array("Feeds"=>$Feeds, "FeedStatus"=>'success',"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip,"RSSarticle"=>$rssarticle,"Twitterarticle"=>$tweetarticle);
							$data=array("Feeds"=>$Feeds , "FeedStatus"=>'success',"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip,"RSSarticle"=>$rssarticle,"Twitterarticle"=>$tweetarticle,"publication_main_name"=>$publication_main_name);

						}else{
							$data=array("FeedStatus"=>'fail');	
						}

						//$data=array("Feeds"=>$Feeds , "FeedStatus"=>'success',"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip);
					}
					else{
						$data=array("FeedStatus"=>'fail');
					}

					if(Config::get('services.appCache'))
					{
						if(Config::get('cache.driver')=='redis')
						{
							$redis->set($cache_key_feed,json_encode($Feeds));
							$redis->expire($cache_key_feed,60);
							$redis->set($cache_key_data,json_encode($data));
							$redis->expire($cache_key_data,60);
						}
						else
						{
							Cache::put( $cache_key_feed, $Feeds, 1 );
							Cache::put( $cache_key_data, json_encode($data), 1 );
						}	
					}

					echo json_encode($data);
				}
				else
				{
					$UserClickedData = UserClickedData::where('u_id','=',$user_id)
						->where('status','=',1)
						->whereBetween('session_date', array($yesterday, $today))
						->get();

					if($UserClickedData->count()>0)
					{
						foreach ($UserClickedData as $key => $value) {
							$user_view[] = $value->feeds_id; 
							$user_tweet_view[] = $value->tweets_id;
						}
					}
					else
					{
						$user_view[] = '';
						$user_tweet_view[] = '';
					}

					$new_rss_skip = '';
					$new_tweet_skip = '';
					$rssarticle = '';
					$tweetarticle = '';
					$Feeds = [];

					if(Config::get('cache.driver')=='redis')
					{
						$data_change = $redis->get($cache_key_data);
					}
					else
					{
						$data_change = Cache::get( $cache_key_data );
					}


					$data = json_decode($data_change);
					//print_r($data);
					foreach ($data as $keys => $values) {
						//print_r($values);
						if($keys=='Feeds')
						{
							foreach ($values as $key => $value) {
								//echo $value->newssource.'<br>';
								if($value->newssource=='rss')
								{
									if(in_array($value->id,$user_view))
									{
										$viewed_status = '1';
									}else{
										$viewed_status = '0';
									}
									$Feeds[] = array( "id" => $value->id,	 
											"source" => $value->source,
											"sourcename" => $value->sourcename,
											"article_link" => $value->article_link,
											"title" => trim($value->title),
											"description" => trim(preg_replace('/\s\s+/', ' ',$value->description)),
											"sourceimage" => $value->sourceimage,
											"newssource"=> $value->newssource,
											"pubdate" =>  $value->pubdate,
											"viewstatus" => $viewed_status,
											"publication_id"=>$value->publication_id,
											"category_id"=>$value->category_id
											);	
								}
								else if ($value->newssource=='twitter') {
									if(in_array($value->id,$user_tweet_view))
									{
										$viewed_status = '1';
									}else{
										$viewed_status = '0';
									}

									$Feeds[] = array( "id" => $value->id,	 
											"source" => $value->source,
											"sourcename" => $value->sourcename,
											"article_link" => $value->article_link,
											"title" => trim($value->title),
											"description" => trim(preg_replace('/\s\s+/', ' ',$value->description)),
											"sourceimage" => $value->sourceimage,
											"newssource"=> $value->newssource,
											"pubdate" =>  $value->pubdate,
											"viewstatus" => $viewed_status,
											"publication_id"=>$value->publication_id,
											"category_id"=>$value->category_id
											);	
								}
							}
						}
						if($keys == 'FeedStatus')
							$FeedStatus = $values;
						elseif($keys == 'rss_skip_limit')
							$new_rss_skip = $values;
						elseif($keys == 'twitter_skip_limit')
							$new_tweet_skip = $values;
						elseif($keys == 'RSSarticle')
							$rssarticle = $values;
						elseif($keys == 'Twitterarticle')
							$tweetarticle = $values;
						elseif($keys == 'publication_main_name')
							$publication_main_name = $values;

					}
					$data=array("Feeds"=>$Feeds , "FeedStatus"=>$FeedStatus,"rss_skip_limit"=>$new_rss_skip,"twitter_skip_limit"=>$new_tweet_skip,"RSSarticle"=>$rssarticle,"Twitterarticle"=>$tweetarticle,"publication_main_name"=>$publication_main_name);

					echo json_encode($data);
				}
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function getCategorySearchId()
	{
		$Feeds_return = array();
		$data = array();
		$response = [
			'categorySearch' => []
		];

		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

		try
		{
			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{
				$cache_key = date('d_y_m').'_categorysearch_ebbu';
				if( !Cache::has( $cache_key) ) {
					$statusCode = 200;

					$CategorySearch = CategorySearch::where('status','=',1)->get();

					if($CategorySearch->count()>0)
					{
						foreach ($CategorySearch as $key => $value) {

							$response['categorySearch'][] = [ "id" => $value->id,	 
							"title" => $value->name,
							];	

						}
					}
					else{
						$statusCode = 200;
						return Response::json($response, $statusCode);
					}
					Cache::put( $cache_key, $response, 1 );
					return Response::json($response, $statusCode);
				}
				else
				{
					$response = Cache::get( $cache_key );
					$statusCode = 200;
					return Response::json($response, $statusCode);
				}
			}
		}
		catch(Exception $e){
			echo $e;
			$statusCode = 404;
			return Response::json($response, $statusCode);
		}
	}
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function addLikeCount()
	{

		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

		try{
			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{
				$id = $_REQUEST['id'];
				$user_id = $_REQUEST['user_id'];
				$news_source = $_REQUEST['news_source'];

                 $UserLikeDataFind = UserLikeData::where('u_id','=',$user_id)->where('link_id','=',$id)->where('status','=',1);
				if($news_source=='rss')
				{
					
					//
					$UserLikeDataFind = $UserLikeDataFind->where('type','=','rss')->first();
				}
				else if($news_source=='twitter')
				{
					
					//
					$UserLikeDataFind = $UserLikeDataFind->where('type','=','twitter')->first();   
				}
				else if($news_source=='promote')
				{
					
					//
					$UserLikeDataFind = $UserLikeDataFind->where('type','=','promote')->first();   
				}

               

                 if(isset($UserLikeDataFind))
                 {
                 

                  $responseInsert = array(
										"status"	=>'500' ,
										"message" 	=> 'You Already liked this article',
										"user_id" 	=> $user_id
										);	
		       
		            
                  
                 }else{

                 	 if($news_source=='rss')
				{
					$EbbuFeeds = EbooFeeds::find($id);
					$EbbuFeeds->likecount; 
					$EbbuFeeds->likecount = $EbbuFeeds->likecount+1;
					$EbbuFeeds->save();
					//
					
				}
				else if($news_source=='twitter')
				{
					$TwitterFeeds = TwitterFeeds::find($id);
					$TwitterFeeds->likecount = $TwitterFeeds->likecount+1;
					$TwitterFeeds->save();	
					//
					
				}
				else if($news_source=='promote')
				{
					$PromoteFeeds = Promote::find($id);
					$PromoteFeeds->likecount = $PromoteFeeds->likecount+1;
					$PromoteFeeds->save();	
					//
					
				}
                 	 $UserLikeData = new UserLikeData;

							if (!$user_id) {
								$user_id = 0;
							}
							$UserLikeData->u_id = $user_id ;
							if($news_source=='rss')
								$UserLikeData->link_id = $id ;
							if($news_source=='twitter')
								$UserLikeData->link_id = $id ;
							if($news_source=='promote')
								$UserLikeData->link_id = $id ;
							$UserLikeData->type = $news_source;
							$UserLikeData->created_at = new DateTime;
							$UserLikeData->status = 1 ;

							$UserLikeData->save();
							$responseInsert = array(
										"status"	=>'100' ,
										"message" 	=> 'You like this article',
										
										);	
                 }
                
				
                 $data=array("LikeResponse"=>$responseInsert );
				 echo json_encode($data);
                

				if(isset($_REQUEST['feed_conducted']) && isset($_REQUEST['search_query_id']))
				{
					$search_query_id=$_REQUEST['search_query_id'];
					$feed_conducted=$_REQUEST['feed_conducted'];

					if($feed_conducted==1 && $search_query_id!='')
					{
						$EbbuUserSearch = EbooUserSearch::where('u_id','=',$user_id)
							->where('id','=',$search_query_id)
							->get();
						$EbbuUserSearchs = EbooUserSearch::find($EbbuUserSearch[0]->id);
						$EbbuUserSearchs->articles_clicked = $EbbuUserSearch[0]->articles_clicked+1;
						$EbbuUserSearchs->save();
					}
				}
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function addReadFeed()
	{

		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}


		/*try{*/
			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{ 
				$status = $_REQUEST['status'];
				$id = $_REQUEST['id'];
				$user_id = $_REQUEST['user_id'];
				$news_source = $_REQUEST['news_source'];

				$usercheck =     UserReadData::where('u_id','=',$user_id)->where('link_id','=',$id)->where('type','=',$news_source)->first();
				$cache_key = date('d_y_m').'_user_'.$_REQUEST['user_id'].'_read';
				Cache::forget($cache_key);

                //get user count for read maximum 10 links only
                $UserReadDataFindMax = UserReadData::where('u_id','=',$user_id)->where('status','=',1);
                  
                $UserReadDataFindMaxCount = $UserReadDataFindMax->get();


				if(isset($usercheck))
				{
					UserReadData::where('u_id','=',$user_id)->where('link_id','=',$id)->update(array('status'=>$status));
					if($status ==1){ $msg = "Enable";}else{$msg = "Disabled"; }
					$responseInsert = array(
						"status"	=>'100' ,
						"message" 	=> 'You status has been Changed as '.$msg,
						"count"		=> count($UserReadDataFindMaxCount),
						);		
				}else{

					$UserReadDataFind = UserReadData::where('u_id','=',$user_id)->where('link_id','=',$id)->where('status','=',1);
					if($news_source=='rss')
					{
						$EbbuFeeds = EbooFeeds::find($id);
						$EbbuFeeds->readcount = $EbbuFeeds->readcount+1;
						$EbbuFeeds->save();

						$UserReadDataFind = $UserReadDataFind->where('type','=','rss')->get();
					}
					else if($news_source=='twitter')
					{
						$TwitterFeeds = TwitterFeeds::find($id);
						$TwitterFeeds->readcount = $TwitterFeeds->readcount+1;
						$TwitterFeeds->save();

						$UserReadDataFind = $UserReadDataFind->where('type','=','twitter')->get();   	
					}
					else if($news_source=='promote')
					{
						$PromoteFeeds = Promote::find($id);
						$PromoteFeeds->readcount = $PromoteFeeds->readcount+1;
						$PromoteFeeds->save();

						$UserReadDataFind = $UserReadDataFind->where('type','=','promote')->get();   	
					}
				 

                  	//new user
                  	$UserReadDataFindNewUser = $UserReadDataFindMax->first();

                  	//checking new user or not, if isset user check maximum read limit
                  	if(isset($UserReadDataFindNewUser))
                  	{
	                   //    check maximum read limit, if below 10 else show error
                  		if(count($UserReadDataFindMaxCount) < 101)
                  		{


                 			// check if user read only one time  read article
                  			if(count($UserReadDataFind)>0)
                  			{
                  				count($UserReadDataFind);
                  				$responseInsert = array(
                  					"status"	=>'500' ,
                  					"message" 	=> 'You Already read this article',
										//"user_id" 	=> $UserReadDataFind[0]->id
                  					"count"		=> count($UserReadDataFindMaxCount)
                  					);	
                  			}else{
                  				$UserReadData = new UserReadData;

                  				if (!$user_id) {
                  					$user_id = 0;
                  				}
                  				$UserReadData->u_id = $user_id ;
                  				$UserReadData->link_id = $id ;
                  				$UserReadData->type = $news_source;
                  				$UserReadData->created_at = new DateTime;
                  				$UserReadData->status = 1 ;

                  				$UserReadData->save();
                  				$responseInsert = array(
                  					"status"	=>'100' ,
                  					"message" 	=> 'You Read this article',
                  					"count"		=> count($UserReadDataFindMaxCount)
                  				);	
                  			}
                  		}else{
                 	  		// user reached limit
                  			$responseInsert = array(
                  				"status"	=> '500' ,
                  				"message" 	=> 'You can only save a maximum of 100 articles to your Reading List. Delete some of your saved articles to make more room.',
                  				"count"		=> count($UserReadDataFindMaxCount)
                  			);	
                  		}
                  	}else
                  	{
                  		$UserReadData = new UserReadData;

                  		if (!$user_id) {
                  			$user_id = 0;
                  		}
                  		$UserReadData->u_id = $user_id ;
                  		$UserReadData->link_id = $id ;
                  		$UserReadData->type = $news_source;
                  		$UserReadData->created_at = new DateTime;
                  		$UserReadData->status = 1 ;

                  		$UserReadData->save();
                  		$responseInsert = array(
                  			"status"	=>'100' ,
                  			"message" 	=> 'You Read this article'
                  			
                  			);	
                  	} 
                }

                $data=array("ReadResponse"=>$responseInsert );
                echo json_encode($data);
            } 
        /*}catch(Exception $e){
        	echo $e->getMessage();
        }*/
	}
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function adminlists()
	{
		$Feeds_return = array();
		$data = array();
		$response = [
		'admins' => []
		];

		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

		try
		{
			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{
				$cache_key = date('d_y_m').'_limit_'.$_REQUEST['limit_count'].'_new_skip_'.$_REQUEST['new_skip'];
				if( !Cache::has( $cache_key) ) {
					$statusCode = 200;

					$Admin = EbooAdmin::where('admin_user','!=','')
					->skip($_REQUEST['new_skip'])->take($_REQUEST['limit_count'])->get();

					if($Admin->count()>0)
					{
						foreach ($Admin as $key => $value) {

							$response['admins'][] = [ "id" => $value->id,	 
							"title" => $value->admin_user,
							];	

						}
					}
					else{
						$statusCode = 200;
						return Response::json($response, $statusCode);
					}
					Cache::put( $cache_key, $response, 360 );
					return Response::json($response, $statusCode);
				}
				else
				{
					$response = Cache::get( $cache_key );
					$statusCode = 200;
					return Response::json($response, $statusCode);
				}
			}
		}
		catch(Exception $e){
			echo $e;
			$statusCode = 404;
			return Response::json($response, $statusCode);
		}
	}


	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function singleadmin()
	{
		$Feeds_return = array();
		$data = array();
		$response = [
			'admins' => []
		];


		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

		try
		{
			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{
				$cache_key = 'admin_id_'.$_REQUEST['admin_id'];
				if( !Cache::has( $cache_key) ) {
					$statusCode = 200;

					$Admin = EbooAdmin::find($_REQUEST['admin_id']);

					if($Admin->count()>0)
					{
							$response['admins'][] = [ 
								"id" => $Admin->id,	 
								"title" => $Admin->admin_user,
							];
					}
					else{
						return Response::json($response, $statusCode);
					}
					Cache::put( $cache_key, $response, 360 );
					return Response::json($response, $statusCode);
				}
				else
				{
					$response = Cache::get( $cache_key );
					$statusCode = 200;
					return Response::json($response, $statusCode);
				}
			}
		}
		catch(Exception $e){
			$statusCode = 404;
			return Response::json($response, $statusCode);
		}
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function getUserId($user_id='')
	{
		if($user_id!='')
		{
			$user_id = $user_id;

		}else{
			$user_id = NULL;
		}
		return $user_id;
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function getSkipLimit($count_rss=0,$count_tweet=0)
	{
		if($count_rss>=5 && $count_tweet>=5)
		{
			$count_allowed_rss=5;
			$count_allowed_tweet=5;
		}
		elseif($count_rss>=5 && $count_tweet<5)
		{
			$count_allowed_rss=$count_rss-$count_tweet;
			$count_allowed_tweet=$count_tweet;
		}
		elseif($count_rss<5 && $count_tweet>=5)		
		{
			$count_allowed_tweet=$count_tweet-$count_rss;
			$count_allowed_rss=$count_rss;

		}
		elseif($count_rss<5 && $count_tweet<5)
		{
			$count_allowed_rss=$count_rss;
			$count_allowed_tweet=$count_tweet;
		}
		$response = [ 
			"count_allowed_rss" => $count_allowed_rss,
			"count_allowed_tweet" => $count_allowed_tweet,
		];
		return json_encode($response);
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function getPubDate($likecount='')
	{
		if( $likecount > 10 ) {
			//$pubdate = '<img src="UIIcons/Small-Heart.png" style="width:6px !important;" >';
              $pubdate = $likecount;
		}else{
			$pubdate = '';
		}
		
		return $pubdate;
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function getArticleTitle($article_title='')
	{
		$article_title = trim(preg_replace( "/\r|\n/", " ", $article_title));
		$title = strlen(trim($article_title))>75 ? mb_substr(trim($article_title),0,72).'...' : trim($article_title);
		return stripslashes($title);
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function getFeedArray($EbbuFeeds='',$user_view='',$selected_publication_id=0,$selected_category_id=0,$new_rss_skip=0,$feed=1)
	{
		$Feeds = array();
		foreach ($EbbuFeeds as $key => $value) {
			
			/*$publication_name = EbooFeeds::getPublicationName($value->rss_link_id);
			$category_name = EbooFeeds::getCategoryName($value->rss_link_id);
			$publication_image = EbooFeeds::getPublicationImage($value->rss_link_id);*/

			$cache_key_getPublicationName = 'FeedsPublicationName_'.$value->rss_link_id;
			$cache_key_category_name = 'FeedsCategoryName_'.$value->rss_link_id;
			$cache_key_publication_image = 'FeedsPublicationImage_'.$value->rss_link_id;

			if(Cache::has( $cache_key_getPublicationName))
			{
				$publication_name = Cache::get( $cache_key_getPublicationName );
			}
			else
			{
				$publication_name = EbooFeeds::getPublicationName($value->rss_link_id);
				Cache::put( $cache_key_getPublicationName, $publication_name, 360 );
			}

			if(Cache::has( $cache_key_category_name))
			{
				$category_name = Cache::get( $cache_key_category_name );
			}
			else
			{
				$category_name = EbooFeeds::getCategoryName($value->rss_link_id);
				Cache::put( $cache_key_category_name, $category_name, 360 );
			}

			if(Cache::has( $cache_key_publication_image))
			{
				$publication_image = Cache::get( $cache_key_publication_image );
			}
			else
			{
				$publication_image = EbooFeeds::getPublicationImage($value->rss_link_id);
				Cache::put( $cache_key_publication_image, $publication_image, 360 );
			}

			$pubdate = $this->getPubDate($value->likecount);
             
			/*$rss_like_count =  $value->likecount;
				if($rss_like_count >10)
				{
					$rss_like_counts =  $value->likecount;
				}else{
					 $rss_like_counts ="";
				}*/

			if(in_array($value->id,$user_view))
			{
				$viewed_status='1';
			}else{
				$viewed_status='0';
			}

			$title = $this->getArticleTitle($value->article_title);

			$publication_id = EbooFeeds::getPublicationId($value->rss_link_id);
			$category_id = EbooFeeds::getCategoryId($value->rss_link_id);

			if($selected_publication_id!=0 && $selected_category_id!=0)
			{
				$Feeds[] = array( "id" => $value->id,
					"source" => htmlspecialchars($publication_name,ENT_QUOTES),
					"sourcename" => htmlspecialchars($publication_name,ENT_QUOTES),
					"article_link" => $value->article_link,
					"title" => htmlspecialchars($title,ENT_QUOTES),
					"description" => "",
					"sourceimage" => htmlspecialchars($publication_image,ENT_QUOTES),
					"newssource"=>'rss',
					"pubdate" =>  htmlspecialchars($pubdate,ENT_QUOTES),
					"viewstatus" => htmlspecialchars($viewed_status,ENT_QUOTES),
					"publication_id"=>htmlspecialchars($publication_id,ENT_QUOTES),
					"category_id"=>htmlspecialchars($category_id,ENT_QUOTES)
					);

			}
			else
			{
				$Feeds[] = array( "id" => $value->id,
					"source" => htmlspecialchars($publication_name,ENT_QUOTES),
					"sourcename" => htmlspecialchars($publication_name,ENT_QUOTES),
					"article_link" => $value->article_link,
					"title" => htmlspecialchars($title,ENT_QUOTES),
					"description" => "",
					"sourceimage" => htmlspecialchars($publication_image,ENT_QUOTES),
					"newssource"=>'rss',
					"pubdate" =>  htmlspecialchars($pubdate,ENT_QUOTES),
					"viewstatus" => htmlspecialchars($viewed_status,ENT_QUOTES),
					"publication_id"=>htmlspecialchars($publication_id,ENT_QUOTES),
					"category_id"=>htmlspecialchars($category_id,ENT_QUOTES)
					);
			}
			if ($feed>=$new_rss_skip) {
				break;
			}
			$feed++;
		}
		return $Feeds;
	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	protected function getTwitterArray($TwitterFeeds='',$user_tweet_view='',$selected_publication_id=0,$selected_category_id=0,$new_tweet_skip=0,$tweet=1)
	{
		$Feeds = array();
		foreach ($TwitterFeeds as $key => $value) {
			$TwitterRss = TwitterRss::find($value->twitter_handler_id);
			if(count($TwitterRss)>0)
			{
				$cache_key_getPublicationName = 'twitterPublicationName_'.$value->twitter_handler_id;
				$cache_key_category_name = 'twitterCategoryName_'.$value->twitter_handler_id;
				$cache_key_publication_image = 'twitterPublicationImage_'.$value->twitter_handler_id;

				if(Cache::has( $cache_key_getPublicationName))
				{
					$publication_name = Cache::get( $cache_key_getPublicationName );
				}
				else
				{
					$publication_name = TwitterFeeds::getPublicationName($value->twitter_handler_id);
					Cache::put( $cache_key_getPublicationName, $publication_name, 360 );
				}

				if(Cache::has( $cache_key_category_name))
				{
					$category_name = Cache::get( $cache_key_category_name );
				}
				else
				{
					$category_name = TwitterFeeds::getCategoryName($value->twitter_handler_id);
					Cache::put( $cache_key_category_name, $category_name, 360 );
				}

				if(Cache::has( $cache_key_publication_image))
				{
					$publication_image = Cache::get( $cache_key_publication_image );
				}
				else
				{
					$publication_image = TwitterFeeds::getPublicationImage($value->twitter_handler_id);
					Cache::put( $cache_key_publication_image, $publication_image, 360 );
				}

				/*$publication_name = TwitterFeeds::getPublicationName($value->twitter_handler_id);
				$category_name = TwitterFeeds::getCategoryName($value->twitter_handler_id);
				$publication_image = TwitterFeeds::getPublicationImage($value->twitter_handler_id);*/

				$pubdate = $this->getPubDate($value->likecount);
				/*$tweets_count =  $value->likecount;
				if($tweets_count >10)
				{
					$tweets_counts =  $value->likecount;
				}else{
					 $tweets_counts ="";
				}*/
				

				$publication_id_tweet = TwitterFeeds::getPublicationId($value->twitter_handler_id);
				$category_id_tweet = TwitterFeeds::getCategoryId($value->twitter_handler_id);

				if(in_array($value->id,$user_tweet_view))
				{
					$viewed_status='1';
				}else{
					$viewed_status='0';
				}
							//$article_title = trim(preg_replace( "/\r|\n/", "", preg_replace('/[^A-Za-z0-9\-]/', ' ', $value->tweet_text)));
				$title = $this->getArticleTitle($value->tweet_text);
				if($selected_publication_id!=0 && $selected_category_id!=0)
				{

					$Feeds[]=array( "id" => $value->id,	 
						"source" => htmlspecialchars($publication_name,ENT_QUOTES),
						"sourcename" => htmlspecialchars($publication_name,ENT_QUOTES),
						"article_link" => $value->article_link,
						"title" => htmlspecialchars($title,ENT_QUOTES),
						"description" => "",
						"sourceimage" => htmlspecialchars($publication_image,ENT_QUOTES),
						"newssource"=>'twitter',
						//"twitterid" => $value->id_str,
						"pubdate" =>  htmlspecialchars($pubdate,ENT_QUOTES),
						"viewstatus" => htmlspecialchars($viewed_status,ENT_QUOTES),
						"publication_id"=>htmlspecialchars($publication_id_tweet,ENT_QUOTES),
						"category_id"=>htmlspecialchars($category_id_tweet,ENT_QUOTES)
						);
				}
				else
				{
					$Feeds[]=array( "id" => $value->id,	 
						"source" => htmlspecialchars($publication_name,ENT_QUOTES),
						"sourcename" => htmlspecialchars($publication_name,ENT_QUOTES),
						"article_link" => $value->article_link,
						"title" => htmlspecialchars($title,ENT_QUOTES),
						"description" => "",
						"sourceimage" => htmlspecialchars($publication_image,ENT_QUOTES),
						"newssource"=>'twitter',
						//"twitterid" => $value->id_str,
						"pubdate" =>  htmlspecialchars($pubdate,ENT_QUOTES),
						"viewstatus" => htmlspecialchars($viewed_status,ENT_QUOTES),
						"publication_id"=>htmlspecialchars($publication_id_tweet,ENT_QUOTES),
						"category_id"=>htmlspecialchars($category_id_tweet,ENT_QUOTES)
						);
				}
			} //End count condition
			if ($tweet>=$new_tweet_skip) {
				break;
			}
			$tweet++;
		}

		return $Feeds;
	}
	protected function getPromoteArray($EbbuPromote='',$user_view='',$selected_publication_id=0,$selected_category_id=0,$new_rss_skip=0,$feed=1)
	{
		$Feeds = array();
		foreach ($EbbuPromote as $key => $value) {
			$publication_name = $value->name;
			$category_name = $value->name;
			$publication_image = "promote_logo/".$value->logo;

			$pubdate = $this->getPubDate($value->likecount);
               //for impression
			   $promote_impression = Promote::find($value->id);
			   $promote_impression->impressions = $promote_impression->impressions+1;
			   $promote_impression->save();
		   	

			if(in_array($value->id,$user_view))
			{
				$viewed_status='1';
			}else{
				$viewed_status='0';
			}

			$title = $this->getArticleTitle($value->headline);

			$publication_id = "";
			$category_id = "";
            $pubdate  = "";

			if($selected_publication_id!=0 && $selected_category_id!=0)
			{
				$Feeds[] = array( "id" => $value->id,
					"source" => htmlspecialchars($publication_name,ENT_QUOTES),
					"sourcename" => htmlspecialchars($publication_name,ENT_QUOTES),
					"article_link" =>  $value->url,
					"title" => htmlspecialchars($title,ENT_QUOTES),
					"description" => "",
					"sourceimage" => htmlspecialchars($publication_image,ENT_QUOTES),
					"newssource"=>'promote',
					"pubdate" =>  htmlspecialchars($pubdate,ENT_QUOTES),
					"viewstatus" => htmlspecialchars($viewed_status,ENT_QUOTES),
					"publication_id"=>htmlspecialchars($publication_id,ENT_QUOTES),
					"category_id"=>htmlspecialchars($category_id,ENT_QUOTES)
					);

			}
			else
			{
				$Feeds[] = array( "id" => $value->id,
					"source" => htmlspecialchars($publication_name,ENT_QUOTES),
					"sourcename" => htmlspecialchars($publication_name,ENT_QUOTES),
					"article_link" => $value->url,
					"title" => htmlspecialchars($title,ENT_QUOTES),
					"description" => "",
					"sourceimage" => htmlspecialchars($publication_image,ENT_QUOTES),
					"newssource"=>'promote',
					"pubdate" =>  htmlspecialchars($pubdate,ENT_QUOTES),
					"viewstatus" => htmlspecialchars($viewed_status,ENT_QUOTES),
					"publication_id"=>htmlspecialchars($publication_id,ENT_QUOTES),
					"category_id"=>htmlspecialchars($category_id,ENT_QUOTES)
					);
			}
			if ($feed>=$new_rss_skip) {
				break;
			}
			$feed++;
		}
		return $Feeds;
	}

	protected function super_unique($array,$key)
	{
		$temp_array = array();
		foreach ($array as &$v) {
			if (!isset($temp_array[$v[$key]]))
				$temp_array[$v[$key]] =& $v;
		}
		$array = array_values($temp_array);
		return $array;
	}

  	protected function userReadList()
  	{
  		$Feeds = array();
  		$UerReadPromoteDatas = array();
  		$userListPromote = array();

		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

  		try{
  			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
  			{

  				$user_id = $_REQUEST['user_id'];
  				//die();
                //cache key

  				$cache_key = date('d_y_m').'_user_'.$_REQUEST['user_id'].'_read';

  				if( Cache::has( $cache_key  ) ) {
  					$data = Cache::get( $cache_key  );
  				}
  				else
  				{

  					$UserReadData = UserReadData::where('u_id','=',$user_id)
  						->where('status','=',1)
  						->whereIn('type',array('rss','twitter'))
  						->get();
  					$UserReadPromoteData = UserReadData::where('u_id','=',$user_id)
  						->where('status','=',1)
  						->where('type','=','promote')
  						->select('link_id')
  						->get();

  					$date = new DateTime;
  					$formatted_date = $date->format('Y-m-d');


  					foreach($UserReadPromoteData as $object)
  					{
  						$UserReadPromoteDataArray[] = $object->link_id;
  					}

  					if (count($UserReadPromoteData)>0) {
  						$UerReadPromoteDatas = Promote::where('status','=',1)
  						->where('expiry_date','>',$formatted_date)
  						->whereIn('id',array($UserReadPromoteDataArray))
  						->get();
  					}
  					$Ebbupromote = Promote::where('status','=',1)
  						->where('list','=',2)
  						->where('expiry_date','>',$formatted_date)
  						->select('*',DB::raw('RAND() * weight AS w'))
  						->orderBy('w', 'DESC')
  						->take(1)->get();

  					$user_view[] = '';

  					if(count($UserReadData )>0||count($Ebbupromote)>0||count($UerReadPromoteDatas)>0){


  						$userlist = $this->getUserReadListArray($UserReadData);
  						$promote = $this->getPromoteArray($Ebbupromote,$user_view,$selected_publication_id=0,$selected_category_id=0,$new_rss_skip=0,$feed=1);
  						$FeedsPromote = array_merge($userlist,$promote);
  						if (count($UerReadPromoteDatas)>0) {
  							$userListPromote = $this->getPromoteArray($UerReadPromoteDatas,$user_view,$selected_publication_id=0,$selected_category_id=0,$new_rss_skip=0,$feed=1);
  						}

  						$Feeds = array_merge($FeedsPromote,$userListPromote);

						
  						if(count($Feeds)>0)
  						{
  							shuffle($Feeds);
  							$data = array("Feeds"=>$Feeds, "FeedStatus"=>'success');

  						}else{
  							$responseInsert = array(
  								"FeedStatus"	=>'fail' ,
  								"message" 	=> 'user information not found',
  							);	 
  							$data=array("ReadResponse"=>$responseInsert);  
  						}
  					}else{

  						$responseInsert = array(
  							"FeedStatus"	=>'fail' ,
  							"message" 	=> 'user information not found',
  						);	 
  						$data=array("ReadResponse"=>$responseInsert);  

  					}

  					Cache::put( $cache_key, $data, 1 );
  				}

  				echo json_encode($data);
  			}

  		}catch(Exception $e){
  			echo $e->getMessage();
  		}
  	}

  	protected function getUserReadListArray($UserReadData)
  	{
  		$ReadList = array();
  		foreach ($UserReadData as $key => $value) {

  			if($value->type == 'rss'){

  				$Rssfeeds = UserReadData::join('feeds', 'feeds.id', '=', 'user_read_data.link_id')
  					->join('rss', 'rss.id', '=', 'feeds.rss_link_id')
  					->join('publication', 'publication.id', '=', 'rss.publication_id')
  					->where('user_read_data.link_id','=',$value->link_id)
  					->where('user_read_data.status','=',1)
  					->select('feeds.id','feeds.likecount','feeds.article_link','publication.publication_image','publication.publication_name','user_read_data.type','rss.publication_id','rss.category_id','feeds.article_title','user_read_data.u_id','user_read_data.id as user_read_id')
  					->first();

  				if(count($Rssfeeds)>0)
  				{
  					$rss_like_count =  $Rssfeeds->likecount;
  					$pubdate = $this->getPubDate($rss_like_count);
  					$viewed_status ='';

  					$title = $this->getArticleTitle($Rssfeeds->article_title);

  					$ReadList[] = array( "id" => $Rssfeeds->id,
  						"source" => htmlspecialchars($Rssfeeds->publication_name,ENT_QUOTES),
  						"sourcename" => htmlspecialchars($Rssfeeds->publication_name,ENT_QUOTES),
  						"article_link" => $Rssfeeds->article_link,
  						"title" => htmlspecialchars($title,ENT_QUOTES),
  						"description" => "",
  						"sourceimage" => htmlspecialchars($Rssfeeds->publication_image,ENT_QUOTES),
  						"newssource"=>$value->type,
  						"pubdate" =>  htmlspecialchars($pubdate,ENT_QUOTES),
  						"viewstatus" => htmlspecialchars($viewed_status,ENT_QUOTES),
  						"publication_id"=>htmlspecialchars($Rssfeeds->publication_id,ENT_QUOTES),
  						"category_id"=>htmlspecialchars( $Rssfeeds->category_id,ENT_QUOTES)
  					);
  				}else{
  					UserReadData::where('u_id','=',$value->u_id)->where('link_id','=',$value->link_id)
  					->update(array('status'=>2));

  				}


  			}else if($value->type == 'twitter'){

  				$TweetFeeds = UserReadData::join('twitter_feeds', 'twitter_feeds.id', '=', 'user_read_data.link_id')
  				->join('twitter_handler', 'twitter_handler.id', '=', 'twitter_feeds.twitter_handler_id')
  				->join('publication', 'publication.id', '=', 'twitter_handler.publication_id')
  				->where('user_read_data.link_id','=',$value->link_id)
  				->where('user_read_data.status','=',1)
  				->select('twitter_feeds.id','twitter_feeds.likecount','twitter_feeds.article_link','publication.publication_image','publication.publication_name','user_read_data.type','twitter_handler.publication_id','twitter_feeds.tweet_text','user_read_data.u_id','user_read_data.id as user_read_id')
  				->first();



  				if(count($TweetFeeds)>0)
  				{
  					$twitter_like_count =  $TweetFeeds->likecount;
  					$pubdate = $this->getPubDate($twitter_like_count);
  					$viewed_status ='';

  					$title = $this->getArticleTitle($TweetFeeds->tweet_text);
  					
  					$ReadList[] = array( "id" => $TweetFeeds->id,
  						"source" => htmlspecialchars($TweetFeeds->publication_name,ENT_QUOTES),
  						"sourcename" => htmlspecialchars($TweetFeeds->publication_name,ENT_QUOTES),
  						"article_link" => $TweetFeeds->article_link,
  						"title" => htmlspecialchars($title,ENT_QUOTES),
  						"description" => "",
  						"sourceimage" => htmlspecialchars($TweetFeeds->publication_image,ENT_QUOTES),
  						"newssource"=>$TweetFeeds->type,
  						"pubdate" =>  htmlspecialchars($pubdate,ENT_QUOTES),
  						"viewstatus" => htmlspecialchars($viewed_status,ENT_QUOTES),
  						"publication_id"=>htmlspecialchars($TweetFeeds->publication_id,ENT_QUOTES),
  						"category_id"=>htmlspecialchars($TweetFeeds->category_id,ENT_QUOTES)
  						);
  				}
  				else{
  					UserReadData::where('u_id','=',$value->u_id)->where('link_id','=',$value->link_id)
  					->update(array('status'=>2));

  				}

  			}
			

			$ReadList++;
		}
		return $ReadList;
	}
	protected function get_category_search()
	{
		if(isset($_REQUEST['user_session']))
		{
			$user_session = $_REQUEST['user_session'];
		}
		else
		{
			$user_session = '';
		}

		try{
			if(($this->userSessionVerify($_REQUEST['user_id'],$user_session))||($_REQUEST['security_token'] == $this->token_security))
			{

				$cache_key_category_search = 'AllCategorySearch';

				if(Cache::has( $cache_key_category_search))
				{
					$CategorySearch = Cache::get( $cache_key_category_search );
				}
				else
				{
					$CategorySearch = CategorySearch::where('status','=',1)->limit(7)->get();
					Cache::put( $cache_key_category_search, $CategorySearch, 360 );
				}

				if(count($CategorySearch)>0)
				{
					$catSearchList = array();
					foreach($CategorySearch as $key =>$value)
					{
						$catSearchList[] = array(
							'id'=>$value->id,
							'name' =>$value->name
							); 
						$catSearchList++;
					}
					$responseInsert = array(
						"status"	=>'200',
						"message" 	=> 'Category Search Available',
						);
					$data = array("CategorySearchList"=>$catSearchList,"response"=>$responseInsert);
				}else{
					$responseInsert = array(
						"status"	=>'500' ,
						"message" 	=> 'Category Search Not Available',
						);	 
					$data=array("response"=>$responseInsert);  
				}	
				echo json_encode($data);
			}
		}catch(Exception $e){
			echo $e->getMessage();
		}
	}
}

?>