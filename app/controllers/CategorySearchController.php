<?php

class CategorySearchController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    
		if( Cache::has( 'CategorySearch' ) ) {
            $CategorySearch = Cache::get( 'CategorySearch' );
        }
        else
        {
            $CategorySearch = CategorySearch::all();
            Cache::put( 'CategorySearch', $CategorySearch, 1 );
        }
     // echo "<pre>";
     // print_r($CategorySearch);die();
		  return View::make('category_search.index')
            ->with('category_search', $CategorySearch);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('category_search.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		 
        $search_term = Input::get('search_term_ajax');
       // print_r($search_term);die();
		   // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'name'       => 'required|unique:category_search'
            
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('category_search/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            if($search_term[0] =='')
            {
              return Redirect::to('category_search/create')
                ->withErrors(array('Mesage' => 'You Have to enter search term.'))
                ->withInput(Input::except('password'));
              //die('its found');
            }
            
            // store
            $CategorySearch = new CategorySearch;
            $CategorySearch->name  	= Input::get('name');
            $CategorySearch->created_at      = new DateTime;
            $CategorySearch->status 			= Input::get('status');
            $CategorySearch->save();
            Cache::flush();

             
            

            

            // echo "<pre>";
            // print_r($search_term);die();
              foreach($search_term as $search_term_list){
                  if( $search_term_list != ''){      
             
              	$CategorySearchTerm = new SrchTermModel;
              	$CategorySearchtermname = SrchTermModel::where('category_search_id','=',$CategorySearch->id)->where('name','=',$search_term_list)->first();
                 if($CategorySearchtermname ==''){
              	$CategorySearchTerm->category_search_id = $CategorySearch->id;
              	$CategorySearchTerm->name =  $search_term_list;
                $CategorySearchTerm->status       = Input::get('status');
              	$CategorySearchTerm->save();
              }
            }
              } 
             
             Cache::flush();

            // redirect
            Session::flash('message', 'Successfully created Category!');
            return Redirect::to('category_search');
        }

    }
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		 // get the admin
        $CategorySearch = CategorySearch::find($id);
       
        // show the view and pass the admin to it
        return View::make('category_search.show')
            ->with('category_search', $CategorySearch);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		 // get the admin
        $CategorySearch = CategorySearch::find($id);
        $CategorySearchTerm = SrchTermModel::where('category_search_id','=',$id)->get();

        

        // show the view and pass the admin to it
        return View::make('category_search.edit')
            ->with('category_search', $CategorySearch)
             ->with('category_search_term', $CategorySearchTerm);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
    
            
            
   
		 $CategorySearch = CategorySearch::find($id);
		  // validate
        // read more on validation at http://laravel.com/docs/validation
		if($CategorySearch->name == Input::get('name'))
		{
           
		}

        $rules = array(
            'name'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('category_search/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
          
            
            $CategorySearch->name  	= Input::get('name');
            $CategorySearch->created_at      = new DateTime;
            $CategorySearch->status 			= Input::get('status');
            $CategorySearch->save();
            Cache::flush();

             
            /* For old search term id*/
              $search_term = Input::get('search_term');
              $search_term_id = Input::get('search_term_id');
              $i = 0;
                foreach($search_term as $list)
                {
                  if( $list != ''){
                   $CategorySearchTerm = SrchTermModel::updateSearchTerm($id,$search_term_id[$i],$list,Input::get('status'));
                   }
                   $i++;
                }
           /* end For old search term id*/

            
               /*  For New search term */
           
              $search_term_ajax = Input::get('search_term_ajax');

             if(count($search_term_ajax)!=0)
               {
                $i = 1;
                foreach($search_term_ajax as $search_term_list)
                  {
                      if( $search_term_list != ''){
                    	$CategorySearchTerm = new SrchTermModel;
                    	$CategorySearchtermname = SrchTermModel::where('category_search_id','=',$CategorySearch->id)->where('name','=',$search_term_list)->first();
                       if($CategorySearchtermname ==''){
                    	$CategorySearchTerm->category_search_id = $CategorySearch->id;
                    	$CategorySearchTerm->name =  $search_term_list;
                      $CategorySearchTerm->status       = Input::get('status');
                    	$CategorySearchTerm->save();
                      }
                    }
                    $i++;
                  } 
               }
                 /* end For New search term */
             Cache::flush();
             
            // redirect
            Session::flash('message', 'Successfully created Category!');
            return Redirect::to('category_search');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 $CategorySearch = CategorySearch::find($id);
        $CategorySearch->delete();
        Cache::flush();
        
        $CategorySearchTerm = SrchTermModel::where('category_search_id','=',$id);
        $CategorySearchTerm->delete();
        // redirect
        Session::flash('message', 'Successfully deleted the Category Search!');
        return Redirect::to('category_search');
	}



}
