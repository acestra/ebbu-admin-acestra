<?php

class EbooAdminController extends \BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

        if( Cache::has( 'EbooAdmin' ) ) {
            $EbooAdmin = Cache::get( 'EbooAdmin' );
        }
        else
        {
            $EbooAdmin = EbooAdmin::all();
            Cache::put( 'EbooAdmin', $EbooAdmin, 1 );
        }

        // get all the admin
        //$EbooAdmin = EbooAdmin::all();
        $admin_roles = EbooAdminRoles::all();

        // load the view and pass the admin
        return View::make('admin.index')
            ->with('admin', $EbooAdmin)
            ->with('admin_roles', $admin_roles);

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $EbooAdminRoles = EbooAdminRoles::all();
        $selectRoles = array();
        foreach($EbooAdminRoles as $role) {
            $selectRoles[$role->id] = $role->rolename;
        }
		return View::make('admin.create')
            ->with('admin_roles', $selectRoles);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'admin_user'       => 'required|Unique:admin',
            'email'      => 'required|email|Unique:admin',
            'password'      => 'required|min:4',
            'role_id'       => 'required',
            'pin'           => 'required|integer|min:4',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $EbooAdmin = new EbooAdmin;
            $EbooAdmin->admin_user  	= Input::get('admin_user');
            $EbooAdmin->email      	= Input::get('email');
            $EbooAdmin->password        = Hash::make(Input::get('password'));
            $EbooAdmin->role_id         = Input::get('role_id');
            $EbooAdmin->pin        = Input::get('pin');
            $EbooAdmin->status      = Input::get('status');
            $EbooAdmin->save();
            Cache::flush();

            // redirect
            Session::flash('message', 'Successfully created EbooAdmin!');
            return Redirect::to('admin');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        // get the admin
        $EbooAdmin = EbooAdmin::find($id);

        // show the view and pass the admin to it
        return View::make('admin.show')
            ->with('admin', $EbooAdmin);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        // get the admin
        $EbooAdmin = EbooAdmin::find($id);
        $EbooAdminRoles = EbooAdminRoles::all();
        $selectRoles = array();
        foreach($EbooAdminRoles as $role) {
            $selectRoles[$role->id] = $role->rolename;
        }

        // show the edit form and pass the admin
        return View::make('admin.edit')
            ->with('admin', $EbooAdmin)
            ->with('admin_roles', $selectRoles);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'pin'           => 'required|integer|min:4',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('admin/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $EbooAdmin = EbooAdmin::find($id);
            $EbooAdmin->pin        = Input::get('pin');
            $EbooAdmin->status        = Input::get('status');
            $EbooAdmin->role_id     = Input::get('role_id');
            $EbooAdmin->session_date    = new DateTime;
            $EbooAdmin->save();
            Cache::flush();

            // redirect
            Session::flash('message', 'Successfully updated admin!');
            return Redirect::to('admin');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $EbooAdmin = EbooAdmin::find($id);
        $EbooAdmin->delete();
        Cache::flush();

        // redirect
        Session::flash('message', 'Successfully deleted the admin!');
        return Redirect::to('admin');
	}


}
