<?php

class EbooCategoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

        if( Cache::has( 'EbooCategory' ) ) {
            $EbooCategory = Cache::get( 'EbooCategory' );
        }
        else
        {
            $EbooCategory = EbooCategory::all();
            Cache::put( 'EbooCategory', $EbooCategory, 1 );
        }


        // load the view and pass the admin
        return View::make('category.index')
            ->with('category', $EbooCategory);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('category.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'category_name'       => 'required|unique:category',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('category/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $EbooCategory = new EbooCategory;
            $EbooCategory->category_name  	= Input::get('category_name');
            $EbooCategory->session_date      = new DateTime;
            $EbooCategory->status 			= Input::get('status');
            $EbooCategory->save();
            Cache::flush();

            // redirect
            Session::flash('message', 'Successfully created Category!');
            return Redirect::to('category');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        // get the admin
        $EbooCategory = EbooCategory::find($id);

        // show the view and pass the admin to it
        return View::make('category.show')
            ->with('category', $EbooCategory);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        // get the admin
        $EbooCategory = EbooCategory::find($id);

        // show the edit form and pass the admin
        return View::make('category.edit')
            ->with('category', $EbooCategory);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $EbooCategory = EbooCategory::find($id);

        if($EbooCategory->category_name!=Input::get('category_name'))
        {
        // validate
        // read more on validation at http://laravel.com/docs/validation
            $rules = array(
                'category_name'       => 'required|unique:category',
                );
            $validator = Validator::make(Input::all(), $rules);

        // process the login
            if ($validator->fails()) {
                return Redirect::to('category/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
            } else {
            // store

                $EbooCategory->category_name       = Input::get('category_name');
                $EbooCategory->session_date      = new DateTime;
                $EbooCategory->status           = Input::get('status');
                $EbooCategory->save();
                Cache::flush();

            // redirect
                Session::flash('message', 'Successfully updated Category!');
                return Redirect::to('category');
            }
        }
        else
        {
            // redirect
            Session::flash('message', 'Nothing Updated!');
            return Redirect::to('category');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $EbooCategory = EbooCategory::find($id);
        $EbooCategory->delete();
        Cache::flush();

        // redirect
        Session::flash('message', 'Successfully deleted the category!');
        return Redirect::to('category');
	}


}
