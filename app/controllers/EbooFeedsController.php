<?php

class EbooFeedsController extends \BaseController {

    public function __construct( ) { 
    	ini_set('max_execution_time', 0); 
    	ini_set('memory_limit','250M');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        // load the view and pass the admin
        return View::make('feeds.index');

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$EbooCategory = EbooCategory::all();
		$selectCategories = array();
		foreach($EbooCategory as $category) {
    		$selectedCategories[$category->id] = $category->category_name;
		}
		return View::make('feeds.create')
			->with('category', $selectedCategories);;
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
        	'rss_link_id'       => 'required',
        	'article_title'       => 'required',
            'article_description'       => 'required',
            'article_link'       => 'required|url',
            'article_pubdate'       => 'required|date',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('feedsync/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $EbooFeeds = new EbooFeeds;
            $EbooFeeds->category_id  	= Input::get('category_id');
            $EbooFeeds->publication_id  	= Input::get('publication_id');
            $EbooFeeds->feeds_link  	= Input::get('feeds_link');
            $EbooFeeds->session_date      = new DateTime;
            $EbooFeeds->status  	= Input::get('status');
            $EbooFeeds->save();
			Cache::flush();
            // redirect
            Session::flash('message', 'Successfully created feeds!');
            return Redirect::to('feedsync');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        // get the admin
        $EbooFeeds = EbooFeeds::find($id);

        // show the view and pass the admin to it
        return View::make('feeds.show')
            ->with('feeds', $EbooFeeds);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        // get the admin
        $EbooFeeds = EbooFeeds::find($id);
		$EbooCategory = EbooCategory::all();
		$selectCategories = array();
		foreach($EbooCategory as $category) {
    		$selectedCategories[$category->id] = $category->category_name;
		}

		$EbooPublication = EbooPublication::all();
		$selectPublication = array();
		foreach($EbooPublication as $Publication) {
    		$selectPublication[$Publication->id] = $Publication->publication_name;
		}

        // show the edit form and pass the admin
        return View::make('feeds.edit')
            ->with('feeds', $EbooFeeds)
            ->with('category', $selectedCategories)
            ->with('source', $selectPublication);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
        	'article_title'       => 'required',
            'article_description'       => 'required',
            'article_link'       => 'required|url',
            'article_pubdate'       => 'required|date',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('feedsync/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $EbooFeeds = EbooFeeds::find($id);
            $EbooFeeds->article_title  	= Input::get('article_title');
            $EbooFeeds->article_description       = Input::get('article_description');
            $EbooFeeds->article_link  	= Input::get('article_link');
            $EbooFeeds->article_pubdate       = Input::get('article_pubdate');
            $EbooFeeds->session_date      = new DateTime;
            $EbooFeeds->status  	= Input::get('status');
            $EbooFeeds->save();
            Cache::flush();

            // redirect
            Session::flash('message', 'Successfully updated feeds!');
            return Redirect::to('feedsync');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $EbooFeeds = EbooFeeds::find($id);
        $EbooFeeds->delete();
        Cache::flush();

        // redirect
        Session::flash('message', 'Successfully deleted the feeds!');
        return Redirect::to('feedsync');
	}

	/** 
	* Get the Category Name 
	*/

	public function feedsync()
	{
		
		$limitDay =  '-'.Config::get('services.limitday').' day';
		$EbooRss = EbooRss::where('status','=',1)
			//->where('id','=',720)
			->orderBy('session_date','asc')
			->get();

		foreach ($EbooRss as $rss_source)
		{

			$EbbuRss_change = EbooRss::find($rss_source->id);

			$rss_link = $rss_source->rss_link;

			$rss = new DOMDocument();
			try
			{
	
				$rss->load($rss_link, LIBXML_NOCDATA);
                
                $curl = curl_init();

				curl_setopt_array($curl, Array(
					CURLOPT_URL            => $rss_link,
					CURLOPT_RETURNTRANSFER => TRUE,
					CURLOPT_ENCODING       => 'UTF-8'
					));

				$data = curl_exec($curl);
				curl_close($curl);
               
                /* before it was in $xml = simplexml_load_string($data);
                **
                ** Some of the links not working properly
                ** That's we have to change as below
                **
                */ 
                //  $xml = simplexml_load_file($rss_link);  

                $xml = simplexml_load_string($data);  
           /*  echo "<pre>";
	print_r($xml);            	
exit();  */
				
             if(isset($xml->entry)){
             	/*echo "if";
             	  echo "<pre>";
								print_r($xml->entry);            	
							exit();  */
             	     $this->getXmlOptionEntry($rss_link,$rss_source->id);
             }else{
             	// echo "else";
					/*
                    ** old obj code** 
                       foreach ($rss->getElementsByTagName('item') as $node) {
							$article_title = addslashes(trim($node->getElementsByTagName('title')->item(0)->nodeValue));
							$article_pubdate = date("Y-m-d g:i", strtotime($node->getElementsByTagName('pubDate')->item(0)->nodeValue));
							$article_link = $node->getElementsByTagName('link')->item(0)->nodeValue;
					   }
					 ** ABOVE CODE IS NOT SUPPORT FOR ALL FEEDS ** 
					 ** SO I HAVE MODIFIED OBJ LOOP AS FEEDSYNCCOMMAND.PHP CODE **  
					*/
                     
                    $articles = $xml->channel->item;
                         /* echo "<pre>";
								print_r($articles);            	
							exit();  */
					foreach ($articles as $item) {
					  
					     $article_title = addslashes(trim($item->title));
                         $article_pubdate = date("Y-m-d g:i", strtotime($item->pubDate));	
					     $article_link = $item->link;
	             	     $EbooFeeds_find = EbooFeeds::where('article_title','=',$article_title)->get();

					if($EbooFeeds_find->count()==0){
      					$rules = array(
							'article_title'       => 'required|unique:feeds',
							'article_link'       => 'required|unique:feeds',
							'article_pubdate'       => 'required'
							);
						$input_value = array(
							'article_title'       => $article_title,
							'article_link'       => $article_link,
							'article_pubdate'       => $article_pubdate
							);
						
						$validator = Validator::make($input_value , $rules);

						if ($validator->fails()) {
							Session::flash('message', 'Can`t updated feeds! ');
							//echo "fail";
						} else {
							$EbooFeeds = new EbooFeeds;
							$yesterday = date('Y-m-d',strtotime("-10 days"));
							$formattedDate = date('Y-m-d',strtotime($article_pubdate));
							if($yesterday<=$formattedDate)
							{
								//echo "land";
								$EbooFeeds->rss_link_id = $rss_source->id;
								$EbooFeeds->article_title = $article_title;
								$EbooFeeds->article_description = "";//strip_tags($node->getElementsByTagName('description')->item(0)->nodeValue);
								$EbooFeeds->article_link = $article_link;
								$EbooFeeds->clickcount =  0;
								$EbooFeeds->likecount  =  0;
								$EbooFeeds->readcount  =  0; 
								$EbooFeeds->article_pubdate = $article_pubdate;
								$EbooFeeds->session_date      = new DateTime;
								$EbooFeeds->status  	= 1;
								$EbooFeeds->save();
								$EbooFeeds->id;
							}else{
								//echo "out of date";
							}
							
						}
					}else{
					  	//echo "count greater than 1 ";

					}
					
				}	
				}
		
			
				$EbbuRss_change->session_date = new DateTime;
				$EbbuRss_change->save();
			}
			catch(Exception $e){ 
				/* the data provided is not valid XML */
				//echo $e;
				//die;
				Session::flash('message', 'Can`t updated feeds! '.$rss_source->id);
				/*$EbooRss = EbooRss::find($rss_source->id);
				$EbooRss->status = 2;
				$EbooRss->save();*/
			}
		}


		try {

			$today = Date('Y-m-d');
			$date = DateTime::createFromFormat('Y-m-d', $today);
			$date = date_modify($date,$limitDay);
			$date = date_format($date , 'Y-m-d');


			$EbooFeeds_last = EbooFeeds::where('article_pubdate','<=',$date)->get();		

			foreach ($EbooFeeds_last as $key => $value) {
				$EbooFeeds_change = EbooFeeds::find($value->id);
				/*$EbooFeedsOld = new EbooFeedsOld;
				$EbooFeedsOld->rss_link_id = $EbooFeeds_change->id;
				$EbooFeedsOld->article_title = trim($EbooFeeds_change->article_title);
				$EbooFeedsOld->article_description = $EbooFeeds_change->article_description ;
				$EbooFeedsOld->article_link = $EbooFeeds_change->article_link;
				$EbooFeedsOld->clickcount = $EbooFeeds_change->clickcount;
				$EbooFeedsOld->likecount = $EbooFeeds_change->likecount;
				$EbooFeedsOld->readcount = $EbooFeeds_change->readcount;
				$EbooFeedsOld->article_pubdate = $EbooFeeds_change->article_pubdate;
				$EbooFeedsOld->session_date      = new DateTime;
				$EbooFeedsOld->status = 2;
				$EbooFeedsOld->save();*/
				$EbooFeeds = EbooFeeds::find($value->id);
				$EbooFeeds->delete();
			}			
		} catch (Exception $e) {
			Session::flash('message', 'Can`t updated feeds! ');	
		}


		Cache::flush();
		Session::flash('message', 'Successfully updated feeds!');
		//echo "Successfully updated feeds!";
	 	return Redirect::to('feedsync');
	}


	public function ebbuFeedsAjax()
	{
		//$EbooFeeds = EbooFeeds::select('id', 'rss_link_id','article_title','article_pubdate','clickcount','status')->get();
		$Feeds_return = array();
		$data = array();
		$new_skip = $_REQUEST['start']*$_REQUEST['draw'];
		$response = [
			"draw" => $_REQUEST['draw'],
			"recordsTotal" => EbooFeeds::count(),
			"recordsFiltered" => EbooFeeds::count(),
			'data' => []
		];
		try
		{
			$statusCode = 200;
			if (!$_REQUEST['search']['value']) {
				$EbooFeeds = EbooFeeds::select('id', 'rss_link_id','article_title','article_pubdate','clickcount','status');
				$check = '';
				$order = "";
				foreach($_REQUEST['order'] as $key => $value)
				{
					if($value['column']==0)
						$column = 'id';
					else if($value['column']==3)
						$column = 'article_title';
					else if($value['column']==4)
						$column = 'clickcount';
					else if($value['column']==5)
						$column = 'article_pubdate';
					else if($value['column']==6)
						$column = 'status';
					else
						$check = "Available";

					$order =  $value['dir'];

					if ($check!='Available') {
						$EbooFeeds = $EbooFeeds->orderBy( $column, $order);
					}

				}
				$EbooFeeds = $EbooFeeds->skip($_REQUEST['start'])->take($_REQUEST['length'])->get();
			}
			else
			{
				$EbooFeeds = EbooFeeds::select('id', 'rss_link_id','article_title','article_pubdate','clickcount','status')
					->where('status','=',1)
					->where('article_title','like', '%'.$_REQUEST['search']['value'].'%');

				$response["recordsFiltered"] = $EbooFeeds->count();

				$EbooFeeds = $EbooFeeds->skip($_REQUEST['start'])->take($_REQUEST['length'])
					->get();	

				$response["recordsTotal"] = EbooFeeds::count(); 
			}


			if($EbooFeeds->count()>0)
			{
				foreach ($EbooFeeds as $key => $value) {

					if( $value->status ==1) 
						$status =  "Enable"; 
					else
						$status = "Disable";


					$response['data'][] = [ $value->id,	 
					EbooFeeds::getCategoryName($value->rss_link_id),
					EbooFeeds::getPublicationName($value->rss_link_id),
					stripslashes($value->article_title),
					$value->clickcount,
					date("F j, Y, g:i a" , strtotime($value->article_pubdate) ),
					$status,
					];	
				}
			}
			else{
				$statusCode = 200;
				return Response::json($response, $statusCode);
			}

			return Response::json($response, $statusCode);
		}
		catch(Exception $e){
			//echo $e;
			$statusCode = 404;
			return Response::json($response, $statusCode);
		}
	}

	public function getXmlOptionEntry ($rss_link,$rss_source) {

		    $curl = curl_init();

				curl_setopt_array($curl, Array(
					CURLOPT_URL            => $rss_link,
					CURLOPT_RETURNTRANSFER => TRUE,
					CURLOPT_ENCODING       => 'UTF-8'
					));

				$data = curl_exec($curl);
				curl_close($curl);

				$xml = simplexml_load_string($data);

         	foreach($xml->entry as $item)
				{
					$article_title = addslashes(trim($item->title));
					$article_pubdate = date("Y-m-d g:i", strtotime($item->published));
					$article_link = $item->link['href'];
					$EbooFeeds_find = EbooFeeds::where('article_title','=',$article_title)->get();

					if($EbooFeeds_find->count()==0){

						$rules = array(
							'article_title'       => 'required|unique:feeds',
							'article_link'       => 'required|unique:feeds',
							'article_pubdate'       => 'required'
							);
						$input_value = array(
							'article_title'       => $article_title,
							'article_link'       => $article_link,
							'article_pubdate'       => $article_pubdate
							);
						
						$validator = Validator::make($input_value , $rules);

						if ($validator->fails()) {
							$validatorCount++;
						} else {
							$EbooFeeds = new EbooFeeds;
							$yesterday = date('Y-m-d',strtotime("-10 days"));
							$formattedDate = date('Y-m-d',strtotime($article_pubdate));
							if($yesterday<=$formattedDate)
							{
								$EbooFeeds->rss_link_id = $rss_source;
								$EbooFeeds->article_title = $article_title;
								$EbooFeeds->article_description = "";
								$EbooFeeds->article_link = $article_link;
								$EbooFeeds->article_pubdate = $article_pubdate;
								$EbooFeeds->clickcount = 0;
								$EbooFeeds->likecount = 0;
								$EbooFeeds->readcount = 0; 
								$EbooFeeds->session_date      = new DateTime;
								$EbooFeeds->status  	= 1;
								$EbooFeeds->save();
							}
						}
					}
				}
	}


}