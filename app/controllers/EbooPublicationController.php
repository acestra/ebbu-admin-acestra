<?php

class EbooPublicationController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        if( Cache::has( 'EbooPublication' ) ) {
            $EbooPublication = Cache::get( 'EbooPublication' );
        }
        else
        {
            $EbooPublication = EbooPublication::all();
            Cache::put( 'EbooPublication', $EbooPublication, 1 );
        }

        // load the view and pass the admin
        return View::make('sources.index')
            ->with('sources', $EbooPublication);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return View::make('sources.create');
	}


	/**
	 * Store a newly created resource in images.
	 *
	 * @return Response
	 */
	public function store()
	{
		        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'publication_name'       => 'required|unique:publication',
            'publication_image' 	=>'required',
            );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('sources/create')
            ->withErrors($validator);
        } else {
            if (Input::hasFile('publication_image'))
            {
                $file = Input::file('publication_image');

                $destinationPath = 'images';

                $filename = $file->getClientOriginalName();

                $filename = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
                $filename = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $filename);
                $filename = strtolower(trim($filename, '-'));
                $filename = preg_replace("/[\/_|+ -]+/", '-', $filename);
                $filename = $filename.'.'.$file->getClientOriginalExtension();

            //$extension =$file->getClientOriginalExtension(); 
                $upload_success = Input::file('publication_image')->move($destinationPath, $filename);

                if( $upload_success ) {
                    $file_name_out = trim($destinationPath.'/'.$filename);
                    $EbooPublication = new EbooPublication;
                    $EbooPublication->publication_name  	= Input::get('publication_name');
                    $EbooPublication->publication_image  	= $file_name_out;
                    $EbooPublication->session_date      = new DateTime;
                    $EbooPublication->status  	= Input::get('status');
                    $EbooPublication->save();
                    Cache::flush();
                   

            // redirect
                    Session::flash('message', 'Successfully created sources!');
                    return Redirect::to('sources');
               } else {
                   return Response::json('error', 400);
               }
           }

       }
    }


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        // get the admin
        $EbooPublication = EbooPublication::find($id);

        // show the view and pass the admin to it
        return View::make('sources.show')
            ->with('sources', $EbooPublication);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        // get the admin
        $EbooPublication = EbooPublication::find($id);

        // show the edit form and pass the admin
        return View::make('sources.edit')
            ->with('sources', $EbooPublication);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $file_name_out = '';
        $EbooPublication = EbooPublication::find($id);
        if($EbooPublication->publication_name!=Input::get('publication_name'))
        {
            // validate
            // read more on validation at http://laravel.com/docs/validation
            $rules = array(
                'publication_name'       => 'required|unique:publication',
                );
            $validator = Validator::make(Input::all(), $rules);

            // process the login
            if ($validator->fails()) {
                return Redirect::to('sources/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
            } else {
                if (Input::hasFile('publication_image'))
                {
                    $file = Input::file('publication_image');

                    $destinationPath = 'images';

                    $filename = $file->getClientOriginalName();

                    $filename = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
                    $filename = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $filename);
                    $filename = strtolower(trim($filename, '-'));
                    $filename = preg_replace("/[\/_|+ -]+/", '-', $filename);
                    $filename = $filename.'.'.$file->getClientOriginalExtension();

                    $upload_success = Input::file('publication_image')->move($destinationPath, $filename);

                    if( $upload_success ) {
                        $file_name_out = trim($destinationPath.'/'.$filename);
                    } else {
                     return Response::json('error', 400);
                 }
             }

            // store
             $EbooPublication->publication_name      = Input::get('publication_name');
             if ($file_name_out!='') {
                 $EbooPublication->publication_image     = $file_name_out;
             }
             $EbooPublication->session_date      = new DateTime;
             $EbooPublication->status    = Input::get('status');
             $EbooPublication->save();
             Cache::flush();
             // status chang on link feeds
                     $source_id = $EbooPublication->id;
                     $status = Input::get('status');
                     EbooPublication::disableEnableSorceRelatedFeeds($source_id,$status);
            // redirect
             Session::flash('message', 'Successfully updated sources!');
             return Redirect::to('sources');
         }
     }
        else
        {
                if (Input::hasFile('publication_image'))
                {
                    $file = Input::file('publication_image');

                    $destinationPath = 'images';

                    $filename = $file->getClientOriginalName();

                    $filename = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
                    $filename = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $filename);
                    $filename = strtolower(trim($filename, '-'));
                    $filename = preg_replace("/[\/_|+ -]+/", '-', $filename);
                    $filename = $filename.'.'.$file->getClientOriginalExtension();

                    $upload_success = Input::file('publication_image')->move($destinationPath, $filename);

                    if( $upload_success ) {
                        $file_name_out = trim($destinationPath.'/'.$filename);
                    } else {
                     return Response::json('error', 400);
                 }

             }

             if ($file_name_out!='') {
                 $EbooPublication->publication_image     = $file_name_out;
             }
             $EbooPublication->session_date      = new DateTime;
             $EbooPublication->status    = Input::get('status');
             $EbooPublication->save();
             Cache::flush();
                // status chang on link feeds
                     $source_id = $EbooPublication->id;
                     $status = Input::get('status');
                     EbooPublication::disableEnableSorceRelatedFeeds($source_id,$status);
            // redirect
            Session::flash('message', 'Successfully updated sources!');
            return Redirect::to('sources');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $EbooPublication = EbooPublication::find($id);
          $Rssfeeds = DB::table('publication')
              ->join('rss', 'publication.id', '=', 'rss.publication_id')
              ->join('feeds', 'rss.id', '=', 'feeds.rss_link_id')
              ->where('rss.publication_id','=',$id)
                ->where('publication.status','=',1)
                ->get();
           
           $TweetFeeds = DB::table('publication')
              ->join('twitter_handler', 'publication.id', '=', 'twitter_handler.publication_id')
              ->join('twitter_feeds', 'twitter_handler.id', '=', 'twitter_feeds.twitter_handler_id')
              ->where('twitter_handler.publication_id','=',$id)
                ->where('publication.status','=',1)
                ->get();

           $rss_count =  count($Rssfeeds);
           $twitter_count = count($TweetFeeds);
           if($rss_count =='0' && $twitter_count =='0'){
                $EbooPublication->delete();
                Cache::flush();
                Session::flash('message', 'Successfully deleted the sources!');
                return Redirect::to('sources');
           }else{
           
              Session::flash('error', 'Error to  deleted the sources because this source linked with feeds!');
              return Redirect::to('sources');
           }
	}

}
