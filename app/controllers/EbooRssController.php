<?php

class EbooRssController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        if( Cache::has( 'EbooRss' ) ) {
            $EbooRss = Cache::get( 'EbooRss' );
        }
        else
        {
            $EbooRss = EbooRss::all();
            Cache::put( 'EbooRss', $EbooRss, 1 );
        }

        // get all the admin
        //$EbooRss = EbooRss::all();

        // load the view and pass the admin
        return View::make('rss.index')
            ->with('rss', $EbooRss);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$EbooCategory = EbooCategory::where('status','=',1)->get();
		$selectedCategories = array();
		foreach($EbooCategory as $category) {
    		$selectedCategories[$category->id] = $category->category_name;
		}

		$EbooPublication = EbooPublication::where('status','=',1)->get();
		$selectPublication = array();
		foreach($EbooPublication as $Publication) {
    		$selectPublication[$Publication->id] = $Publication->publication_name;
		}

		return View::make('rss.create')
			->with('category', $selectedCategories)
			->with('source', $selectPublication);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
        	'category_id'       => 'required',
        	'publication_id'       => 'required',
            'rss_link'       => 'required|url|unique:rss',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('rss/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $EbooRss = new EbooRss;
            $EbooRss->category_id  	= Input::get('category_id');
            $EbooRss->publication_id  	= Input::get('publication_id');
            $EbooRss->rss_link  	= Input::get('rss_link');
            $EbooRss->session_date      = new DateTime;
            $EbooRss->status  	= Input::get('status');
            $EbooRss->save();
            Cache::flush();

            // redirect
            Session::flash('message', 'Successfully created rss!');
            return Redirect::to('rss');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        // get the admin
        $EbooRss = EbooRss::find($id);

        // show the view and pass the admin to it
        return View::make('rss.show')
            ->with('rss', $EbooRss);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        // get the admin
        $EbooRss = EbooRss::find($id);
		//$EbooCategory = EbooCategory::all();
		$EbooCategory = EbooCategory::where('status','=',1)->get();
		$selectedCategories = array();
		foreach($EbooCategory as $category) {
    		$selectedCategories[$category->id] = $category->category_name;
		}

		$EbooPublication = EbooPublication::where('status','=',1)->get();
		$selectPublication = array();
		foreach($EbooPublication as $Publication) {
    		$selectPublication[$Publication->id] = $Publication->publication_name;
		}

        // show the edit form and pass the admin
        return View::make('rss.edit')
            ->with('rss', $EbooRss)
            ->with('category', $selectedCategories)
            ->with('source', $selectPublication);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$EbooRss = EbooRss::find($id);
		if($EbooRss->rss_link!=Input::get('rss_link'))
		{
			$rules = array(
					'category_id'       => 'required',
					'publication_id'       => 'required',
					'rss_link'       => 'required|url|unique:rss',
				      );
			$validator = Validator::make(Input::all(), $rules);

			// process the login
			if ($validator->fails()) {
				return Redirect::to('rss/' . $id . '/edit')
					->withErrors($validator)
					->withInput(Input::except('password'));
			} else {
				// store
				$EbooRss->category_id   = Input::get('category_id');
				$EbooRss->publication_id    = Input::get('publication_id');
				$EbooRss->rss_link       = Input::get('rss_link');
				$EbooRss->session_date      = new DateTime;
				$EbooRss->status    = Input::get('status');
				$EbooRss->save();
				Cache::flush();

				// redirect
				Session::flash('message', 'Successfully updated rss!');
				return Redirect::to('rss');
			}            
		}
		else
		{
			$rules = array(
					'category_id'       => 'required',
					'publication_id'       => 'required',
					'rss_link'       => 'required',
				      );
			$validator = Validator::make(Input::all(), $rules);

			// process the login
			if ($validator->fails()) {
				return Redirect::to('rss/' . $id . '/edit')
					->withErrors($validator)
					->withInput(Input::except('password'));
			} else {
				// store
				$EbooRss->category_id   = Input::get('category_id');
				$EbooRss->publication_id    = Input::get('publication_id');
				$EbooRss->rss_link       = Input::get('rss_link');
				$EbooRss->session_date      = new DateTime;
				$EbooRss->status    = Input::get('status');
				$EbooRss->save();
				Cache::flush();

				// redirect
				Session::flash('message', 'Successfully updated rss!');
				return Redirect::to('rss');
			}
		}

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $EbooRss = EbooRss::find($id);
        $EbooRss->delete();
        Cache::flush();

        // redirect
        Session::flash('message', 'Successfully deleted the rss!');
        return Redirect::to('rss');
	}

}
