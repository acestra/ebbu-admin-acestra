<?php

class FeedbackController extends \BaseController {

    public function __construct( ) { 
    	ini_set('max_execution_time', 0); 
    	ini_set('memory_limit','250M');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

        if( Cache::has( 'Feedback' ) ) {
            $Feedback = Cache::get( 'Feedback' );
        }
        else
        {
            $Feedback = Feedback::all();
            Cache::put( 'Feedback', $Feedback, 1 );
        }


        // load the view and pass the admin
        return View::make('feedback.index')
            ->with('feedback', $Feedback);

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        // get the admin
        $Feedback = Feedback::find($id);

        // show the view and pass the admin to it
        return View::make('feedback.show')
            ->with('feedback', $Feedback);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

	}

	public function ebbuFeedsAjax()
	{
		//$Feedback = Feedback::select('id', 'rss_link_id','article_title','article_pubdate','clickcount','status')->get();
		$Feeds_return = array();
		$data = array();
		$new_skip = $_REQUEST['start']*$_REQUEST['draw'];
		$response = [
			"draw" => $_REQUEST['draw'],
			"recordsTotal" => Feedback::count(),
			"recordsFiltered" => Feedback::count(),
			'data' => []
		];
		try
		{
			$statusCode = 200;
			if (!$_REQUEST['search']['value']) {
				$Feedback = Feedback::select('id', 'rss_link_id','article_title','article_pubdate','clickcount','status');
				$check = '';
				$order = "";
				foreach($_REQUEST['order'] as $key => $value)
				{
					if($value['column']==0)
						$column = 'id';
					else if($value['column']==3)
						$column = 'article_title';
					else if($value['column']==4)
						$column = 'clickcount';
					else if($value['column']==5)
						$column = 'article_pubdate';
					else if($value['column']==6)
						$column = 'status';
					else
						$check = "Available";

					$order =  $value['dir'];

					if ($check!='Available') {
						$Feedback = $Feedback->orderBy( $column, $order);
					}

				}
				$Feedback = $Feedback->skip($_REQUEST['start'])->take($_REQUEST['length'])->get();
			}
			else
			{
				$Feedback = Feedback::select('id', 'rss_link_id','article_title','article_pubdate','clickcount','status')
					->where('status','=',1)
					->where('article_title','like', '%'.$_REQUEST['search']['value'].'%');

				$response["recordsFiltered"] = $Feedback->count();

				$Feedback = $Feedback->skip($_REQUEST['start'])->take($_REQUEST['length'])
					->get();	

				$response["recordsTotal"] = Feedback::count(); 
			}


			if($Feedback->count()>0)
			{
				foreach ($Feedback as $key => $value) {

					if( $value->status ==1) 
						$status =  "Enable"; 
					else
						$status = "Disable";


					$response['data'][] = [ $value->id,	 
					Feedback::getCategoryName($value->rss_link_id),
					Feedback::getPublicationName($value->rss_link_id),
					$value->article_title,
					$value->clickcount,
					date("F j, Y, g:i a" , strtotime($value->article_pubdate) ),
					$status,
					];	
				}
			}
			else{
				$statusCode = 200;
				return Response::json($response, $statusCode);
			}

			return Response::json($response, $statusCode);
		}
		catch(Exception $e){
			//echo $e;
			$statusCode = 404;
			return Response::json($response, $statusCode);
		}
	}


}
