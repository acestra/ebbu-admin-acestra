<?php

class FeedsOldController extends BaseController {

    public function __construct( ) { 
    	ini_set('max_execution_time', 0); 
    	ini_set('memory_limit','250M');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        // load the view and pass the admin
        return View::make('feedsold.index');

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$EbooCategory = EbooCategory::all();
		$selectCategories = array();
		foreach($EbooCategory as $category) {
    		$selectedCategories[$category->id] = $category->category_name;
		}
		return View::make('feeds.create')
			->with('category', $selectedCategories);;
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
        	'rss_link_id'       => 'required',
        	'article_title'       => 'required',
            'article_description'       => 'required',
            'article_link'       => 'required|url',
            'article_pubdate'       => 'required|date',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('feedsync/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $EbooFeeds = new EbooFeeds;
            $EbooFeeds->category_id  	= Input::get('category_id');
            $EbooFeeds->publication_id  	= Input::get('publication_id');
            $EbooFeeds->feeds_link  	= Input::get('feeds_link');
            $EbooFeeds->session_date      = new DateTime;
            $EbooFeeds->status  	= Input::get('status');
            $EbooFeeds->save();
			Cache::flush();
            // redirect
            Session::flash('message', 'Successfully created feeds!');
            return Redirect::to('feedsync');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        // get the admin
        $EbooFeeds = EbooFeeds::find($id);

        // show the view and pass the admin to it
        return View::make('feeds.show')
            ->with('feeds', $EbooFeeds);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        // get the admin
        $EbooFeeds = EbooFeeds::find($id);
		$EbooCategory = EbooCategory::all();
		$selectCategories = array();
		foreach($EbooCategory as $category) {
    		$selectedCategories[$category->id] = $category->category_name;
		}

		$EbooPublication = EbooPublication::all();
		$selectPublication = array();
		foreach($EbooPublication as $Publication) {
    		$selectPublication[$Publication->id] = $Publication->publication_name;
		}

        // show the edit form and pass the admin
        return View::make('feeds.edit')
            ->with('feeds', $EbooFeeds)
            ->with('category', $selectedCategories)
            ->with('source', $selectPublication);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
        	'article_title'       => 'required',
            'article_description'       => 'required',
            'article_link'       => 'required|url',
            'article_pubdate'       => 'required|date',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('feedsync/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $EbooFeeds = EbooFeeds::find($id);
            $EbooFeeds->article_title  	= Input::get('article_title');
            $EbooFeeds->article_description       = Input::get('article_description');
            $EbooFeeds->article_link  	= Input::get('article_link');
            $EbooFeeds->article_pubdate       = Input::get('article_pubdate');
            $EbooFeeds->session_date      = new DateTime;
            $EbooFeeds->status  	= Input::get('status');
            $EbooFeeds->save();
            Cache::flush();

            // redirect
            Session::flash('message', 'Successfully updated feeds!');
            return Redirect::to('feedsync');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $EbooFeeds = EbooFeeds::find($id);
        $EbooFeeds->delete();
        Cache::flush();

        // redirect
        Session::flash('message', 'Successfully deleted the feeds!');
        return Redirect::to('feedsync');
	}


}
