<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
        // get all the admin
        $Users = Users::all();
        $EbooUserUsage = EbooUserUsage::all();
        $EbooUserSearch = EbooUserSearch::groupBy('search_query')->get();

        $EbooCategory = EbooCategory::all();
        $EbooRss = EbooRss::all();
        $EbooPublication = EbooPublication::all();
        $TwitterRss = TwitterRss::all();

        $EbooCategoryActive = EbooCategory::where('status','=',1)->get();
        $EbooRssActive = EbooRss::where('status','=',1)->get();
        $EbooPublicationActive = EbooPublication::where('status','=',1)->get();
        $TwitterRssActive = TwitterRss::where('status','=',1)->get();

        $total_usage = 0 ; 
        foreach ($EbooUserUsage as $key => $value) {
            $total_usage = $total_usage + $value->total_usage ;
        }

        $Users = Users::all();
        $EbooUserUsage = EbooUserUsage::all();
        $EbooUserSearch = EbooUserSearch::groupBy('search_query')->get();
        $total_usage = 0 ; 
        foreach ($EbooUserUsage as $key => $value) {
            $total_usage = $total_usage + $value->total_usage ;
        }

        // load the view and pass the admin
        return View::make('home')
            ->with('Users', $Users->count())
            ->with('total_usage', gmdate("H:i:s", $total_usage))
            ->with('total_search_word', $EbooUserSearch->count())
            ->with('EbooCategory', $EbooCategory->count())
            ->with('EbooRss', $EbooRss->count())
            ->with('TwitterRss', $TwitterRss->count())
            ->with('EbooPublication', $EbooPublication->count())
            ->with('EbooCategoryActive', $EbooCategoryActive->count())
            ->with('EbooRssActive', $EbooRssActive->count())
            ->with('total_search_word', $EbooUserSearch->count())
            ->with('Users', $Users->count())
            ->with('EbooPublicationActive', $EbooPublicationActive->count())
            ->with('TwitterRssActive', $TwitterRssActive->count());
	}

	public function source()
	{
		return View::make('source');
	}

    public function dailyFeed()
    {
        $Feeds_return = array();
        $data = array();
        $response = [];
        try
        {
            $statusCode = 200;
            $date = date("Y-m-d" , strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . " -10 days"));
            //echo $date;
            $EbooFeeds = EbooFeeds::where('article_pubdate','>',$date)
                ->select(DB::raw('DATE(session_date) as date') , DB::raw('count(*) as total'))
                ->groupBy('date')
                ->get();
            if($EbooFeeds->count()>0)
            {
                foreach ($EbooFeeds as $key => $value) {

                    $response['rss'][] = [ strtotime($value->date)*1000,   $value->total ,];   

                }
            }
            else{
                return Response::json($response, $statusCode);
            }

            return Response::json($response, $statusCode);

        }
        catch(Exception $e){
            $statusCode = 404;
            return Response::json($response, $statusCode);
        }
    }
    public function dailyTwitter()
    {
        $Feeds_return = array();
        $data = array();
        $response = [];
        try
        {
            $statusCode = 200;
            $date = date("Y-m-d" , strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . " -10 days"));
            //echo $date;
            $TwitterFeeds = TwitterFeeds::where('created_date','>',$date)
                ->where('status','=',1)
                ->select(DB::raw('DATE(session_date) as date') , DB::raw('count(*) as total'))
                ->groupBy('date')
                ->get();
            if($TwitterFeeds->count()>0)
            { 

                foreach ($TwitterFeeds as $key => $value) {

                    $response['twitter'][] = [ strtotime($value->date)*1000,   $value->total ,];   

                }
            }
            else{
                return Response::json($response, $statusCode);
            }

            return Response::json($response, $statusCode);

        }
        catch(Exception $e){
            $statusCode = 404;
            return Response::json($response, $statusCode);
        }
    }
    public function dailyUser()
    {
        $Feeds_return = array();
        $data = array();
        $response = [];
        try
        {
            $statusCode = 200;
            $Users = Users::where('status','=',1)
                ->select(DB::raw('MONTHNAME(session_date) as date') , DB::raw('count(*) as total') , DB::raw('DATE(session_date) as dates') , DB::raw('YEAR(session_date) AS year') )
                ->groupBy('date', 'year')
                ->orderBy('session_date','ASC')
                ->get();
            if($Users->count()>0)
            {
                foreach ($Users as $key => $value) {
                    $response['user'][] = [ strtotime($value->dates)*1000,   $value->total , $value->date];   

                }
            }
            else{
                return Response::json($response, $statusCode);
            }

            return Response::json($response, $statusCode);

        }
        catch(Exception $e){
            $statusCode = 404;
            return Response::json($response, $statusCode);
        }
    }


    public function dailyUserSearch()
    {
        $Feeds_return = array();
        $data = array();
        $response = [];
        try
        {
            $statusCode = 200;
            $EbooUserSearch = EbooUserSearch::where('status','=',1)
                ->select(DB::raw('MONTHNAME(session_date) as date') , DB::raw('count(*) as total') , DB::raw('DATE(session_date) as dates') , DB::raw('YEAR(session_date) AS year') )
                ->groupBy('date', 'year')
                ->orderBy('session_date','ASC')
                ->get();
            if($EbooUserSearch->count()>0)
            {
                foreach ($EbooUserSearch as $key => $value) {
                    $response['usersearch'][] = [ strtotime($value->dates)*1000,   $value->total ,];   

                }
            }
            else{
                return Response::json($response, $statusCode);
            }

            return Response::json($response, $statusCode);

        }
        catch(Exception $e){
            $statusCode = 404;
            return Response::json($response, $statusCode);
        }
    }

}
