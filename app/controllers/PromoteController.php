<?php

class PromoteController extends \BaseController {


      public function __construct( ) { 
    	ini_set('max_execution_time', 0); 
    	ini_set('memory_limit','250M');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
         
          if( Cache::has( 'Promote' ) ) {
            $Promote = Cache::get( 'Promote' );
        }
        else
        {
            $Promote = Promote::all();
            Cache::put( 'Promote', $Promote, 1 );
        }

      //print_r($EbooPromote);die();
		  return View::make('promote.index')
            ->with('promote', $Promote);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('promote.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
    //echo $file = Input::file('logo');die();
    

		 $rules = array(
            'name'       => 'required|unique:promote',
            'weight'     => 'required|numeric|min:1|max:100',
            'headline'   =>  'required|min:30',
             'url'       =>'required|url',
             'expiry_date' =>'required|date',
             'logo' => 'required'
             

         );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
          
            return Redirect::to('promote/create')
                ->withErrors($validator) 
                 ->withInput(Input::except('logo'));
               
        } else {
          /*image validation for 160 *160 */
           list($width, $height) = getimagesize(Input::file('logo'));
            if($width <=160 && $height <=160)
            {
             
            
        	/*image */
        	 if (Input::hasFile('logo')) { 
                $file = Input::file('logo');
                $originalname = $file->getClientOriginalName();
                $destinationPath = 'promote_logo'; // upload path
                $extension = Input::file('logo')->getClientOriginalExtension(); // getting image extension
                $fileName = Input::get('name').'_'.rand(11111,99999).'.'.$extension; // renameing image
                Input::file('logo')->move($destinationPath, $fileName); // uploading file to given path
               }


               $Promote = new Promote();
               $Promote->name =     Input::get('name');
               $Promote->headline =    Input::get('headline');
               $Promote->url =   Input::get('url');
               $Promote->expiry_date =   Input::get('expiry_date');
               $Promote->list =   Input::get('list');
               $Promote->impressions =   0;
               $Promote->clickcount =   0;
               $Promote->likecount =   0;
               $Promote->readcount =   0;
               $Promote->weight =   Input::get('weight');
               $Promote->logo =   $fileName;
               $Promote->created_at =   new DateTime;
               $Promote->status =   Input::get('status');
               $Promote->save();
               
               Cache::flush();

               Session::flash('message', 'Successfully created Promote!');
               return Redirect::to('promote');

        	
          }else{
              return Redirect::to('promote/create')
                ->withInput(Input::except('logo'))
                ->withErrors(array('Image Size' => 'Please make sure image size is 160 *160.'));
            }
            }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		 // get the admin
        $Promote = Promote::find($id);

        $UserReadDataFind = UserReadData::where('link_id','=',$id)->get();

        // show the view and pass the admin to it
        return View::make('promote.show')
            ->with('promote', $Promote)
            ->with('UserReadDataFind',$UserReadDataFind);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		 // get the admin
        $Promote = Promote::find($id);

        // show the view and pass the admin to it
        return View::make('promote.edit')
            ->with('promote', $Promote);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		 $Promote = Promote::find($id);

        
        // validate
        // read more on validation at http://laravel.com/docs/validation
     
            $rules = array(
                'name'       => 'required',
                'weight'     => 'required|numeric|min:1|max:100',
                'headline'   =>  'required|min:30',
                'url'       =>'required|url',
                'expiry_date' =>'required|date',
                
                );
            $validator = Validator::make(Input::all(), $rules);

        // process the login
            if ($validator->fails()) {
                return Redirect::to('promote/' . $id . '/edit')
                ->withInput(Input::except('logo'))
                ->withErrors($validator);
            } else {
            // store
               if (Input::hasFile('logo')) {
                  /*image validation for 160 *160 */
                   list($width, $height) = getimagesize(Input::file('logo'));
               
          
            if($width <=160 && $height <=160)
           {
             
                $file = Input::file('logo');
                $originalname = $file->getClientOriginalName();
                $destinationPath = 'promote_logo'; // upload path
                $extension = Input::file('logo')->getClientOriginalExtension(); // getting image extension
                $fileName = Input::get('name').'_'.rand(11111,99999).'.'.$extension; // renameing image
                Input::file('logo')->move($destinationPath, $fileName); // uploading file to given path
              
                
                $Promote->name =     Input::get('name');
                $Promote->headline =    Input::get('headline');
                $Promote->url =   Input::get('url');
                $Promote->expiry_date =   Input::get('expiry_date');
                $Promote->list =   Input::get('list');
                $Promote->weight =   Input::get('weight');
                $Promote->logo =   $fileName;
                $Promote->updated_at =   new DateTime;
                $Promote->status =   Input::get('status');
                $Promote->save();
                Cache::flush();
                 
            // redirect
                Session::flash('message', 'Successfully updated Promote!');
                return Redirect::to('promote');
            
            }else{
              return Redirect::to('promote/' . $id . '/edit')
                ->withInput(Input::except('logo'))
                ->withErrors(array('Image Size' => 'Please make sure image size is 160 *160.'));
            }
          }else{

             $Promote->name =     Input::get('name');
             $Promote->headline =    Input::get('headline');
             $Promote->url =   Input::get('url');
             $Promote->expiry_date =   Input::get('expiry_date');
             $Promote->list =   Input::get('list');
             $Promote->weight =   Input::get('weight');
             $Promote->logo =   $Promote->logo;
             $Promote->updated_at =   new DateTime;
             $Promote->status =   Input::get('status');
             $Promote->save();
             Cache::flush();
                 // redirect
                Session::flash('message', 'Successfully updated Promote!');
                return Redirect::to('promote');
           }
          }
        

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		  $Promote = Promote::find($id);
        $Promote->delete();
        Cache::flush();

        // redirect
        Session::flash('message', 'Successfully deleted the promote!');
        return Redirect::to('promote');
	}


}
