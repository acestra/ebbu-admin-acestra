<?php

class SourceController extends BaseController {

	public function source()
	{
        // get all the admin
        $EbooRss = EbooRss::all();

        // load the view and pass the admin
        return View::make('source')
            ->with('rss', $EbooRss);
		//return View::make('source');
	}

	public function feedsync()
	{
        // get all the admin
        $EbooRss = EbooRss::all();

        // load the view and pass the admin
        return View::make('feedsync')
            ->with('rss', $EbooRss);
		//return View::make('source');
	}
    public function source_link($id)
    {
            $source = EbooPublication::find($id);
            
           $Rssfeeds = DB::table('publication')
              ->join('rss', 'publication.id', '=', 'rss.publication_id')
              ->join('feeds', 'rss.id', '=', 'feeds.rss_link_id')
              ->where('rss.publication_id','=',$id)
                ->where('publication.status','=',1)
                ->get();
           
           $TweetFeeds = DB::table('publication')
              ->join('twitter_handler', 'publication.id', '=', 'twitter_handler.publication_id')
              ->join('twitter_feeds', 'twitter_handler.id', '=', 'twitter_feeds.twitter_handler_id')
              ->where('twitter_handler.publication_id','=',$id)
                ->where('publication.status','=',1)
                ->get();
            
             return View::make('source_view')
               ->with(array('source'=> $source,'Rssfeeds'=>$Rssfeeds,'TweetFeeds'=>$TweetFeeds));
    }
}
