<?php

class TrendingController  extends BaseController {

	public function trending()
	{
            
        /*$user_detail = DB::table('user_clicked_data')
            ->join('users', 'user_clicked_data.u_id', '=', 'users.id')
            //->join('user_search', 'user_clicked_data.u_id', '=', 'user_search.u_id')
            //->join('user_usage', 'user_clicked_data.u_id', '=', 'user_usage.u_id')
            //->select('user_clicked_data.u_id', 'users.user_id', 'user_search.search_query')
            ->select('user_clicked_data.u_id')
            ->where('status','=',1)
            ->groupBy('u_id')
            ->get();
    
print_r($user_detail);
*/

        $select_user_detail = UserClickedData::where('status','=',1)->groupBy('u_id')->get();

        $return_user = array();
        if(count($select_user_detail)>0)
        {
            $i=0;
            foreach ($select_user_detail as $key => $value) {
                $u_id = $value->u_id;
                $select_user_data = Users::where('id','=',$u_id)
                ->where('status','=',1)
                ->select('user_id')
                ->get();
                $user_id = 'none';
                foreach($select_user_data as $val_user)
                {
                    $user_id = $val_user->user_id;
                }
                $select_user_details = UserClickedData::where('u_id','=',$u_id)
                ->where('status','=',1)
                ->get();
                foreach ($select_user_details as $key => $value_details) {
                    $session_date = $value_details->session_date; 
                }
                $select_search_key = EbooUserSearch::where('u_id','=',$u_id)
                ->where('status','=',1)
                ->select('search_query')
                ->orderBy('id', 'asc')
                ->take(10)
                ->groupBy('search_query')
                ->get();
                $search_query = "";
                foreach($select_search_key as $key_val)
                {
                    if ($search_query=='') {
                        $search_query .= $key_val->search_query ;
                    }
                    else
                    {
                        $search_query .= ','.$key_val->search_query ;
                    }
                }
                $select_total_usage = EbooUserUsage::where('u_id','=',$u_id)
                ->where('status','=',1)
                ->get();
                $total_usage = 0;
                foreach($select_total_usage as $total_val)
                {
                    $total_usage =  $total_usage + $total_val->total_usage;
                }
                 $feeds_counts = UserClickedData::where('u_id','=',$u_id)->where('feeds_id','!=',0)->groupBy("feeds_id")->get();
                $tweets_count = UserClickedData::where('u_id','=',$u_id)->where('tweets_id','!=',0)->groupBy("tweets_id")->get();

                $return_user[$i]['id'] = $u_id;
                $return_user[$i]['user_id'] = $user_id;
                //$return_user[$i]['feeds_count'] = count($select_user_details);
                 $return_user[$i]['feeds_counts'] = count($feeds_counts);//new
                $return_user[$i]['tweets_count'] = count($tweets_count);//new
                $return_user[$i]['session_date'] = $session_date;
                $return_user[$i]['search_query'] = $search_query;
                $return_user[$i]['total_usage'] = gmdate("H:i:s", $total_usage);

                $i++;
            }
            //print_r($return_user);
        }

        $total_usage = array();
        foreach ($return_user as $key => $row)
        {
            $total_usage[$key] = $row['total_usage'];
        }
        //echo "<pre>";
        array_multisort($total_usage, SORT_DESC, $return_user);
        //print_r($return_user);
        //die;

        // get all the admin
        $Users = Users::all();
        $EbooUserUsage = EbooUserUsage::all();
        $EbooUserSearch = EbooUserSearch::groupBy('search_query')->get();
        $total_usage = 0 ; 
        foreach ($EbooUserUsage as $key => $value) {
            $total_usage = $total_usage + $value->total_usage ;
        }

        // load the view and pass the admin
        return View::make('trending')
            ->with('Users', $Users->count())
            ->with('total_usage', gmdate("H:i:s", $total_usage))
            ->with('total_search_word', $EbooUserSearch->count())
            ->with('return_user', $return_user);

	}


    public function trendingAjax()
    {
        $select_user_detail = UserClickedData::where('status','=',1)->groupBy('u_id')->get();

        $return_user = array();
        if(count($select_user_detail)>0)
        {
            $i=0;
            foreach ($select_user_detail as $key => $value) {
                $u_id = $value->u_id;
                $select_user_data = Users::where('id','=',$u_id)
                ->where('status','=',1)
                ->select('user_id')
                ->get();
                $user_id = 'none';
                foreach($select_user_data as $val_user)
                {
                    $user_id = $val_user->user_id;
                }
                $select_user_details = UserClickedData::where('u_id','=',$u_id)
                ->where('status','=',1)
                ->get();
                foreach ($select_user_details as $key => $value_details) {
                    $session_date = $value_details->session_date; 
                }
                $select_search_key = EbooUserSearch::where('u_id','=',$u_id)
                ->where('status','=',1)
                ->select('search_query')
                ->orderBy('id', 'asc')
                ->take(10)
                ->groupBy('search_query')
                ->get();
                $search_query = "";
                foreach($select_search_key as $key_val)
                {
                    if ($search_query=='') {
                        $search_query .= $key_val->search_query ;
                    }
                    else
                    {
                        $search_query .= ','.$key_val->search_query ;
                    }
                }
                $select_total_usage = EbooUserUsage::where('u_id','=',$u_id)
                ->where('status','=',1)
                ->get();
                $total_usage = 0;
                foreach($select_total_usage as $total_val)
                {
                    $total_usage = $total_usage + $total_val->total_usage;
                }
                $return_user[$i]['user_id'] = $user_id;
                $return_user[$i]['feeds_count'] = count($select_user_details);
                $return_user[$i]['session_date'] = $session_date;
                $return_user[$i]['search_query'] = $search_query;
                $return_user[$i]['total_usage'] = gmdate("H:i:s", $total_usage);

                $i++;
            }
            //print_r($return_user);
        }

        $total_usage = array();
        foreach ($return_user as $key => $row)
        {
            $total_usage[$key] = $row['total_usage'];
        }
        //echo "<pre>";
        array_multisort($total_usage, SORT_DESC, $return_user);
        //print_r($return_user);
        //die;

        // get all the admin
        $Users = Users::all();
        $EbooUserUsage = EbooUserUsage::all();
        $EbooUserSearch = EbooUserSearch::groupBy('search_query')->get();
        $total_usage = 0 ; 
        foreach ($EbooUserUsage as $key => $value) {
            $total_usage = $total_usage + $value->total_usage ;
        }
        $EbooFeeds = EbooFeeds::where('status','=',1)->orderBy('clickcount', 'desc')->take(100)->get();
        $TwitterFeeds = TwitterFeeds::where('status','=',1)->orderBy('click_count', 'desc')->take(100)->get();
        $html = View::make('trendingajax')
        ->with('Users', $Users->count())
        ->with('total_usage', gmdate("H:i:s", $total_usage))
        ->with('total_search_word', $EbooUserSearch->count())
        ->with('feeds', $EbooFeeds)
        ->with('twitterfeeds', $TwitterFeeds)
        ->render();
            //return Response::json(array('html' => $html));
        return $html;

    }
}
