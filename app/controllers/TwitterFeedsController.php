<?php

class TwitterFeedsController extends \BaseController {

    public function __construct( ) { 
    	ini_set('max_execution_time', 0); 
    	ini_set('memory_limit','250M');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

        // load the view and pass the admin
        return View::make('twitterfeeds.index');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$EbooCategory = EbooCategory::all();
		$selectCategories = array();
		foreach($EbooCategory as $category) {
    		$selectedCategories[$category->id] = $category->category_name;
		}
		return View::make('twitterfeeds.create')
			->with('category', $selectedCategories);;
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
        	'twitter_handler_id'       => 'required',
        	'tweet_text'       => 'required',
            'article_description'       => 'required',
            'article_link'       => 'required|url',
            'article_pubdate'       => 'required|date',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('twitterfeeds/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $TwitterFeeds = new TwitterFeeds;
            $TwitterFeeds->category_id  	= Input::get('category_id');
            $TwitterFeeds->publication_id  	= Input::get('publication_id');
            $TwitterFeeds->feeds_link  	= Input::get('feeds_link');
            $TwitterFeeds->session_date      = new DateTime;
            $TwitterFeeds->status  	= Input::get('status');
            $TwitterFeeds->save();
			Cache::flush();
            // redirect
            Session::flash('message', 'Successfully created feeds!');
            return Redirect::to('twitterfeeds');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        // get the admin
        $TwitterFeeds = TwitterFeeds::find($id);

        // show the view and pass the admin to it
        return View::make('twitterfeeds.show')
            ->with('feeds', $TwitterFeeds);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        // get the admin
        $TwitterFeeds = TwitterFeeds::find($id);
		$EbooCategory = EbooCategory::all();
		$selectCategories = array();
		foreach($EbooCategory as $category) {
    		$selectedCategories[$category->id] = $category->category_name;
		}

		$EbooPublication = EbooPublication::all();
		$selectPublication = array();
		foreach($EbooPublication as $Publication) {
    		$selectPublication[$Publication->id] = $Publication->publication_name;
		}

        // show the edit form and pass the admin
        return View::make('twitterfeeds.edit')
            ->with('feeds', $TwitterFeeds)
            ->with('category', $selectedCategories)
            ->with('source', $selectPublication);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
        	'tweet_text'       => 'required',
            'article_description'       => 'required',
            'article_link'       => 'required|url',
            'article_pubdate'       => 'required|date',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('twitterfeeds/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $TwitterFeeds = TwitterFeeds::find($id);
            $TwitterFeeds->tweet_text  	= Input::get('tweet_text');
            $TwitterFeeds->article_description       = Input::get('article_description');
            $TwitterFeeds->article_link  	= Input::get('article_link');
            $TwitterFeeds->created_date       = Input::get('article_pubdate');
            $TwitterFeeds->session_date      = new DateTime;
            $TwitterFeeds->status  	= Input::get('status');
            $TwitterFeeds->save();
            Cache::flush();

            // redirect
            Session::flash('message', 'Successfully updated feeds!');
            return Redirect::to('twitterfeeds');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $TwitterFeeds = TwitterFeeds::find($id);
        $TwitterFeeds->delete();
        Cache::flush();

        // redirect
        Session::flash('message', 'Successfully deleted the feeds!');
        return Redirect::to('twitterfeeds');
	}


	/** 
	* Get the Twitter Feed Sync
	*/

	public function twitterfeedsync()
	{

		$limitDay =  '-'.Config::get('services.limitday').' day';
		$consumer_key =  Config::get('twitter.consumer_key');
		$consumer_secret =  Config::get('twitter.consumer_secret');
		$twitter_limit =  Config::get('twitter.twitter_limit');

		try{
			$connection = new TwitterOAuth($consumer_key, $consumer_secret);

			$yesterday = date('Y-m-d h:i:s',strtotime("-1 minutes "));
			$TwitterRss = TwitterRss::where('status','=',1)
				->where('session_date','<',$yesterday)
				->orderBy('session_date','asc');

			$TwitterRss->count() < 150 ? $taken = $TwitterRss->count() : $taken = round($TwitterRss->count()/2);

			$TwitterRss = $TwitterRss->take($taken)->get();

			foreach ($TwitterRss as $twitter_source)
			{
				$TwitterRss_change = TwitterRss::find($twitter_source->id);
				try
				{
					$tweets = $connection->get('statuses/user_timeline', array('screen_name' => $twitter_source->twitter_handler ,'exclude_replies' => 'true', 'include_rts' => 'false', 'count' => $twitter_limit));
					if(!empty($tweets)) {
						foreach($tweets as $tweet) {
							if ($tweet->entities->urls) {
								$entities = $tweet->entities->urls[0]->expanded_url;
								$TwitterFeeds_find = TwitterFeeds::where('id_str','=',$tweet->id_str)->get();

								if($TwitterFeeds_find->count()==0){

									$tweetText = $tweet->text;
									$tweetText = preg_replace("#(http://|(www.))(([^s<]{4,68})[^s<]*)#", '', $tweetText);
									$tweetText = preg_replace('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $tweetText);
									$created_date = $tweet->created_at;

									$rules = array(
										'tweet_text'       => 'required|unique:twitter_feeds',
										'article_link'       => 'required|unique:twitter_feeds',
										'created_date'       => 'required'
										);
									$input_value = array(
										'tweet_text'       => trim($tweetText),
										'article_link'       => $entities,
										'created_date'       => date("Y-m-d", strtotime($created_date))
										);
									$validator = Validator::make($input_value , $rules);

									if ($validator->fails()) {
										Session::flash('message', 'Can`t updated feeds! ');	
										//echo $validator->messages();
										//die;
										/*
										return Redirect::to('twittersync/')
										->withErrors($validator)
										->withInput(Input::except('password'));
										*/
									} else {
										$TwitterFeeds = new TwitterFeeds;

										$yesterday=date('Y-m-d',strtotime("-10 days"));
										$formattedDate = date("Y-m-d", strtotime($created_date));
										if($yesterday<=$formattedDate)
										{
											$TwitterFeeds->twitter_handler_id = $twitter_source->id;
											$TwitterFeeds->tweet_text = addslashes($tweetText);
											$TwitterFeeds->article_link = $entities;
											$TwitterFeeds->id_str = $tweet->id_str;
											$TwitterFeeds->retweet_count = $tweet->retweet_count;
											$TwitterFeeds->click_count = 0;
											$TwitterFeeds->likecount = 0;
											$TwitterFeeds->readcount = 0;
											$TwitterFeeds->created_date = date("Y-m-d h:i:s", strtotime($created_date));
											$TwitterFeeds->session_date      = new DateTime;
											$TwitterFeeds->status  	= 1;
											$TwitterFeeds->save();
										}
									}
								}
							}
						}
					}
				}
				catch(Exception $e){ 
					/*echo "<pre>";
					echo $e;
					die;*/
					Session::flash('message', 'Can`t updated feeds! '.$twitter_source->id);
					/*
					   $TwitterRss = TwitterRss::find($twitter_source->id);
					   $TwitterRss->status = 2;
					   $TwitterRss->save();
					 */
				}
				$TwitterRss_change->session_date      = new DateTime;
				$TwitterRss_change->save();
			}
			//die;
		}
		catch(Exception $e){ 
			/*echo "<pre>";
			echo $e;
			die;*/
			Session::flash('message', 'Can`t updated feeds! '.$rss_source->id);

		}

		try {

			$today = Date('Y-m-d');
			$date = DateTime::createFromFormat('Y-m-d', $today);
			$date = date_modify($date,$limitDay);
			$date = date_format($date , 'Y-m-d');

			$TwitterFeeds_last = TwitterFeeds::where('created_date','<=',$date)->get();		

			foreach ($TwitterFeeds_last as $key => $value) {
				$TwitterFeeds_change = TwitterFeeds::find($value->id);
				$TwitterFeedsOld = new TwitterFeedsOld;
				$TwitterFeedsOld->twitter_handler_id = $TwitterFeeds_change->id;
				$TwitterFeedsOld->tweet_text = trim($TwitterFeeds_change->tweet_text);
				$TwitterFeedsOld->article_link = $TwitterFeeds_change->article_link;
				$TwitterFeedsOld->created_date = $TwitterFeeds_change->created_date;
				$TwitterFeedsOld->id_str = $TwitterFeeds_change->id_str;
				$TwitterFeedsOld->retweet_count = $TwitterFeeds_change->retweet_count;
				$TwitterFeedsOld->click_count = $TwitterFeeds_change->click_count;
				$TwitterFeedsOld->likecount = $TwitterFeeds_change->likecount;
				$TwitterFeedsOld->readcount = $TwitterFeeds_change->readcount;
				$TwitterFeedsOld->session_date      = new DateTime;
				$TwitterFeedsOld->status = 2;
				$TwitterFeedsOld->save();
				$TwitterFeeds = TwitterFeeds::find($value->id);
				$TwitterFeeds->delete();
			}
			$EbbuFeeds = EbooFeeds::select('id','article_title',DB::raw('count(*) as total'))
				->groupBy('article_title')
				->havingRaw("total > 1")
				->get();

			if($EbbuFeeds->count()>0)
			{
				foreach($EbbuFeeds as $EbbuFeedsDelete)
				{
					$EbbuDelete = EbooFeeds::find($EbbuFeedsDelete->id);
					$EbbuDelete->delete();
				}
			}

		} catch (Exception $e) {
			Session::flash('message', 'Can`t updated feeds! ');	
		}

		Cache::flush();
		Session::flash('message', 'Successfully updated feeds!');
		return Redirect::to('twittersync');
	}

	public function twitterFeedsAjax()
	{
		//print_r($_REQUEST);
		$Feeds_return = array();
		$data = array();
		$new_skip = $_REQUEST['start']*$_REQUEST['draw'];
		$response = [
			"draw" => $_REQUEST['draw'],
			"recordsTotal" => TwitterFeeds::count(),
			"recordsFiltered" => TwitterFeeds::count(),
			'data' => []
		];
		try
		{
			$statusCode = 200;
			if (!$_REQUEST['search']['value']) {
				$TwitterFeeds = TwitterFeeds::select('id', 'twitter_handler_id','tweet_text','created_date','click_count','status');

				$check = '';
				$order = "";
				foreach($_REQUEST['order'] as $key => $value)
				{
					if($value['column']==0)
						$column = 'id';
					else if($value['column']==3)
						$column = 'tweet_text';
					else if($value['column']==4)
						$column = 'click_count';
					else if($value['column']==5)
						$column = 'created_date';
					else if($value['column']==6)
						$column = 'status';
					else
						$check = "Available";

					$order =  $value['dir'];

					if ($check!='Available') {
						$TwitterFeeds = $TwitterFeeds->orderBy( $column, $order);
					}

				}
				$TwitterFeeds = $TwitterFeeds->skip($_REQUEST['start'])->take($_REQUEST['length'])->get();

			}
			else
			{
				$TwitterFeeds = TwitterFeeds::select('id', 'twitter_handler_id','tweet_text','created_date','click_count','status')
					->where('status','=',1)
					->where('tweet_text','like', '%'.$_REQUEST['search']['value'].'%');

				$response["recordsFiltered"] = $TwitterFeeds->count();

				$TwitterFeeds = $TwitterFeeds->skip($_REQUEST['start'])->take($_REQUEST['length'])
						->get();	

				$response["recordsTotal"] = TwitterFeeds::count(); 

			}


			if($TwitterFeeds->count()>0)
			{
				foreach ($TwitterFeeds as $key => $value) {

					if( $value->status ==1) 
						$status =  "Enable"; 
					else
						$status = "Disable";


					$response['data'][] = [ $value->id,	 
					TwitterFeeds::getCategoryName($value->twitter_handler_id),
					TwitterFeeds::getPublicationName($value->twitter_handler_id),
					stripslashes($value->tweet_text),
					$value->click_count,
					date("F j, Y, g:i a" , strtotime($value->created_date) ),
					$status,
					];	
				}
			}
			else{
				$statusCode = 200;
				return Response::json($response, $statusCode);
			}

			return Response::json($response, $statusCode);
		}
		catch(Exception $e){
			//echo $e;
			$statusCode = 404;
			return Response::json($response, $statusCode);
		}
	}

}
