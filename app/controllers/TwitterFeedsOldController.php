<?php

class TwitterFeedsOldController extends BaseController {

    public function __construct( ) { 
    	ini_set('max_execution_time', 0); 
    	ini_set('memory_limit','250M');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

        // load the view and pass the admin
        return View::make('twitterfeedsold.index');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$EbooCategory = EbooCategory::all();
		$selectCategories = array();
		foreach($EbooCategory as $category) {
    		$selectedCategories[$category->id] = $category->category_name;
		}
		return View::make('twitterfeedsold.create')
			->with('category', $selectedCategories);;
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
        	'twitter_handler_id'       => 'required',
        	'tweet_text'       => 'required',
            'article_description'       => 'required',
            'article_link'       => 'required|url',
            'article_pubdate'       => 'required|date',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('twitterfeedsold/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $TwitterFeeds = new TwitterFeeds;
            $TwitterFeeds->category_id  	= Input::get('category_id');
            $TwitterFeeds->publication_id  	= Input::get('publication_id');
            $TwitterFeeds->feeds_link  	= Input::get('feeds_link');
            $TwitterFeeds->session_date      = new DateTime;
            $TwitterFeeds->status  	= Input::get('status');
            $TwitterFeeds->save();
			Cache::flush();
            // redirect
            Session::flash('message', 'Successfully created feeds!');
            return Redirect::to('twitterfeedsold');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        // get the admin
        $TwitterFeeds = TwitterFeeds::find($id);

        // show the view and pass the admin to it
        return View::make('twitterfeedsold.show')
            ->with('feeds', $TwitterFeeds);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        // get the admin
        $TwitterFeeds = TwitterFeeds::find($id);
		$EbooCategory = EbooCategory::all();
		$selectCategories = array();
		foreach($EbooCategory as $category) {
    		$selectedCategories[$category->id] = $category->category_name;
		}

		$EbooPublication = EbooPublication::all();
		$selectPublication = array();
		foreach($EbooPublication as $Publication) {
    		$selectPublication[$Publication->id] = $Publication->publication_name;
		}

        // show the edit form and pass the admin
        return View::make('twitterfeedsold.edit')
            ->with('feeds', $TwitterFeeds)
            ->with('category', $selectedCategories)
            ->with('source', $selectPublication);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
        	'tweet_text'       => 'required',
            'article_description'       => 'required',
            'article_link'       => 'required|url',
            'article_pubdate'       => 'required|date',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('twitterfeedsold/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $TwitterFeeds = TwitterFeeds::find($id);
            $TwitterFeeds->tweet_text  	= Input::get('tweet_text');
            $TwitterFeeds->article_description       = Input::get('article_description');
            $TwitterFeeds->article_link  	= Input::get('article_link');
            $TwitterFeeds->created_date       = Input::get('article_pubdate');
            $TwitterFeeds->session_date      = new DateTime;
            $TwitterFeeds->status  	= Input::get('status');
            $TwitterFeeds->save();
            Cache::flush();

            // redirect
            Session::flash('message', 'Successfully updated feeds!');
            return Redirect::to('twitterfeedsold');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $TwitterFeeds = TwitterFeeds::find($id);
        $TwitterFeeds->delete();
        Cache::flush();

        // redirect
        Session::flash('message', 'Successfully deleted the feeds!');
        return Redirect::to('twitterfeedsold');
	}
}
