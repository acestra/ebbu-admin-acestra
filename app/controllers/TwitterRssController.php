<?php

class TwitterRssController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        if( Cache::has( 'TwitterRss' ) ) {
            $TwitterRss = Cache::get( 'TwitterRss' );
        }
        else
        {
            $TwitterRss = TwitterRss::all();
            Cache::put( 'TwitterRss', $TwitterRss, 1 );
        }

        // get all the admin
        //$TwitterRss = TwitterRss::all();

        // load the view and pass the admin
        return View::make('twitterrss.index')
            ->with('twitterrss', $TwitterRss);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$EbooCategory = EbooCategory::where('status','=',1)->get();
		$selectedCategories = array();
		foreach($EbooCategory as $category) {
    		$selectedCategories[$category->id] = $category->category_name;
		}

		$EbooPublication = EbooPublication::where('status','=',1)->get();
		$selectPublication = array();
		foreach($EbooPublication as $Publication) {
    		$selectPublication[$Publication->id] = $Publication->publication_name;
		}

		return View::make('twitterrss.create')
			->with('category', $selectedCategories)
			->with('source', $selectPublication);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
        	'category_id'       => 'required',
        	'publication_id'       => 'required',
            'twitter_handler'       => 'required|unique:twitter_handler',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('twitterrss/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $TwitterRss = new TwitterRss;
            $TwitterRss->category_id  	= Input::get('category_id');
            $TwitterRss->publication_id  	= Input::get('publication_id');
            $TwitterRss->twitter_handler  	= Input::get('twitter_handler');
            $TwitterRss->status  	= Input::get('status');
            $TwitterRss->save();
            Cache::flush();

            // redirect
            Session::flash('message', 'Successfully created twitterrss!');
            return Redirect::to('twitterrss');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        // get the admin
        $TwitterRss = TwitterRss::find($id);

        // show the view and pass the admin to it
        return View::make('twitterrss.show')
            ->with('twitterrss', $TwitterRss);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        // get the admin
        $TwitterRss = TwitterRss::find($id);
		//$EbooCategory = EbooCategory::all();
		$EbooCategory = EbooCategory::where('status','=',1)->get();
		$selectedCategories = array();
		foreach($EbooCategory as $category) {
    		$selectedCategories[$category->id] = $category->category_name;
		}

		$EbooPublication = EbooPublication::where('status','=',1)->get();
		$selectPublication = array();
		foreach($EbooPublication as $Publication) {
    		$selectPublication[$Publication->id] = $Publication->publication_name;
		}

        // show the edit form and pass the admin
        return View::make('twitterrss.edit')
            ->with('twitterrss', $TwitterRss)
            ->with('category', $selectedCategories)
            ->with('source', $selectPublication);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $TwitterRss = TwitterRss::find($id);
        if($TwitterRss->twitter_handler!=Input::get('twitter_handler'))
        {
            $rules = array(
                'category_id'       => 'required',
                'publication_id'       => 'required',
                'twitter_handler'       => 'required|unique:twitter_handler',
                );
            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::to('twitterrss/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
            } else {
                $TwitterRss->category_id   = Input::get('category_id');
                $TwitterRss->publication_id    = Input::get('publication_id');
                $TwitterRss->twitter_handler       = Input::get('twitter_handler');
                $TwitterRss->session_date      = new DateTime;
                $TwitterRss->status    = Input::get('status');
                $TwitterRss->save();
                Cache::flush();

                Session::flash('message', 'Successfully updated twitterrss!');
                return Redirect::to('twitterrss');
            }            
        }
        else
        {

            $rules = array(
                'category_id'       => 'required',
                'publication_id'       => 'required',
                );
            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::to('twitterrss/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
            } else {
                $TwitterRss->category_id   = Input::get('category_id');
                $TwitterRss->publication_id    = Input::get('publication_id');
                $TwitterRss->twitter_handler       = Input::get('twitter_handler');
                $TwitterRss->session_date      = new DateTime;
                $TwitterRss->status    = Input::get('status');
                $TwitterRss->save();
                Cache::flush();

                Session::flash('message', 'Successfully updated twitterrss!');
                return Redirect::to('twitterrss');
            }            
        }

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $TwitterRss = TwitterRss::find($id);
        $TwitterRss->delete();
        Cache::flush();

        // redirect
        Session::flash('message', 'Successfully deleted the twitterrss!');
        return Redirect::to('twitterrss');
	}

}
