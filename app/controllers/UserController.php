<?php

class UserController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function login()
	{
		//echo Hash::make('admin1234');
		if(Auth::check())
		{
			return Redirect::route("user.profile");
		}
		else
		{
			$data = [];
			if ($this->isPostRequest()) {
				$validator = $this->getLoginValidator();
				if ($validator->passes()) {
					$remember = Input::has('remember') ? true : false ;
					$credentials = $this->getLoginCredentials();

					$auth =  Auth::attempt($credentials,$remember) ;

					if($auth) {
						return Redirect::route("home");
					}

					return Redirect::back()->withErrors([
						"password" => ["Credentials invalid."]
					]);
				} else {
					return Redirect::back()
						->withInput()
						->withErrors($validator);
				}
			}
			return View::make("user.login",$data);
		}
	}
	protected function isPostRequest()
	{
		return Input::server("REQUEST_METHOD") == "POST";
	}

	protected function getLoginValidator()
	{
		return Validator::make(Input::all(), [
			"email" => "required",
			"password" => "required",
			"pin" => "required"
		]);
	}
 
	protected function getLoginCredentials()
	{
		return [
			"email" => Input::get("email"),
			"password" => Input::get("password"),
			"pin" => Input::get("pin"),
			"status" => 1
		];
	}

	public function logout()
	{
		Auth::logout();

		return Redirect::route("user.login");
	}

	public function profile()
	{
		return View::make("user.profile");
	}
	public function profileEdit()
	{
		if (Auth::check()) {
			if ($this->isPostRequest()) {
				$EbooAdmin = EbooAdmin::find(Auth::user()->id);

				$data = Input::all();

				$rules = array(
					'password' => 'required|min:6|confirmed',
					'password_confirmation' => 'required|min:6',
					'pin' => 'required|min:4|max:4|confirmed',
					'pin_confirmation' => 'required|min:4|max:4'
					);

    			// Create a new validator instance.
				$validator = Validator::make($data, $rules);
				if ($validator->fails()) {
					return Redirect::route("user.profile")
					->withErrors($validator)
					->withInput(Input::except('password'));
				} else {
					$EbooAdmin->password =  Hash::make(Input::get('password'));
					$EbooAdmin->pin =  Input::get('pin');
					$EbooAdmin->save();
				}
				return Redirect::route("user.profile");
			}
			else
			{
				$EbooAdmin = EbooAdmin::find(Auth::user()->id);
				return View::make("user.profileedit")
				->with('user',$EbooAdmin);
			}
		}
		else
		{
			die;
		}
	}
}
