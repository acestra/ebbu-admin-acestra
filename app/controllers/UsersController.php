<?php

class UsersController extends BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        // get all the users
        $Users = Users::all();

        // load the view and pass the users
        return View::make('users.index')
            ->with('users', $Users);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $UsersRoles = UsersRoles::all();
        $selectRoles = array();
        foreach($UsersRoles as $role) {
            $selectRoles[$role->id] = $role->rolename;
        }
		return View::make('users.create')
            ->with('users_roles', $selectRoles);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'users_user'       => 'required|Unique:users',
            'email'      => 'required|email|Unique:users',
            'password'      => 'required|min:4',
            'role_id'       => 'required',
            'pin'           => 'required|integer|min:4',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('users/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $Users = new Users;
            $Users->users_user  	= Input::get('users_user');
            $Users->email      	= Input::get('email');
            $Users->password        = Hash::make(Input::get('password'));
            $Users->role_id         = Input::get('role_id');
            $Users->pin        = Input::get('pin');
            $Users->status      = Input::get('status');
            $Users->save();
            Cache::flush();

            // redirect
            Session::flash('message', 'Successfully created Users!');
            return Redirect::to('users');
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        // get the users
        $Users = Users::find($id);

        $userRssFeedClicked = UserClickedData::where('u_id','=',$id)
            ->where('feeds_id','!=',0)
            ->groupBy('feeds_id')
            ->orderBy('session_date','DESC')->take(10)->get();

        $userTwitterFeedClicked = UserClickedData::where('u_id','=',$id)
            ->where('tweets_id','!=',0)
            ->groupBy('tweets_id')
            ->orderBy('session_date','DESC')->take(10)->get();

        // show the view and pass the users to it
        return View::make('users.show')
            ->with('users', $Users)
            ->with('userRssFeedClicked', $userRssFeedClicked)
            ->with('userRssFeedClickedCount', $userRssFeedClicked->count())
            ->with('userTwitterFeedClickedCount', $userTwitterFeedClicked->count())
            ->with('userTwitterFeedClicked', $userTwitterFeedClicked);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        // get the users
        $Users = Users::find($id);
        $UsersRoles = UsersRoles::all();
        $selectRoles = array();
        foreach($UsersRoles as $role) {
            $selectRoles[$role->id] = $role->rolename;
        }

        // show the edit form and pass the users
        return View::make('users.edit')
            ->with('users', $Users)
            ->with('users_roles', $selectRoles);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'pin'           => 'required|integer|min:4',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('users/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $Users = Users::find($id);
            $Users->pin        = Input::get('pin');
            $Users->status        = Input::get('status');
            $Users->role_id     = Input::get('role_id');
            $Users->session_date    = new DateTime;
            $Users->save();
            Cache::flush();

            // redirect
            Session::flash('message', 'Successfully updated users!');
            return Redirect::to('users');
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        // delete
        $Users = Users::find($id);
        $Users->delete();
        Cache::flush();

        // redirect
        Session::flash('message', 'Successfully deleted the users!');
        return Redirect::to('users');
	}


}
