<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbooAdminTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (Schema::hasTable('admin'))
		{
			Schema::dropIfExists("admin");
			Schema::create('admin', function(Blueprint $table)
			{
				$table->increments("id");
				$table->string("admin_user");
				$table->string("password");
				$table->string("email_id");
				$table->string("email");
				$table->integer('pin');
				$table->string("reset_pass_code");
				$table->string("remember_token")->nullable();
				$table->date('session_date');
				$table->integer('status');
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('admin', function(Blueprint $table)
		{
			Schema::dropIfExists("admin");
		});
	}

}
