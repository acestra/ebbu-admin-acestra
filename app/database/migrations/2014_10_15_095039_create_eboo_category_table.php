<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbooCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('category'))
		{
			Schema::create('category', function(Blueprint $table)
			{
				$table->increments("id");
				$table->string("category_name");
				$table->date('session_date');
				$table->integer('status');
				$table->timestamps();		
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('category');
	}

}
