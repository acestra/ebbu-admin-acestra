<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbooRssTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('rss'))
		{
			Schema::create('rss', function(Blueprint $table)
			{
				$table->increments("id");
				$table->integer("category_id");
				$table->integer("publication_id");
				$table->string("rss_link");
				$table->date('session_date');
				$table->integer('status');
				$table->timestamps();		
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('rss');
	}

}
