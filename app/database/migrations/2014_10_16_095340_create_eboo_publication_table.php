<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbooPublicationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('publication'))
		{
			Schema::create('publication', function(Blueprint $table)
			{
				$table->increments("id");
				$table->string("publication_name");
				$table->string("publication_image");
				$table->date('session_date');
				$table->integer('status');
				$table->timestamps();		
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('publication');
	}

}
