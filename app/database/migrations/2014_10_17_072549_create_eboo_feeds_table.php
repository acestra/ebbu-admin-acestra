<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbooFeedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('feeds'))
		{
			Schema::create('feeds', function(Blueprint $table)
			{
				$table->increments("id");
				$table->integer('rss_link_id');
				$table->string("article_title");
				$table->string("article_description");
				$table->string("article_link");
				$table->date('article_pubdate');
				$table->date('session_date');
				$table->integer('status');
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('feeds');
	}

}
