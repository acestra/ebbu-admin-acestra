<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbooAdminRoles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('admin_roles'))
		{
			Schema::create('admin_roles', function(Blueprint $table)
			{
				$table->increments("id");
				$table->string('rolename');
				$table->timestamps();	
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('admin_roles');
	}

}
