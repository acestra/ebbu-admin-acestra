<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbooUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('users'))
		{
			Schema::create('users', function(Blueprint $table)
			{
				$table->increments("id");
				$table->string("user_id");
				$table->string("location");
				$table->string("city");
				$table->string('state');
				$table->string("pincode");
				$table->string("country");
				$table->string("hardware_info");
				$table->string("software_version");
				$table->string("fb_handle");
				$table->string("tumblr_handle");
				$table->string("linkedin_handle");
				$table->integer("first_usage");
				$table->date('session_date');
				$table->integer('status');
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			Schema::dropIfExists("users");
		});
	}

}
