<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbooUserUsageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('user_usage'))
		{
			Schema::create('user_usage', function(Blueprint $table)
			{
				$table->increments("id");
				$table->string("u_id");
				$table->string("total_usage");
				$table->string("links_clicked");
				$table->date('session_date');
				$table->integer('status');
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_usage', function(Blueprint $table)
		{
			Schema::dropIfExists("user_usage");
		});
	}

}
