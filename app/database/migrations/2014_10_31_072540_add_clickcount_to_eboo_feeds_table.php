<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClickcountToEbooFeedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('feeds', function(Blueprint $table)
		{
			if (!Schema::hasColumn('feeds', 'clickcount'))
			{
				$table->integer('clickcount')->after('article_pubdate');
			}
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feeds', function(Blueprint $table)
		{
			$table->dropColumn('clickcount');
		});
	}

}
