<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbooUserClickedDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('user_clicked_data'))
		{
			Schema::create('user_clicked_data', function(Blueprint $table)
			{
				$table->increments("id");
				$table->string("u_id");
				$table->string("feeds_id");
				$table->date('session_date');
				$table->integer('status');
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_clicked_data', function(Blueprint $table)
		{
			Schema::dropIfExists("user_clicked_data");
		});
	}

}
