<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbooUserSearchTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('user_search'))
		{
			Schema::create('user_search', function(Blueprint $table)
			{
				$table->increments("id");
				$table->string("u_id");
				$table->string("search_query");
				$table->integer("articles_clicked");
				$table->date('session_date');
				$table->integer('status');
				$table->timestamps();
			});
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_search', function(Blueprint $table)
		{
			Schema::dropIfExists("user_search");
		});
	}

}
