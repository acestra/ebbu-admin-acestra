<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOauthSessionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('oauth_session'))
		{
			Schema::create('oauth_session', function(Blueprint $table)
			{
				$table->increments("id");
				$table->char("session",32);
				$table->char("state",32);
				$table->mediumText("access_token");
				$table->dateTime('expiry');
				$table->char("type",12);
				$table->char("server",12);
				$table->dateTime('creation');
				$table->mediumText("access_token_secret");
				$table->char("authorized",1);
				$table->integer("user");
				$table->mediumText("refresh_token");
				$table->timestamps();
				$table->unique( array('session','server') );
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('oauth_session', function(Blueprint $table)
		{
			Schema::dropIfExists("oauth_session");
		});
	}

}
