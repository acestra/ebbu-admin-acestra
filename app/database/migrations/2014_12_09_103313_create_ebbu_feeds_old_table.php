<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbbuFeedsOldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('feeds_old'))
		{
			Schema::create('feeds_old', function(Blueprint $table)
			{
				$table->increments("id");
				$table->integer("id_old");
				$table->integer("rss_link_id");
				$table->string("article_title");
				$table->text("article_description");
				$table->text('article_link');
				$table->dateTime("article_pubdate");
				$table->integer("clickcount");
				$table->dateTime('session_date');
				$table->integer("status");
				$table->timestamps();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feeds_old', function(Blueprint $table)
		{
			Schema::dropIfExists("feeds_old");
		});
	}

}
