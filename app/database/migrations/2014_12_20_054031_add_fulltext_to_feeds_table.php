<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFulltextToFeedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('feeds', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE '.DB::getTablePrefix().'feeds ENGINE = MyISAM');
			DB::statement('ALTER TABLE '.DB::getTablePrefix().'feeds ADD FULLTEXT search(article_title)');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feeds', function(Blueprint $table)
		{
			$table->dropIndex('search');
		});
	}

}
