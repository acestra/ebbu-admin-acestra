<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwitterRssTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('twitter_handler'))
		{
			Schema::create('twitter_handler', function(Blueprint $table)
			{
				$table->increments("id");
				$table->integer("category_id");
				$table->integer("publication_id");
				$table->string("twitter_handler");
				$table->integer('status');
				$table->timestamps();		
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('twitter_handler', function(Blueprint $table)
		{
			Schema::dropIfExists('twitter_handler');
		});
	}

}
