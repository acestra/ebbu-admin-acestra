<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwitterFeedsOldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('admin'))
		{
			Schema::create('twitter_feeds_old', function(Blueprint $table)
			{
				$table->increments("id");
				$table->integer("id_old");
				$table->integer('twitter_handler_id');
				$table->string("tweet_text");
				$table->string("article_link");
				$table->date('created_date');
				$table->integer('retweet_count');
				$table->bigInteger('id_str');
				$table->integer('click_count');
				$table->date('session_date');
				$table->integer('status');
				$table->timestamps();		
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('twitter_feeds_old', function(Blueprint $table)
		{
			Schema::dropIfExists('twitter_feeds_old');
		});
	}

}
