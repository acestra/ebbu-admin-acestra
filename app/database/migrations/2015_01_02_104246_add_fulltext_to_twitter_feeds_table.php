<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFulltextToTwitterFeedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('twitter_feeds', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE '.DB::getTablePrefix().'twitter_feeds ENGINE = MyISAM');
			DB::statement('ALTER TABLE '.DB::getTablePrefix().'twitter_feeds ADD FULLTEXT searchtwitter(tweet_text)');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('twitter_feeds', function(Blueprint $table)
		{
			$table->dropIndex('searchtwitter');
		});
	}

}
