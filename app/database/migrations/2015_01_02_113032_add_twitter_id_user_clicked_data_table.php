<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTwitterIdUserClickedDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_clicked_data', function(Blueprint $table)
		{
			if (!Schema::hasColumn('user_clicked_data', 'tweets_id'))
				$table->integer('tweets_id')->after('feeds_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_clicked_data', function(Blueprint $table)
		{
			$table->dropColumn('tweets_id');
		});
	}

}
