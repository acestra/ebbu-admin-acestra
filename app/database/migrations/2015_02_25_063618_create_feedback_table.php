<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('feedback'))
		{
			Schema::create('feedback', function(Blueprint $table)
			{
				$table->increments("id");
				$table->integer("u_id");
				$table->string("name");
				$table->string("email_address");
				$table->text("feedback");
				$table->integer("rating");
				$table->date('session_date');
				$table->integer('status');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feedback', function(Blueprint $table)
		{
			Schema::dropIfExists('feedback');
		});
	}

}
