<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromote extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('promote'))
		{
			Schema::create('promote', function(Blueprint $table)
			{
				$table->increments("id");
				$table->string("name",500);
				$table->string("logo",500);
				$table->string("headline",500);
				$table->string("url",500);
				$table->date('expiry_date');
				$table->integer("weight");
				$table->integer("impressions");
                $table->integer("clickcount");
                $table->timestamps();
				$table->integer('status');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promote', function(Blueprint $table)
		{
			Schema::dropIfExists('promote');
		});
	}

}
