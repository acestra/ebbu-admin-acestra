<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategorySearch extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('category_search'))
		{
			Schema::create('category_search', function(Blueprint $table)
			{
				$table->increments("id");
				$table->string("name",500);
				$table->timestamps();
				$table->integer('status');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('category_search', function(Blueprint $table)
		{
			Schema::dropIfExists('category_search');
		});
	}

}
