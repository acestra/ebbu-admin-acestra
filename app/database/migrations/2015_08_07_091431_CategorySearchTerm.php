<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategorySearchTerm extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('category_search_term'))
		{
			Schema::create('category_search_term', function(Blueprint $table)
			{
				$table->increments("id");
				$table->integer('category_search_id');
				$table->string("name",500);
				$table->timestamps();
				$table->integer('status');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('category_search_term', function(Blueprint $table)
		{
			Schema::dropIfExists('category_search_term');
		});
	}

}
