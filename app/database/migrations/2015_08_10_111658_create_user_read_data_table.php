<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserReadDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_read_data', function(Blueprint $table)
		{
			$table->increments("id");
			$table->string("u_id");
			$table->string("link_id");
			$table->string('type');
			$table->integer('status');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_read_data', function(Blueprint $table)
		{
			Schema::dropIfExists('user_read_data');
		});
	}

}
