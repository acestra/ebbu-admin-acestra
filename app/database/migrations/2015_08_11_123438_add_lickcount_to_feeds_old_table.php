<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLickcountToFeedsOldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('feeds_old', function(Blueprint $table)
		{
			$table->integer('likecount')->after('clickcount');
			$table->integer('readcount')->after('likecount');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('feeds_old', function(Blueprint $table)
		{
			$table->dropColumn('likecount');
			$table->dropColumn('readcount');
		});
	}

}
