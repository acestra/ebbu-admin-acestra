<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLickcountToTwitterFeedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('twitter_feeds', function(Blueprint $table)
		{
			$table->integer('likecount')->after('click_count');
			$table->integer('readcount')->after('likecount');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('twitter_feeds', function(Blueprint $table)
		{
			$table->dropColumn('likecount');
			$table->dropColumn('readcount');
		});
	}

}
