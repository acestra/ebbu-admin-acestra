<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserVerification extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable('user_verification'))
		{
			Schema::create('user_verification', function(Blueprint $table)
			{
				$table->increments("id");
				$table->integer('user_id');
				$table->integer('verification_code');
				$table->dateTime('expire_date');
				$table->integer('status');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_verification', function(Blueprint $table)
		{
			Schema::dropIfExists('user_verification');
		});
	}

}
