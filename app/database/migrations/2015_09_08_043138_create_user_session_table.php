<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSessionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_session', function(Blueprint $table)
		{
			$table->increments("id");
			$table->integer('user_id');
			$table->string('session_code');
			$table->integer('status');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_session', function(Blueprint $table)
		{
			Schema::dropIfExists('user_session');
		});
	}

}
