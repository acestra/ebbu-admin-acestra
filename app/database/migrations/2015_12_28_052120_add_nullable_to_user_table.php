<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableToUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			DB::statement('
				ALTER TABLE `'.DB::getTablePrefix().'users` 
				MODIFY `location` VARCHAR(1000) NULL,
				MODIFY `city` VARCHAR(500) NULL,
				MODIFY `state` VARCHAR(500) NULL,
				MODIFY `pincode` VARCHAR(500) NULL,
				MODIFY `country` VARCHAR(500) NULL,
				MODIFY `hardware_info` VARCHAR(500) NULL,
				MODIFY `software_version` VARCHAR(500) NULL,
				MODIFY `fb_handle` VARCHAR(500) NULL,
				MODIFY `tumblr_handle` VARCHAR(500) NULL,
				MODIFY `linkedin_handle` VARCHAR(500) NULL,
				MODIFY `first_usage` VARCHAR(500) NULL;
			');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			DB::statement('
				ALTER TABLE `'.DB::getTablePrefix().'users`
				MODIFY `location` VARCHAR(1000) NOT NULL,
				MODIFY `city` VARCHAR(500) NOT NULL,
				MODIFY `state` VARCHAR(500) NOT NULL,
				MODIFY `pincode` VARCHAR(500) NOT NULL,
				MODIFY `country` VARCHAR(500) NOT NULL,
				MODIFY `hardware_info` VARCHAR(500) NOT NULL,
				MODIFY `software_version` VARCHAR(500) NOT NULL,
				MODIFY `fb_handle` VARCHAR(500) NOT NULL,
				MODIFY `tumblr_handle` VARCHAR(500) NOT NULL,
				MODIFY `linkedin_handle` VARCHAR(500) NOT NULL,
				MODIFY `first_usage` VARCHAR(500) NOT NULL;
			');
		});
	}

}
