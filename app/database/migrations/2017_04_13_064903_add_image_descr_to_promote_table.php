<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageDescrToPromoteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('promote', function(Blueprint $table)
		{
			$table->string('image',500)->after('logo');
			$table->string('description',500)->after('readcount')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('promote', function(Blueprint $table)
		{
			$table->dropColumn('image');
			$table->dropColumn('description');
		});
	}

}
