<?php

class RolesSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('admin_roles')->truncate();


        $users = array(
            array(
                'rolename'        => 'admin',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'rolename'        => 'moderator',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'rolename'        => 'user',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            )
        );

        DB::table('admin_roles')->insert( $users );
    }

}
