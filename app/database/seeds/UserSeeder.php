<?php

class UserSeeder extends DatabaseSeeder {

    public function run()
    {
        DB::table('admin')->delete();


        $users = array(
            array(
                'admin_user'        => 'administrator',
                'email'          => 'admin@example.org',
                'password'          => Hash::make('admin'),
                'pin'               => '1234',
                'role_id'           => 1,
                'status'            => 1,
            ),
            array(
                'admin_user'        => 'user',
                'email'          => 'user@example.org',
                'password'          => Hash::make('user'),
                'pin'               => '1234',
                'role_id'           => 2,
                'status'            => 1,
            )
        );

        DB::table('admin')->insert( $users );
    }

}
