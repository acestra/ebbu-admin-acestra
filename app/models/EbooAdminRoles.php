<?php

class EbooAdminRoles extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'admin_roles';

	/**
	 * The database table removed timestamp from this model.
	 *
	 * @var string
	 */

	public  $timestamps = false;
	
	/** 
	* Get the Category Name 
	*/

	public static function getRoleName($id)
	{
		$EbooAdminRoles = EbooAdminRoles::find($id);

		if(count($EbooAdminRoles))
		{
			return $EbooAdminRoles->rolename;
		}
		else
		{
			return "Not Assign Role";
		}	

		
	}

}
