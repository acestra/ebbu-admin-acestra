<?php

class EbooCategory extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'category';


	/**
	 * The database table removed timestamp from this model.
	 *
	 * @var string
	 */

	public  $timestamps = false;

	/** 
	* Get the Category Name 
	*/

	public static function getCategoryName($id)
	{
		$EbooCategory = EbooCategory::find($id);
		if(count($EbooCategory)!=0) {
			return $EbooCategory->category_name;
		}
		else
		{
			return "Default";
		}
	}

}
