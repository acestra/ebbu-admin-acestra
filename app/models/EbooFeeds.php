<?php

class EbooFeeds extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'feeds';


	/**
	 * The database table removed timestamp from this model.
	 *
	 * @var string
	 */

	public  $timestamps = false;

	
	/** 
	* Get the Category Name 
	*/

	public static function getCategoryName($id)
	{
		//return $id;
		$EbooRss = EbooRss::find($id);
		if(count($EbooRss))
		{
			$EbooCategory = EbooCategory::getCategoryName($EbooRss->category_id);
			return $EbooCategory;
		}
		else
		{
			return "Default";
		}
	}

	/** 
	* Get the Category Id 
	*/

	public static function getCategoryId($id)
	{
		//return $id;
		$EbooRss = EbooRss::find($id);
		if(count($EbooRss))
		{
			return $EbooRss->category_id;
		}
		else
		{
			return "0";
		}
	}

	/** 
	* Get the Publisher Name 
	*/

	public static function getPublicationName($id)
	{
		$EbooRss = EbooRss::find($id);
		if(count($EbooRss))
		{
			$EbooPublication = EbooPublication::getPublicationName($EbooRss->publication_id);
			return $EbooPublication;
		}
		else
		{
			return "Default";
		}	
	}
	/** 
	* Get the Publisher Image 
	*/

	public static function getPublicationImage($id)
	{
		$EbooRss = EbooRss::find($id);
		if(count($EbooRss))
		{
			$EbooPublication = EbooPublication::getPublicationImage($EbooRss->publication_id);
			return $EbooPublication;
		}
		else
		{
			return "images/logo.png";
		}	
	}
	/** 
	* Get the Publisher Id 
	*/

	public static function getPublicationId($id)
	{
		$EbooRss = EbooRss::find($id);
		if(count($EbooRss))
		{
			return $EbooRss->publication_id;
		}
		else
		{
			return "0";
		}	
	}
	public static function getRssFeed($sourceId)
	{
          $getRssId = EbooRss::where('publication_id','=',40)->where('status','=',1)->get();
          
          return $getRssId;
	}
}
