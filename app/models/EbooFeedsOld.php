<?php

class EbooFeedsOld extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'feeds_old';


	/**
	 * The database table removed timestamp from this model.
	 *
	 * @var string
	 */

	public  $timestamps = false;
	
	/** 
	* Get the Category Name 
	*/

	public static function getCategoryName($id)
	{
		//return $id;
		$EbooRss = EbooRss::find($id);
		if(count($EbooRss))
		{
			$EbooCategory = EbooCategory::getCategoryName($EbooRss->category_id);
			return $EbooCategory;
		}
		else
		{
			return "Default";
		}
	}

	/** 
	* Get the Publisher Name 
	*/

	public static function getPublicationName($id)
	{
		$EbooRss = EbooRss::find($id);
		if(count($EbooRss))
		{
			$EbooPublication = EbooPublication::getPublicationName($EbooRss->publication_id);
			return $EbooPublication;
		}
		else
		{
			return "Default";
		}	
	}
}
