<?php

class EbooPublication extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'publication';


	/**
	 * The database table removed timestamp from this model.
	 *
	 * @var string
	 */

	public  $timestamps = false;
	
	/** 
	* Get the Publisher Name 
	*/

	public static function getPublicationName($id)
	{
		$EbooPublication = EbooPublication::find($id);
		if(count($EbooPublication)!=0) {
			return $EbooPublication->publication_name;
		}
		else
		{
			return "Default";
		}
	}
	/** 
	* Get the Publisher Image
	*/

	public static function getPublicationImage($id)
	{
		$EbooPublication = EbooPublication::find($id);
		if(count($EbooPublication)!=0) {
			return $EbooPublication->publication_image;
		}
		else
		{
			return "Default";
		}
	}
	public static function disableEnableSorceRelatedFeeds($souce_id,$status_id)
	{
          $Rssfeeds = DB::table('publication')
              ->join('rss', 'publication.id', '=', 'rss.publication_id')
              //->join('feeds', 'rss.id', '=', 'feeds.rss_link_id')
              ->where('rss.publication_id','=',$souce_id)
               ->update(array('rss.status'=>$status_id,'publication.status'=>$status_id));

          $TweetFeeds = DB::table('publication')
              ->join('twitter_handler', 'publication.id', '=', 'twitter_handler.publication_id')
             // ->join('twitter_feeds', 'twitter_handler.id', '=', 'twitter_feeds.twitter_handler_id')
              ->where('twitter_handler.publication_id','=',$souce_id)
                ->update(array('twitter_handler.status'=>$status_id,'publication.status'=>$status_id)); 


	}
	
}
