<?php

class EbooUserSearch extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_search';

	/**
	 * The database table removed timestamp from this model.
	 *
	 * @var string
	 */

	public  $timestamps = false;
	
}
