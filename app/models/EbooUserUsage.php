<?php

class EbooUserUsage extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_usage';

	/**
	 * The database table removed timestamp from this model.
	 *
	 * @var string
	 */

	public  $timestamps = false;
	
}
