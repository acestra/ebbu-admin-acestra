<?php

class Feedback extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'feedback';

	/**
	 * The database table removed timestamp from this model.
	 *
	 * @var string
	 */

	public  $timestamps = false;
}
