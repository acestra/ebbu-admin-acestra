<?php

class SrchTermModel extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'category_search_term';


	/**
	 * The database table removed timestamp from this model.
	 *
	 * @var string
	 */

	public  $timestamps = false;

	/** 
	* Get the Category Name 
	*/
    
	public static function getSearchTermName($searchid)
     {
     	 $EbooCategorySearchTerm = SrchTermModel::where('category_search_id','=',$searchid)->get();
		if(count($EbooCategorySearchTerm)!=0) {

           foreach($EbooCategorySearchTerm as $key => $list)
           {
           	if($key ==0)
           	{
                echo $list->name;
           	}else{
           		echo ",".$list->name;

           	}
           	//
		}
           }
			
		else
		{
			return "Default";
		}
     }
     public static function updateSearchTerm($searchId,$termId,$termName,$status)
     {
     	    $CategorySearchTerm = SrchTermModel::where('id','=',$termId)
     	                                  ->where('category_search_id','=',$searchId)
     	                                 ->update(array('name'=> $termName,'status'=>$status));
     }

}
