<?php

class TwitterFeeds extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'twitter_feeds';


	/**
	 * The database table removed timestamp from this model.
	 *
	 * @var string
	 */

	public  $timestamps = false;
	
	/** 
	* Get the Category Name 
	*/

	public static function getCategoryName($id)
	{
		//return $id;
		$TwitterRss = TwitterRss::find($id);
		if(count($TwitterRss))
		{
			$EbooCategory = EbooCategory::getCategoryName($TwitterRss->category_id);
			return $EbooCategory;
		}
		else
		{
			return "Default";
		}
	}

	/** 
	* Get the Category Id 
	*/

	public static function getCategoryId($id)
	{
		//return $id;
		$TwitterRss = TwitterRss::find($id);
		if(count($TwitterRss))
		{
			return $TwitterRss->category_id;
		}
		else
		{
			return "0";
		}
	}

	/** 
	* Get the Publisher Name 
	*/

	public static function getPublicationName($id)
	{
		$TwitterRss = TwitterRss::find($id);
		if(count($TwitterRss))
		{
			$EbooPublication = EbooPublication::getPublicationName($TwitterRss->publication_id);
			return $EbooPublication;
		}
		else
		{
			return "Default";
		}	
	}
	/** 
	* Get the Publisher Image 
	*/

	public static function getPublicationImage($id)
	{
		$TwitterRss = TwitterRss::find($id);
		if(count($TwitterRss))
		{
			$EbooPublication = EbooPublication::getPublicationImage($TwitterRss->publication_id);
			return $EbooPublication;
		}
		else
		{
			return "images/logo.png";
		}	
	}
	/** 
	* Get the Publisher Id 
	*/

	public static function getPublicationId($id)
	{
		$TwitterRss = TwitterRss::find($id);
		if(count($TwitterRss))
		{
			return $TwitterRss->publication_id;
		}
		else
		{
			return "0";
		}	
	}
}
