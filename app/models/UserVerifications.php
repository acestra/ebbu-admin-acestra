<?php

class UserVerifications extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_verification';

	/**
	 * The database table removed timestamp from this model.
	 *
	 * @var string
	 */

	public  $timestamps = false;
	
	public static function checkUserVCode($user_id,$verification_code)
	{
        $today = date('Y-m-d H:i:s');
		$checkUserVerification = UserVerifications::where('user_id','=',$user_id)
                                                        ->where('verification_code','=',$verification_code)
                                                        ->where('expire_date','>',$today)
		                                                   ->first();
		 if($checkUserVerification)
		 {
		 	 return TRUE;
		 }else{
		 	 return FALSE;
		 }
	}
}
