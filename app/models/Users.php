<?php

class Users extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The database table removed timestamp from this model.
	 *
	 * @var string
	 */

	public  $timestamps = false;
	

	
	/** 
	* Get the Click Count 
	*/

	public static function getclickCount($id)
	{
		//return $id;
		$UserClickedCount = UserClickedData::where('u_id','=',$id)->get();
		if(count($UserClickedCount)>0)
		{
			return $UserClickedCount->count();
		}
		else
		{
			return 0;
		}
	}

	
	/** 
	* Get the Like Count 
	*/

	public static function getLikeCount($id)
	{
		//return $id;
		$UserClickedCount = UserClickedData::where('u_id','=',$id)->get();
		if(count($UserClickedCount)>0)
		{
			return $UserClickedCount->count();
		}
		else
		{
			return 0;
		}
	}

	/** 
	* Get the Rss Detail 
	*/

	public static function getFeedDetail($id)
	{
		//return $id;
		$EbbuFeeds = EbooFeeds::where('id','=',$id)->get();
		if($EbbuFeeds->count()==0)
		{
			$EbbuFeedsOld = EbooFeedsOld::where('id_old','=',$id)->get();
			return $EbbuFeedsOld;
		}
		else
		{
			return $EbbuFeeds;
		}
	}


	/** 
	* Get the Twitter Detail 
	*/

	public static function getTwitterDetail($id)
	{
		//return $id;
		$TwitterFeeds = TwitterFeeds::where('id','=',$id)->get();
		if(count($TwitterFeeds)==0)
		{
			$EbbuFeedsOld = TwitterFeedsOld::where('id_old','=',$id)->get();
			return $EbbuFeedsOld;
		}
		else
		{
			return $TwitterFeeds;
		}
	}
}
