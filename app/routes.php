<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(["before" => "auth"], function() {
	Route::any('/' , [
		"as" => "home",
		"uses" => 'HomeController@showWelcome'
	]);

	/* User Profile */ 

	Route::any("/profile", [
		"as" => "user.profile",
		"uses" => "UserController@profile"
	]);

	Route::any("/profile/edit", [
		"as" => "user.profileedit",
		"uses" => "UserController@profileEdit"
	]);
	
	Route::any('/source' , [
		"as" => "source",
		"uses" => 'SourceController@source'
	]);

	Route::any('/feedsync/syc' , [
		"as" => "feedsync.syc",
		"uses" => 'EbooFeedsController@feedsync'
	]);
	Route::any('/twittersync/twittersync' , [
		"as" => "twittersync.twittersync",
		"uses" => 'TwitterFeedsController@twitterfeedsync'
	]);
	/* User Logout */ 

	Route::get("/logout", [	
 		"as" => "user.logout",
 		"uses" => "UserController@logout"
 	]);

        //echo "<pre>";
	if (Auth::check()) {
		$USer = Auth::user();
		$EbooAdminRoles = EbooAdminRoles::find($USer->role_id);

		if($EbooAdminRoles->rolename=='admin')
		{

			Event::listen('admin.menu.create', function()
			{
				return array(
					'icon'  => 'glyphicon glyphicon-random',
					'title' => 'Feed',
					'event' => 'multi',
					'menu'  => array(
						'Feed RSS' => 'feedsync',
						'Twitter RSS' => 'twittersync',
						)
					);
			}, 2);
			Event::listen('admin.menu.create', function()
			{
				return array(
					'icon'  => 'glyphicon glyphicon-signal',
					'title' => 'User Experience',
					'event' => 'multi',
					'menu'  => array(
						'Trending' => 'trending',
						'Promoted Articles' => array(
							'View' => 'promote',
							'Add' => 'promote/create'
							),
						'Categories' => array(
							'View' => 'category_search',
							'Add' => 'category_search/create'
							)
						)
					);
			}, 3);
			Event::listen('admin.menu.create', function()
			{
				return array(
					'icon'  => 'glyphicon glyphicon-user',
					'title' => 'Manage User',
					'event' => 'multi',
					'menu'  => array(
						'Manage Admin' => 'admin',
						'Manage User' => 'users',
						'Feedback' => 'feedback'
						)
					);
			}, 1);
			Event::listen('admin.menu.create', function()
			{
				return array(
					'icon'  => 'glyphicon glyphicon-leaf',
					'title' => 'Source',
					'event' => 'multi',
					'menu'  => array(
						'RSS' => array(
							'View' => 'rss',
							'Add' => 'rss/create'
							),
						'Twitter RSS' => array(
							'View' => 'twitterrss',
							'Add' => 'twitterrss/create'
							),
						'Source' => array(
							'View' => 'sources',
							'Add' => 'sources/create'
							),
						'Category' => array(
							'View' => 'category',
							'Add' => 'category/create'
							)
						)
					);
			}, 4);

			Route::resource('admin', 'EbooAdminController');
			Route::resource('category', 'EbooCategoryController');
			Route::resource('rss', 'EbooRssController');
			Route::resource('twitterrss', 'TwitterRssController');
			Route::resource('sources', 'EbooPublicationController');
			Route::resource('feedsync', 'EbooFeedsController');
			Route::resource('twittersync', 'TwitterFeedsController');
			Route::resource('feedback', 'FeedbackController');
			Route::resource('users', 'UsersController');
			Route::resource('feedsyncold', 'FeedsOldController');
			Route::resource('twittersyncold', 'TwitterFeedsOldController');
			Route::resource('promote', 'PromoteController');
			Route::resource('category_search', 'CategorySearchController');
            Route::any('remove_more_in_edit', 'AjaxController@remove_more_in_edit');
            Route::any('source_link/{id}', 'SourceController@source_link');
            Route::any('user_ver', 'AjaxController@user_ver');
            
            
			
			Route::any("/trending", [
				"as" => "trending",
				"uses" => "TrendingController@trending"
			]);
			Route::any("/dailyFeed", [
				"as" => "dailyFeed",
				"uses" => "HomeController@dailyFeed"
			]);
			Route::any("/dailyTwitter", [
				"as" => "dailyTwitter",
				"uses" => "HomeController@dailyTwitter"
			]);
			Route::any("/dailyUserSearch", [
				"as" => "dailyUserSearch",
				"uses" => "HomeController@dailyUserSearch"
			]);
			Route::any("/dailyUser", [
				"as" => "dailyUser",
				"uses" => "HomeController@dailyUser"
			]);
			Route::any("/trending-ajax", [
				"as" => "trending-ajax",
				"uses" => "TrendingController@trendingAjax"
			]);
			Route::any("/twittersync-ajax", [
				"as" => "twittersync-ajax",
				"uses" => "TwitterFeedsController@twitterFeedsAjax"
			]);
			Route::any("/feedsync-ajax", [
				"as" => "feedsync-ajax",
				"uses" => "EbooFeedsController@ebbuFeedsAjax"
			]);

			Route::group(array('prefix' => 'ajax'), function()
			{
				Route::any("/feedsync-old", [
					"as" => "feedsync-old",
					"uses" => "AjaxController@ebbuFeedsOldAjax"
					]);
				Route::any("/twittersync-old", [
					"as" => "twittersync-old",
					"uses" => "AjaxController@twitterFeedsOldAjax"
					]);
			});

		}
		else if ( $EbooAdminRoles->rolename == 'moderator')
		{
			Event::listen('admin.menu.create', function()
			{
				return array(
					'icon'  => 'glyphicon glyphicon-signal',
					'title' => 'Trending',
					'event' => 'single',
					'menu'  => 'trending'
					);
			}, 3);

			Event::listen('admin.menu.create', function()
			{
				return array(
					'icon'  => 'glyphicon glyphicon-leaf',
					'title' => 'Source',
					'event' => 'multi',
					'menu'  => array(
						'RSS' => 'rss',
						'Source' => 'sources',
						'Category' => 'category'
						)
					);
			}, 4);
			//Route::resource('admin', 'EbooAdminController', [ 'except' => 'destroy','edit' ]);
			Route::resource('category', 'EbooCategoryController', [ 'only' => 'index' ]);
			Route::resource('rss', 'EbooRssController', [ 'only' => 'index' ]);
			Route::resource('sources', 'EbooPublicationController', [ 'only' => 'index' ]);
			Route::resource('feedsync', 'EbooFeedsController', [ 'only' => 'index' ]);
			Route::any("/trending", [
				"as" => "trending",
				"uses" => "TrendingController@trending"
			]);
		}
		else if ( $EbooAdminRoles->rolename == 'user')
		{
			Event::listen('admin.menu.create', function()
			{
				return array(
					'icon'  => 'glyphicon glyphicon-signal',
					'title' => 'Trending',
					'event' => 'single',
					'menu'  => 'trending'
					);
			}, 3);
			
			Route::resource('admin', 'EbooAdminController', [ 'only' => 'index' ]);
			Route::resource('category', 'EbooCategoryController', [ 'only' => 'index' ]);
			Route::resource('rss', 'EbooRssController', [ 'only' => 'index' ]);
			Route::resource('twitterrss', 'TwitterRssController', [ 'only' => 'index' ]);
			Route::resource('sources', 'EbooPublicationController', [ 'only' => 'index' ]);
			Route::resource('feedsync', 'EbooFeedsController', [ 'only' => 'index' ]);
			Route::resource('twittersync', 'TwitterFeedsController', [ 'only' => 'index' ]);

			Route::resource('feedback', 'FeedbackController', [ 'only' => 'index' ]);
			Route::resource('feedsyncold', 'FeedsOldController', [ 'only' => 'index' ]);
			Route::resource('twittersyncold', 'TwitterFeedsOldController', [ 'only' => 'index' ]);
			Route::resource('promote', 'PromoteController', [ 'only' => 'index' ]);
			Route::resource('category_search', 'CategorySearchController', [ 'only' => 'index' ]);
			Route::resource('users', 'UsersController', [ 'only' => 'index' ]);

			Route::any("/trending", [
				"as" => "trending",
				"uses" => "TrendingController@trending"
			]);
		}
	}
	else
	{
		Route::resource('admin', 'EbooAdminController', [ 'only' => 'index' ]);
		Route::resource('category', 'EbooCategoryController', [ 'only' => 'index' ]);
		Route::resource('rss', 'EbooRssController', [ 'only' => 'index' ]);
		Route::resource('twitterrss', 'TwitterRssController', [ 'only' => 'index' ]);
		Route::resource('sources', 'EbooPublicationController', [ 'only' => 'index' ]);
		Route::resource('feedsync', 'EbooFeedsController', [ 'only' => 'index' ]);
		Route::resource('twittersync', 'TwitterFeedsController', [ 'only' => 'index' ]);
		Route::resource('users', 'UsersController', [ 'only' => 'index' ]);

		Route::resource('feedback', 'FeedbackController', [ 'only' => 'index' ]);
		Route::resource('feedsyncold', 'FeedsOldController', [ 'only' => 'index' ]);
		Route::resource('twittersyncold', 'TwitterFeedsOldController', [ 'only' => 'index' ]);
		Route::resource('promote', 'PromoteController', [ 'only' => 'index' ]);
		Route::resource('category_search', 'CategorySearchController', [ 'only' => 'index' ]);

		Route::any("/trending", [
			"as" => "trending",
			"uses" => "TrendingController@trending"
			]);
		Route::any("/trending-ajax", [
			"as" => "trending-ajax",
			"uses" => "TrendingController@trendingAjax"
			]);
		Route::any("/twittersync-ajax", [
			"as" => "twittersync-ajax",
			"uses" => "TwitterFeedsController@twitterFeedsAjax"
			]);
		Route::any("/feedsync-ajax", [
			"as" => "feedsync-ajax",
			"uses" => "EbooFeedsController@ebbuFeedsAjax"
			]);
	}

});

Route::any("/login", [
	"as" => "user.login",
	"uses" => "UserController@login"
]);


Event::listen('admin.menu.settings.extend', function()
{
    return array(
        'Server' => '[url]',
        'This' => '[url]'
    );
});


Route::controller('password', 'RemindersController');

Route::group(array('prefix' => 'app'), function()
{

	Route::any("/userinsert", [
		"as" => "app.userinsert",
		"uses" => "AppController@userinsert"
		]);

	Route::any("/feedsearch", [
		"as" => "app.feedsearch",
		"uses" => "AppController@feedsearch"
		]);

	Route::any("/searchword", [
		"as" => "app.searchword",
		"uses" => "AppController@searchword"
		]);

	Route::any("/trending", [
		"as" => "app.trending",
		"uses" => "AppController@trending"
		]);
	Route::any("/clickcount", [
		"as" => "app.clickcount",
		"uses" => "AppController@clickcount"
		]);

	Route::any("/updatetime", [
		"as" => "app.updatetime",
		"uses" => "AppController@updatetime"
		]);

	Route::any("/feedback", [
		"as" => "app.feedback",
		"uses" => "AppController@feedback"
		]);
	
	Route::any("/sourceview", [
		"as" => "app.sourceview",
		"uses" => "AppController@sourceview"
		]);

	Route::any("/trendingsource", [
		"as" => "app.trendingsource",
		"uses" => "AppController@trendingsource"
		]);

	Route::any("/adminlists", [
		"as" => "app.adminlists",
		"uses" => "AppController@adminlists"
		]);
	Route::any("/singleadmin", [
		"as" => "app.singleadmin",
		"uses" => "AppController@singleadmin"
		]);

	Route::group(array('prefix' => 'v2'), function()
	{
		Route::any("/userinsert", [
			"as" => "app.userinsert",
			"uses" => "AppV2Controller@userinsert"
			]);
		Route::any("/user_verification", [
			"as" => "app.user_verification",
			"uses" => "AppV2Controller@user_verification"
			]);
        
		Route::any("/feedsearch", [
			"as" => "app.feedsearch",
			"uses" => "AppV2Controller@feedsearch"
			]);

		Route::any("/searchword", [
			"as" => "app.searchword",
			"uses" => "AppV2Controller@searchword"
			]);

		Route::any("/trending", [
			"as" => "app.trending",
			"uses" => "AppV2Controller@trending"
			]);

		Route::any("/promote_trending", [
			"as" => "app.promote_trending",
			"uses" => "AppV2Controller@promote_trending"
			]);

		Route::any("/clickcount", [
			"as" => "app.clickcount",
			"uses" => "AppV2Controller@clickcount"
			]);
		Route::any("/addLikeCount", [
			"as" => "app.addLikeCount",
			"uses" => "AppV2Controller@addLikeCount"
			]);
		Route::any("/addReadFeed", [
			"as" => "app.addReadFeed",
			"uses" => "AppV2Controller@addReadFeed"
			]);
		Route::any("/userReadList", [
			"as" => "app.userReadList",
			"uses" => "AppV2Controller@userReadList"
			]);
		Route::any("/get_category_search", [
			"as" => "app.get_category_search",
			"uses" => "AppV2Controller@get_category_search"
			]);
		Route::any("/sample_wight", [
			"as" => "app.sample_wight",
			"uses" => "AppV2Controller@sample_wight"
			]);

		

		Route::any("/updatetime", [
			"as" => "app.updatetime",
			"uses" => "AppV2Controller@updatetime"
			]);

		Route::any("/feedback", [
			"as" => "app.feedback",
			"uses" => "AppV2Controller@feedback"
			]);

		Route::any("/sourceview", [
			"as" => "app.sourceview",
			"uses" => "AppV2Controller@sourceview"
			]);

		Route::any("/trendingsource", [
			"as" => "app.trendingsource",
			"uses" => "AppV2Controller@trendingsource"
			]);

		Route::any("/adminlists", [
			"as" => "app.adminlists",
			"uses" => "AppV2Controller@adminlists"
			]);
		Route::any("/singleadmin", [
			"as" => "app.singleadmin",
			"uses" => "AppV2Controller@singleadmin"
			]);
	});

});


Route::get('cron-feed', function()
{
	if (php_sapi_name() == 'cli') {   
		if (isset($_SERVER['TERM'])) {   
			echo "The script was run from a manual invocation on a shell";   
		} else {   
			define('STDIN',fopen("php://stdin","r"));
			Artisan::call('ebbu:feedsync' , array('--quiet' => true));
		}   
	} else { 
		echo "The script was run from a webserver, or something else";   
	}
});

Route::get('cron-twitter', function()
{
	if (php_sapi_name() == 'cli') {   
		if (isset($_SERVER['TERM'])) {   
			echo "The script was run from a manual invocation on a shell";   
		} else {   
			define('STDIN',fopen("php://stdin","r"));
			Artisan::call('ebbu:twittersync' , array('--quiet' => true));
		}   
	} else { 
		echo "The script was run from a webserver, or something else";   
	}
});

Route::get('cron-feed-move', function()
{
	if (php_sapi_name() == 'cli') {   
		if (isset($_SERVER['TERM'])) {   
			echo "The script was run from a manual invocation on a shell";   
		} else {   
			define('STDIN',fopen("php://stdin","r"));
			Artisan::call('ebbu:feeddelete' , array('--quiet' => true));
		}   
	} else { 
		Artisan::call('ebbu:feeddelete' , array('--quiet' => true));
		echo "The script was run from a webserver, or something else";   
	}
});
