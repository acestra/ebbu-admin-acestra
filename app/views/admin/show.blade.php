@extends("layout.layout")
    @section("content")
<div class="container">

    <ul class="nav navbar-nav pull-right">
        <li><a class="btn btn-small btn-info " href="{{ URL::to('admin') }}">View All admin</a></li>
        <li><a class="btn btn-small btn-default " href="{{ URL::to('admin/create') }}">Create a Admin</a>
    </ul>

<h1>Showing {{ $admin->admin_user }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $admin->admin_user }}</h2>
        <p>
            <strong>Email:</strong> {{ $admin->email }}<br>
            <strong>Pin:</strong> {{ $admin->pin }}<br>
            <strong>Status:</strong> {{ $admin->status }}
        </p>
    </div>

</div>
@stop