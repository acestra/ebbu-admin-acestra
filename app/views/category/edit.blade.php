@extends("layout.layout")
@section("content")
<div class="row">
	<div class="col-lg-8 col-md-offset-2">
		<h1></h1>

		<!-- if there are creation errors, they will show here -->
		@if($errors->count()!=0)
		<div class="alert alert-danger" role="alert">
			{{ HTML::ul($errors->all()) }}
		</div>
		@endif

		{{ Form::model($category, array('route' => array('category.update', $category->id), 'method' => 'PUT' , 'class' => 'form-horizontal')) }}
		<fieldset>

			<!-- Form Name -->
			<legend>Edit Category "{{ $category->category_name }}"</legend>
			<div class="form-group">
				{{ Form::label('category_name', 'Category Name', array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('category_name', null, array('class' => 'form-control' , "placeholder" => "Category Name")) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('status', 'Status', array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('status', array('0' => 'Select a Status', '1' => 'Enable', '2' => 'Disable'), Input::old('status') , array('class' => 'form-control')) }}
				</div>
			</div>
			<div class="col-sm-offset-3 col-sm-9">
				<div class="">
					{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
					<a href="{{ URL::previous() }}" class="btn btn-default">Back</a>
				</div>
			</div>
		</fieldset>
		{{ Form::close() }}

	</div>
	@stop