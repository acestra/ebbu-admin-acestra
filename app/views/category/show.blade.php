@extends("layout.layout")
    @section("content")
    <div class="row">
        <div class="col-lg-12">
<h1>Showing {{ $category->category_name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $category->category_name }}</h2>
        <p>
            <strong>Email:</strong> {{ $category->category_name }}<br>
            <strong>Level:</strong> {{ $category->admin_level }}
        </p>
    </div>

</div>
@stop