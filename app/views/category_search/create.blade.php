@extends("layout.layout")
@section("content")
<div class="row">
	<div class="col-lg-8 col-md-offset-2">
		<h1></h1>

		<!-- if there are creation errors, they will show here -->
		@if($errors->count()!=0)
		<div class="alert alert-danger" role="alert">
			{{ HTML::ul($errors->all()) }}
		</div>
		@endif

		{{ Form::open(array('url' => 'category_search', 'class' => 'form-horizontal','files' => true)) }}
		<fieldset>

			<!-- Form Name -->
			<legend>Add Category</legend>
			<div class="form-group">
				{{ Form::label('Category Name', 'Main Category Name' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('name', Input::old('name'), array('class' => 'form-control' , "placeholder" => "")) }}
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('status', 'Status' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('status', array('0' => 'Select a Status', '1' => 'Enable', '2' => 'Disable'), Input::old('status') , array('class' => 'form-control')) }}
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" id="Search Term">Search Term </label>
				<div class="col-sm-9">
				     <input type="text" class="form-control" name="search_term_ajax[]"/>
				     
				</div>
				
			</div>
			<input type="hidden" class="cat_term_db" value=""/> 
			<div class="new_add"></div>
			<div class="form-group add_more">
				
				<div class="col-lg-offset-3 col-sm-9">
					{{ Form::button('Add More Search Term', array('class' => 'btn btn-success add_search_term')) }}
				</div>
				
			</div>
			

			<div class="col-sm-offset-3 col-sm-9">
				<div class="">
					{{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
					<a href="{{ URL::previous() }}" class="btn btn-default">Back</a>
				</div>
			</div>
		</fieldset>
		{{ Form::close() }}

	</div>
	
	@stop