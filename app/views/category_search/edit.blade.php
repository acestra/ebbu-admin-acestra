@extends("layout.layout")
@section("content")
<div class="row">
	<div class="col-lg-8 col-md-offset-2">
		<h1></h1>

		<!-- if there are creation errors, they will show here -->
		@if($errors->count()!=0)
		<div class="alert alert-danger" role="alert">
			{{ HTML::ul($errors->all()) }}
		</div>
		@endif
    
		{{ Form::model($category_search, array('route' => array('category_search.update', $category_search->id), 'method' => 'PUT' , 'class' => 'form-horizontal','files' => true)) }}
		<fieldset>

			<!-- Form Name -->
			<legend>Update a Category</legend>
			<div class="form-group">
				{{ Form::label('Main Category Name', 'Main Category Name' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('name', Input::old('name'), array('class' => 'form-control' , "placeholder" => "Main Category Name")) }}
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('status', 'Status' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('status', array('0' => 'Select a Status', '1' => 'Enable', '2' => 'Disable'), Input::old('status') , array('class' => 'form-control')) }}
				</div>
			</div>
			
			  <?php $i = 1; 
           $term_count = count($category_search_term);

			  ?>
			  <input type="hidden" class="cat_term_db" value="{{ $term_count }}"/> 
			  @foreach($category_search_term as $key => $value)
		              <div class="form-group" id="remove_more_edit_div_{{  $value->id }}" style="display:block">
		              	<label class="col-sm-3 control-label" id="Search Term">Search Term {{ $i }}</label>
		              <div class="col-sm-8">
		             
		              {{ Form::text('search_term[]', $value->name, array('class' => 'form-control')) }}
		              <input type="hidden" name="search_term_id[]" value="{{ $value->id }}" >
		              </div>
		                	<div class="col-sm-1"><a onClick='remove_more_ajax({{ $value->id}})' style="cursor:pointer">X</a></div>
		              </div>
		              <?php $i++;?>
			  @endforeach
			  <div class="new_add"></div>
			<div class="form-group add_more">
				
				<div class="col-lg-offset-3 col-sm-9">
					{{ Form::button('Add More Search Term', array('class' => 'btn btn-success add_search_term')) }}
				</div>
				
			</div>
			

			<div class="col-sm-offset-3 col-sm-9">
				<div class="">
					{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
					<a href="{{ URL::previous() }}" class="btn btn-default">Back</a>
				</div>
			</div>
		</fieldset>
		{{ Form::close() }}

	</div>
	<style type="text/css">
     #change_logo{
     	dicplay:none;
     }
	</style>
	@stop