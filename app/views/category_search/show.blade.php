@extends("layout.layout")
    @section("content")
    <div class="row">
        <div class="col-lg-12">
<h1>Showing {{ $category_search->name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $category_search->name }}</h2>
        <p>
            <strong>Name:</strong> {{ $category_search->name }}<br>
            <strong>Created at:</strong> {{ $category_search->created_at }}
        </p>
    </div>

</div>
@stop