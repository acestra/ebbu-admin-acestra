<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>{{ $detail }}</div><br><br>
		<div>Your <b>Verification Code is {{ $code}} </b></div><br><br>
		<div style="color:#15C;">Let us know what you think.</div><br><br>
		<div>Team Ebbu</div><br>
	</body>
</html>