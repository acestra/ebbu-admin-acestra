@extends("layout.error")
@section("content")
<div class="container">

	<div id="pricing" class="pricing-page">
		<div class="container">
			<h1 class="section_header-new text-center error_header">
				<hr class="left visible-desktop">
				403
				<hr class="right visible-desktop">
			</h1>
		</div>
	</div>

</div>
@stop