@extends("layout.layout")
    @section("content")

    <div class="row">
        <div class="col-lg-12">

          <h1>All Feedback</h1>
          <!-- will be used to show any messages -->
        @if (Session::has('message'))
          <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
        <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <td>User Id</td>
              <td>Feedback message</td>
              <td>Rating</td>
            </tr>
          </thead>
          <tbody>
            @foreach($feedback as $key => $value)
              <tr>
                <td>{{ $value->u_id }}</td>
                <td>{{ $value->feedback }}</td>
                <td>{{ $value->rating }}</td>
              </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <td>User Id</td>
              <td>Feedback message</td>
              <td>Rating</td>
            </tr>
          </tfoot>
      </table>
      </div>
    </div>
@stop