@extends("layout.layout")
    @section("content")

    <div class="row">
        <div class="col-lg-12">

          <ul class="nav navbar-nav pull-right">
            <!--li><a class="btn btn-small btn-info " href="{{ URL::to('feedsync') }}">View All Admin</a></li>
            <li><a class="btn btn-small btn-default " href="{{ URL::to('feedsync/create') }}">Create a Admin</a-->
            <li><a class="btn btn-small btn-danger " href="{{ URL::to('feedsync/syc') }}">Manual Feed Syc</a></li>
          </ul>
          <h1>All Feeds</h1>
          <!-- will be used to show any messages -->
        @if (Session::has('message'))
          <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
<div class="table-responsive">
        <table id="Rss-Feed" class="table table-striped table-bordered">
          <thead>
            <tr>
              <td>ID</td>
              <td>Category Name</td>
              <td>Source Name</td>
              <td>Article Title</td>
              <td>Click Count</td>
              <td>Publish Date</td>
              <td>Status</td>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <td>ID</td>
              <td>Category Name</td>
              <td>Source Name</td>
              <td>Article Title</td>
              <td>Click Count</td>
              <td>Publish Date</td>
              <td>Status</td>
            </tr>
          </tfoot>
      </table>
      </div>
    </div>
@stop