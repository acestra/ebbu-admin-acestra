@extends("layout.layout")
@section("content")
<div class="row">
	<div class="col-lg-8 col-md-offset-2">

		<h1></h1>

		<!-- if there are creation errors, they will show here -->
		@if($errors->count()!=0)
		<div class="alert alert-danger" role="alert">
			{{ HTML::ul($errors->all()) }}
		</div>
		@endif

		{{ Form::model($feeds, array('route' => array('feedsync.update', $feeds->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
		<fieldset>

			<!-- Form Name -->
			<legend>Edit Feed "{{ $feeds->article_title }}"</legend>
			<div class="form-group">
				{{ Form::label('rss_link_id', 'Rss Link ID' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('rss_link_id', null, array('class' => 'form-control' , "placeholder" => "Rss Link ID" , 'disabled' => 'disabled')) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('article_title', 'Article Title' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('article_title', null, array('class' => 'form-control' , "placeholder" => "Rss Link")) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('article_description', 'Article Description' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('article_description', null, array('class' => 'form-control' , "placeholder" => "Rss Link")) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('article_link', 'Article Link' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('article_link', null, array('class' => 'form-control' , "placeholder" => "Rss Link")) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('article_pubdate', 'Article Publish Date' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('article_pubdate', null, array('class' => 'form-control' , "placeholder" => "Rss Link")) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('status', 'Status' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('status', array('0' => 'Select a Status', '1' => 'Enable', '2' => 'Disable'), Input::old('status') , array('class' => 'form-control')) }}
				</div>
			</div>
			<div class="col-sm-offset-3 col-sm-9">
				<div class="">
					{{ Form::submit('Edit Feed!', array('class' => 'btn btn-primary')) }}
					<a href="{{ URL::previous() }}" class="btn btn-default">Back</a>
				</div>
			</div>
		</fieldset>
		{{ Form::close() }}
	</div>
	@stop