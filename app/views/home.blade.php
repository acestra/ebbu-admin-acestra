@extends("layout.layout")
@section("content")
<!-- Example row of columns -->

<div class="row">
	<div class="col-lg-12">
		<div class="row">
			<div class="demo-container col-lg-6 col-md-6">
				<div class="page-header col-lg-12 col-md-12">
					<h3>Monthly User Creation</h3>
				</div>
				<div id="placeholder_user" class="demo-placeholder col-lg-12 col-md-12" style="height:300px;"></div>
			</div>
			<div class="demo-container col-lg-6 col-md-6">
				<div class="page-header col-lg-12 col-md-12">
					<h3>Daily Searches</h3>
				</div>
				<div id="placeholder_usersearch" class="demo-placeholder col-lg-12 col-md-12" style="height:300px;"></div>
			</div>
			<div class="demo-container col-lg-6 col-md-6">
				<div class="page-header col-lg-12 col-md-12">
					<h3>Last 10 Days Imported Feed</h3>
				</div>
				<div id="placeholder_rss" class="demo-placeholder col-lg-12 col-md-12" style="height:300px;"></div>
			</div>
			<div class="demo-container col-lg-6 col-md-6">
				<div class="page-header col-lg-12 col-md-12">
					<h3>Last 10 Days Imported Twitter</h3>
				</div>
				<div id="placeholder_twitter" class="demo-placeholder col-lg-12 col-md-12" style="height:300px;"></div>
			</div>
		</div>
	</div>

	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<h2 class="text-center"> </h2>
				<div class="panel panel-success">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-users fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">{{ $EbooCategory }}</div>
								<div>Total Category!</div>
							</div>
						</div>
					</div>
					<a href="{{ URL::to('/category') }}">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<h2 class="text-center"> </h2>
				<div class="panel panel-info">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-clock-o fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">{{ $EbooPublication }}</div>
								<div>Total Source!</div>
							</div>
						</div>
					</div>
					<a href="{{ URL::to('/rss') }}">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<h2 class="text-center"> </h2>
				<div class="panel panel-warning">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-search fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">{{ $EbooRss }}</div>
								<div>Total Rss!</div>
							</div>
						</div>
					</div>
					<a href="{{ URL::to('/sources') }}">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-6 col-md-6">
				<h2 class="text-center"> </h2>
				<div class="panel panel-red">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-clock-o fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">{{ $TwitterRss }}</div>
								<div>Total Twitter User!</div>
							</div>
						</div>
					</div>
					<a href="{{ URL::to('/twitterrss') }}">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
		</div>          
	</div>	

	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-4 col-md-6">
				<h2 class="text-center"> </h2>
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-users fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">{{ $Users }}</div>
								<div>Total App Users!</div>
							</div>
						</div>
					</div>
					<a href="{{ URL::to('/trending') }}">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<h2 class="text-center"> </h2>
				<div class="panel panel-green">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-clock-o fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">{{ $total_usage }}</div>
								<div>Total App Usage!</div>
							</div>
						</div>
					</div>
					<a href="{{ URL::to('/trending') }}">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<h2 class="text-center"> </h2>
				<div class="panel panel-yellow">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-search fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">{{ $total_search_word }}</div>
								<div>Total User Search word !</div>
							</div>
						</div>
					</div>
					<a href="{{ URL::to('/trending') }}">
						<div class="panel-footer">
							<span class="pull-left">View Details</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
		</div>          
	</div>	

</div>
@stop
@section("script")

{{ HTML::script('jav/jquery.flot.js'); }}
{{ HTML::script('jav/jquery.flot.time.js'); }}
<script type="text/javascript">

	$( document ).ready(function() {
		var sin = [], cos = [];
		$.ajax({                                      
			url: 'dailyFeed', 
			data: "",                

			dataType: 'json',        
			success: function(data)  
			{
				var today = new Date();
				var previousweek = new Date(today.getTime() - 11 * 24 * 60 * 60 * 1000);
				for (var i = 0; i < data['rss'].length; i++) {
					sin.push([data['rss'][i][0], data['rss'][i][1]]);
				}

				$.plot($("#placeholder_rss"),
					[
					{
						data: sin,
						label: "Rss Feed",
						points: {
							fillColor: "#4572A7",
							size: 35
						},
						color: '#4572A7'
					}
					], {
						series: {
							lines: {
								show: true
							},
							points: {
								show: true
							}
						},
						grid: {
							hoverable: true,
							clickable: true
						},
						yaxis: {
							//min: -1.2,
							//max: 1.2
						},
						xaxis: {
							mode: "time",
							minTickSize: [1, "day"],
							min: previousweek.getTime(),
							max: new Date()
						}
					});
			} 
		});

		var twitter = [];
		$.ajax({                                      
			url: 'dailyTwitter', 
			data: "",                

			dataType: 'json',        
			success: function(data)  
			{
				var today = new Date();
				var previousweek = new Date(today.getTime() - 11 * 24 * 60 * 60 * 1000);
				for (var i = 0; i < data['twitter'].length; i++) {
					twitter.push([data['twitter'][i][0], data['twitter'][i][1]]);
				}

				$.plot($("#placeholder_twitter"),
					[
					{
						data: twitter,
						label: "Twitter Feed",
						points: {
							fillColor: "#333",
							size: 35
						},
						color: '#AA4643'
					}
					], {
						series: {
							lines: {
								show: true
							},
							points: {
								show: true
							}
						},
						grid: {
							hoverable: true,
							clickable: true
						},
						yaxis: {
							//min: -1.2,
							//max: 1.2
						},
						xaxis: {
							mode: "time",
							minTickSize: [1, "day"],
							min: previousweek.getTime(),
							max: new Date()
						}
					});
			} 
		});
		var user = [];
		$.ajax({                                      
			url: 'dailyUser', 
			data: "",                

			dataType: 'json',        
			success: function(data)  
			{
				for (var i = 0; i < data['user'].length; i++) {
					user.push([data['user'][i][0], data['user'][i][1]]);
				}

				$.plot($("#placeholder_user"),
					[
					{
						data: user,
						label: "Monthly User",
						points: {
							fillColor: "#333",
							size: 5
						},
						color: '#AA4643'
					}
					], {
						series: {
							lines: {
								show: true
							},
							points: {
								show: true
							}
						},
						grid: {
							hoverable: true,
							clickable: true
						},
						yaxis: {
							//min: -1.2,
							//max: 1.2
						},
						xaxis: {
							mode: "time",
							minTickSize: [1, "month"],
							min: (new Date(2014, 11, 1)).getTime(),
							max: new Date()
						}
					});
			} 
		});

		var usersearch = [];
		$.ajax({                                      
			url: 'dailyUserSearch', 
			data: "",                

			dataType: 'json',        
			success: function(data)  
			{
				for (var i = 0; i < data['usersearch'].length; i++) {
					usersearch.push([data['usersearch'][i][0], data['usersearch'][i][1]]);
				}

				$.plot($("#placeholder_usersearch"),
					[
					{
						data: usersearch,
						label: "Daily User search",
						points: {
							fillColor: "#4572A7",
							size: 5
						},
						color: '#4572A7'
					}
					], {
						series: {
							lines: {
								show: true
							},
							points: {
								show: true
							}
						},
						grid: {
							hoverable: true,
							clickable: true
						},
						yaxis: {
							//min: -1.2,
							//max: 1.2
						},
						xaxis: {
							mode: "time",
							minTickSize: [1, "month"],
							min: (new Date(2014, 11, 1)).getTime(),
							max: new Date()
						}
					});
			} 
		});

		function showTooltip(x, y, contents) {
			$('<div id="tooltip">' + contents + '</div>').css({
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x + 5,
				border: '1px solid #fdd',
				padding: '2px',
				'background-color': '#000',
				color: '#fff'
			}).appendTo("body").fadeIn(200);
		}

		var previousPoint = null;
		$("#placeholder_usersearch").bind("plothover", function(event, pos, item) {
			$("#x").text(pos.x.toFixed(2));
			$("#y").text(pos.y.toFixed(2));

			if (item) {
				if (previousPoint !== item.dataIndex) {
					previousPoint = item.dataIndex;

					$("#tooltip").remove();
					var x = item.datapoint[0].toFixed(2),
					y = item.datapoint[1].toFixed(2);

					var timestamp = parseInt(x.replace(/\.00$/,''));
					var pubDate = js_yyyy_mm_dd_hh_mm_ss(timestamp);

					showTooltip(item.pageX, item.pageY,
						item.series.label + " of " + pubDate + " = " + y);
				}
			}
			else {
				$("#tooltip").remove();
				previousPoint = null;
			}
		});
		$("#placeholder_rss").bind("plothover", function(event, pos, item) {
			$("#x").text(pos.x.toFixed(2));
			$("#y").text(pos.y.toFixed(2));

			if (item) {
				if (previousPoint !== item.dataIndex) {
					previousPoint = item.dataIndex;

					$("#tooltip").remove();
					var x = item.datapoint[0].toFixed(2),
					y = item.datapoint[1].toFixed(2);

					var timestamp = parseInt(x.replace(/\.00$/,''));
					var pubDate = js_yyyy_mm_dd_hh_mm_ss(timestamp);

					showTooltip(item.pageX, item.pageY,
						item.series.label + " of " + pubDate + " = " + y);
				}
			}
			else {
				$("#tooltip").remove();
				previousPoint = null;
			}
		});
		$("#placeholder_twitter").bind("plothover", function(event, pos, item) {
			$("#x").text(pos.x.toFixed(2));
			$("#y").text(pos.y.toFixed(2));

			if (item) {
				if (previousPoint !== item.dataIndex) {
					previousPoint = item.dataIndex;

					$("#tooltip").remove();
					var x = item.datapoint[0].toFixed(2),
					y = item.datapoint[1].toFixed(2);

					var timestamp = parseInt(x.replace(/\.00$/,''));
					var pubDate = js_yyyy_mm_dd_hh_mm_ss(timestamp);

					showTooltip(item.pageX, item.pageY,
						item.series.label + " of " + pubDate + " = " + y);
				}
			}
			else {
				$("#tooltip").remove();
				previousPoint = null;
			}
		});
		$("#placeholder_user").bind("plothover", function(event, pos, item) {
			$("#x").text(pos.x.toFixed(2));
			$("#y").text(pos.y.toFixed(2));

			if (item) {
				if (previousPoint !== item.dataIndex) {
					previousPoint = item.dataIndex;

					$("#tooltip").remove();
					var x = item.datapoint[0].toFixed(2),
					y = item.datapoint[1].toFixed(2);

					var timestamp = parseInt(x.replace(/\.00$/,''));
					var pubDate = js_yyyy_mm_dd_hh_mm_ss(timestamp);

					showTooltip(item.pageX, item.pageY,
						item.series.label + " of " + pubDate + " = " + y);
				}
			}
			else {
				$("#tooltip").remove();
				previousPoint = null;
			}
		});

		function js_yyyy_mm_dd_hh_mm_ss (timestamp) {
			var now = new Date(timestamp);
			year = "" + now.getFullYear();
			month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
			day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
			hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
			minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
			second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
  			return year + "-" + month + "-" + day ;//+ " " + hour + ":" + minute + ":" + second;
		}
	});
</script>
@stop