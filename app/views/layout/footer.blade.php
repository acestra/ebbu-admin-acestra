@section("footer")
<hr>
<footer>
	<p>&copy; {{ date("Y"); }}</p>
</footer>


{{ HTML::script('jav/vendor/jquery-1.10.1.min.js') }}
{{ HTML::script('jav/jquery-ui.js') }}

<!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="jav/vendor/jquery-1.10.1.min.js"><\/script>')</script-->

@yield('script')

{{ HTML::script('jav/vendor/bootstrap.min.js'); }}
{{ HTML::script('jav/jquery.dataTables.min.js'); }}
{{ HTML::script('jav/main.js'); }}
@show