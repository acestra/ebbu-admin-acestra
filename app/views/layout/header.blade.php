@section("header")
<?php $menu = Event::fire('admin.menu.create');  ?>
<div class="container">
  <div class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand eboo-nav" href="{{ URL::to('/') }}"><img src="{{ URL::to('/') }}/images/ebbu-logo.png" alt="ebbu"/></a>
    </div>
    <div class="collapse navbar-collapse eboo-nav">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user "></i> Manage Account <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{ URL::to('/profile') }}"> Account <i class="glyphicon glyphicon-cog pull-right icon-profile"></i></a></li>
            @if (Auth::check())
            <li><a href="{{ URL::route("user.logout") }}"> Logout <i class="glyphicon glyphicon-log-out pull-right icon-profile"></i></a></li>
            @else
            <li><a href="{{ URL::route("user.login") }}"><i class="glyphicon glyphicon-log-in pull-right icon-profile"></i> Login User</a></li>
            @endif
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav ">
        <li><a href="{{ URL::to('/') }}"><i class="glyphicon glyphicon-home"></i> Home</a></li>
        @foreach ($menu as $item)
        <li>
          @if($item['event']=='multi') 
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="{{ $item['icon'] }}"></i> {{ $item['title'] }} <b class="caret"></b></a> 
          {{ 
          $extended = current(Event::fire($item['event']));
          if ($extended) { $item['menu'] = array_merge($item['menu'], $extended); }
        }}
        <ul class="dropdown-menu multi-level">
          @foreach ($item['menu'] as $key => $value)
          @if(is_array($value))
          <li class="dropdown-submenu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$key}}</a>
            <ul class="dropdown-menu">
              @foreach ($value as $keys=>$items)
              <li><a href="{{ URL::to('/') }}/{{ $items }}">{{ $keys }}</a></li>
              @endforeach
            </ul>
          </li>
          @else
          <li><a href="{{ URL::to('/') }}/{{ $value }}">{{ $key }}</a></li>
          @endif
          @endforeach
        </ul>
        @elseif($item['event']=='single') 
        <a href="{{ URL::to('/') }}/{{ $item['menu'] }}"><i class="{{ $item['icon'] }}"></i> {{ $item['title'] }}</a>
        @endif 
      </li>
      @endforeach


              <!--li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-leaf"></i> Source <b class="caret"></b></a>
                <ul class="dropdown-menu multi-level">
                  <li class="dropdown-submenu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">RSS</a>
                    <ul class="dropdown-menu">
                      <li><a href="{{ URL::to("rss") }}">View</a></li>
                      <li><a href="{{ URL::to("rss/create") }}">Add</a></li>
                    </ul>
                  </li>
                  <li class="divider"></li>

                  <li class="dropdown-submenu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Source</a>
                    <ul class="dropdown-menu">
                      <li><a href="{{ URL::to("sources") }}">View</a></li>
                      <li><a href="{{ URL::to("sources/create") }}">Add</a></li>
                    </ul>
                  </li>
                  <li class="divider"></li>

                  <li class="dropdown-submenu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Category</a>
                    <ul class="dropdown-menu">
                      <li><a href="{{ URL::to("category") }}">View</a></li>
                      <li><a href="{{ URL::to("category/create") }}">Add</a></li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li><a href="#contact"><i class="glyphicon glyphicon-signal"></i> Trending</a></li>
              <li id="feedsync" ><a href="{{ URL::to("feedsync") }}"><i class="glyphicon glyphicon-random"></i> Feed Sync</a></li>
              <li><a href="{{ URL::to("admin") }}"><i class="glyphicon glyphicon-user"></i> Manage Admin</a></li-->

            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
      <div class="separator"></div>


@show