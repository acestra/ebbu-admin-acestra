<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
		<meta charset="UTF-8" />
        <title>@yield('title','Ebbu')</title>
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
  		<meta name="description" content="@yield('description','Amazing cupcake wrappers, printable goods')">
  		<meta name="keywords" content="@yield('keywords','printable, cupcake wrappers, party supplies')">
		{{ HTML::style('styles/bootstrap.min.css') }}
		{{ HTML::style('styles/bootstrap-theme.min.css') }}
		{{ HTML::style('styles/jquery.dataTables.min.css')}}
		{{ HTML::style('font-awesome-4.1.0/css/font-awesome.min.css')}}
		{{ HTML::style('styles/jquery-ui.css')}}
		<link href='http://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
		{{ HTML::style('styles/main.css') }}
		<link href='<?php echo url(); ?>/jav/dataTables.tableTools.css' rel='stylesheet' type='text/css'><!-- use for csv -->
	</head>
	<body>
	    <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
    	<div class="container main-feed">
        	<div class="row">
            	<div class="col-sm-12 col-md-12 col-lg-12">     
					@include("layout.header")
						<div id="container">
							@yield("content")
						</div>
					@include("layout.footer")
					@yield("footercontent")
				</div>
			</div>
		</div>
		<script type="text/javascript" src="<?php echo url(); ?>/jav/dataTables.tableTools.js"></script><!--  use for csv-->
	</body>
</html>