      <!-- Static navbar -->
      <div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse-submenu">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div class="navbar-collapse collapse collapse-submenu">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">RSS <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="{{ URL::to("rss") }}">View</a></li>
                  <li><a href="{{ URL::to("rss/create") }}">Add</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Source <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="{{ URL::to("sources") }}">View</a></li>
                  <li><a href="{{ URL::to("sources/create") }}">Add</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Category <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="{{ URL::to("category") }}">View</a></li>
                  <li><a href="{{ URL::to("category/create") }}">Add</a></li>
                </ul>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </div>