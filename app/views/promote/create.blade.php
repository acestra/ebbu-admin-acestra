@extends("layout.layout")
@section("content")
<div class="row">
	<div class="col-lg-8 col-md-offset-2">
		<h1></h1>

		<!-- if there are creation errors, they will show here -->
		@if($errors->count()!=0)
		<div class="alert alert-danger" role="alert">
			{{ HTML::ul($errors->all()) }}
		</div>
		@endif

		{{ Form::open(array('url' => 'promote', 'class' => 'form-horizontal','files' => true)) }}
		<fieldset>

			<!-- Form Name -->
			<legend>Create a Promote</legend>
			<div class="form-group">
				{{ Form::label('Name', 'Source Name' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('name', Input::old('name'), array('class' => 'form-control' , "placeholder" => "")) }}
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('weight', 'Article Weight' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('weight', Input::old('weight'), array('class' => 'form-control' , "placeholder" => "")) }}
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('Logo', 'Logo' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::file('logo','',array('id'=>'','class'=>'form-control')) }}
					<br/>
					<span style='color:red'>Image size Should Maximum 160 * 160 only</span>

				</div>
			</div>
			<div class="form-group">
				{{ Form::label('Name', 'Article Headline' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('headline', Input::old('headline'), array('class' => 'form-control' , "placeholder" => "")) }}
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('Name', 'Article URL' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('url', Input::old('url'), array('class' => 'form-control' , "placeholder" => "")) }}
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('Name', 'Expiry date ' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('expiry_date', '', array('class' => 'form-control promote_expire' ,'id'=>'promote_expire','autocomplete'=>'off')) }}
				
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('list', 'Promote In' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('list', array('0' => '', '1' => 'The Frontpage', '2' => 'Reading List'), Input::old('list') , array('class' => 'form-control')) }}
				</div>
			</div>
			
			<div class="form-group">
				{{ Form::label('status', 'Status' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('status', array('0' => '', '1' => 'Enable', '2' => 'Disable'), Input::old('status') , array('class' => 'form-control')) }}
				</div>
			</div>
			<div class="col-sm-offset-3 col-sm-9">
				<div class="">
					{{ Form::submit('Promote', array('class' => 'btn btn-primary')) }}
					<a href="{{ URL::previous() }}" class="btn btn-default">Back</a>
				</div>
			</div>
		</fieldset>
		{{ Form::close() }}

	</div>
@stop