@extends("layout.layout")
@section("content")
<div class="row">
	<div class="col-lg-8 col-md-offset-2">
		<h1></h1>

		<!-- if there are creation errors, they will show here -->
		@if($errors->count()!=0)
		<div class="alert alert-danger" role="alert">
			{{ HTML::ul($errors->all()) }}
		</div>
		@endif

		{{ Form::model($promote, array('route' => array('promote.update', $promote->id), 'method' => 'PUT' , 'class' => 'form-horizontal','files' => true)) }}
		<fieldset>

			<!-- Form Name -->
			<legend>Edit Promote "{{ $promote->name }}"</legend>
			<div class="form-group">
				{{ Form::label('name', 'Promote Name', array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('name', null, array('class' => 'form-control' , "placeholder" => "Category Name")) }}
				</div>
			</div>
             <div class="form-group">
				{{ Form::label('weight', 'weight' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('weight', null, array('class' => 'form-control' , "placeholder" => "Article Headline")) }}
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('Logo', 'Logo' , array('class' => 'col-sm-3 control-label')) }}
               
				<div class="col-sm-5" id='change_logo'>
					{{ Form::file('logo','',array('id'=>'','class'=>'form-control')) }}

				</div>
                 <div class="col-sm-5" id='change_logo_buttton_div'>
					<button class="btb btn-danger" type='button' id='change_logo_buttton'>Change Logo</button>

				</div>
				 
				<div class="col-sm-4" id="image">
				<?php  $path = "promote_logo/";
				      $image = $path.$promote->logo; 
				   ?>
					{{ HTML::image($image, $promote->logo, array('width' => '90%', 'height' => '50%')) }}
				
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('Name', 'Article Headline' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('headline', null, array('class' => 'form-control' , "placeholder" => "Article Headline", 'minlength' => '20')) }}
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('Name', 'Article URL' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('url', Input::old('url'), array('class' => 'form-control' , "placeholder" => "Article URL")) }}
				</div>
			</div>
			<div class="form-group">
				{{ Form::label('Name', 'Expiry date ' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('expiry_date', Input::old('expiry_date'), array('class' => 'form-control promote_expire' ,'id'=>'promote_expire')) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('list', 'Promot To' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('list', array('0' => 'Select a List', '1' => 'The Frontpage', '2' => 'Reading List'), Input::old('list') , array('class' => 'form-control')) }}
				</div>
			</div>
			
			<div class="form-group">
				{{ Form::label('status', 'Status', array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('status', array('0' => 'Select a Status', '1' => 'Enable', '2' => 'Disable'), Input::old('status') , array('class' => 'form-control')) }}
				</div>
			</div>

			<div class="col-sm-offset-3 col-sm-9">
				<div class="">
					{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
					<a href="{{ URL::previous() }}" class="btn btn-default">Back</a>
				</div>
			</div>
		</fieldset>
		{{ Form::close() }}

	</div>
	<style type="text/css">
     #change_logo{
     	dicplay:none;
     }
	</style>
	@stop