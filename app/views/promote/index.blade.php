@extends("layout.layout")
    @section("content")
<link href='<?php echo url(); ?>/jav/dataTables.tableTools.css' rel='stylesheet' type='text/css'><!-- use for csv -->
    <div class="row">
        <div class="col-lg-12">
<h1>All Promote</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<div class="table-responsive">
  <a class="btn btn-small btn-info " href="{{ URL::to('promote/create') }}">Add</a>
    <table id="example" class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Impressions</td>
                <td>Read Count</td>
                <td>Like Count</td>
                <td>Expire On</td>
                <td>Promote To</td>
                <td>Status</td>
                <td class="action_table">Actions</td>
            </tr>
        </thead>
        <tbody>
            @foreach($promote as $key => $value)
            <tr>
                <td><a href="{{ URL::to('promote/' . $value->id) }}">{{ $value->id }}</a></td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->impressions }}</td>
                <td>{{ $value->readcount }}</td>
                <td>{{ $value->likecount }}</td>
                <td>{{ $value->expiry_date }}</td>
                <td>
                    @if( $value->list ==1) 
                    The Frontpage
                    @else
                    Reading List
                    @endif
                </td>
                <td>
                    @if( $value->status ==1) 
                    Enable
                    @else
                    Disable
                    @endif
                </td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>

                    <!-- delete the nerd (uses the destroy method DESTROY /category/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                    {{ Form::open(array('url' => 'promote/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{Form::button('<i class="glyphicon glyphicon-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-warning'))}}
                    {{ Form::close() }}

                    <!-- show the nerd (uses the show method found at GET /category/{id} -->
                    <!--a class="btn btn-small btn-success " href="{{ URL::to('category/' . $value->id) }}">Show</a-->

                    <!-- edit this nerd (uses the edit method found at GET /category/{id}/edit -->
                    <a class="btn btn-small btn-info " href="{{ URL::to('promote/' . $value->id . '/edit') }}"><i class="glyphicon glyphicon-edit"></i></a>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

</div>

@stop