@extends("layout.layout")
    @section("content")
    <div class="row">
        <div class="col-lg-12">
            <h1>Showing {{ $promote->name }}</h1>

            <div class="jumbotron text-center">
                <h2>{{ $promote->name }}</h2>
            </div>
            <p>
                <strong>Name:</strong> {{ $promote->name }}<br>
                <strong>Logo:</strong> {{ $promote->logo }}
                <strong>Headline:</strong> {{ $promote->headline }}<br>
                <strong>Url:</strong> {{ $promote->url }}
                <strong>Expiry Date:</strong> {{ $promote->expiry_date }}<br>
                <strong>List:</strong> {{ $promote->list }}
                <strong>Weight:</strong> {{ $promote->weight }}<br>
                <strong>Impressions Count:</strong> {{ $promote->impressions }}
                <strong>Click Count:</strong> {{ $promote->clickcount }}<br>
                <strong>Like Count:</strong> {{ $promote->likecount }}
                <strong>Read Count:</strong> {{ $promote->readcount }}
            </p>
            <table id="example" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td>User Name</td>
                        <td>Email</td>
                        <td>Source</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($UserReadDataFind as $key => $value)
                        <tr>
                            <td>{{ $value->username }}</a></td>
                            <td><a href="{{ URL::to('users/' . $value->u_id) }}">{{ $value->u_id }}</a></td>
                            <td>{{ $value->source }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop