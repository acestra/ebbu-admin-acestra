@extends("layout.layout")
@section("content")
<div class="row">
	<div class="col-lg-8 col-md-offset-2">

		<h1></h1>

		<!-- if there are creation errors, they will show here -->

		@if($errors->count()!=0)
		<div class="alert alert-danger" role="alert">
			{{ HTML::ul($errors->all()) }}
		</div>
		@endif

		{{ Form::model($rss, array('route' => array('rss.update', $rss->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
		<fieldset>

			<!-- Form Name -->
			<legend>Edit Rss "{{ $rss->rss_link }}"</legend>
			<div class="form-group">
				{{ Form::label('category_id', 'Category Name' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('category_id', $category , Input::old('category_id'), array('class' => 'form-control')) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('publication_id', 'Source Name' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('publication_id', $source , Input::old('publication_id'), array('class' => 'form-control')) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('rss_link', 'Rss Link' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('rss_link', null, array('class' => 'form-control' , "placeholder" => "Rss Link")) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('status', 'Status' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('status', array('0' => 'Select a Status', '1' => 'Enable', '2' => 'Disable'), Input::old('status') , array('class' => 'form-control')) }}
				</div>
			</div>
			<div class="col-sm-offset-3 col-sm-9">
				<div class="">
					{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				</div>
			</div>
		</fieldset>
		{{ Form::close() }}
	</div>
	@stop