@extends("layout.layout")
    @section("content")

    <div class="row">
        <div class="col-lg-12">
          <h1>All rss</h1>

          <!-- will be used to show any messages -->
        @if (Session::has('message'))
          <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
<div class="table-responsive">
        <table id="example" class="table table-striped table-bordered">
          <thead>
            <tr>
              <td>ID</td>
              <td>Category Name</td>
              <td>Publication Name</td>
              <td>Rss Link</td>
              <td>Updated At</td>
              <td>Status</td>
              <td class="action_table">Actions</td>
            </tr>
          </thead>
          <tbody>
          @foreach($rss as $key => $value)
            <tr>
              <td>{{ $value->id }}</td>
              <td>{{ EbooCategory::getCategoryName($value->category_id) }}</td>
              <td>{{ EbooPublication::getPublicationName($value->publication_id) }}</td>
              <td>{{ $value->rss_link }}</td>
              <td>{{ $value->session_date }}</td>
            <td>
                @if( $value->status ==1) 
                    Enable
                @else
                    Disable
                @endif
            </td>

              <!-- we will also add show, edit, and delete buttons -->
              <td>

                <!-- delete the nerd (uses the destroy method DESTROY /rss/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->
                {{ Form::open(array('url' => 'rss/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{Form::button('<i class="glyphicon glyphicon-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-warning'))}}
                {{ Form::close() }}

                <!-- show the nerd (uses the show method found at GET /rss/{id} -->
                <!--a class="btn btn-small btn-success" href="{{ URL::to('rss/' . $value->id) }}">Show</a-->

                <!-- edit this nerd (uses the edit method found at GET /rss/{id}/edit -->
                <a class="btn btn-small btn-info " href="{{ URL::to('rss/' . $value->id . '/edit') }}"><i class="glyphicon glyphicon-edit"></i></a>

              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
      </div>
    </div>
@stop
