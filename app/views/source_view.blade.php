@extends("layout.layout")
    @section("content")

 <section class="content-wrapper content-midwet">
                <div class="content-header">
                   
                    
                      <div class="col-lg-3">
                      	<img src="{{ url()}}/{{ $source->publication_image }}" class="img-rounded" />
                      </div>
                      <div class="col-lg-9">
                      	  <h1 class="content-title">{{ $source->publication_name }}</h1>
                      </div>
                </div><!-- /.content-header -->

   <div class="container">
        <div class="row">
<ul id="myTab" class="nav nav-tabs">
   <li class="active">
      <a href="#home" data-toggle="tab">
         RSS Feed
      </a>
   </li>
   <li><a href="#ios" data-toggle="tab">Tweet Feed</a></li>
   
</ul>

<div id="myTabContent" class="tab-content">
   <div class="tab-pane fade in active" id="home">
     <div class="table-responsive">
	    <table id="example" class="table table-striped table-bordered">
	        <thead>
	            <tr>
	                <td>Rss ID</td>
	                <td>Category</td>
	                
	                <td>Article Title</td>
	                <td>Article Link</td>
	                <td>Published Date</td>
	                <td>Click Count</td>
	                
	            </tr>
	        </thead>
	        <tbody>
	          
	       
	         @foreach($Rssfeeds as $key => $value)
            <tr>
                 <td>{{ $value->id }}</td>
                 <td><?php echo $CategorySearch = EbooCategory::getCategoryName($value->category_id);?></td>
                 <td>{{ $value->article_title }}</td>
                 <td><a href="{{ $value->article_link }}" target="_blank"> {{ $value->article_link }}</a></td>
                 <td>{{ $value->article_pubdate }}</td>
                 
                  <td>{{ $value->clickcount }}</td>
                
            </tr>
             @endforeach
              </tbody>
	    </table>
  	 </div>
   </div>
   <div class="tab-pane fade" id="ios">
       <div class="table-responsive">
	    <table id="example1" class="table table-striped table-bordered">
	        <thead>
	            <tr>
	                <td>Rss ID</td>
	                <td>Article Title</td>
	                <td>Article Link</td>
	                <td>Published Date</td>
	                <td>Click Count</td>
	            </tr>
	        </thead>
	        <tbody>
	          
	       
	         @foreach($TweetFeeds as $key => $value)
            <tr>
                 <td>{{ $value->id }}</td>
                 <td>{{ $value->tweet_text }}</td>
                 <td><a href="{{ $value->article_link }}" target="_blank"> {{ $value->	article_link }}</a></td>
                 <td>{{ $value->created_date }}</td>
                 
                  <td>{{ $value->click_count }}</td>
                
            </tr>
             @endforeach
              </tbody>
	    </table>
   </div>
   </div>
  
  
</div>
        </div>
   </div>
  </section>              
@stop