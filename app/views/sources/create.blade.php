@extends("layout.layout")
@section("content")
<div class="row">
	<div class="col-lg-8 col-md-offset-2">
		<h1></h1>

		<!-- if there are creation errors, they will show here -->
		@if($errors->count()!=0)
		<div class="alert alert-danger" role="alert">
			{{ HTML::ul($errors->all()) }}
		</div>
		@endif

		{{ Form::open(array('url' => 'sources','files'=>true, 'class' => 'form-horizontal')) }}
		<fieldset>

			<!-- Form Name -->
			<legend>Create a Source</legend>
			<div class="form-group">
				{{ Form::label('publication_name', 'Source Name' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('publication_name', Input::old('publication_name'), array('class' => 'form-control' , "placeholder" => "Source Name")) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('publication_image','Source Image' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::file('publication_image','',array('id'=>'','class'=>'btn btn-primary')) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('status', 'Status' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('status', array('0' => 'Select a Status', '1' => 'Enable', '2' => 'Disable'), Input::old('status') , array('class' => 'form-control')) }}
				</div>
			</div>
			<div class="col-sm-offset-3 col-sm-9">
				<div class="">			
					{{ Form::submit('Create Source!', array('class' => 'btn btn-primary')) }}
					<a href="{{ URL::previous() }}" class="btn btn-default">Back</a>
				</div>
			</div>
		</fieldset>
		{{ Form::close() }}

	</div>
	@stop