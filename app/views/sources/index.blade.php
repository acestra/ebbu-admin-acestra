@extends("layout.layout")
    @section("content")

    <div class="row">
        <div class="col-lg-12">
<h1>All Sources</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@if (Session::has('error'))
    <div class="alert alert-warning">{{ Session::get('error') }}</div>
@endif
<div class="table-responsive">
    <table id="example" class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>sources Name</td>
                <td>sources Image</td>
                <td>Updated At</td>
                <td>Status</td>
                <td class="action_table">Actions</td>
            </tr>
        </thead>
        <tbody>
            @foreach($sources as $key => $value)
            <tr>
                <td>{{ $value->id }}</td>
                
                <td> <a href="{{ URL::to('source_link/' . $value->id) }}"> {{ $value->publication_name }}</a></td>
                <td><img src="{{ URL::to('/') }}/{{ $value->publication_image }}" width="75"/></td>
                <td>{{ $value->session_date }}</td>
                <td>
                    @if( $value->status ==1) 
                    Enable
                    @else
                    Disable
                    @endif
                </td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>

                    <!-- delete the nerd (uses the destroy method DESTROY /sources/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                    {{ Form::open(array('url' => 'sources/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{Form::button('<i class="glyphicon glyphicon-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-warning'))}}
                    {{ Form::close() }}

                    <!-- show the nerd (uses the show method found at GET /sources/{id} -->
                    <!--a class="btn btn-small btn-success" href="{{ URL::to('sources/' . $value->id) }}">Show</a-->

                    <!-- edit this nerd (uses the edit method found at GET /sources/{id}/edit -->
                    <a class="btn btn-small btn-info " href="{{ URL::to('sources/' . $value->id . '/edit') }}"><i class="glyphicon glyphicon-edit"></i></a>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

</div>

@stop