@extends("layout.layout")
    @section("content")
    <div class="row">
        <div class="col-lg-12">
<h1>Showing {{ $sources->publication_name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $sources->publication_name }}</h2>
        <p>
            {{ HTML::image($sources->publication_image) }}
        </p>
    </div>

</div>
@stop