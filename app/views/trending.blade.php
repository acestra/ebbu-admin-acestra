@extends("layout.layout")
@section("content")
<div id="trendingajax" class="table-responsive">

<!-- Example row of columns -->
<div class="row">
	<div class="col-lg-12">
    <div class="row">
      <div class="col-lg-4 col-md-6">
      	<h2 class="text-center"> </h2>
        <div class="panel panel-primary">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-3">
                <i class="fa fa-users fa-5x"></i>
              </div>
              <div class="col-xs-9 text-right">
                <div class="huge">{{ $Users }}</div>
              </div>
            </div>
          </div>
            <div class="panel-footer">
              <span class="pull-left">Total App Users!</span>
              <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
      	<h2 class="text-center"> </h2>
        <div class="panel panel-green">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-3">
                <i class="fa fa-clock-o fa-5x"></i>
              </div>
              <div class="col-xs-9 text-right">
                <div class="huge">{{ $total_usage }}</div>
              </div>
            </div>
          </div>
            <div class="panel-footer">
              <span class="pull-left">Total App Usage!</span>
              <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
      	<h2 class="text-center"> </h2>
        <div class="panel panel-yellow">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-3">
                <i class="fa fa-search fa-5x"></i>
              </div>
              <div class="col-xs-9 text-right">
                <div class="huge">{{ $total_search_word }}</div>
              </div>
            </div>
          </div>
            <div class="panel-footer">
              <span class="pull-left">Total User Search word !</span>
              <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <!--div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">13</div>
                                        <div>Support Tickets!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                      </div-->
      </div>          
    </div>	
  </div>
</div>
    <div class="table-responsive">
    <table id="trending-user" class="table table-striped table-bordered">
      <thead>
        <tr>
          <td>User Email</td>
          <!--<td>Article View</td>-->
            <td>Feeds Count</td>
           <td>Tweets Count</td>
          <td>Latest Search</td>
          <td>Total Usage</td>
          <td>Updated At</td>
        </tr>
      </thead>
      <tbody>
        @foreach($return_user as $key => $value)
        <tr>
          <td><a href="{{ URL::to('users/' . $value['id']) }}">{{ $value['user_id'] }}</a></td>
       
           <td>{{ $value['feeds_counts'] }}</td>
           <td>{{ $value['tweets_count'] }}</td>
          <td>{{ $value['search_query'] }}</td>
          <td>{{ $value['total_usage'] }}</td>
          <td>{{ $value['session_date'] }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>

@stop
  @section('footercontent')
  <script type="text/javascript">
    $(document).ready(function(e) {

      loadnewsfeeds();

    });             


    function loadnewsfeeds()
    {
      $.ajax({
        url:'trending-ajax',
        type:'GET',
        dataType:'html',
        success:function(data){
          $('#example_list').dataTable().fnDestroy();
               $("#trendingajax").html(data);
               $('#trending').dataTable( {
                "order": [[ 3, "desc" ]]
              } );
               $('#trending-twitter').dataTable( {
                "order": [[ 3, "desc" ]]
              } );
             }
           });
    }

setInterval( function(){ loadnewsfeeds();  } , 20000);  

</script>
@stop