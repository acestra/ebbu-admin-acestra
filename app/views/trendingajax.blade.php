<!-- Example row of columns -->
<div class="row">
  <div class="col-lg-12">
    <div class="row">
      <div class="col-lg-4 col-md-6">
        <h2 class="text-center"> </h2>
        <div class="panel panel-primary">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-3">
                <i class="fa fa-users fa-5x"></i>
              </div>
              <div class="col-xs-9 text-right">
                <div class="huge">{{ $Users }}</div>
              </div>
            </div>
          </div>
            <div class="panel-footer">
              <span class="pull-left">Total App Users!</span>
              <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
        <h2 class="text-center"> </h2>
        <div class="panel panel-green">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-3">
                <i class="fa fa-clock-o fa-5x"></i>
              </div>
              <div class="col-xs-9 text-right">
                <div class="huge">{{ $total_usage }}</div>
              </div>
            </div>
          </div>
            <div class="panel-footer">
              <span class="pull-left">Total App Usage!</span>
              <div class="clearfix"></div>
            </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
        <h2 class="text-center"> </h2>
        <div class="panel panel-yellow">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-3">
                <i class="fa fa-search fa-5x"></i>
              </div>
              <div class="col-xs-9 text-right">
                <div class="huge">{{ $total_search_word }}</div>
              </div>
            </div>
          </div>
            <div class="panel-footer">
              <span class="pull-left">Total User Search word !</span>
              <div class="clearfix"></div>
            </div>
        </div>
      </div>
      </div>          
    </div>  
  </div>
  <div class="table-responsive col-xs-6">
    <h4 class="text-center">Twitter Source</h4>
    <table id="trending-twitter" class="table table-striped table-bordered">
      <thead>
        <tr>
          <td>Category Name</td>
          <td>Source Name</td>
          <td>Article Title</td>
          <td>Click Count</td>
        </tr>
      </thead>
      <tbody>
        @foreach($twitterfeeds as $key => $value)
        <tr>
          <td>{{ TwitterFeeds::getCategoryName($value->twitter_handler_id) }}</td>
          <td>{{ TwitterFeeds::getPublicationName($value->twitter_handler_id) }}</td>
          <td>{{ $value->tweet_text }}</td>
          <td>{{ $value->click_count }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="table-responsive col-xs-6">
  <h4 class="text-center">Rss Source</h4>
    <table id="trending" class="table table-striped table-bordered">
      <thead>
        <tr>
          <td>Category Name</td>
          <td>Source Name</td>
          <td>Article Title</td>
          <td>Click Count</td>
        </tr>
      </thead>
      <tbody>
        @foreach($feeds as $key => $value)
        <tr>
          <td>{{ EbooFeeds::getCategoryName($value->rss_link_id) }}</td>
          <td>{{ EbooFeeds::getPublicationName($value->rss_link_id) }}</td>
          <td>{{ $value->article_title }}</td>
          <td>{{ $value->clickcount }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  </div>
