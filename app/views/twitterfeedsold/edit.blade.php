@extends("layout.layout")
@section("content")
<div class="row">
	<div class="col-lg-8 col-md-offset-2">

		<h1></h1>

		<!-- if there are creation errors, they will show here -->
		@if($errors->count()!=0)
		<div class="alert alert-danger" role="alert">
			{{ HTML::ul($errors->all()) }}
		</div>
		@endif

		{{ Form::model($feeds, array('route' => array('twittersync.update', $feeds->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
		<fieldset>

			<!-- Form Name -->
			<legend>Edit Feed "{{ $feeds->tweet_text }}"</legend>
			<div class="form-group">
				{{ Form::label('twitter_handler_id', 'Rss Link ID' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('twitter_handler_id', null, array('class' => 'form-control' , "placeholder" => "Rss Link ID" , 'disabled' => 'disabled')) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('tweet_text', 'Article Title' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('tweet_text', null, array('class' => 'form-control' , "placeholder" => "Rss Link")) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('article_description', 'Article Description' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('article_description', null, array('class' => 'form-control' , "placeholder" => "Rss Link")) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('article_link', 'Article Link' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('article_link', null, array('class' => 'form-control' , "placeholder" => "Rss Link")) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('created_date', 'Article Publish Date' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('created_date', null, array('class' => 'form-control' , "placeholder" => "Rss Link")) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('status', 'Status' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('status', array('0' => 'Select a Status', '1' => 'Enable', '2' => 'Disable'), Input::old('status') , array('class' => 'form-control')) }}
				</div>
			</div>
			<div class="col-sm-offset-3 col-sm-9">
				<div class="">
					{{ Form::submit('Edit Feed!', array('class' => 'btn btn-primary')) }}
					<a href="{{ URL::previous() }}" class="btn btn-default">Back</a>
				</div>
			</div>
		</fieldset>
		{{ Form::close() }}
	</div>
	@stop