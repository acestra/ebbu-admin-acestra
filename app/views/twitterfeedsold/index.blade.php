@extends("layout.layout")
    @section("content")

    <div class="row">
        <div class="col-lg-12">

          <h1>All Old Twitter Feeds</h1>
          <!-- will be used to show any messages -->
        @if (Session::has('message'))
          <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif
<div class="table-responsive">
        <table id="twitter-feed-old" class="table table-striped table-bordered display">
          <thead>
            <tr>
              <td>ID</td>
              <td>Category Name</td>
              <td>Source Name</td>
              <td>Article Title</td>
              <td>Click Count</td>
              <td>Publish Date</td>
              <td>Status</td>
            </tr>
          </thead>
         <tfoot>
            <tr>
              <td>ID</td>
              <td>Category Name</td>
              <td>Source Name</td>
              <td>Article Title</td>
              <td>Click Count</td>
              <td>Publish Date</td>
              <td>Status</td>
            </tr>
          </tfoot>
      </table>
      </div>
    </div>
@stop