@extends("layout.layout")
    @section("content")
    <div class="row">
        <div class="col-lg-12">
<h1>Showing {{ $admin->name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $admin->name }}</h2>
        <p>
            <strong>Email:</strong> {{ $admin->email }}<br>
            <strong>Level:</strong> {{ $admin->admin_level }}
        </p>
    </div>

</div>
@stop