@extends("layout.login")
@section('title')
{{{ 'User Login' }}}
@endsection

@section('description')
{{{ 'My great product'}}}
@endsection

@section('keywords')
{{{ 'default, keywords, for, my, product' }}}
@endsection

	@section("content")
	<div class="panel panel-default login-form">
		<div class="login-logo">
			<a class="navbar-brand col-lg-offset-4" href="{{ URL::to('/password/remind') }}"><img src="images/ebbu-logo.png" alt="ebbu"/></a>
		</div>

		<div class="panel-body">
			{{ Form::open() }}
			{{ $errors->first("password") }}
			<div class="form-group">
				<label for="email">Email</label>
				{{ Form::text("email", Input::old("email"), [ "placeholder" => "Enter email " , "class" => "form-control" , "id" => "username" ]) }}
			</div>

			<div class="form-group">
				<label for="password">Password</label>
				{{ Form::password("password", [ "placeholder" => "Password" ,  "class" => "form-control" , "id" => "password"]) }}
			</div>

			<div class="form-group">
				<label for="pin">Pin</label>
				{{ Form::password("pin", [ "placeholder" => "Pin" ,  "class" => "form-control" , "id" => "pin"]) }}
			</div>
			{{ Form::submit("Sign in", [ "class" => "form-control btn btn-sm btn-default"] ) }}
			<!--button type="button" onClick="return checkcombination()" class="form-control btn btn-sm btn-default">Sign in</button-->
			{{ Form::close() }}
			<div class="panel-footer">
				<a href="{{ URL::to('/password/remind') }}">I forgot my password</a>
			</div>
		</div>
	</div>
@stop