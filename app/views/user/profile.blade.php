@extends("layout.layout")
@section("content")
<div class="row">
	<h1></h1>
	<div class="col-md-5 col-md-offset-4 ">
		@if($errors->count()!=0)
		<div class="alert alert-danger" role="alert">
			{{ HTML::ul($errors->all()) }}
		</div>
		@endif
	</div>
	<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">{{ Auth::user()->admin_user }}</h3>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3 col-lg-3 " align="center"> 
						<img alt="User Pic" src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=100" class="img-circle"> 
					</div>
					<div class=" col-md-9 col-lg-9 "> 
						<table class="table table-user-information">
							<tbody>
								<tr>
									<td>Name:</td>
									<td>{{ Auth::user()->admin_user }}</td>
								</tr>
								<tr>
									<td>Created Date:</td>
									<td>{{ Auth::user()->session_date }}</td>
								</tr>
								<tr>
									<td>Email</td>
									<td><a href="mailto:{{ Auth::user()->email }}">{{ Auth::user()->email }}</a></td>
								</tr>
								<tr>
									<td>Pin</td>
									<td>{{ Auth::user()->pin }}</td>
								</tr>
							</tbody>
						</table>
                  				<!--a href="#" class="btn btn-primary">My Sales Performance</a>
                  				<a href="#" class="btn btn-primary">Team Sales Performance</a-->
                  				</div>
                  			</div>
                  		</div>
                  		<div class="panel-footer">
                  			<a data-original-title="Broadcast Message" data-toggle="tooltip" class="btn btn-sm btn-primary" href="mailto:{{ Auth::user()->email }}"><i class="glyphicon glyphicon-envelope"></i></a>
                  			<span class="pull-right">
                  				<a href="{{ URL::route("user.profileedit") }}" data-original-title="Change this user Password" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                  				<!--a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a-->
                  			</span>
                  		</div>
                  	</div>
                  </div>
              </div>
              @stop