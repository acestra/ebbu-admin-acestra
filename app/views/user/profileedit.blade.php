@extends("layout.layout")
@section("content")
<div class="row">
	<div class="col-md-7 col-md-offset-2 eboo-form">
		{{Form::open(array('class' => 'form-horizontal'))}}
		<fieldset>

			<!-- Form Name -->
			<legend>User Details</legend>

			<div class="form-group">
				{{ Form::label('admin_user', 'Username' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('admin_user', $user->admin_user, array('class' => 'form-control' , "placeholder" => "Username" , 'disabled' => 'disabled')) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('email_id', 'Email' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::email('email_id', $user->email, array('class' => 'form-control' , "placeholder" => "Email" , 'disabled' => 'disabled')) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('password', 'Password' , array('class' => 'col-sm-3 control-label') ) }}
				<div class="col-sm-3">
					{{ Form::password('password',  array('class' => 'form-control' , "placeholder" => "Password")) }}
				</div>
				{{ Form::label('password_confirmation', 'Password confirmation' , array('class' => 'col-sm-3 control-label')  ) }}
				<div class="col-sm-3">
					{{ Form::password('password_confirmation',  array('class' => 'form-control' , "placeholder" => "Password confirmation")) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('pin', 'Pin' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-3">
					{{ Form::text('pin', $user->pin , array('class' => 'form-control' , "placeholder" => "Pin")) }}
				</div>
				{{ Form::label('pin_confirmation', 'Re-Type Pin' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-3">
				{{ Form::text('pin_confirmation', $user->pin , array('class' => 'form-control' , "placeholder" => "Pin")) }}
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
					<div class="">
						{{ link_to(URL::previous(), 'Cancel', ['class' => 'btn btn-default']) }}
						{{ Form::submit("Save", [ "class" => "btn btn-primary"] ) }}
					</div>
				</div>
			</div>
		</fieldset>
		{{ Form::close() }}
	</div><!-- /.col-lg-12 -->
</div><!-- /.row -->
@stop