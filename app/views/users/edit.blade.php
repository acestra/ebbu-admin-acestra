@extends("layout.layout")
@section("content")
    <div class="row">
        <div class="col-xs-12">
		<ul class="nav navbar-nav pull-right">
		<li><a class="btn btn-small btn-info " href="{{ URL::to('admin') }}">View All admin</a></li>
		<!--li><a class="btn btn-small btn-default " href="{{ URL::to('admin/create') }}">Create a Nerd</a-->
		</ul>
		<h1></h1>

		<!-- if there are creation errors, they will show here -->

		@if($errors->count()!=0)
		<div class="alert alert-danger" role="alert">
			{{ HTML::ul($errors->all()) }}
		</div>
		@endif

		{{ Form::model($admin, array('route' => array('admin.update', $admin->id), 'method' => 'PUT', 'class' => 'form-horizontal')) }}
		<fieldset>

			<!-- Form Name -->
			<legend>Edit {{ $admin->admin_user }}</legend>
			<div class="form-group">
				{{ Form::label('admin_user', 'Username' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('admin_user', null, array('class' => 'form-control' , "placeholder" => "Username" , 'disabled' => 'disabled' )) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('email_id', 'Email' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::email('email_id', null, array('class' => 'form-control' , "placeholder" => "Email", 'disabled' => 'disabled')) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('pin', 'Pin' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::text('pin', null, array('class' => 'form-control' , "placeholder" => "Pin")) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('role_id', 'Role' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('role_id', $admin_roles, Input::old('role_id') , array('class' => 'form-control')) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('status', 'Status' , array('class' => 'col-sm-3 control-label')) }}
				<div class="col-sm-9">
					{{ Form::select('status', array('0' => 'Select a Status', '1' => 'Enable', '2' => 'Disable'), Input::old('status') , array('class' => 'form-control')) }}
				</div>
			</div>
			{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
			<a href="{{ URL::previous() }}" class="btn btn-default">Back</a>
		</fieldset>
		{{ Form::close() }}

	</div>
	@stop