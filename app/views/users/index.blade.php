@extends("layout.layout")
    @section("content")

    <div class="row">
        <div class="col-xs-12">

    <ul class="nav navbar-nav pull-right">
        <!--li><a class="btn btn-small btn-info " href="{{ URL::to('users') }}">View All Admin</a></li-->
        <li><a class="btn btn-small btn-default " href="{{ URL::to('users/create') }}">Create a User</a>
    </ul>

<h1>All User</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<div class="table-responsive">
<table id="example" class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>User Name</td>
            <td>Email</td>
            <td>Source</td>
            <td># Click</td>
            <td># Like</td>
            <td>Updated At</td>
            <td>Status</td>
            <td class="action_table">Actions</td>
        </tr>
    </thead>
    <tbody>
    @foreach($users as $key => $value)
        <tr>
            <td><a href="{{ URL::to('users/' . $value->id) }}">{{ $value->id }}</a></td>
            <td>{{ $value->username }}</a></td>
            <td><a href="{{ URL::to('users/' . $value->id) }}">{{ $value->user_id }}</a></td>
            <td>{{ $value->source }}</td>
            <td>{{ Users::getclickCount($value->id) }}</td>
            <td>{{ Users::getLikeCount($value->id) }}</td>
            <td>{{ $value->session_date }}</td>
            <td>
                @if( $value->status ==1) 
                    Enable
                @else
                    Disable
                @endif
            </td>

            <!-- we will also add show, edit, and delete buttons -->
            <td>
                <!-- show the nerd (uses the show method found at GET /users/{id} -->
                <!--a class="btn btn-small btn-success" href="{{ URL::to('users/' . $value->id) }}">Show</a-->

                <!-- edit this nerd (uses the edit method found at GET /users/{id}/edit -->
                <a class="btn btn-small btn-info " href="{{ URL::to('users/' . $value->id . '/edit') }}"><i class="glyphicon glyphicon-edit"></i></a>

                <!-- delete the nerd (uses the destroy method DESTROY /users/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->
                {{ Form::open(array('url' => 'users/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{Form::button('<i class="glyphicon glyphicon-trash"></i>', array('type' => 'submit', 'class' => 'btn btn-warning'))}}
                {{ Form::close() }}

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
</div>

@stop
