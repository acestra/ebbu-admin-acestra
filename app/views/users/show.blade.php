@extends("layout.layout")
    @section("content")

    <section class="content-wrapper content-midwet">
                <div class="content-header">
                    <h1 class="content-title">Bent Smith Profile</h1>
                </div><!-- /.content-header -->

                <div class="profile-header clearfix" style="background-image: url(images/dummy/trianglify6.svg)">
                    <div class="profile-avatar kit-avatar kit-avatar-128 border-transparent pull-left">
                        <img src="images/dummy/profile.jpg" alt="bent smith">
                    </div>
                    <p class="profile-status">This is the <strong>latest activity</strong> of <i>@bent</i>, nek pak diganti yo monggo!</p>
                    <div class="profile-data">
                        <i class="fa fa-briefcase fa-fw"></i>Web designer at Stilearning, LLC <br>
                        <i class="fa fa-envelope fa-fw"></i>{{ $users->user_id }} <br>
                        <i class="fa fa-map-marker fa-fw"></i>Pekalongan, Central Java, Indonesia <br>
                        <i class="fa fa-archive fa-fw"></i>16 Projects 
                        <i class="fa fa-shopping-cart fa-fw"></i>24,170 Sales 
                        <!-- <i class="fa fa-reply fa-fw"></i>8 Canceled -->
                    </div>
                </div><!-- /.profile-header -->

                <div class="container">
                    <ul class="nav nav-tabs nav-tabs-alt hidden-sm hidden-xs pull-left">
                        <li class="active"><a data-toggle="tab" href="#activity">Activity</a></li>
                        <li class=""><a data-toggle="tab" href="#rss">Clicked Rss Feed</a></li>
                        <li class=""><a data-toggle="tab" href="#twitter">Clicked Twitter Feed</a></li>
                        <li class=""><a data-toggle="tab" href="#posts">Like</a></li>
                        <li class=""><a data-toggle="tab" href="#about">About</a></li>
                    </ul><!-- /.nav -->
                    <div class="btn-group visible-sm visible-xs pull-left">
                        <a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bars"></i>
                        </a>
                        <ul class="dropdown-menu pull-left" role="menu">
                            <li class="active"><a data-toggle="tab" href="#activity">Activity</a></li>
                            <li><a data-toggle="tab" href="#rss">Clicked Rss Feed</a></li>
                            <li><a data-toggle="tab" href="#twitter">Clicked Twitter Feed</a></li>
                            <li><a data-toggle="tab" href="#posts">Like</a></li>
                            <li class="divider"></li>
                            <li><a data-toggle="tab" href="#about">About</a></li>
                        </ul>
                    </div><!-- /.btn-group -->

                </div><!-- /.content-actions -->

                <div class="content">

                    <!-- PROFILE
                    ================================================== -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="activity">
                            <!-- Activity -->
                            <div class="activity-wrapper">
                                <div class="activity-list">
                                    <div class="activity-status">
                                        <div class="pull-right text-muted">
                                            <strong>Update</strong> <small>2 hours ago</small>
                                        </div>
                                        <a data-original-title="read changelog" href="#" class="activity-icon bg-belpet" rel="tooltip" title="">
                                            <i class="fa fa-refresh"></i>
                                        </a><!-- /.activity-icon -->
                                        <div class="activity-description">
                                            <strong>Feed</strong> just update at <strong>{{ date('F d, Y') }}</strong>
                                        </div><!-- /.activity-description -->
                                    </div><!-- /.activity-status -->

                                    <div class="activity-comments">
                                <?php  
                                    $getFeedDetailCount = 0; 
                                    $notFound = 0 ;
                                ?>

                                @foreach($userRssFeedClicked as $value)
                                    <?php $getFeedDetail = Users::getFeedDetail($value->feeds_id); ?>
                                    @if(count($getFeedDetail)>0)
                                        @foreach($getFeedDetail as $key => $valueDetail)
                                        <div class="media">
                                            <p><strong><a href="#">{{ EbooFeeds::getPublicationName($valueDetail->rss_link_id) }}</a></strong> <small class="text-muted">{{ date('F d, Y', strtotime($valueDetail->article_pubdate)) }}</small></p>
                                            <a class="kit-avatar kit-avatar-32 pull-left" href="#">
                                                <img src="{{ URL::to('/') }}/{{ TwitterFeeds::getPublicationImage($valueDetail->twitter_handler_id) }}" alt="{{ $valueDetail->article_title }}" class="img-responsive col-xs-4">
                                            </a>
                                            <div class="media-body">
                                                {{ $valueDetail->article_title }}
                                            </div>
                                        </div><!-- /.media as comment-item -->
                                        @endforeach
                                    @else
                                            <?php   $notFound++; ?>
                                    @endif
                                @endforeach
                                @if(count($userRssFeedClicked)==$notFound)
                                    <h3> Not Found </h3>
                                @endif

                                    </div><!-- /.activity-comments -->

                                </div><!-- /.activity-list -->

                                <div class="activity-list">
                                    <div class="activity-status">
                                        <div class="pull-right text-muted">
                                            <strong>Update</strong> <small>4 hours ago</small>
                                        </div>
                                        <a data-original-title="go to file" href="#" class="activity-icon bg-nephem" rel="tooltip" title="">
                                            <i class="fa fa-cloud-upload"></i>
                                        </a><!-- /.activity-icon -->
                                        <div class="activity-description">
                                            <strong>Twitter</strong> just update at <strong>{{ date('F d, Y') }}</strong>
                                        </div><!-- /.activity-description -->
                                    </div><!-- /.activity-status -->

                                    <div class="activity-comments">
                                @foreach($userTwitterFeedClicked as $key => $value)
                                    <?php  $getTwitterDetail = Users::getTwitterDetail($value->tweets_id); ?>
                                    @foreach($getTwitterDetail as $key => $valueDetail)
                                        <div class="media">
                                            <p><strong><a href="#">{{ TwitterFeeds::getPublicationName($valueDetail->twitter_handler_id)  }}</a></strong> <small class="text-muted">{{ date('F d, Y', strtotime($valueDetail->created_date)) }}</small></p>
                                            <a class="kit-avatar kit-avatar-32 pull-left" href="#">
                                                <img src="{{ URL::to('/') }}/{{ TwitterFeeds::getPublicationImage($valueDetail->twitter_handler_id) }}" alt="{{ $valueDetail->tweet_text }}" class="img-responsive col-xs-4">
                                            </a>
                                            <div class="media-body">
                                                {{ $valueDetail->tweet_text }}
                                            </div>
                                        </div><!-- /.media as comment-item -->
                                    @endforeach
                                @endforeach

                                        <!--div class="media">
                                            <p><strong><a href="#">Isabella Huber</a></strong> <small class="text-muted">2 hours ago</small></p>
                                            <a class="kit-avatar kit-avatar-32 pull-left" href="#">
                                                <img src="images/dummy/uifaces14.jpg" alt="user">
                                            </a>
                                            <div class="media-body">
                                                Sem egestas morbi. Nullam placerat nunc. Ut eget, urna nibh dui. Interdum volutpat, vestibulum cras elementum. Id ornare sed, tortor massa. Habitant mollis, quis vivamus.
                                                <hr>
                                                <div class="pull-right"><a href="#">download all</a></div>
                                                <p class="lead">Attachments <small class="text-muted">(3 files, 1.21 MB)</small></p>
                                                <ul class="list-unstyled">
                                                    <li class="clearfix">
                                                        <div class="pull-right">
                                                            <a href="#">view</a>&nbsp;&nbsp;&nbsp;
                                                            <a href="#">download</a>
                                                        </div>
                                                        <p>Design_brief.zip <small>(654 KB)</small></p>
                                                    </li>
                                                    <li class="clearfix">
                                                        <div class="pull-right">
                                                            <a href="#">view</a>&nbsp;&nbsp;&nbsp;
                                                            <a href="#">download</a>
                                                        </div>
                                                        <p>Revision_191213.jpg <small>(184 KB)</small></p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div-->
                                    </div><!-- /.activity-comments -->

                                </div><!-- /.activity-list -->

                                <div class="activity-list">
                                    <div class="activity-status">
                                        <div class="pull-right text-muted">
                                            <strong>Update</strong> <small>5 hours ago</small>
                                        </div>
                                        <a data-original-title="see Jake Thomas profile" href="#" class="activity-icon bg-pomeal" rel="tooltip" title="">
                                            <i class="fa fa-user"></i>
                                        </a><!-- /.activity-icon -->
                                        <div class="activity-description">
                                            <strong>Promot</strong> just update at <strong>{{ date('F d, Y') }}</strong>
                                        </div><!-- /.activity-description -->
                                    </div><!-- /.activity-status -->

                                    <div class="activity-comments">
                                        <div class="media">
                                            <p><strong><a href="#">Albert Croitor</a></strong> <small class="text-muted">1 hours ago</small></p>
                                            <a class="kit-avatar kit-avatar-32 pull-left" href="#">
                                                <img src="images/dummy/uifaces13.jpg" alt="user">
                                            </a>
                                            <div class="media-body">
                                                Wisi dolor dolor, congue aliquam, sapien vestibulum ut. Nulla mi massa, purus id a, a metus leo. Non duis vel, ante eleifend, egestas arcu. Faucibus nulla, libero turpis neque. Libero nullam temporibus, euismod malesuada eros. Ornare est, vestibulum sodales.
                                            </div>
                                        </div><!-- /.media as comment-item -->
                                    </div><!-- /.activity-comments -->

                                </div><!-- /.activity-list -->
                            </div><!-- /.activity-wrapper -->
                        </div><!-- /.tab-pane #activity -->



                        <div class="tab-pane" id="rss">
                            <div class="posts-wrapper">
                                <?php  
                                    $getFeedDetailCount = 0; 
                                    $notFound = 0 ;
                                ?>

                                @foreach($userRssFeedClicked as $value)
                                    <?php $getFeedDetail = Users::getFeedDetail($value->feeds_id); ?>
                                    @if(count($getFeedDetail)>0)
                                        @foreach($getFeedDetail as $key => $valueDetail)
                                            <div class="post-item">
                                                <h4 class="post-title"><a href="#">{{ EbooFeeds::getPublicationName($valueDetail->rss_link_id)  }}</a></h4>
                                                <hr>
                                                <!--div class="post-attributes">
                                                    <span class="post-status badge badge-success">Publish</span>
                                                    <span class="badge badge-info">life</span>
                                                    <span class="badge badge-info">freelance</span>
                                                    <span class="badge badge-info">work</span>
                                                </div-->
                                                <div class="post-content">
                                                    <div class="media">
                                                        <a class="post-img" href="#">
                                                            <img src="{{ URL::to('/') }}/{{ TwitterFeeds::getPublicationImage($valueDetail->twitter_handler_id) }}" alt="{{ $valueDetail->tweet_text }}" class="img-responsive">
                                                        </a>
                                                        <div class="media-body">
                                                            <p>{{ $valueDetail->article_title }}</p>
                                                        </div>
                                                    </div>
                                                </div><!-- /.post-content -->
                                                <hr>
                                                <div class="pull-right">
                                                    <div class="text-muted">Posted on {{ date('F d, Y', strtotime($valueDetail->article_pubdate)) }}</div>
                                                </div>
                                                <div class="help-block">
                                                    <a href="#" class="post-details">Total Click({{ $valueDetail->clickcount }}) <i class="fa fa-share"></i></a>
                                                    <a href="#" class="post-details">Total Like ({{ $valueDetail->clickcount }}) <i class="fa fa-heart"></i></a>
                                                </div>
                                            </div><!-- /.post-item -->
                                        @endforeach
                                    @else
                                            <?php   $notFound++; ?>
                                    @endif
                                @endforeach
                                @if(count($userRssFeedClicked)==$notFound)
                                    <h3> Not Found </h3>
                                @endif
                            </div><!-- /.posts-wrapper -->
                            <div class="text-center">
                                <button type="button" class="btn btn-sm btn-default">Load more...</button>
                            </div>
                        </div><!-- /.tab-pane #rss -->


                        <div class="tab-pane" id="twitter">
                            <div class="posts-wrapper">
                                @foreach($userTwitterFeedClicked as $key => $value)
                                    <?php  $getTwitterDetail = Users::getTwitterDetail($value->tweets_id); ?>
                                    @foreach($getTwitterDetail as $key => $valueDetail)
                                        <div class="post-item">
                                            <h4 class="post-title"><a href="#">{{ TwitterFeeds::getPublicationName($valueDetail->twitter_handler_id) }}</a></h4>
                                            <hr>
                                            <!--div class="post-attributes">
                                                <span class="post-status badge badge-success">Publish</span>
                                                <span class="badge badge-info">life</span>
                                                <span class="badge badge-info">freelance</span>
                                                <span class="badge badge-info">work</span>
                                            </div-->
                                            <div class="post-content">
                                                <div class="media">
                                                    <a class="post-img" href="#">
                                                        <img src="{{ URL::to('/') }}/{{ TwitterFeeds::getPublicationImage($valueDetail->twitter_handler_id) }}" alt="{{ $valueDetail->tweet_text }}" class="img-responsive">
                                                    </a>
                                                    <div class="media-body">
                                                        <p>{{ $valueDetail->tweet_text }}</p>
                                                        <p>{{ TwitterFeeds::getPublicationName($valueDetail->twitter_handler_id) }}</p>
                                                    </div>
                                                </div>
                                            </div><!-- /.post-content -->
                                            <hr>
                                            <div class="pull-right">
                                                <div class="text-muted">Posted on {{ date('F d, Y', strtotime($valueDetail->created_date)) }}</div>
                                            </div>
                                            <div class="help-block">
                                                <a href="#" class="post-details">Total Click({{ $valueDetail->click_count }}) <i class="fa fa-share"></i></a>
                                                <a href="#" class="post-details">Total Like ({{ $valueDetail->click_count }}) <i class="fa fa-heart"></i></a>
                                            </div>
                                        </div><!-- /.post-item -->
                                    @endforeach
                                @endforeach
                            </div><!-- /.posts-wrapper -->
                            <div class="text-center">
                                <button type="button" class="btn btn-sm btn-default">Load more...</button>
                            </div>
                        </div><!-- /.tab-pane #twitter -->


                        <div class="tab-pane" id="posts">
                            <div class="posts-wrapper">
                                <div class="post-item">
                                    <h4 class="post-title"><a href="#">The Ideas to Become a Creative Professionals</a></h4>
                                    <hr>
                                    <div class="post-attributes">
                                        <span class="post-status badge badge-success">Publish</span>
                                        <span class="badge badge-info">life</span>
                                        <span class="badge badge-info">freelance</span>
                                        <span class="badge badge-info">work</span>
                                    </div>
                                    <div class="post-content">
                                        <div class="media">
                                            <a class="post-img" href="#">
                                                <img src="images/dummy/people1.jpg" alt="people1">
                                            </a>
                                            <div class="media-body">
                                                <p>Mollis ligula facilisi nulla vivamus mauris potenti. Diam quam tristique arcu suspendisse sit, lectus egestas placerat nulla nullam et nisl. Iaculis leo ac mattis suspendisse, risus tincidunt a ultrices, magna et id diam pellentesque massa. Magna arcu vulputate, eu aliquam, magna morbi tellus diam morbi vel sed, in tristique augue justo fermentum, sed ac quis nunc nam eget suscipit. Aliquam at nulla lacus magna sit. Erat massa placerat viverra ante leo ac. Feugiat nunc felis nullam placerat cras, nec penatibus. Ac rutrum non donec quam, eleifend molestie rhoncus quam pede commodo platea, quisque nunc quis magnis scelerisque...</p>
                                            </div>
                                        </div>
                                    </div><!-- /.post-content -->
                                    <hr>
                                    <div class="pull-right">
                                        <div class="text-muted">Posted on June 16, 2014</div>
                                    </div>
                                    <div class="help-block">
                                        <a href="#" class="post-details">Shared (52) <i class="fa fa-share"></i></a>
                                        <a href="#" class="post-details">Favorited (61) <i class="fa fa-heart"></i></a>
                                        <a href="#" class="post-details">Comments (34) <i class="fa fa-comments"></i></a>
                                    </div>
                                </div><!-- /.post-item -->


                            </div><!-- /.posts-wrapper -->
                            
                            <div class="text-center">
                                <button type="button" class="btn btn-sm btn-default">Load more...</button>
                            </div>
                        </div><!-- /.tab-pane #posts -->



                        <div class="tab-pane" id="about">
                            <div class="about-wrapper">
                                <p>In interdum nunc, ut mi risus, lectus adipiscing in. Dictum vivamus, in nibh mattis, nulla vestibulum. Massa tristique, sapien condimentum. Nisl esse tincidunt, imperdiet laoreet sed, aenean nibh duis. Lacus pellentesque, wisi nunc tempor.</p>
                                <p>Nunc malesuada pellentesque, tempor id, viverra odio. Leo porta potenti, nunc lectus sem, nunc posuere. Libero in, quis velit. Mauris in nunc. Odio augue eu, et magna vehicula. Elit vel, est bibendum purus, urna in velit. Adipiscing mauris quam, laoreet suscipit, dignissim sed. Parturient duis, nulla rutrum accumsan. Ligula urna orci, vitae ultrices. Nibh orci suspendisse. Sit etiam magna, molestie vitae nec, rhoncus quis.</p>
                                
                                <blockquote>
                                    <p class="lead">Eros sagittis, leo sociosqu. Malesuada quam ipsum, nam pellentesque, ut tellus. Dictum venenatis. Quam elit. In curabitur. Dictumst leo.</p>
                                </blockquote>
                                <hr>
                                <p><strong>Skills &amp; Knowledges</strong></p>
                                <div class="row">
                                    <div class="col-lg-3 col-sm-4 col-xs-6">
                                        <ul class="fa-ul">
                                            <li><i class="fa fa-angle-right fa-li"></i> Photoshop</li>
                                            <li><i class="fa fa-angle-right fa-li"></i> HTML5 &amp; CSS3</li>
                                            <li><i class="fa fa-angle-right fa-li"></i> Twitter Bootstrap</li>
                                            <li><i class="fa fa-angle-right fa-li"></i> PHP</li>
                                            <li><i class="fa fa-angle-right fa-li"></i> Yeoman</li>
                                        </ul>
                                    </div><!-- /.cols -->
                                    <div class="col-lg-3 col-sm-4 col-xs-6">
                                        <ul class="fa-ul">
                                            <li><i class="fa fa-angle-right fa-li"></i> Laravel</li>
                                            <li><i class="fa fa-angle-right fa-li"></i> Brand Development</li>
                                            <li><i class="fa fa-angle-right fa-li"></i> Product Development</li>
                                            <li><i class="fa fa-angle-right fa-li"></i> Market Research</li>
                                        </ul>
                                    </div><!-- /.cols -->
                                </div><!-- /.row -->
                                <hr>
                                <p><strong>Interests</strong></p>
                                <div class="row">
                                    <div class="col-lg-3 col-sm-4 col-xs-6">
                                        <ul class="fa-ul">
                                            <li><i class="fa fa-angle-right fa-li"></i> UI Design</li>
                                            <li><i class="fa fa-angle-right fa-li"></i> UX / Wireframe</li>
                                            <li><i class="fa fa-angle-right fa-li"></i> Porro quis</li>
                                            <li><i class="fa fa-angle-right fa-li"></i> Consectetur adipisicing</li>
                                            <li><i class="fa fa-angle-right fa-li"></i> Itaque</li>
                                        </ul>
                                    </div><!-- /.cols -->
                                    <div class="col-lg-3 col-sm-4 col-xs-6">
                                        <ul class="fa-ul">
                                            <li><i class="fa fa-angle-right fa-li"></i> Ab fuga non dolorum</li>
                                            <li><i class="fa fa-angle-right fa-li"></i> Design</li>
                                        </ul>
                                    </div><!-- /.cols -->
                                </div><!-- /.row -->
                            </div><!-- /.about-wrapper -->
                        </div><!-- /.tab-pane #about -->

                    </div><!-- /.tab-content -->


                </div><!-- /.content -->
            </section>
@stop