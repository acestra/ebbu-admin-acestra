-- MySQL dump 10.13  Distrib 5.6.22, for Linux (i686)
--
-- Host: localhost    Database: ebbu416
-- ------------------------------------------------------
-- Server version	5.6.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ebbu_admin`
--

DROP TABLE IF EXISTS `ebbu_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `pin` int(11) NOT NULL,
  `reset_pass_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `session_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_admin`
--

LOCK TABLES `ebbu_admin` WRITE;
/*!40000 ALTER TABLE `ebbu_admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_admin_roles`
--

DROP TABLE IF EXISTS `ebbu_admin_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rolename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_admin_roles`
--

LOCK TABLES `ebbu_admin_roles` WRITE;
/*!40000 ALTER TABLE `ebbu_admin_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_admin_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_category`
--

DROP TABLE IF EXISTS `ebbu_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(500) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_category`
--

LOCK TABLES `ebbu_category` WRITE;
/*!40000 ALTER TABLE `ebbu_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_feedback`
--

DROP TABLE IF EXISTS `ebbu_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email_address` varchar(200) NOT NULL,
  `feedback` text NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_feedback`
--

LOCK TABLES `ebbu_feedback` WRITE;
/*!40000 ALTER TABLE `ebbu_feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_feeds`
--

DROP TABLE IF EXISTS `ebbu_feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rss_link_id` int(11) NOT NULL,
  `article_title` varchar(500) NOT NULL,
  `article_description` text NOT NULL,
  `article_link` text NOT NULL,
  `article_pubdate` datetime NOT NULL,
  `clickcount` int(11) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `article_title` (`article_title`),
  FULLTEXT KEY `search` (`article_title`)
) ENGINE=MyISAM AUTO_INCREMENT=653615 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_feeds`
--

LOCK TABLES `ebbu_feeds` WRITE;
/*!40000 ALTER TABLE `ebbu_feeds` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_feeds_old`
--

DROP TABLE IF EXISTS `ebbu_feeds_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_feeds_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_old` int(11) NOT NULL,
  `rss_link_id` int(11) NOT NULL,
  `article_title` varchar(500) NOT NULL,
  `article_description` text NOT NULL,
  `article_link` text NOT NULL,
  `article_pubdate` datetime NOT NULL,
  `clickcount` int(11) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=614894 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_feeds_old`
--

LOCK TABLES `ebbu_feeds_old` WRITE;
/*!40000 ALTER TABLE `ebbu_feeds_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_feeds_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_migrations`
--

DROP TABLE IF EXISTS `ebbu_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_migrations`
--

LOCK TABLES `ebbu_migrations` WRITE;
/*!40000 ALTER TABLE `ebbu_migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_oauth_session`
--

DROP TABLE IF EXISTS `ebbu_oauth_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_oauth_session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `state` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `access_token` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `expiry` datetime NOT NULL,
  `type` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `server` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `creation` datetime NOT NULL,
  `access_token_secret` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `authorized` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `user` int(11) NOT NULL,
  `refresh_token` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_session_session_server_unique` (`session`,`server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_oauth_session`
--

LOCK TABLES `ebbu_oauth_session` WRITE;
/*!40000 ALTER TABLE `ebbu_oauth_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_oauth_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_password_reminders`
--

DROP TABLE IF EXISTS `ebbu_password_reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_reminders_email_index` (`email`),
  KEY `password_reminders_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_password_reminders`
--

LOCK TABLES `ebbu_password_reminders` WRITE;
/*!40000 ALTER TABLE `ebbu_password_reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_password_reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_publication`
--

DROP TABLE IF EXISTS `ebbu_publication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_publication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_name` varchar(500) NOT NULL,
  `publication_image` varchar(500) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_publication`
--

LOCK TABLES `ebbu_publication` WRITE;
/*!40000 ALTER TABLE `ebbu_publication` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_publication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_rss`
--

DROP TABLE IF EXISTS `ebbu_rss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_rss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `rss_link` varchar(500) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_rss`
--

LOCK TABLES `ebbu_rss` WRITE;
/*!40000 ALTER TABLE `ebbu_rss` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_rss` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_serverside_error`
--

DROP TABLE IF EXISTS `ebbu_serverside_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_serverside_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_type` varchar(500) NOT NULL,
  `page_name` varchar(500) NOT NULL,
  `error` text NOT NULL,
  `browser` text NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=230 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_serverside_error`
--

LOCK TABLES `ebbu_serverside_error` WRITE;
/*!40000 ALTER TABLE `ebbu_serverside_error` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_serverside_error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_twitter_feeds`
--

DROP TABLE IF EXISTS `ebbu_twitter_feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_twitter_feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `twitter_handler_id` int(11) NOT NULL,
  `tweet_text` text NOT NULL,
  `article_link` text NOT NULL,
  `created_date` datetime NOT NULL,
  `retweet_count` int(11) NOT NULL,
  `id_str` bigint(11) NOT NULL,
  `click_count` int(11) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `tweet_text` (`tweet_text`),
  FULLTEXT KEY `article_link` (`article_link`),
  FULLTEXT KEY `searchtwitter` (`tweet_text`)
) ENGINE=MyISAM AUTO_INCREMENT=358852 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_twitter_feeds`
--

LOCK TABLES `ebbu_twitter_feeds` WRITE;
/*!40000 ALTER TABLE `ebbu_twitter_feeds` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_twitter_feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_twitter_feeds_old`
--

DROP TABLE IF EXISTS `ebbu_twitter_feeds_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_twitter_feeds_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_old` int(11) NOT NULL,
  `twitter_handler_id` int(11) NOT NULL,
  `tweet_text` text NOT NULL,
  `article_link` varchar(500) NOT NULL,
  `created_date` datetime NOT NULL,
  `retweet_count` int(11) NOT NULL,
  `id_str` bigint(20) NOT NULL,
  `click_count` int(11) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=332818 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_twitter_feeds_old`
--

LOCK TABLES `ebbu_twitter_feeds_old` WRITE;
/*!40000 ALTER TABLE `ebbu_twitter_feeds_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_twitter_feeds_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_twitter_handler`
--

DROP TABLE IF EXISTS `ebbu_twitter_handler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_twitter_handler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `twitter_handler` varchar(500) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=316 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_twitter_handler`
--

LOCK TABLES `ebbu_twitter_handler` WRITE;
/*!40000 ALTER TABLE `ebbu_twitter_handler` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_twitter_handler` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_user_clicked_data`
--

DROP TABLE IF EXISTS `ebbu_user_clicked_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_user_clicked_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `feeds_id` int(11) NOT NULL,
  `tweets_id` int(11) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5302 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_user_clicked_data`
--

LOCK TABLES `ebbu_user_clicked_data` WRITE;
/*!40000 ALTER TABLE `ebbu_user_clicked_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_user_clicked_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_user_saved_feed`
--

DROP TABLE IF EXISTS `ebbu_user_saved_feed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_user_saved_feed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `rss_link_id` int(11) NOT NULL,
  `feed_title` text NOT NULL,
  `feed_description` text NOT NULL,
  `feed_link` varchar(500) NOT NULL,
  `feed_pubdate` varchar(500) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_user_saved_feed`
--

LOCK TABLES `ebbu_user_saved_feed` WRITE;
/*!40000 ALTER TABLE `ebbu_user_saved_feed` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_user_saved_feed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_user_search`
--

DROP TABLE IF EXISTS `ebbu_user_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_user_search` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `search_query` text NOT NULL,
  `articles_clicked` int(11) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5311 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_user_search`
--

LOCK TABLES `ebbu_user_search` WRITE;
/*!40000 ALTER TABLE `ebbu_user_search` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_user_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_user_usage`
--

DROP TABLE IF EXISTS `ebbu_user_usage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_user_usage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `total_usage` bigint(20) NOT NULL,
  `links_clicked` int(11) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_user_usage`
--

LOCK TABLES `ebbu_user_usage` WRITE;
/*!40000 ALTER TABLE `ebbu_user_usage` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_user_usage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ebbu_users`
--

DROP TABLE IF EXISTS `ebbu_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ebbu_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL,
  `location` varchar(1000) NOT NULL,
  `city` varchar(500) NOT NULL,
  `state` varchar(500) NOT NULL,
  `pincode` int(11) NOT NULL,
  `country` varchar(500) NOT NULL,
  `hardware_info` varchar(500) NOT NULL,
  `software_version` varchar(500) NOT NULL,
  `fb_handle` varchar(500) NOT NULL,
  `tumblr_handle` varchar(500) NOT NULL,
  `linkedin_handle` varchar(500) NOT NULL,
  `session_date` datetime NOT NULL,
  `first_usage` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ebbu_users`
--

LOCK TABLES `ebbu_users` WRITE;
/*!40000 ALTER TABLE `ebbu_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `ebbu_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-05  3:48:10
