-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: 166.62.8.46
-- Generation Time: Oct 30, 2014 at 09:01 AM
-- Server version: 5.5.37
-- PHP Version: 5.1.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `eboo1`
--

-- --------------------------------------------------------

--
-- Table structure for table `eboo_category`
--

CREATE TABLE `eboo_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(500) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `eboo_category`
--

INSERT INTO `eboo_category` VALUES(1, 'SPORTS', '2014-10-19 09:07:49', 1);
INSERT INTO `eboo_category` VALUES(2, 'FOOD', '2014-10-19 09:08:04', 1);
INSERT INTO `eboo_category` VALUES(3, 'Pop Culturists', '2014-10-08 15:08:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eboo_feeds`
--

CREATE TABLE `eboo_feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rss_link_id` int(11) NOT NULL,
  `article_title` varchar(500) NOT NULL,
  `article_description` text NOT NULL,
  `article_link` text NOT NULL,
  `article_pubdate` datetime NOT NULL,
  `clickcount` int(11) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=130 ;

--
-- Dumping data for table `eboo_feeds`
--

INSERT INTO `eboo_feeds` VALUES(1, 1, 'European Slowdown Drives Roller-Coaster Week In Markets', 'Concerns in Europe over a slowdown in economic growth have been rattling global financial markets.  NPR''s Scott Simon talks with correspondent John Ydstie about the volatile week on Wall Street.', 'http://www.npr.org/2014/10/18/357153406/european-slowdown-drives-roller-coaster-week-in-markets?utm_medium=RSS&utm_campaign=business', '2014-10-18 07:43:00', 42, '2014-10-19 23:11:06', 1);
INSERT INTO `eboo_feeds` VALUES(2, 1, 'Once A Year, Farmers Go Back To Picking Corn By Hand â€” For Fun', 'Farmers across the Midwest harvest billions of bushels of corn nowadays using giant machines called combines. But a contest keeps a more primitive corn-picking technique alive: human hands.', 'http://www.npr.org/blogs/thesalt/2014/10/18/357020240/once-a-year-farmers-go-back-to-picking-corn-by-hand-for-fun?utm_medium=RSS&utm_campaign=business', '2014-10-18 05:44:00', 7, '2014-10-19 23:11:06', 1);
INSERT INTO `eboo_feeds` VALUES(3, 1, 'Tech Week: Egg Freezing, Gamergate And Online Giving', 'Debates about the role of women in the technology workforce and in gaming are swirling over two notable stories this week.', 'http://www.npr.org/blogs/alltechconsidered/2014/10/17/356937250/tech-week-egg-freezing-gamergate-and-online-giving?utm_medium=RSS&utm_campaign=business', '2014-10-18 05:36:00', 10, '2014-10-19 23:11:06', 1);
INSERT INTO `eboo_feeds` VALUES(4, 2, 'VIDEO: Top secret space plane lands in US', 'A unmanned US plane on a top-secret, two-year mission to space has returned to Earth and landed in California.', 'http://www.bbc.co.uk/news/world-us-canada-29672506#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-18 08:07:32', 38, '2014-10-19 23:11:07', 1);
INSERT INTO `eboo_feeds` VALUES(5, 2, 'VIDEO: Obama: ''Don''t give in to hysteria''', 'President Obama has told Americans there must not be hysteria in response to the Ebola outbreak.', 'http://www.bbc.co.uk/news/world-us-canada-29672839#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-18 01:34:15', 27, '2014-10-19 23:11:07', 1);
INSERT INTO `eboo_feeds` VALUES(6, 2, 'VIDEO: Pope''s synod setback on gay policy', 'Pope Francis suffers a setback as proposals for wider acceptance of gay people failed to win a two-thirds majority at the Catholic Church synod.', 'http://www.bbc.co.uk/news/world-europe-29678241#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-18 09:48:24', 1, '2014-10-19 23:11:07', 1);
INSERT INTO `eboo_feeds` VALUES(7, 2, 'VIDEO: Brit seeks ''soccer'' in the US', 'Ben Zand is travelling across the country as part of BBC''s Pop Up project. In every city he visits, he''s checking in on football''s fate in the states.', 'http://www.bbc.co.uk/news/magazine-29669006#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-18 11:02:18', 1, '2014-10-19 23:11:07', 1);
INSERT INTO `eboo_feeds` VALUES(8, 2, 'VIDEO: New dinosaur ''may be in T-Rex family''', 'Scientists think that a newly discovered species of dinosaur, whose remains were found after 20 years of excavation in Venezuela, maybe related to the Tyrannosaurus Rex.', 'http://www.bbc.co.uk/news/science-environment-29675353#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-18 04:59:26', 8, '2014-10-19 23:11:07', 1);
INSERT INTO `eboo_feeds` VALUES(9, 2, 'VIDEO: Minor scuffles in HK night protests', 'Pro-democracy demonstrators in Hong Kong have clashed with police as they return to the streets in the Mong Kok district, just hours after they were cleared by the authorities.', 'http://www.bbc.co.uk/news/world-asia-china-29675354#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-18 05:15:16', 6, '2014-10-19 23:11:07', 1);
INSERT INTO `eboo_feeds` VALUES(10, 1, 'Unrest In Ferguson May Speed Up Decline Of Real Estate', 'Many in the city are worried about its future, and there''s speculation there will be a "mass migration" should violence erupt again. But some residents remain committed to the city.', 'http://www.npr.org/2014/10/20/357612090/unrest-in-ferguson-may-speed-up-decline-of-real-estate?utm_medium=RSS&utm_campaign=business', '2014-10-20 07:48:00', 8, '2014-10-21 01:09:03', 1);
INSERT INTO `eboo_feeds` VALUES(11, 1, 'Toyota Becomes Latest Automaker To Issue Recalls Over Faulty Airbags', 'Since 2008, almost 16 million vehicles have been recalled over worries that airbags might explode if exposed to high humidity for long periods of time.', 'http://www.npr.org/blogs/thetwo-way/2014/10/20/357629910/toyota-becomes-latest-automaker-to-issue-recalls-over-faulty-airbags?utm_medium=RSS&utm_campaign=business', '2014-10-20 04:52:00', 4, '2014-10-21 01:09:03', 1);
INSERT INTO `eboo_feeds` VALUES(12, 1, 'Tunisia''s Emerging Tech Sector Hampered By Old Policies', 'When Tunisia''s young people protested in 2011, they had one key demand: jobs. Now, despite new political leadership, that demand remains unmet â€” even in tech, the sector that offers the most promise.', 'http://www.npr.org/blogs/alltechconsidered/2014/10/20/357590073/in-tunisia-democracy-doesn-t-create-jobs?utm_medium=RSS&utm_campaign=business', '2014-10-20 04:44:00', 1, '2014-10-21 01:09:03', 1);
INSERT INTO `eboo_feeds` VALUES(13, 1, 'Climate Change Has Coffee Growers In Haiti Seeking Higher Ground', 'Haiti''s once-flourishing coffee trade has been badly battered. The latest threat: climate change. Locals who still rely on coffee for their livelihood must learn to grow it in changing climes.', 'http://www.npr.org/blogs/thesalt/2014/10/20/357589088/climate-change-has-coffee-growers-in-haiti-seeking-higher-ground?utm_medium=RSS&utm_campaign=business', '2014-10-20 03:53:00', 0, '2014-10-21 01:09:03', 1);
INSERT INTO `eboo_feeds` VALUES(14, 1, 'Will Apple''s Mobile Wallet Replace Your Leather Wallet?', 'Many have tried and failed with this kind of payment option before. But Apple''s launch is bigger, with more financial institutions'' support, and consumers may be more security-conscious.', 'http://www.npr.org/blogs/alltechconsidered/2014/10/20/357508522/will-apple-s-mobile-wallet-replace-your-leather-wallet?utm_medium=RSS&utm_campaign=business', '2014-10-20 06:23:00', 0, '2014-10-21 01:09:03', 1);
INSERT INTO `eboo_feeds` VALUES(15, 1, 'The Look Of Power: How Women Have Dressed For Success', 'Just as women were entering the corporate workplace in big numbers, the shapeless power suit emerged. Over time, the "power look" changed. How do women project power in the modern office?', 'http://www.npr.org/2014/10/20/356468554/the-look-of-power-how-women-have-dressed-for-success?utm_medium=RSS&utm_campaign=business', '2014-10-20 04:23:00', 0, '2014-10-21 01:09:03', 1);
INSERT INTO `eboo_feeds` VALUES(16, 2, 'VIDEO: Why US man joined Kurdish fighters', 'The BBC''s Jim Muir speaks to an American who has come to fight Islamic State militants on the front line in north-east Syria.', 'http://www.bbc.co.uk/news/world-middle-east-29689832#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 05:22:02', 2, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(17, 2, 'VIDEO: ''The rebels beat me with guns''', 'In the first part of his road trip across Ukraine ahead of parliamentary elections, Steve Rosenberg meets a candidate who was persecuted by pro-Russian rebels.', 'http://www.bbc.co.uk/news/world-europe-29693467#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 11:45:45', 0, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(18, 2, 'VIDEO: Pistorius siblings: ''Truth manipulated''', 'The siblings of Oscar Pistorius,, Carl and Aimee Pistorius, speak of "twisted truths" in a television interview ahead of his sentencing on Tuesday.', 'http://www.bbc.co.uk/news/world-africa-29694967#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 07:27:10', 0, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(19, 2, 'VIDEO: Whitlam: ''A radical politician''', 'The former Australian prime minister Gough Whitlam has died at the age of ninety-eight.', 'http://www.bbc.co.uk/news/world-australia-29699661#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 11:58:35', 0, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(20, 2, 'VIDEO: Big league for gamers in Seoul', 'Tens of thousands of fans turn out to watch competitors play each other at the League of Legends World Championship in Seoul, South Korea.', 'http://www.bbc.co.uk/news/business-29683714#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 07:21:47', 0, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(21, 2, 'VIDEO: First prosecution over avatar ''Sweetie''', 'A year ago a charity in the Netherlands created a fake profile of a girl called Sweetie to pose in teen chat rooms.', 'http://www.bbc.co.uk/news/uk-29695913#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 10:44:45', 1, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(22, 2, 'VIDEO: Donetsk rocked by chemical plant blast', 'A powerful explosion at a chemical plant in the rebel-held city of Donetsk in eastern Ukraine has reverberated across the city.', 'http://www.bbc.co.uk/news/world-europe-29694960#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 07:15:27', 0, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(23, 2, 'VIDEO: Investigation into US ''pumpkin riot''', 'An annual pumpkin festival in Keene, New Hampshire, turned violent as the event descended into a night of rioting.', 'http://www.bbc.co.uk/news/world-us-canada-29685121#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 10:26:17', 0, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(24, 2, 'VIDEO: Russian racism casts shadow on World Cup', 'FIFA has said that Russia''s preparations for hosting the World Cup in 2018 are "on track" so far.', 'http://www.bbc.co.uk/news/world-us-canada-29685125#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 01:14:27', 5, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(25, 2, 'VIDEO: Robot dragon versus giant spider', 'Two mythical creatures, a giant spider and a huge dragon, have faced one another in battle at Beijing''s Olympic Park.', 'http://www.bbc.co.uk/news/world-asia-china-29685117#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 06:09:32', 0, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(26, 2, 'VIDEO: Women break mass skydive record', 'The record for the largest all-female skydive formation has been broken by 117 women in California.', 'http://www.bbc.co.uk/news/world-us-canada-29685123#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 12:21:05', 0, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(27, 2, 'VIDEO: Nigeria''s ''hero doctor'' who spotted Ebola', 'The WHO is expected to declare Nigeria free from Ebola on Monday with no reported cases of the virus for six weeks due to a rapid and thorough response from healthcare professionals.', 'http://www.bbc.co.uk/news/world-africa-29682325#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 01:26:44', 0, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(28, 2, 'VIDEO: China''s crackdown on corruption', 'A high profile campaign against corruption and bribery by the Chinese ruling Communist Party has gained huge public support.', 'http://www.bbc.co.uk/news/world-asia-29685549#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 10:04:17', 1, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(29, 2, 'VIDEO: New Indonesian president sworn in', 'Joko Widodo is sworn in as the new president of Indonesia, the world''s third largest democracy.', 'http://www.bbc.co.uk/news/world-asia-29685116#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 09:24:13', 1, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(30, 2, 'VIDEO: Ebola survivorsâ€™ blood ''saving lives''', 'Blood from survivors of the Ebola virus is being used to treat patients suffering from the disease.', 'http://www.bbc.co.uk/news/health-29689537#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 12:21:11', 0, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(31, 2, 'VIDEO: Stand-off continues in Mong Kok', 'The stand-off between protesters and police continues in Mong Kok, Hong Kong.', 'http://www.bbc.co.uk/news/world-asia-29684661#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 01:20:50', 0, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(32, 2, 'VIDEO: White suburbs want to create new city', 'Thousands of residents in the community of St George have signed a petition urging the creation of a new city separate to Baton Rouge. Are bad schools or race the motivation?', 'http://www.bbc.co.uk/news/magazine-29666348#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 12:22:33', 0, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(33, 2, 'VIDEO: US airdrops weapons to Kurds', 'The US carries out airdrops of weapons and supplies to Kurdish fighters, despite a lack of support from Turkey.', 'http://www.bbc.co.uk/news/world-middle-east-29685119#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 07:12:14', 1, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(34, 2, 'VIDEO: Nepal survivor: ''Not my day to die''', 'UK police sergeant Paul Sherridan was in the Himalayas for Nepal''s worst ever trekking disaster and helped many trekkers to safety.', 'http://www.bbc.co.uk/news/world-asia-29685120#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-20 08:42:30', 0, '2014-10-21 01:09:04', 1);
INSERT INTO `eboo_feeds` VALUES(35, 1, 'Calling 911 On Your Cell? It''s Harder To Find You Than You Think', 'If you call 911 from inside a tall building, emergency responders may have difficulty finding you. Cellphone GPS technology currently doesn''t work well indoors â€” but the FCC hopes to change that.', 'http://www.npr.org/blogs/alltechconsidered/2014/10/23/358307881/calling-911-on-your-cell-its-harder-to-find-you-than-you-think?utm_medium=RSS&utm_campaign=business', '2014-10-23 06:24:00', 6, '2014-10-24 00:32:59', 1);
INSERT INTO `eboo_feeds` VALUES(36, 1, 'How ''Foodies'' Were Duped Into Thinking McDonald''s Was High-End Food', 'A viral video shows people lauding fare billed as an "organic" fast-food option that was actually McDonald''s. It wasn''t just pranksters playing tricks on these poor folks, but maybe their brains, too.', 'http://www.npr.org/blogs/thesalt/2014/10/23/358324106/don-t-mock-these-organic-food-experts-for-praising-mcdonald-s?utm_medium=RSS&utm_campaign=business', '2014-10-23 05:53:00', 2, '2014-10-24 00:32:59', 1);
INSERT INTO `eboo_feeds` VALUES(37, 1, 'For U.S. Queen Of Alligator-Skin Goods, Business Is Booming', 'Christy Redd''s Georgia company moves 25,000 skins a year, with some made into handbags sold for tens of thousands of dollars. But activists say the ways the gators are grown and slaughtered are awful.', 'http://www.npr.org/2014/10/23/358175205/for-u-s-queen-of-alligator-skin-goods-business-is-booming?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:27:00', 7, '2014-10-24 00:32:59', 1);
INSERT INTO `eboo_feeds` VALUES(38, 1, 'Cigarette-Maker Reynolds American To Ban Smoking At Work', 'Until now, Reynolds employees have been able to light up at their desks. But come January, workers will have to either go outside or use specially equipped smoking rooms.', 'http://www.npr.org/2014/10/23/358363553/cigarette-maker-reynolds-american-to-ban-smoking-at-work?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:22:00', 1, '2014-10-24 00:32:59', 1);
INSERT INTO `eboo_feeds` VALUES(39, 1, 'Please Do Not Leave A Message: Why Millennials Hate Voice Mail ', '"When it comes to voice mail, they''re just over it," says Jane Buckingham, a trend expert. But it''s still important at work, so younger generations will have to learn what to do after the beep.', 'http://www.npr.org/blogs/alltechconsidered/2014/10/23/358301467/please-do-not-leave-a-message-why-millennials-hate-voice-mail?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:22:00', 1, '2014-10-24 00:32:59', 1);
INSERT INTO `eboo_feeds` VALUES(40, 1, 'Mark Zuckerberg Shows Off His Mandarin Chinese Skills', 'The Facebook co-founder and CEO spoke at Tsinghua University in Beijing for about 30 minutes. In Mandarin. His audience liked it.', 'http://www.npr.org/blogs/alltechconsidered/2014/10/23/358300880/mark-zuckerberg-shows-off-his-mandarin-chinese-skills?utm_medium=RSS&utm_campaign=business', '2014-10-23 12:00:00', 0, '2014-10-24 00:32:59', 1);
INSERT INTO `eboo_feeds` VALUES(41, 1, 'You''re Enjoying Low Gas Prices, But Is It Really A Good Sign? ', 'All around the country, drivers are seeing signs that gas prices are depressed. Those drops helped hold down the latest consumer price index. But economists worry about too much of a good thing.', 'http://www.npr.org/2014/10/23/358062856/youre-enjoying-low-gas-prices-but-is-it-really-a-good-sign?utm_medium=RSS&utm_campaign=business', '2014-10-23 10:52:00', 0, '2014-10-24 00:32:59', 1);
INSERT INTO `eboo_feeds` VALUES(42, 1, 'To Get Women To Work In Computer Science, Schools Get Them To Class', 'In 1984, the percentage of women studying computer science flattened, and then plunged. Computer science programs are trying to get that number back up.', 'http://www.npr.org/2014/10/23/358238982/to-get-women-to-work-in-computer-science-schools-get-them-to-class?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:47:00', 0, '2014-10-24 00:32:59', 1);
INSERT INTO `eboo_feeds` VALUES(43, 1, 'What The New Factory Worker Should Know', 'In previous generations, manufacturing jobs were dirty, dangerous and low-skill. The new factory jobs are almost all clean, require increasingly higher skills and take very few people to do them.', 'http://www.npr.org/2014/10/23/358238891/what-the-new-factory-worker-should-know?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:47:00', 0, '2014-10-24 00:32:59', 1);
INSERT INTO `eboo_feeds` VALUES(44, 1, 'No Mere Merry-Go-Round: Ohio Carousel Maker Carves From Scratch', 'Wooden carousels, with hand-carved and painted horses, seem like a relic of the past. But Carousel Works in Mansfield, Ohio, is one of a few companies still making them to order.', 'http://www.npr.org/2014/10/23/358051063/no-mere-merry-go-round-ohio-carousel-maker-carves-from-scratch?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:47:00', 0, '2014-10-24 00:32:59', 1);
INSERT INTO `eboo_feeds` VALUES(45, 2, 'VIDEO: CCTV of Ottawa shooting suspect', 'The Royal Canadian Mounted Police (RCMP) releases video footage of the suspected gunman arriving at the country''s parliament building during Wednesday''s shooting incident.', 'http://www.bbc.co.uk/news/world-us-canada-29749775#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 08:28:51', 0, '2014-10-24 00:33:00', 1);
INSERT INTO `eboo_feeds` VALUES(46, 2, 'VIDEO: Ebola: Scenes of horror in Liberia', 'There have now been more than 4,800 reported deaths from Ebola, with Liberia the worst affected country. Around 50 new cases are being reported every day in the capital Monrovia.', 'http://www.bbc.co.uk/news/world-africa-29748701#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 09:36:43', 0, '2014-10-24 00:33:00', 1);
INSERT INTO `eboo_feeds` VALUES(47, 2, 'AUDIO: ''19 relatives have died from Ebola''', 'A pastor who says he has lost 19 members of his family to Ebola has been speaking to the BBC.', 'http://www.bbc.co.uk/news/world-africa-29749689#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 07:08:54', 0, '2014-10-24 00:33:00', 1);
INSERT INTO `eboo_feeds` VALUES(48, 2, 'VIDEO: Art on motorbikes in Kampala', 'Artists in Uganda are using motorbikes to carry and display street art installations as part of the Kampala Contemporary Art Festival.', 'http://www.bbc.co.uk/news/world-africa-29748703#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 07:19:26', 0, '2014-10-24 00:33:00', 1);
INSERT INTO `eboo_feeds` VALUES(49, 2, 'VIDEO: Exhibition on a changing Afghanistan', 'The Imperial War Museum''s latest exhibition focuses on Afghanistan as British troops prepare to leave.', 'http://www.bbc.co.uk/news/uk-29744048#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 11:45:29', 0, '2014-10-24 00:33:00', 1);
INSERT INTO `eboo_feeds` VALUES(50, 2, 'VIDEO: Daily life goes on inside Kashmir', 'Daily life and trade is continuing in Kashmir despite the recent violence being the worst in a decade, as BBC Urdu''s Shumaila Jaffery reports.', 'http://www.bbc.co.uk/news/world-asia-29739308#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 10:43:16', 0, '2014-10-24 00:33:00', 1);
INSERT INTO `eboo_feeds` VALUES(51, 2, 'VIDEO: Can King Khan top Diwali box office?', 'Farah Khan''s much-anticipated new film, Happy New Year, is released for Diwali.', 'http://www.bbc.co.uk/news/entertainment-arts-29735566#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 06:16:31', 0, '2014-10-24 00:33:00', 1);
INSERT INTO `eboo_feeds` VALUES(52, 2, 'VIDEO: Louisiana''s vanishing voodoo culture', 'The BBC Pop Up bureau explores the rich heritage of voodoo culture among the Cajun and Creole communities of Louisiana.', 'http://www.bbc.co.uk/news/world-us-canada-29712917#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 01:55:59', 1, '2014-10-24 00:33:00', 1);
INSERT INTO `eboo_feeds` VALUES(53, 2, 'VIDEO: Sentamu appeal to Ebola-hit countries', 'People of faith in countries struggling to combat the world''s worst outbreak of Ebola should not meet in large numbers, the UK''s Archbishop of York says.', 'http://www.bbc.co.uk/news/world-africa-29735568#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 08:42:47', 0, '2014-10-24 00:33:00', 1);
INSERT INTO `eboo_feeds` VALUES(54, 2, 'VIDEO: Egypt''s battle to end FGM', 'A court date is set for a verdict in Egypt''s landmark trial in a case of female genital mutilation (FGM).', 'http://www.bbc.co.uk/news/world-middle-east-29735574#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 02:37:02', 0, '2014-10-24 00:33:00', 1);
INSERT INTO `eboo_feeds` VALUES(55, 1, 'Calling 911 On Your Cell? It''s Harder To Find You Than You Think', 'If you call 911 from inside a tall building, emergency responders may have difficulty finding you. Cellphone GPS technology currently doesn''t work well indoors â€” but the FCC hopes to change that.', 'http://www.npr.org/blogs/alltechconsidered/2014/10/23/358307881/calling-911-on-your-cell-its-harder-to-find-you-than-you-think?utm_medium=RSS&utm_campaign=business', '2014-10-23 06:24:00', 0, '2014-10-24 07:37:17', 1);
INSERT INTO `eboo_feeds` VALUES(56, 1, 'How ''Foodies'' Were Duped Into Thinking McDonald''s Was High-End Food', 'A viral video shows people lauding fare billed as an "organic" fast-food option that was actually McDonald''s. It wasn''t just pranksters playing tricks on these poor folks, but maybe their brains, too.', 'http://www.npr.org/blogs/thesalt/2014/10/23/358324106/don-t-mock-these-organic-food-experts-for-praising-mcdonald-s?utm_medium=RSS&utm_campaign=business', '2014-10-23 05:53:00', 1, '2014-10-24 07:37:17', 1);
INSERT INTO `eboo_feeds` VALUES(57, 1, 'For U.S. Queen Of Alligator-Skin Goods, Business Is Booming', 'Christy Redd''s Georgia company moves 25,000 skins a year, with some made into handbags sold for tens of thousands of dollars. But activists say the ways the gators are grown and slaughtered are awful.', 'http://www.npr.org/2014/10/23/358175205/for-u-s-queen-of-alligator-skin-goods-business-is-booming?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:27:00', 0, '2014-10-24 07:37:17', 1);
INSERT INTO `eboo_feeds` VALUES(58, 1, 'Cigarette-Maker Reynolds American To Ban Smoking At Work', 'Until now, Reynolds employees have been able to light up at their desks. But come January, workers will have to either go outside or use specially equipped smoking rooms.', 'http://www.npr.org/2014/10/23/358363553/cigarette-maker-reynolds-american-to-ban-smoking-at-work?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:22:00', 0, '2014-10-24 07:37:17', 1);
INSERT INTO `eboo_feeds` VALUES(59, 1, 'Please Do Not Leave A Message: Why Millennials Hate Voice Mail ', '"When it comes to voice mail, they''re just over it," says Jane Buckingham, a trend expert. But it''s still important at work, so younger generations will have to learn what to do after the beep.', 'http://www.npr.org/blogs/alltechconsidered/2014/10/23/358301467/please-do-not-leave-a-message-why-millennials-hate-voice-mail?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:22:00', 0, '2014-10-24 07:37:17', 1);
INSERT INTO `eboo_feeds` VALUES(60, 1, 'Mark Zuckerberg Shows Off His Mandarin Chinese Skills', 'The Facebook co-founder and CEO spoke at Tsinghua University in Beijing for about 30 minutes. In Mandarin. His audience liked it.', 'http://www.npr.org/blogs/alltechconsidered/2014/10/23/358300880/mark-zuckerberg-shows-off-his-mandarin-chinese-skills?utm_medium=RSS&utm_campaign=business', '2014-10-23 12:00:00', 0, '2014-10-24 07:37:17', 1);
INSERT INTO `eboo_feeds` VALUES(61, 1, 'You''re Enjoying Low Gas Prices, But Is It Really A Good Sign? ', 'All around the country, drivers are seeing signs that gas prices are depressed. Those drops helped hold down the latest consumer price index. But economists worry about too much of a good thing.', 'http://www.npr.org/2014/10/23/358062856/youre-enjoying-low-gas-prices-but-is-it-really-a-good-sign?utm_medium=RSS&utm_campaign=business', '2014-10-23 10:52:00', 0, '2014-10-24 07:37:17', 1);
INSERT INTO `eboo_feeds` VALUES(62, 1, 'To Get Women To Work In Computer Science, Schools Get Them To Class', 'In 1984, the percentage of women studying computer science flattened, and then plunged. Computer science programs are trying to get that number back up.', 'http://www.npr.org/2014/10/23/358238982/to-get-women-to-work-in-computer-science-schools-get-them-to-class?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:47:00', 0, '2014-10-24 07:37:17', 1);
INSERT INTO `eboo_feeds` VALUES(63, 1, 'What The New Factory Worker Should Know', 'In previous generations, manufacturing jobs were dirty, dangerous and low-skill. The new factory jobs are almost all clean, require increasingly higher skills and take very few people to do them.', 'http://www.npr.org/2014/10/23/358238891/what-the-new-factory-worker-should-know?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:47:00', 0, '2014-10-24 07:37:17', 1);
INSERT INTO `eboo_feeds` VALUES(64, 1, 'No Mere Merry-Go-Round: Ohio Carousel Maker Carves From Scratch', 'Wooden carousels, with hand-carved and painted horses, seem like a relic of the past. But Carousel Works in Mansfield, Ohio, is one of a few companies still making them to order.', 'http://www.npr.org/2014/10/23/358051063/no-mere-merry-go-round-ohio-carousel-maker-carves-from-scratch?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:47:00', 1, '2014-10-24 07:37:17', 1);
INSERT INTO `eboo_feeds` VALUES(65, 2, 'VIDEO: CCTV of Ottawa shooting suspect', 'The Royal Canadian Mounted Police (RCMP) releases video footage of the suspected gunman arriving at the country''s parliament building during Wednesday''s shooting incident.', 'http://www.bbc.co.uk/news/world-us-canada-29749775#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 08:28:51', 0, '2014-10-24 07:37:18', 1);
INSERT INTO `eboo_feeds` VALUES(66, 2, 'VIDEO: Spirit of revolution simmers in Kiev', 'Steve Rosenberg visits Kiev to see how the recent violence in Ukraine has had an impact on who is running for parliament.', 'http://www.bbc.co.uk/news/world-europe-29744259#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 11:08:42', 0, '2014-10-24 07:37:18', 1);
INSERT INTO `eboo_feeds` VALUES(67, 2, 'VIDEO: Ebola: Scenes of horror in Liberia', 'There have now been more than 4,800 reported deaths from Ebola, with Liberia the worst affected country. Around 50 new cases are being reported every day in the capital Monrovia.', 'http://www.bbc.co.uk/news/world-africa-29748701#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 09:36:43', 0, '2014-10-24 07:37:18', 1);
INSERT INTO `eboo_feeds` VALUES(68, 2, 'VIDEO: Exhibition on a changing Afghanistan', 'The Imperial War Museum''s latest exhibition focuses on Afghanistan as British troops prepare to leave.', 'http://www.bbc.co.uk/news/uk-29744048#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 11:45:29', 0, '2014-10-24 07:37:18', 1);
INSERT INTO `eboo_feeds` VALUES(69, 2, 'VIDEO: Art on motorbikes in Kampala', 'Artists in Uganda are using motorbikes to carry and display street art installations as part of the Kampala Contemporary Art Festival.', 'http://www.bbc.co.uk/news/world-africa-29748703#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 07:19:26', 0, '2014-10-24 07:37:18', 1);
INSERT INTO `eboo_feeds` VALUES(70, 2, 'AUDIO: ''19 relatives have died from Ebola''', 'A pastor who says he has lost 19 members of his family to Ebola has been speaking to the BBC.', 'http://www.bbc.co.uk/news/world-africa-29749689#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 07:08:54', 0, '2014-10-24 07:37:18', 1);
INSERT INTO `eboo_feeds` VALUES(71, 2, 'VIDEO: Daily life goes on inside Kashmir', 'Daily life and trade is continuing in Kashmir despite the recent violence being the worst in a decade, as BBC Urdu''s Shumaila Jaffery reports.', 'http://www.bbc.co.uk/news/world-asia-29739308#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 10:43:16', 0, '2014-10-24 07:37:18', 1);
INSERT INTO `eboo_feeds` VALUES(72, 1, 'Calling 911 On Your Cell? It''s Harder To Find You Than You Think', 'If you call 911 from inside a tall building, emergency responders may have difficulty finding you. Cellphone GPS technology currently doesn''t work well indoors â€” but the FCC hopes to change that.', 'http://www.npr.org/blogs/alltechconsidered/2014/10/23/358307881/calling-911-on-your-cell-its-harder-to-find-you-than-you-think?utm_medium=RSS&utm_campaign=business', '2014-10-23 06:24:00', 0, '2014-10-24 21:20:24', 1);
INSERT INTO `eboo_feeds` VALUES(73, 1, 'How ''Foodies'' Were Duped Into Thinking McDonald''s Was High-End Food', 'A viral video shows people lauding fare billed as an "organic" fast-food option that was actually McDonald''s. It wasn''t just pranksters playing tricks on these poor folks, but maybe their brains, too.', 'http://www.npr.org/blogs/thesalt/2014/10/23/358324106/don-t-mock-these-organic-food-experts-for-praising-mcdonald-s?utm_medium=RSS&utm_campaign=business', '2014-10-23 05:53:00', 0, '2014-10-24 21:20:24', 1);
INSERT INTO `eboo_feeds` VALUES(74, 1, 'For U.S. Queen Of Alligator-Skin Goods, Business Is Booming', 'Christy Redd''s Georgia company moves 25,000 skins a year, with some made into handbags sold for tens of thousands of dollars. But activists say the ways the gators are grown and slaughtered are awful.', 'http://www.npr.org/2014/10/23/358175205/for-u-s-queen-of-alligator-skin-goods-business-is-booming?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:27:00', 0, '2014-10-24 21:20:24', 1);
INSERT INTO `eboo_feeds` VALUES(75, 1, 'Cigarette-Maker Reynolds American To Ban Smoking At Work', 'Until now, Reynolds employees have been able to light up at their desks. But come January, workers will have to either go outside or use specially equipped smoking rooms.', 'http://www.npr.org/2014/10/23/358363553/cigarette-maker-reynolds-american-to-ban-smoking-at-work?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:22:00', 0, '2014-10-24 21:20:24', 1);
INSERT INTO `eboo_feeds` VALUES(76, 1, 'FDA Cracks Down On Fake Ebola Cures Sold Online', 'The Food and Drug Administration has issued warning letters to companies marketing products claimed to be cures for Ebola. One firm says it will drop such claims â€” but it''s still selling the product.', 'http://www.npr.org/blogs/health/2014/10/23/358318848/fda-cracks-down-on-fake-ebola-cures-sold-online?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:22:00', 1, '2014-10-24 21:20:24', 1);
INSERT INTO `eboo_feeds` VALUES(77, 1, 'Please Do Not Leave A Message: Why Millennials Hate Voice Mail ', '"When it comes to voice mail, they''re just over it," says Jane Buckingham, a trend expert. But it''s still important at work, so younger generations will have to learn what to do after the beep.', 'http://www.npr.org/blogs/alltechconsidered/2014/10/23/358301467/please-do-not-leave-a-message-why-millennials-hate-voice-mail?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:22:00', 0, '2014-10-24 21:20:24', 1);
INSERT INTO `eboo_feeds` VALUES(78, 1, 'Mark Zuckerberg Shows Off His Mandarin Chinese Skills', 'The Facebook co-founder and CEO spoke at Tsinghua University in Beijing for about 30 minutes. In Mandarin. His audience liked it.', 'http://www.npr.org/blogs/alltechconsidered/2014/10/23/358300880/mark-zuckerberg-shows-off-his-mandarin-chinese-skills?utm_medium=RSS&utm_campaign=business', '2014-10-23 12:00:00', 0, '2014-10-24 21:20:24', 1);
INSERT INTO `eboo_feeds` VALUES(79, 1, 'You''re Enjoying Low Gas Prices, But Is It Really A Good Sign? ', 'All around the country, drivers are seeing signs that gas prices are depressed. Those drops helped hold down the latest consumer price index. But economists worry about too much of a good thing.', 'http://www.npr.org/2014/10/23/358062856/youre-enjoying-low-gas-prices-but-is-it-really-a-good-sign?utm_medium=RSS&utm_campaign=business', '2014-10-23 10:52:00', 0, '2014-10-24 21:20:24', 1);
INSERT INTO `eboo_feeds` VALUES(80, 1, 'To Get Women To Work In Computer Science, Schools Get Them To Class', 'In 1984, the percentage of women studying computer science flattened, and then plunged. Computer science programs are trying to get that number back up.', 'http://www.npr.org/2014/10/23/358238982/to-get-women-to-work-in-computer-science-schools-get-them-to-class?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:47:00', 0, '2014-10-24 21:20:24', 1);
INSERT INTO `eboo_feeds` VALUES(81, 1, 'What The New Factory Worker Should Know', 'In previous generations, manufacturing jobs were dirty, dangerous and low-skill. The new factory jobs are almost all clean, require increasingly higher skills and take very few people to do them.', 'http://www.npr.org/2014/10/23/358238891/what-the-new-factory-worker-should-know?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:47:00', 0, '2014-10-24 21:20:24', 1);
INSERT INTO `eboo_feeds` VALUES(82, 1, 'No Mere Merry-Go-Round: Ohio Carousel Maker Carves From Scratch', 'Wooden carousels, with hand-carved and painted horses, seem like a relic of the past. But Carousel Works in Mansfield, Ohio, is one of a few companies still making them to order.', 'http://www.npr.org/2014/10/23/358051063/no-mere-merry-go-round-ohio-carousel-maker-carves-from-scratch?utm_medium=RSS&utm_campaign=business', '2014-10-23 04:47:00', 0, '2014-10-24 21:20:24', 1);
INSERT INTO `eboo_feeds` VALUES(83, 2, 'VIDEO: Spirit of revolution simmers in Kiev', 'Steve Rosenberg visits Kiev to see how the recent violence in Ukraine has had an impact on who is running for parliament.', 'http://www.bbc.co.uk/news/world-europe-29744259#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 11:08:42', 0, '2014-10-24 21:20:25', 1);
INSERT INTO `eboo_feeds` VALUES(84, 2, 'VIDEO: CCTV of Ottawa shooting suspect', 'The Royal Canadian Mounted Police (RCMP) releases video footage of the suspected gunman arriving at the country''s parliament building during Wednesday''s shooting incident.', 'http://www.bbc.co.uk/news/world-us-canada-29749775#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 08:28:51', 0, '2014-10-24 21:20:25', 1);
INSERT INTO `eboo_feeds` VALUES(85, 2, 'VIDEO: Ebola: Scenes of horror in Liberia', 'There have now been more than 4,800 reported deaths from Ebola, with Liberia the worst affected country. Around 50 new cases are being reported every day in the capital Monrovia.', 'http://www.bbc.co.uk/news/world-africa-29748701#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 09:36:43', 6, '2014-10-24 21:20:25', 1);
INSERT INTO `eboo_feeds` VALUES(86, 2, 'VIDEO: Exhibition on a changing Afghanistan', 'The Imperial War Museum''s latest exhibition focuses on Afghanistan as British troops prepare to leave.', 'http://www.bbc.co.uk/news/uk-29744048#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 11:45:29', 0, '2014-10-24 21:20:25', 1);
INSERT INTO `eboo_feeds` VALUES(87, 2, 'VIDEO: Art on motorbikes in Kampala', 'Artists in Uganda are using motorbikes to carry and display street art installations as part of the Kampala Contemporary Art Festival.', 'http://www.bbc.co.uk/news/world-africa-29748703#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 07:19:26', 1, '2014-10-24 21:20:25', 1);
INSERT INTO `eboo_feeds` VALUES(88, 2, 'AUDIO: ''19 relatives have died from Ebola''', 'A pastor who says he has lost 19 members of his family to Ebola has been speaking to the BBC.', 'http://www.bbc.co.uk/news/world-africa-29749689#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 07:08:54', 0, '2014-10-24 21:20:25', 1);
INSERT INTO `eboo_feeds` VALUES(89, 2, 'VIDEO: Daily life goes on inside Kashmir', 'Daily life and trade is continuing in Kashmir despite the recent violence being the worst in a decade, as BBC Urdu''s Shumaila Jaffery reports.', 'http://www.bbc.co.uk/news/world-asia-29739308#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-23 10:43:16', 2, '2014-10-24 21:20:25', 1);
INSERT INTO `eboo_feeds` VALUES(90, 1, 'Iranian Entrepreneurs Make Pitches That Are Just Practice, For Now', 'Young Iranians are brimming with ideas for tech startups. But extensive financial sanctions facing their country prevent them from entering the global marketplace.', 'http://www.npr.org/blogs/parallels/2014/10/25/356646970/iranian-entrepreneurs-make-pitches-that-are-just-practice-for-now?utm_medium=RSS&utm_campaign=business', '2014-10-25 01:58:00', 7, '2014-10-26 22:23:56', 1);
INSERT INTO `eboo_feeds` VALUES(91, 1, 'Tech Week: Voice Mail Hang-Ups, Apple Pay And Zuckerberg''s Chinese', 'In this week''s roundup, Apple rolls out its mobile payment system but confronts a security test in China, the problem with voice mail messages and Mark Zuckerberg shows off his Mandarin.', 'http://www.npr.org/blogs/alltechconsidered/2014/10/24/358554011/tech-week-voice-mail-hang-ups-apple-pay-and-zuckerbergs-chinese?utm_medium=RSS&utm_campaign=business', '2014-10-25 05:40:00', 8, '2014-10-26 22:23:56', 1);
INSERT INTO `eboo_feeds` VALUES(92, 1, 'Tracing A Gin-Soaked Trail In London', 'Around the world, new gin distilleries are popping up like mushrooms after a rain. NPR traces the boom to its historic roots in London, which once had 250 distilleries within the city limits alone.', 'http://www.npr.org/blogs/thesalt/2014/10/16/356708745/tracing-a-gin-soaked-trail-in-london?utm_medium=RSS&utm_campaign=business', '2014-10-25 05:35:00', 3, '2014-10-26 22:23:56', 1);
INSERT INTO `eboo_feeds` VALUES(93, 2, 'VIDEO: Russia turns back clocks for good', 'Russia prepares to turn back the clocks, moving the country on to "permanent winter time" - a move which has divided opinion', 'http://www.bbc.co.uk/news/world-europe-29774041#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-25 10:48:49', 2, '2014-10-26 22:23:57', 1);
INSERT INTO `eboo_feeds` VALUES(94, 2, 'VIDEO: Meeting Afghanistan''s ''thin blue line''', 'By the end of the year, British forces will have ended combat operations in Afghanistan leaving Afghan security forces to fight the Taliban threat.', 'http://www.bbc.co.uk/news/world-asia-29775923#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-25 11:47:59', 9, '2014-10-26 22:23:57', 1);
INSERT INTO `eboo_feeds` VALUES(95, 1, 'Competition Brews In the World Of Mobile Payments', 'With much fanfare earlier this month Apple launched its mobile wallet â€” Apple Pay. Now CVS and Rite Aid have disabled Apple Pay from working in their stores.', 'http://www.npr.org/2014/10/27/359403483/competition-brews-in-the-world-of-mobile-payments?utm_medium=RSS&utm_campaign=business', '2014-10-27 04:22:00', 4, '2014-10-28 02:39:02', 1);
INSERT INTO `eboo_feeds` VALUES(96, 1, 'CVS Pulls Apple Pay, And Many See A Fight Over Mobile Wallets', 'Rite Aid took the same step, leading many observers to note that the two companies are part of a group of retailers that''s developing its own payment system, called CurrentC.', 'http://www.npr.org/blogs/thetwo-way/2014/10/27/359350099/cvs-pulls-apple-play-and-many-see-a-fight-over-mobile-wallets?utm_medium=RSS&utm_campaign=business', '2014-10-27 02:23:00', 2, '2014-10-28 02:39:02', 1);
INSERT INTO `eboo_feeds` VALUES(97, 1, 'Witches, Vampires And Pirates: 5 Years Of America''s Most Popular Costumes', 'Thanks to shows like <em>The Walking Dead</em> and movies like <em>World War Z</em>, zombies are climbing the costume charts. The undead have risen from the 13th most popular costume up to No. 4 in just five years.', 'http://www.npr.org/blogs/money/2014/10/27/359324848/witches-vampires-and-pirates-5-years-of-americas-most-popular-costumes?utm_medium=RSS&utm_campaign=business', '2014-10-27 12:59:00', 2, '2014-10-28 02:39:02', 1);
INSERT INTO `eboo_feeds` VALUES(98, 1, 'Chiquita Fruit Company Is Bought By Two Brazilian Firms', 'The Charlotte, N.C.-based company traces its roots to the 1870s, when American entrepreneurs brought bananas to U.S. consumers from the Caribbean.', 'http://www.npr.org/blogs/thetwo-way/2014/10/27/359334087/chiquita-fruit-company-is-bought-by-brazilian-firms?utm_medium=RSS&utm_campaign=business', '2014-10-27 11:29:00', 1, '2014-10-28 02:39:02', 1);
INSERT INTO `eboo_feeds` VALUES(99, 1, 'Love Is Saying ''Sari'': The Quest To Save A South Asian Tradition', 'Commentator Sandip Roy says the traditional sari has been falling out of fashion in the new India, but designers are turning to pop art prints and other changes to boost its appeal.', 'http://www.npr.org/blogs/parallels/2014/10/27/358366979/love-is-saying-sari-the-quest-to-save-a-south-asian-tradition?utm_medium=RSS&utm_campaign=business', '2014-10-27 04:36:00', 0, '2014-10-28 02:39:02', 1);
INSERT INTO `eboo_feeds` VALUES(100, 2, 'VIDEO: Meet Kenya''s young female boxers', 'Kenya''s schoolgirls are fighting back against sexual violence - by learning how to box in self-defence classes.', 'http://www.bbc.co.uk/news/world-africa-29758794#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-27 11:00:39', 0, '2014-10-28 02:39:03', 1);
INSERT INTO `eboo_feeds` VALUES(101, 2, 'VIDEO: Egypt FM on Greste imprisonment', 'Egypt''s foreign minister Sameh Shukry talks to the BBC''s Frank Gardner about the fight against so-called Islamic State and the convictions of three Al-Jazeera journalists.', 'http://www.bbc.co.uk/news/world-middle-east-29792942#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-27 06:31:58', 0, '2014-10-28 02:39:03', 1);
INSERT INTO `eboo_feeds` VALUES(102, 2, 'VIDEO: What goes on in a politician''s mind?', 'An introduction to the 2014 US mid-term election through the eyes of "you", a hypothetical senator running for re-election.', 'http://www.bbc.co.uk/news/world-us-canada-29793262#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-27 11:45:28', 0, '2014-10-28 02:39:03', 1);
INSERT INTO `eboo_feeds` VALUES(103, 2, 'VIDEO: Sting''s Broadway musical debut', 'British pop star Sting makes his debut as a Broadway composer and lyricist in a musical called The Last Ship.', 'http://www.bbc.co.uk/news/entertainment-arts-29785786#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-27 01:26:13', 0, '2014-10-28 02:39:03', 1);
INSERT INTO `eboo_feeds` VALUES(104, 2, 'VIDEO: Death penalty call for ferry captain', 'South Korean prosecutors are seeking the death penalty for the captain of the Sewol ferry, which sank in April killing more than 300 people.', 'http://www.bbc.co.uk/news/world-asia-29782907#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-27 03:27:23', 0, '2014-10-28 02:39:03', 1);
INSERT INTO `eboo_feeds` VALUES(105, 2, 'VIDEO: Brazil re-elects Dilma Rousseff', 'President Dilma Rousseff has been re-elected for a second term, after securing more than 51% of votes in a closely-fought election.', 'http://www.bbc.co.uk/news/world-latin-america-29782757#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-27 07:43:56', 0, '2014-10-28 02:39:03', 1);
INSERT INTO `eboo_feeds` VALUES(106, 2, 'VIDEO: Singapore''s aquatic robot challenge', 'Engineering students from around the world compete in Singapore to create an unmanned boat.', 'http://www.bbc.co.uk/news/technology-29781969#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-27 08:14:54', 0, '2014-10-28 02:39:03', 1);
INSERT INTO `eboo_feeds` VALUES(107, 2, 'VIDEO: Tearful tribute to SA footballer', 'South Africa''s football coach Ephraim Mashaba has paid tribute to captain Senzo Meyiwa, who died after being shot by intruders.', 'http://www.bbc.co.uk/news/world-africa-29782905#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-27 10:48:33', 0, '2014-10-28 02:39:03', 1);
INSERT INTO `eboo_feeds` VALUES(108, 2, 'VIDEO: Keeping the Taliban at bay in Kabul', 'As British and US troops end combat operations in Afghanistan, millions of Afghans are now left wondering if their country and its capital Kabul will remain out of Taliban hands.', 'http://www.bbc.co.uk/news/world-asia-29782899#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-27 07:54:09', 0, '2014-10-28 02:39:03', 1);
INSERT INTO `eboo_feeds` VALUES(109, 2, 'VIDEO: Australia Muslims hold mosque open day', 'Following a reported rise in attacks on Muslims in Australia, mosques hold an open day to challenge misconceptions about Islam.', 'http://www.bbc.co.uk/news/world-asia-29781961#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-27 02:22:42', 1, '2014-10-28 02:39:03', 1);
INSERT INTO `eboo_feeds` VALUES(110, 1, 'Marvel Studios To Diversify Its Movies', 'The comic juggernaut announced Tuesday that one of its upcoming films will have a female lead, and another will be headlined by a black man.', 'http://www.npr.org/blogs/thetwo-way/2014/10/28/359680603/marvel-studios-to-diversify-its-movies?utm_medium=RSS&utm_campaign=business', '2014-10-28 08:43:00', 9, '2014-10-29 01:52:31', 1);
INSERT INTO `eboo_feeds` VALUES(111, 1, 'Firm Buys Big Bike-Share Service; Expansion And Higher Rates Seen', 'Announcing the deal today, the company said it would expand its service and raise the annual fee to $149 for its New York customers. Alta also operates in other large cities.', 'http://www.npr.org/blogs/thetwo-way/2014/10/28/359658191/firm-buys-big-bike-share-service-expansion-and-higher-rates-seen?utm_medium=RSS&utm_campaign=business', '2014-10-28 06:23:00', 15, '2014-10-29 01:52:31', 1);
INSERT INTO `eboo_feeds` VALUES(112, 1, 'The Next Shirt You Buy May Say ''Made In Ethiopia.'' Here''s Why', 'The garment industry is still seen as a good partner for jump-starting the economies of undeveloped nations. Manufacturers entering these countries say they''re trying to treat workers more ethically.', 'http://www.npr.org/2014/10/28/359655632/the-next-shirt-you-buy-may-say-made-in-ethiopia-heres-why?utm_medium=RSS&utm_campaign=business', '2014-10-28 06:16:00', 2, '2014-10-29 01:52:31', 1);
INSERT INTO `eboo_feeds` VALUES(113, 1, 'Why Does Saudi Arabia Seem So Comfortable With Falling Oil Prices?', 'Normally, the "central banker of oil" would slow production to push up prices. Not so now. Some say it''s a geopolitical tactic aimed at Russia and Iran; others say it''s just protecting market share.', 'http://www.npr.org/blogs/parallels/2014/10/28/359601443/why-does-saudi-arabia-seem-so-comfortable-with-falling-oil-prices?utm_medium=RSS&utm_campaign=business', '2014-10-28 02:15:00', 2, '2014-10-29 01:52:31', 1);
INSERT INTO `eboo_feeds` VALUES(114, 1, 'Safety Regulators Turn Attention To Faulty Air Bags', 'Honda reports at least two deaths related to defective inflators in air bags. The air bags, made by Japanese supplier Takata, are in Toyota and other automakers'' vehicles, too.', 'http://www.npr.org/2014/10/28/359512055/safety-regulators-turn-attention-to-faulty-air-bags?utm_medium=RSS&utm_campaign=business', '2014-10-28 04:39:00', 2, '2014-10-29 01:52:31', 1);
INSERT INTO `eboo_feeds` VALUES(115, 1, 'Behold The Entrenched â€” And Reviled â€” Annual Review', 'Performance review season is nearing, and if you''re like most people, there''s no celebrating that. Studies show that 60 to 90 percent of employees dislike the ritual. So some firms are backing away.', 'http://www.npr.org/2014/10/28/358636126/behold-the-entrenched-and-reviled-annual-review?utm_medium=RSS&utm_campaign=business', '2014-10-28 04:39:00', 1, '2014-10-29 01:52:31', 1);
INSERT INTO `eboo_feeds` VALUES(116, 2, 'VIDEO: ''I escaped death in N Korea''', 'A young girl who escaped North Korea has spoken to the BBC about the violence and starvation she experienced when she lived there.', 'http://www.bbc.co.uk/news/world-south-asia-29809557#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 08:43:38', 0, '2014-10-29 01:52:32', 1);
INSERT INTO `eboo_feeds` VALUES(117, 2, 'VIDEO: Moment unmanned rocket explodes', 'An unmanned cargo rocket has exploded seconds after lifting off from a commercial launch pad in Virginia.', 'http://www.bbc.co.uk/news/world-us-canada-29813109#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 11:44:24', 1, '2014-10-29 01:52:32', 1);
INSERT INTO `eboo_feeds` VALUES(118, 2, 'VIDEO: Hawaiian residents flee lava flow', 'Residents of a Hawaiian village threatened by lava have begun evacuating as the flow reaches the first property in its path.', 'http://www.bbc.co.uk/news/world-us-canada-29809558#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 09:30:56', 1, '2014-10-29 01:52:32', 1);
INSERT INTO `eboo_feeds` VALUES(119, 2, 'VIDEO: Video shows massive Kobane blast', 'The BBC''s Jiyar Gol has witnessed a massive blast hit the Syrian city of Kobane - despite militants'' claims that the battle is "nearly over".', 'http://www.bbc.co.uk/news/world-middle-east-29804030#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 12:53:39', 2, '2014-10-29 01:52:32', 1);
INSERT INTO `eboo_feeds` VALUES(120, 2, 'VIDEO: The assassination of Indira Gandhi', 'Thirty years ago, Indiaâ€™s Prime Minister Indira Gandhi was murdered in her garden. One of her aides saw everything.', 'http://www.bbc.co.uk/news/world-29725430#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 09:20:52', 1, '2014-10-29 01:52:32', 1);
INSERT INTO `eboo_feeds` VALUES(121, 2, 'VIDEO: ''I left my fiancÃ©e to be a barber''', 'A hairdresser in Egypt says she is proving people in Egypt who think a woman who works as a hairdresser is not decent wrong.', 'http://www.bbc.co.uk/news/world-africa-29792553#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 06:49:38', 1, '2014-10-29 01:52:32', 1);
INSERT INTO `eboo_feeds` VALUES(122, 2, 'VIDEO: Sodomy trial ''politically motivated''', 'Jennifer Pak reports from outside the Malaysia court which is hearing opposition leader Anwar Ibrahim''s final appeal against his sodomy conviction.', 'http://www.bbc.co.uk/news/world-asia-29798211#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 10:15:28', 1, '2014-10-29 01:52:32', 1);
INSERT INTO `eboo_feeds` VALUES(123, 2, 'VIDEO: Mexico unearths ''new mass grave''', 'Mexican authorities searching for 43 students who disappeared after a protest last month are investigating a suspected mass grave.', 'http://www.bbc.co.uk/news/world-latin-america-29797271#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 06:06:57', 1, '2014-10-29 01:52:32', 1);
INSERT INTO `eboo_feeds` VALUES(124, 2, 'VIDEO: ''Stalemate'' a month on from HK demos', 'Hong Kong''s civil disobedience movement began exactly one month ago.', 'http://www.bbc.co.uk/news/world-asia-29798208#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 06:53:52', 2, '2014-10-29 01:52:32', 1);
INSERT INTO `eboo_feeds` VALUES(125, 2, 'VIDEO: Turkey PM ''will help coalition forces''', 'Turkish Prime Minister, Ahmet Davutoglu, has told the BBC''s Lyse Doucet that Turkey will continue to help coalition forces if there is a co-ordinated plan for the creation of a ''''democratic'''' Syria.', 'http://www.bbc.co.uk/news/world-europe-29797270#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 06:12:55', 2, '2014-10-29 01:52:32', 1);
INSERT INTO `eboo_feeds` VALUES(126, 2, 'VIDEO: Greek shipwreck yields ancient finds', 'Divers have discovered cargo from an ancient Greek shipwreck near the Aeolian Islands off the coast of Italy.', 'http://www.bbc.co.uk/news/world-europe-29797269#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 04:07:32', 1, '2014-10-29 01:52:32', 1);
INSERT INTO `eboo_feeds` VALUES(127, 2, 'VIDEO: Ebola sufferers ''robbed of dignity''', 'The BBC''s Gabriel Gatehouse reports from Monrovia, the capital of Liberia, on the devastating impact of Ebola upon its people.', 'http://www.bbc.co.uk/news/world-africa-29797263#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 12:23:35', 1, '2014-10-29 01:52:32', 1);
INSERT INTO `eboo_feeds` VALUES(128, 2, 'VIDEO: Pakistan polio plan ''a disaster''', 'The polio vaccination programme in Pakistan has been described as ''''a disaster'''' by monitors reporting to the World Health Organization.', 'http://www.bbc.co.uk/news/world-asia-29797265#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 04:06:10', 25, '2014-10-29 01:52:32', 1);
INSERT INTO `eboo_feeds` VALUES(129, 2, 'VIDEO: Is this the work of Caravaggio?', 'A painting sold by Sotheby''s in London for Â£42,000 is at the centre of a legal battle, amid claims it could be the work of Italian baroque master Caravaggio with a much greater value of around Â£11m.', 'http://www.bbc.co.uk/news/entertainment-arts-29798049#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-10-28 06:08:20', 3, '2014-10-29 01:52:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eboo_publication`
--

CREATE TABLE `eboo_publication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_name` varchar(500) NOT NULL,
  `publication_image` varchar(500) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `eboo_publication`
--

INSERT INTO `eboo_publication` VALUES(1, 'FOXNEWS', 'images/newname.png', '2014-10-19 09:04:31', 1);
INSERT INTO `eboo_publication` VALUES(2, 'ALLRECIPIES', 'images/ALL RECIPIES214235346.png', '2014-10-19 09:00:55', 1);
INSERT INTO `eboo_publication` VALUES(3, 'GOTHAMIST', 'images/FOX12.png', '2014-10-19 09:05:51', 1);
INSERT INTO `eboo_publication` VALUES(4, 'YAHOO1', 'images/YAHOO1.png', '2014-10-08 17:13:36', 0);
INSERT INTO `eboo_publication` VALUES(5, 'chumma', 'images/123456.png', '2014-10-08 12:45:38', 0);

-- --------------------------------------------------------

--
-- Table structure for table `eboo_rss`
--

CREATE TABLE `eboo_rss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publication_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `rss_link` varchar(500) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `eboo_rss`
--

INSERT INTO `eboo_rss` VALUES(1, 1, 1, 'http://www.npr.org/rss/rss.php?id=1006', '2014-10-09 09:53:31', 1);
INSERT INTO `eboo_rss` VALUES(2, 2, 2, 'http://feeds.bbci.co.uk/news/video_and_audio/world/rss.xml?edition=uk', '2014-10-09 11:21:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eboo_serverside_error`
--

CREATE TABLE `eboo_serverside_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_type` varchar(500) NOT NULL,
  `page_name` varchar(500) NOT NULL,
  `error` text NOT NULL,
  `browser` text NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `eboo_serverside_error`
--

INSERT INTO `eboo_serverside_error` VALUES(1, 'admin', 'upload.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''YAHOO'',''images/newname.png'',''2014-10-08 09:41:56'',''1'')'' at line 1', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-08 09:41:56', 1);
INSERT INTO `eboo_serverside_error` VALUES(2, 'admin', 'updatefeedlist.php', 'Unknown column ''publication'' in ''field list''', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 06:52:08', 1);
INSERT INTO `eboo_serverside_error` VALUES(3, 'admin', 'updatefeedlist.php', 'Unknown column ''publication'' in ''field list''', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 07:02:48', 1);
INSERT INTO `eboo_serverside_error` VALUES(4, 'admin', 'updatefeedlist.php', 'Unknown column ''publication'' in ''field list''', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 07:03:43', 1);
INSERT INTO `eboo_serverside_error` VALUES(5, 'admin', 'updatefeedlist.php', 'Unknown column ''publication'' in ''field list''', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 07:07:09', 1);
INSERT INTO `eboo_serverside_error` VALUES(6, 'admin', 'updatefeedlist.php', 'Unknown column ''publication'' in ''field list''', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 07:08:04', 1);
INSERT INTO `eboo_serverside_error` VALUES(7, 'admin', 'updatefeedlist.php', 'Unknown column ''publication'' in ''field list''', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 07:09:07', 1);
INSERT INTO `eboo_serverside_error` VALUES(8, 'admin', 'updatefeedlist.php', 'Unknown column ''publication'' in ''field list''', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 07:10:55', 1);
INSERT INTO `eboo_serverside_error` VALUES(9, 'admin', 'updatefeedlist.php', 'Unknown column ''publication'' in ''field list''', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 07:16:26', 1);
INSERT INTO `eboo_serverside_error` VALUES(10, 'admin', 'updatefeedlist.php', 'Unknown column ''publication'' in ''field list''', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 07:16:58', 1);
INSERT INTO `eboo_serverside_error` VALUES(11, 'admin', 'showfeedlist.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''where id=''1'' and  status=''1'''' at line 1', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 07:45:18', 1);
INSERT INTO `eboo_serverside_error` VALUES(12, 'admin', 'showfeedlist.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''where id=''1'' and  status=''1'''' at line 1', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 07:45:47', 1);
INSERT INTO `eboo_serverside_error` VALUES(13, 'admin', 'showfeedlist.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''where id=''1'' and  status=''1'''' at line 1', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 07:46:56', 1);
INSERT INTO `eboo_serverside_error` VALUES(14, 'admin', 'dailyupdatepage.php', 'Table ''eboo.category'' doesn''t exist', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 09:54:33', 1);
INSERT INTO `eboo_serverside_error` VALUES(15, 'admin', 'feedsync.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''desc limit 1'' at line 1', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 10:51:22', 1);
INSERT INTO `eboo_serverside_error` VALUES(16, 'admin', 'feedsync.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''desc limit 1'' at line 1', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 10:51:29', 1);
INSERT INTO `eboo_serverside_error` VALUES(17, 'admin', 'feedsync.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''desc limit 1'' at line 1', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 10:52:44', 1);
INSERT INTO `eboo_serverside_error` VALUES(18, 'admin', 'feedsync.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''order by desc'' at line 1', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:32.0) Gecko/20100101 Firefox/32.0', '2014-10-09 10:53:44', 1);
INSERT INTO `eboo_serverside_error` VALUES(19, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36', '2014-10-18 02:45:24', 1);
INSERT INTO `eboo_serverside_error` VALUES(20, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36', '2014-10-18 02:45:30', 1);
INSERT INTO `eboo_serverside_error` VALUES(21, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36', '2014-10-18 02:46:25', 1);
INSERT INTO `eboo_serverside_error` VALUES(22, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36', '2014-10-18 02:46:27', 1);
INSERT INTO `eboo_serverside_error` VALUES(23, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36', '2014-10-18 02:46:29', 1);
INSERT INTO `eboo_serverside_error` VALUES(24, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:33.0) Gecko/20100101 Firefox/33.0', '2014-10-18 02:46:55', 1);
INSERT INTO `eboo_serverside_error` VALUES(25, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:33.0) Gecko/20100101 Firefox/33.0', '2014-10-18 02:47:43', 1);
INSERT INTO `eboo_serverside_error` VALUES(26, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:33.0) Gecko/20100101 Firefox/33.0', '2014-10-18 02:47:45', 1);
INSERT INTO `eboo_serverside_error` VALUES(27, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:33.0) Gecko/20100101 Firefox/33.0', '2014-10-18 02:47:46', 1);
INSERT INTO `eboo_serverside_error` VALUES(28, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:33.0) Gecko/20100101 Firefox/33.0', '2014-10-18 02:47:47', 1);
INSERT INTO `eboo_serverside_error` VALUES(29, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36', '2014-10-18 02:48:27', 1);
INSERT INTO `eboo_serverside_error` VALUES(30, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:33.0) Gecko/20100101 Firefox/33.0', '2014-10-18 02:49:03', 1);
INSERT INTO `eboo_serverside_error` VALUES(31, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:33.0) Gecko/20100101 Firefox/33.0', '2014-10-18 02:49:18', 1);
INSERT INTO `eboo_serverside_error` VALUES(32, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''rand'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:33.0) Gecko/20100101 Firefox/33.0', '2014-10-18 02:49:21', 1);
INSERT INTO `eboo_serverside_error` VALUES(33, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''%''''%  order by session_date desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171393392)', '2014-10-18 08:55:12', 1);
INSERT INTO `eboo_serverside_error` VALUES(34, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''%''''%  order by session_date desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (173283744)', '2014-10-18 08:57:15', 1);
INSERT INTO `eboo_serverside_error` VALUES(35, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''%''''%  order by session_date desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (173283744)', '2014-10-18 08:57:22', 1);
INSERT INTO `eboo_serverside_error` VALUES(36, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''%''''%  order by session_date desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171470208)', '2014-10-18 08:58:57', 1);
INSERT INTO `eboo_serverside_error` VALUES(37, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''%''ambu''%  order by session_date desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171374704)', '2014-10-18 09:00:54', 1);
INSERT INTO `eboo_serverside_error` VALUES(38, 'admin', 'feedsearch.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''%''Ambu''%  order by session_date desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171374704)', '2014-10-18 09:00:54', 1);
INSERT INTO `eboo_serverside_error` VALUES(39, 'admin', 'dailyupdatepage.php', 'Column count doesn''t match value count at row 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36', '2014-10-19 22:59:06', 1);
INSERT INTO `eboo_serverside_error` VALUES(40, 'admin', 'dailyupdatepage.php', 'Column count doesn''t match value count at row 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36', '2014-10-19 22:59:26', 1);
INSERT INTO `eboo_serverside_error` VALUES(41, 'application', 'trending.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''where pubdate between (2014-10-17 and 2014--10-19)  order by clickcount desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171286464)', '2014-10-19 23:38:53', 1);
INSERT INTO `eboo_serverside_error` VALUES(42, 'application', 'trending.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''where pubdate between (2014-10-17 and 2014--10-19)  order by clickcount desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (158744160)', '2014-10-19 23:40:13', 1);
INSERT INTO `eboo_serverside_error` VALUES(43, 'application', 'trending.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''where pubdate between (2014-10-17 and 2014--10-19)  order by clickcount desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171037840)', '2014-10-19 23:40:41', 1);
INSERT INTO `eboo_serverside_error` VALUES(44, 'application', 'trending.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''where pubdate between (2014-10-17 and 2014--10-19)  order by clickcount desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171298128)', '2014-10-19 23:41:46', 1);
INSERT INTO `eboo_serverside_error` VALUES(45, 'application', 'trending.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''where pubdate between (2014-10-17 and 2014--10-19)  order by clickcount desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171415120)', '2014-10-19 23:42:39', 1);
INSERT INTO `eboo_serverside_error` VALUES(46, 'application', 'trending.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''where pubdate between (2014-10-17 and 2014--10-19)  order by clickcount desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171518144)', '2014-10-19 23:43:26', 1);
INSERT INTO `eboo_serverside_error` VALUES(47, 'application', 'trending.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''where pubdate between (2014-10-17 and 2014--10-19)  order by clickcount desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171360480)', '2014-10-19 23:44:12', 1);
INSERT INTO `eboo_serverside_error` VALUES(48, 'application', 'trending.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''where pubdate between (2014-10-17 and 2014--10-19)  order by clickcount desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171478272)', '2014-10-19 23:46:59', 1);
INSERT INTO `eboo_serverside_error` VALUES(49, 'application', 'trending.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''where pubdate between (2014-10-17 and 2014--10-19)  order by clickcount desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171380592)', '2014-10-19 23:49:58', 1);
INSERT INTO `eboo_serverside_error` VALUES(50, 'application', 'trending.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''where pubdate between (2014-10-17 and 2014--10-19)  order by clickcount desc'' at line 1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36', '2014-10-19 23:50:23', 1);
INSERT INTO `eboo_serverside_error` VALUES(51, 'application', 'trending.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''order by clickcount desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171312848)', '2014-10-20 00:00:51', 1);
INSERT INTO `eboo_serverside_error` VALUES(52, 'application', 'trending.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''order by clickcount desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171065296)', '2014-10-20 00:05:53', 1);
INSERT INTO `eboo_serverside_error` VALUES(53, 'application', 'trending.php', 'You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''order by clickcount desc'' at line 1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (158968320)', '2014-10-20 00:13:14', 1);
INSERT INTO `eboo_serverside_error` VALUES(54, 'application', 'clickcount.php', 'Unknown column ''u_id'' in ''field list''', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171110608)', '2014-10-30 01:42:29', 1);
INSERT INTO `eboo_serverside_error` VALUES(55, 'application', 'clickcount.php', 'Unknown column ''u_id'' in ''field list''', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171110608)', '2014-10-30 01:42:33', 1);
INSERT INTO `eboo_serverside_error` VALUES(56, 'application', 'clickcount.php', 'Unknown column ''u_id'' in ''field list''', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171110608)', '2014-10-30 01:42:48', 1);
INSERT INTO `eboo_serverside_error` VALUES(57, 'application', 'clickcount.php', 'Unknown column ''u_id'' in ''field list''', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (171110608)', '2014-10-30 01:42:52', 1);
INSERT INTO `eboo_serverside_error` VALUES(58, 'application', 'userinsert.php', 'Duplicate entry ''4'' for key ''PRIMARY''', 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Mobile/10B141 (158921520)', '2014-10-30 07:00:10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eboo_users`
--

CREATE TABLE `eboo_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL,
  `location` varchar(1000) NOT NULL,
  `city` varchar(500) NOT NULL,
  `state` varchar(500) NOT NULL,
  `pincode` int(11) NOT NULL,
  `country` varchar(500) NOT NULL,
  `hardware_info` varchar(500) NOT NULL,
  `software_version` varchar(500) NOT NULL,
  `fb_handle` varchar(500) NOT NULL,
  `tumblr_handle` varchar(500) NOT NULL,
  `linkedin_handle` varchar(500) NOT NULL,
  `session_date` datetime NOT NULL,
  `first_usage` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `eboo_users`
--

INSERT INTO `eboo_users` VALUES(2, 'ram@gmail.com', '', '', '', 0, '', '', '', '', '', '', '2014-10-30 04:21:14', 0, 1);
INSERT INTO `eboo_users` VALUES(3, 'eboo1@gmail.com', '', '', '', 0, '', '', '', '', '', '', '2014-10-30 04:28:42', 0, 1);
INSERT INTO `eboo_users` VALUES(4, 'eboo@gmail.com', '', '', '', 0, '', '', '', '', '', '', '2014-10-30 04:18:18', 0, 1);
INSERT INTO `eboo_users` VALUES(5, 'rameshtest@gmail.com', '', '', '', 0, '', '', '', '', '', '', '2014-10-30 07:00:17', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `eboo_user_clicked_data`
--

CREATE TABLE `eboo_user_clicked_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `feeds_id` int(11) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `eboo_user_clicked_data`
--

INSERT INTO `eboo_user_clicked_data` VALUES(1, 4, 111, '2014-10-30 01:45:34', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(2, 4, 128, '2014-10-30 01:45:37', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(3, 4, 110, '2014-10-30 01:45:39', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(4, 4, 128, '2014-10-30 01:45:41', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(5, 4, 119, '2014-10-30 02:12:49', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(6, 4, 119, '2014-10-30 02:12:52', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(7, 4, 118, '2014-10-30 02:12:55', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(8, 4, 113, '2014-10-30 02:14:08', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(9, 4, 112, '2014-10-30 02:14:11', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(10, 4, 122, '2014-10-30 02:14:16', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(11, 4, 123, '2014-10-30 02:14:19', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(12, 4, 127, '2014-10-30 02:16:38', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(13, 4, 129, '2014-10-30 02:16:54', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(14, 4, 129, '2014-10-30 02:16:58', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(15, 4, 129, '2014-10-30 02:17:02', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(16, 4, 115, '2014-10-30 03:41:46', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(17, 4, 125, '2014-10-30 03:41:50', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(18, 4, 126, '2014-10-30 03:41:53', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(19, 4, 124, '2014-10-30 03:42:24', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(20, 4, 120, '2014-10-30 03:47:15', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(21, 4, 125, '2014-10-30 03:47:48', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(22, 4, 117, '2014-10-30 03:49:56', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(23, 4, 121, '2014-10-30 03:50:10', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(24, 0, 0, '2014-10-30 03:51:18', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(25, 4, 2, '2014-10-30 03:55:28', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(26, 4, 124, '2014-10-30 03:58:18', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(27, 4, 128, '2014-10-30 04:00:38', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(28, 4, 128, '2014-10-30 04:02:21', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(29, 4, 128, '2014-10-30 04:05:55', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(30, 4, 128, '2014-10-30 04:06:01', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(31, 4, 128, '2014-10-30 04:06:04', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(32, 4, 128, '2014-10-30 04:07:58', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(33, 4, 128, '2014-10-30 04:09:53', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(34, 4, 128, '2014-10-30 04:09:58', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(35, 4, 128, '2014-10-30 04:10:12', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(36, 4, 128, '2014-10-30 04:10:15', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(37, 4, 128, '2014-10-30 04:10:57', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(38, 4, 128, '2014-10-30 04:12:12', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(39, 4, 128, '2014-10-30 04:12:25', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(40, 0, 111, '2014-10-30 04:19:17', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(41, 0, 111, '2014-10-30 04:19:32', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(42, 0, 111, '2014-10-30 04:21:35', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(43, 3, 110, '2014-10-30 05:23:50', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(44, 3, 114, '2014-10-30 05:25:45', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(45, 3, 114, '2014-10-30 05:25:50', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(46, 3, 110, '2014-10-30 05:28:09', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(47, 3, 128, '2014-10-30 06:44:55', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(48, 5, 128, '2014-10-30 07:11:01', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(49, 5, 128, '2014-10-30 07:11:16', 1);
INSERT INTO `eboo_user_clicked_data` VALUES(50, 5, 111, '2014-10-30 07:11:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eboo_user_saved_feed`
--

CREATE TABLE `eboo_user_saved_feed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `rss_link_id` int(11) NOT NULL,
  `feed_title` text NOT NULL,
  `feed_description` text NOT NULL,
  `feed_link` varchar(500) NOT NULL,
  `feed_pubdate` varchar(500) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `eboo_user_saved_feed`
--


-- --------------------------------------------------------

--
-- Table structure for table `eboo_user_search`
--

CREATE TABLE `eboo_user_search` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `search_query` text NOT NULL,
  `articles_clicked` int(11) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `eboo_user_search`
--

INSERT INTO `eboo_user_search` VALUES(1, 4, 'turkey', 0, '2014-10-30 03:47:42', 1);
INSERT INTO `eboo_user_search` VALUES(2, 4, 'death', 0, '2014-10-30 03:48:46', 1);
INSERT INTO `eboo_user_search` VALUES(3, 4, 'safety', 0, '2014-10-30 03:49:18', 1);
INSERT INTO `eboo_user_search` VALUES(4, 4, 'a', 0, '2014-10-30 03:49:46', 1);
INSERT INTO `eboo_user_search` VALUES(5, 4, 'from', 1, '2014-10-30 03:57:59', 1);
INSERT INTO `eboo_user_search` VALUES(6, 4, 'ebola', 0, '2014-10-30 04:00:02', 1);
INSERT INTO `eboo_user_search` VALUES(7, 4, 'pakis', 2, '2014-10-30 04:00:29', 1);
INSERT INTO `eboo_user_search` VALUES(8, 0, 'aaa', 0, '2014-10-30 04:19:23', 1);
INSERT INTO `eboo_user_search` VALUES(9, 0, 'a', 1, '2014-10-30 04:19:28', 1);
INSERT INTO `eboo_user_search` VALUES(10, 0, 'a', 1, '2014-10-30 04:21:26', 1);
INSERT INTO `eboo_user_search` VALUES(11, 5, 'as', 0, '2014-10-30 07:19:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eboo_user_usage`
--

CREATE TABLE `eboo_user_usage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `total_usage` bigint(20) NOT NULL,
  `links_clicked` int(11) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `eboo_user_usage`
--

INSERT INTO `eboo_user_usage` VALUES(1, 5, 30, 0, '2014-10-30 07:22:42', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ecboo_admin`
--

CREATE TABLE `ecboo_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_user` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `pin` int(11) NOT NULL,
  `reset_pass_code` varchar(100) NOT NULL,
  `session_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ecboo_admin`
--

INSERT INTO `ecboo_admin` VALUES(1, 'admin', 'Admin@123', 'info@eboo.com', 1234, '', '0000-00-00 00:00:00', 1);
