-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 05, 2014 at 06:26 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `eboo_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `eboo_admin`
--

CREATE TABLE IF NOT EXISTS `eboo_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admin_user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `pin` int(11) NOT NULL,
  `reset_pass_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `session_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `eboo_admin`
--

INSERT INTO `eboo_admin` (`id`, `admin_user`, `password`, `email`, `role_id`, `pin`, `reset_pass_code`, `remember_token`, `session_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$WgU22XZQvgjktDodCLMdi.tNw3UJGZPMMNKacZuyQHwi1wSW3H1eS', 'admin@example.org', 1, 1234, '', 'u8Sv2GERJiR8Q9SCBw4z3XlURz6Zl8Y71hp4EFct7m4RH6iSWQx5s2tQwxPM', '0000-00-00', 1, '0000-00-00 00:00:00', '2014-12-04 07:43:43'),
(2, 'user', '$2y$10$ahjyrIWMFQNc2FGHtd9gZOjjPLDlL2c60pZLehcqVrVmAys8balr2', 'user@example.org', 2, 1234, '', NULL, '0000-00-00', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `eboo_admin_roles`
--

CREATE TABLE IF NOT EXISTS `eboo_admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rolename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `eboo_admin_roles`
--

INSERT INTO `eboo_admin_roles` (`id`, `rolename`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2014-12-02 00:50:28', '2014-12-02 00:50:28'),
(2, 'moderator', '2014-12-02 00:50:28', '2014-12-02 00:50:28'),
(3, 'user', '2014-12-02 00:50:28', '2014-12-02 00:50:28');

-- --------------------------------------------------------

--
-- Table structure for table `eboo_category`
--

CREATE TABLE IF NOT EXISTS `eboo_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `eboo_category`
--

INSERT INTO `eboo_category` (`id`, `category_name`, `session_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 'News', '2014-12-02', 1, '2014-12-02 01:04:39', '2014-12-02 01:04:39'),
(2, 'News', '2014-12-04', 1, '2014-12-04 01:36:34', '2014-12-04 01:36:34');

-- --------------------------------------------------------

--
-- Table structure for table `eboo_feeds`
--

CREATE TABLE IF NOT EXISTS `eboo_feeds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rss_link_id` int(11) NOT NULL,
  `article_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `article_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `article_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `article_pubdate` date NOT NULL,
  `clickcount` int(11) NOT NULL,
  `session_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=123 ;

--
-- Dumping data for table `eboo_feeds`
--

INSERT INTO `eboo_feeds` (`id`, `rss_link_id`, `article_title`, `article_description`, `article_link`, `article_pubdate`, `clickcount`, `session_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'VIDEO: ''I was shot twice in Kiev protest''', 'A year ago crowds were protesting in Kiev''s main Square. One of the protesters talked BBC News about his experiences.', 'http://www.bbc.co.uk/news/world-europe-30243323#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-30', 0, '2014-12-02', 2, '2014-12-02 01:07:02', '2014-12-03 23:44:47'),
(2, 1, 'VIDEO: Pope and Patriarch issue joint pledge', 'The Pope has issued a joint call with the eastern Orthodox Church for Christians and Muslims to work together to stop the killing in the Middle East.', 'http://www.bbc.co.uk/news/world-europe-30266960#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-30', 0, '2014-12-02', 2, '2014-12-02 01:07:02', '2014-12-03 23:44:47'),
(3, 1, 'VIDEO: Kabul police boss quits amid attacks', 'Kabul''s police chief has resigned, following a surge in attacks by Afghan militants on foreigners in the city over the past two weeks.', 'http://www.bbc.co.uk/news/world-asia-30266961#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-30', 0, '2014-12-02', 2, '2014-12-02 01:07:02', '2014-12-03 23:44:47'),
(4, 1, 'VIDEO: New threat to humpback whales?', 'Once hunted to near extinction, the humpback whales of the Canadian Pacific are back in larger numbers, and the government has downgraded their status from "threatened" to that "of special concern".', 'http://www.bbc.co.uk/news/world-us-canada-30266958#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-30', 0, '2014-12-02', 2, '2014-12-02 01:07:02', '2014-12-03 23:44:47'),
(5, 1, 'VIDEO: Funeral for hero who tackled bullies', 'Hundreds of mourners have attended the funeral in Germany of a 23-year-old woman who was fatally injured while confronting a group of men harassing two girls.', 'http://www.bbc.co.uk/news/world-europe-30317749#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:43', '2014-12-03 23:44:43'),
(6, 1, 'VIDEO: Risking death for life in Europe', 'Thousands of Africans die every year trying to cross borders into Europe, in search of a better life.', 'http://www.bbc.co.uk/news/world-africa-30321796#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:43', '2014-12-03 23:44:43'),
(7, 1, 'VIDEO: No charges in police chokehold case', 'A grand jury in New York has declined to press criminal charges against a white police officer over the death of an unarmed black man he put into a chokehold - a practice banned by the New York police department.', 'http://www.bbc.co.uk/news/world-us-canada-30321840#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:43', '2014-12-03 23:44:43'),
(8, 1, 'VIDEO: Printing a 3D portrait of Obama', 'President Obama has become the first US president to have his portrait produced through a 3D scanning process.', 'http://www.bbc.co.uk/news/world-us-canada-30306967#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:43', '2014-12-03 23:44:43'),
(9, 1, 'VIDEO: Israel''s PM seeks early election', 'Benjamin Netanyahu sacks two senior ministers and announces he plans to dissolve Israel''s parliament, triggering an early general election.', 'http://www.bbc.co.uk/news/world-middle-east-30305274#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:43', '2014-12-03 23:44:43'),
(10, 1, 'VIDEO: Bhopal leak site 30 years on', 'It has been 30 years since a lethal gas leak from a Union Carbide India''s pesticide factory in the central Indian city of Bhopal killed thousands of people.', 'http://www.bbc.co.uk/news/world-asia-30301659#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:43', '2014-12-03 23:44:43'),
(11, 1, 'VIDEO: Elderly people left behind in Donetsk', 'The BBC''s Fergal Keane visited the front lines around the rebel stronghold of Donetsk to find out what has happened to the people still living in the conflict zone in the east of Ukraine.', 'http://www.bbc.co.uk/news/world-europe-30289699#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(12, 1, 'VIDEO: Could Mugabe''s wife become president?', 'Speculation is building that the President of Zimbabwe, Robert Mugabe, is preparing for his wife to take power.', 'http://www.bbc.co.uk/news/world-africa-30302501#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(13, 1, 'VIDEO: Bangladesh: UK man guilty of contempt', 'A Bangladesh court finds a British man guilty of contempt for challenging the official death toll from the 1971 war of independence.', 'http://www.bbc.co.uk/news/world-asia-30298784#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(14, 1, 'VIDEO: Man clings to ice in Iowa river', 'A man has had a lucky escape from the Des Moines river in the US state of Iowa, after he was spotted clinging to ice by city workers.', 'http://www.bbc.co.uk/news/world-us-canada-30289698#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(15, 1, 'VIDEO: Mexicans protest against president', 'Protesters in Mexico clash with police following a largely peaceful rally in support of 43 students who went missing in September.', 'http://www.bbc.co.uk/news/world-latin-america-30288934#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(16, 1, 'VIDEO: Phone companies target rural India', 'Mobile phone use by women is frowned on in some rural areas of India, but phone companies and women are fighting back, as David Reid reports.', 'http://www.bbc.co.uk/news/technology-30288933#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(17, 1, 'VIDEO: HK protest: ''I can''t just stay at home''', 'The BBC''s John Sudworth has been out with students at the main Hong Kong Occupy movement''s protest camp in Hong Kong''s Admiralty district.', 'http://www.bbc.co.uk/news/world-asia-30289703#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(18, 1, 'VIDEO: Jordan drawn into conflict with IS', 'In four months, the jihadists who call themselves Islamic State has swept through significant chunks of Syria and Iraq, winning themselves what is in effect a state', 'http://www.bbc.co.uk/news/world-middle-east-30286961#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(19, 1, 'VIDEO: Tackling HIV in Myanmar', 'As access to Myanmar increases, is the country left more vulnerable to diseases such as HIV/Aids? Nick Wood reports.', 'http://www.bbc.co.uk/news/health-30244390#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(20, 1, 'VIDEO: N Korea blamed for cyber attack', 'The FBI has warned American businesses that hackers are using malicious software to launch damaging cyber-attacks, after one attack against Sony Pictures, the BBC''s Alastair Leithead reports.', 'http://www.bbc.co.uk/news/world-asia-30288522#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(21, 1, 'VIDEO: Ebola fight shifts to Mali', 'The focus of efforts to stop the spread of the Ebola virus has broadened to the west African state of Mali, where the country has had seven confirmed cases so far, Alex Duval-Smith reports.', 'http://www.bbc.co.uk/news/world-africa-30288523#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(22, 1, 'VIDEO: Obama''s call for police body-camera', 'US President Barack Obama has requested $263m (£167m) to improve police training, pay for body cameras and restore trust in policing.', 'http://www.bbc.co.uk/news/world-us-canada-30286959#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(23, 1, 'VIDEO: ''Brain changes'' in US football players', 'Some teenagers appear to show changes in their brains after one season of playing American football, a small study suggests.', 'http://www.bbc.co.uk/news/world-us-canada-30286962#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(24, 1, 'VIDEO: Life in Kobane after airstrikes', 'Alan Johnston reports on life in the Syrian town of Kobane, following airstrikes targeting IS militants in the town.', 'http://www.bbc.co.uk/news/world-middle-east-30287381#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:43', '2014-12-04 23:35:53'),
(25, 1, 'VIDEO: Pope Francis visits Blue Mosque', 'Mark Lowen reports from Istanbul in Turkey on a visit by Pope Francis to the historic city.', 'http://www.bbc.co.uk/news/world-europe-30259206#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-29', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:47'),
(26, 1, 'VIDEO: Can driving improve Aboriginal health?', 'A project that helps Aboriginal Australians gain their driving licenses could have positive health outcomes for themselves and their community.', 'http://www.bbc.co.uk/news/health-30244393#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-29', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:47'),
(27, 1, 'VIDEO: Australia makes $1.5bn drugs haul', 'Three tonnes of ecstasy and crystal meth with an estimated street value of $1.5bn are seized by Australian police and customs officers.', 'http://www.bbc.co.uk/news/world-australia-30259213#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-29', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:47'),
(28, 1, 'VIDEO: Violence flares after Mubarak cleared', 'A court in Egypt has dropped charges against former President Hosni Mubarak over the killing of 239 protesters during the 2011 uprising against him.', 'http://www.bbc.co.uk/news/world-middle-east-30259323#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-29', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:47'),
(29, 1, 'VIDEO: Inside an American Thanksgiving', 'Matt Danzico and Benjamin Zand of BBC Pop Up spend the holiday at the Danzico residence to investigate how Americans spend Thanksgiving.', 'http://www.bbc.co.uk/news/world-us-canada-30259320#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-29', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:47'),
(30, 1, 'VIDEO: Navy doctor trains in inner-city Chicago', 'To prepare for combat injuries, some military doctors look to treating victims of violence in inner-city Chicago', 'http://www.bbc.co.uk/news/world-us-canada-30243321#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-29', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:47'),
(31, 1, 'VIDEO: New Star Wars trailer causes a storm', 'The teaser trailer for Star Wars: The Force Awakens has just been released and is already being dissected by fans and trending on social media.', 'http://www.bbc.co.uk/news/entertainment-arts-30245680#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:47'),
(32, 1, 'VIDEO: #PutOutYourBats tributes to Hughes', 'Australians have been paying tribute to Phillip Hughes by putting their cricket bats outside their homes, as Phil Mercer reports.', 'http://www.bbc.co.uk/news/world-australia-30241732#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:47'),
(33, 1, 'VIDEO: Flash floods in the French Riviera', 'At least one person has died and three others are missing after torrential rain left homes and roads inundated on the French Riviera.', 'http://www.bbc.co.uk/news/world-europe-30241730#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:47'),
(34, 1, 'VIDEO: Putin''s tiger ''raids Chinese farms''', 'A Siberian tiger, released into the wild by Russian President Vladimir Putin, has apparently been killing animals on a Chinese farm.', 'http://www.bbc.co.uk/news/world-asia-30241178#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:47'),
(35, 1, 'VIDEO: Dozens die in Nigeria mosque attack', 'Many people have been killed in a gun and bomb attack during Friday prayers at one of the biggest mosques in the Nigerian city of Kano.', 'http://www.bbc.co.uk/news/world-africa-30243951#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:47'),
(36, 1, 'VIDEO: Anti-gay Russian gangs film wins award', 'As part of the Rory Peck Awards, the 2014 special Sony Impact Prize went to Ben Steele for his documentary which investigates the Russian vigilantes intimidating the country''s gay community.', 'http://www.bbc.co.uk/news/world-europe-30241735#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:47'),
(37, 1, 'VIDEO: Dancing woman flouts Iranian rules', 'An online video which appears to show an Iranian woman defying Iran''s strict laws by dancing publicly on the Tehran metro has gone viral.', 'http://www.bbc.co.uk/news/world-middle-east-30243950#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:47'),
(38, 1, 'VIDEO: Planes tossed around in Brisbane storm', 'A huge clean-up operation is under way in the Australian city of Brisbane after it was hit by its worst storm in three decades.', 'http://www.bbc.co.uk/news/world-australia-30241728#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:48'),
(39, 1, 'VIDEO: Black Friday: Wal-Mart workers revolt', 'Workers at Wal-Mart, America''s largest retailer, stage a massive Black Friday strike calling for higher wages and better working conditions.', 'http://www.bbc.co.uk/news/30242139#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:48'),
(40, 1, 'VIDEO: The National Front party''s rebranding', 'France''s far-right National Front party is gathering in Lyon ahead of a crucial meeting to decide its future direction.', 'http://www.bbc.co.uk/news/world-europe-30247480#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:48'),
(41, 1, 'VIDEO: African-European talks on migration', 'European and African ministers have been meeting in Rome to discuss ways to deal with heavy migration between the two continents.', 'http://www.bbc.co.uk/news/world-europe-30241731#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:48'),
(42, 1, 'VIDEO: Tensions ease in protest-hit Ferguson', 'A level of calm has returned to Ferguson''s streets following two nights of unrest.', 'http://www.bbc.co.uk/news/world-us-canada-30242919#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:48'),
(43, 1, 'VIDEO: Tension over tea plantation killing', 'Workers kill the owner of a tea plantation in north-eastern India in a dispute over pay.', 'http://www.bbc.co.uk/news/world-asia-30241177#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:48'),
(44, 1, 'VIDEO: Turkey''s declining Christianity', 'A century ago Christians made up 20% of Turkey''s population, the figure is now just 0.2%.', 'http://www.bbc.co.uk/news/world-europe-30241181#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:48'),
(45, 1, 'VIDEO: President pledges Mexico will change', 'The Mexican president promises to reform the corrupt authorities in the country and break their links with organised crime.', 'http://www.bbc.co.uk/news/world-latin-america-30241179#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:48'),
(46, 1, 'VIDEO: War torn Gaza hit by flooding', 'The United Nations Relief and Works Agency has declared an emergency in Gaza City following heavy rain and flooding.', 'http://www.bbc.co.uk/news/world-middle-east-30241737#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:48'),
(47, 1, 'VIDEO: Poroshenko in reform pledge', 'The Ukrainian President Petro Poroshenko has promised a programme of radical reform during an address at the opening session of the country''s new parliament.', 'http://www.bbc.co.uk/news/world-europe-30240159#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-27', 0, '2014-12-04', 2, '2014-12-03 23:44:44', '2014-12-03 23:44:48'),
(48, 1, 'VIDEO: Football club pay for woman''s home', 'Rayo Vallecano football club in Spain say they will help an evicted woman get a new home.', 'http://www.bbc.co.uk/news/world-europe-30220701#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-27', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(49, 1, 'VIDEO: ''I''ve had the Ebola vaccine''', 'WHO spokeswoman Dr Margaret Harris had a shot of the experimental Ebola vaccine last week and tells the BBC about her experience.', 'http://www.bbc.co.uk/news/health-30238582#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-27', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(50, 1, 'VIDEO: Thailand''s democracy in limbo', 'Thailand has been ruled by the military since the coup and the country may not return to democracy for another 18 months.', 'http://www.bbc.co.uk/news/world-asia-30241175#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-28', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(51, 1, 'VIDEO: Greece rescues 700 migrants from ship', 'Greek authorities have rescued over 700 migrants from a cargo ship which lost engine power in gale-force winds.', 'http://www.bbc.co.uk/news/world-30226833#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-27', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(52, 1, 'VIDEO: Phillip Hughes dies aged 25', 'BBC Sport looks back at the career of Australia batsman Phillip Hughes, who has died aged 25 after being hit on the head.', 'http://www.bbc.co.uk/sport/0/cricket/30220429', '2014-11-27', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(53, 1, 'VIDEO: Tamir Rice shot ''within two seconds''', 'Video footage of the fatal shooting of a 12-year-old in Cleveland shows he was shot within two seconds of the police arriving.', 'http://www.bbc.co.uk/news/world-us-canada-30220700#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-27', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(54, 1, 'VIDEO: Snow and rain calms Ferguson protest', 'After two days of unrest, the streets of Ferguson in Missouri were mostly quiet on Wednesday night, as Rajini Vaidyanathan reports.', 'http://www.bbc.co.uk/news/world-us-canada-30226829#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-27', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(55, 1, 'VIDEO: Hail storm causes chaos in Brisbane', 'A powerful storm cut power to tens of thousands of homes and businesses in Brisbane, Australia, bringing "golf ball-sized" hailstones with it.', 'http://www.bbc.co.uk/news/world-australia-30236163#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-27', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(56, 1, 'VIDEO: Parrots find home in Caracas', 'Caracas, the Venezuelan capital, is notorious for its crime and pollution, but it also, somewhat strangely for a city, has a thriving population of macaws.', 'http://www.bbc.co.uk/news/world-latin-america-30220699#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-27', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(57, 1, 'VIDEO: The new wave of Israeli filmmakers', 'Israeli directors are creating everything from westerns to cannibalistic fantasies as Talking Movies'' Tom Brook has been finding out.', 'http://www.bbc.co.uk/news/entertainment-arts-30207913#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-27', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(58, 1, 'VIDEO: Polio workers killed in Pakistan', 'Authorities in Balochistan, Pakistan, have suspended a campaign against polio after four workers were shot dead by unidentified gunmen in the city of Quetta.', 'http://www.bbc.co.uk/news/world-asia-30216658#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-26', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(59, 1, 'VIDEO: Can drones get water to stranded Joe?', 'In an annual challenge, teams attempt to deliver water using drones to a remote part of the Australian Outback.', 'http://www.bbc.co.uk/news/technology-30196627#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-26', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(60, 1, 'VIDEO: Why are Thais raising three fingers?', 'A Thai protester says the military government has reacted aggressively to their use of the salute featured in the Hunger Games films.', 'http://www.bbc.co.uk/news/world-asia-30225067#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-27', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(61, 1, 'VIDEO: ''I found rare Shakespeare Folio''', 'Librarian and Medieval literature expert Rémy Cordonnier tells the BBC about the moment he discovered a rare and valuable William Shakespeare First Folio.', 'http://www.bbc.co.uk/news/entertainment-arts-30207912#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-26', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(62, 1, 'VIDEO: What does Ukraine''s future hold?', 'One year since popular protests began in Ukraine, a new government prepares to take office, amid calls for reform.', 'http://www.bbc.co.uk/news/world-europe-30205653#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-26', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(63, 1, 'VIDEO: A new face for Africa''s men', 'The male grooming and beauty industry is booming in South Africa, with products now targeting a new audience - black men, as the BBC''s Milton Nkosi reports.', 'http://www.bbc.co.uk/news/world-africa-30193477#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-26', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(64, 1, 'VIDEO: Mobile showers for the homeless', 'A new initiative in San Francisco offers the city''s homeless population mobile showers and toilets in a converted bus.', 'http://www.bbc.co.uk/news/technology-30203229#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-26', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(65, 1, 'VIDEO: Angelina Jolie on directing Unbroken', 'Oscar winning actress Angelina Jolie has attended the premiere in London of her new film, Unbroken.', 'http://www.bbc.co.uk/news/entertainment-arts-30203202#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-25', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:48'),
(66, 1, 'VIDEO: In search of the American Dream', '1954 saw the closure of Ellis Island. Isabel Belarsky passed through the American mass immigration centre.', 'http://www.bbc.co.uk/news/magazine-30098411#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-26', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:49'),
(67, 1, 'VIDEO: Flash floods kill dozens in Morocco', 'Flooding in southern Morocco has killed 32 people and six others are missing after the heavy rains, according to officials.', 'http://www.bbc.co.uk/news/world-africa-30199889#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-25', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:49'),
(68, 1, 'VIDEO: How to hack a molecular microscope', 'A PhD student from Brunel University London has saved himself £100,000 by ''hacking'' his own kit.', 'http://www.bbc.co.uk/news/technology-30200752#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-26', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:49'),
(69, 1, 'VIDEO: ''Tense'' in Dewani trial courtroom', 'Jon Kay reports from the courtroom in Cape Town, as South African prosecutors try to persuade a judge not to drop the murder case against British businessman Shrien Dewani.', 'http://www.bbc.co.uk/news/uk-30189543#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-25', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:49'),
(70, 1, 'VIDEO: Cairo collapse leaves 17 dead', 'At least 17 people have been killed after a block of flats collapsed in the Egyptian capital, Cairo, the BBC''s Orla Guerin reports', 'http://www.bbc.co.uk/news/world-middle-east-30194529#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-25', 0, '2014-12-04', 2, '2014-12-03 23:44:45', '2014-12-03 23:44:49'),
(71, 1, 'VIDEO: Pope claims Europe appears ''elderly''', 'Pope Francis warns that the world sees Europe as "somewhat elderly and haggard" during a speech to the European Parliament in Strasbourg.', 'http://www.bbc.co.uk/news/world-30200083#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-25', 0, '2014-12-04', 2, '2014-12-03 23:44:46', '2014-12-03 23:44:49'),
(72, 1, 'VIDEO: Fury turns to violence in Ferguson', 'The US town of Ferguson has seen rioting and looting after a jury decided not to bring charges over the killing of a black teenager.', 'http://www.bbc.co.uk/news/world-us-canada-30187762#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-25', 0, '2014-12-04', 2, '2014-12-03 23:44:46', '2014-12-03 23:44:49'),
(73, 1, 'VIDEO: Extreme adventurers adopt plucky dog', 'A team of Swedish extreme adventurers on a 10-day race through the Amazon rainforest have adopted a stray dog who started following them around after they fed him.', 'http://www.bbc.co.uk/news/world-latin-america-30194531#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-25', 0, '2014-12-04', 2, '2014-12-03 23:44:46', '2014-12-03 23:44:49'),
(74, 1, 'VIDEO: Underwear protest by Mexican farmers', 'Mexican farmers hold a protest wearing just underpants and a photograph to protest against alleged abuse of power.', 'http://www.bbc.co.uk/news/world-latin-america-30191574#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-25', 0, '2014-12-04', 2, '2014-12-03 23:44:46', '2014-12-03 23:44:49'),
(75, 1, 'VIDEO: Ebola survivors offer ''glimmer of hope''', 'The BBC''s Global Health Correspondent Tulip Mazumdar reports from the Guinean capital Conakry on how some Ebola survivors hope to ease the Ebola crisis in their country.', 'http://www.bbc.co.uk/news/world-africa-30175773#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-24', 0, '2014-12-04', 2, '2014-12-03 23:44:46', '2014-12-03 23:44:49'),
(76, 1, 'VIDEO: Thai taste machine tests authenticity', 'A group of scientists in Bangkok have invented a machine to determine whether Thai food tastes authentic.', 'http://www.bbc.co.uk/news/world-asia-30143804#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-25', 0, '2014-12-04', 2, '2014-12-03 23:44:46', '2014-12-03 23:44:49'),
(77, 1, 'VIDEO: ''End of life care needs more effort''', 'Professor Atul Gawande of Harvard University on why more effort needs to be made to improve the last stage in people''s lives.', 'http://www.bbc.co.uk/news/health-30180747#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-25', 0, '2014-12-04', 2, '2014-12-03 23:44:46', '2014-12-03 23:44:49'),
(78, 1, 'VIDEO: Volcano erupts on Cape Verde island', 'A volcano has erupted on the Cape Verde island of Fogo, resulting in the evacuation of hundreds of residents and the closure of a local airport.', 'http://www.bbc.co.uk/news/world-africa-30175979#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-11-24', 0, '2014-12-04', 2, '2014-12-03 23:44:46', '2014-12-03 23:44:49'),
(79, 2, 'Australian eyes turn from Macksville to Adelaide', 'SYDNEY (Reuters) - The funeral of Phillip Hughes has brought an element of closure for Australia''s players according to coach Darren Lehmann, who now hopes they will honour their former team mate in the way they play the first test against India next week', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/yjwCNoM6vr0/story01.htm', '2014-12-04', 0, '2014-12-04', 1, '2014-12-03 23:44:46', '2014-12-03 23:44:46'),
(80, 2, 'Ukraine energy minister says "no threat" from accident at nuclear plant', 'KIEV (Reuters) - An accident at a nuclear power plant in Zaporizhzhya in southeastern Ukraine poses no danger to health or the environment, energy authorities said on Wednesday, an assessment later corroborated by the French nuclear institute IRSN.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/lF4BmFLcH6E/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:46', '2014-12-03 23:44:46'),
(81, 2, 'Victims call for justice 30 years after Bhopal disaster', 'BHOPAL (Thomson Reuters Foundation) - Hundreds of protesters gathered on Wednesday outside the abandoned factory which caused the world''s deadliest industrial disaster in Bhopal, burning effigies and demanding justice for survivors suffering 30 years late', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/3h3mUwx7PUg/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:46', '2014-12-03 23:44:46'),
(82, 2, 'Modi under pressure over minister''s tirade against non-Hindus', 'NEW DELHI (Reuters) - Prime Minister Narendra Modi is under growing pressure to sack a minister over a tirade she made against religious minorities, as his outraged opponents disrupted parliament for a second day on Wednesday.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/I0t-pwPsjl0/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:46', '2014-12-03 23:44:46'),
(83, 2, 'Government eases FDI rules for construction sector', 'NEW DELHI (Reuters) - India has eased foreign direct investment rules for the construction sector, the government said on Wednesday, in an effort to attract more money into the country to build new hotels, housing and townships.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/ZyMZMVJiWx0/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:46', '2014-12-03 23:44:46'),
(84, 2, 'Hong Kong "Occupy" leaders surrender as pro-democracy protests appear to wither', 'HONG KONG (Reuters) - Leaders of Hong Kong''s Occupy Central movement surrendered to police on Wednesday for their role in democracy protests that the government has deemed illegal, the latest sign that the civil disobedience campaign may be running out of', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/2JPiETm3fGI/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:46', '2014-12-03 23:44:46'),
(85, 2, 'China and Turkey fall behind in global corruption index', 'BERLIN (Reuters) - Turkey and China''s rating on perceived corruption has fallen steeply, according to Transparency International (TI), and the global watchdog called for closer international cooperation to root out graft and abuses of power.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/LG2xXBHb_c8/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:46', '2014-12-03 23:44:46'),
(86, 2, 'Indian army kill six militants crossing into Kashmir to disrupt vote', 'HANDWARA, India (Reuters) - Soldiers killed six militants near the border with Pakistan in Jammu & Kashmir, the army said on Wednesday, in the biggest single-day shootouts in months at the heavily militarised border.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/FruTXQ4-b7M/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:47', '2014-12-03 23:44:47'),
(87, 2, 'Pakistan police register blasphemy case against "disco mullah"', 'ISLAMABAD (Reuters) - Pakistan police said on Wednesday they were investigating blasphemy allegations against a man dubbed the "disco mullah" who quit a career in pop music to become a preacher.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/IOikPohm4S0/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:47', '2014-12-03 23:44:47'),
(88, 2, 'Australia bids emotional farewell to Hughes', 'MACKSVILLE, Australia (Reuters) - Australia bid an emotional farewell to cricketer Phillip Hughes at a funeral in his hometown on Wednesday with a live coast-to-coast broadcast allowing a nation to unite in celebration of the life of a sportsman cut down ', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/kI4k4b3_KCM/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:47', '2014-12-03 23:44:47'),
(89, 2, 'Disabled women in India "locked up, abused"', 'LONDON (Thomson Reuters Foundation) - Vidya was alone at home in Mumbai when three people posing as government health workers knocked on her door.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/4MtwcgBRl6k/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:47', '2014-12-03 23:44:47'),
(90, 2, 'Clarke''s haunting tribute to team mate Hughes', 'MACKSVILLE, Australia (Reuters) - Australia captain Michael Clarke urged mourners to "dig in and get through to tea" as he choked back tears in an emotional eulogy delivered to Phillip Hughes at the cricketer''s funeral in Macksville, New South Wales, on W', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/FANXXhRLGUw/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:47', '2014-12-03 23:44:47'),
(91, 2, 'Officials hiding ill-gotten gains undermine corruption fight - watchdog', 'LONDON (Thomson Reuters Foundation) - Efforts to end corruption will be undermined as long as corrupt officials are able to hide their ill-gotten gains in other countries, a global watchdog said on Wednesday as it released its annual corruption rankings.\n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/wVVX-BDSKB4/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:47', '2014-12-03 23:44:47'),
(92, 2, 'Rattled by Chinese submarines, India joins other nations in rebuilding fleet', 'NEW DELHI (Reuters) - India is speeding up a navy modernisation programme and leaning on its neighbours to curb Chinese submarine activity in the Indian Ocean, as nations in the region become increasingly jittery over Beijing''s growing undersea prowess.\n ', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/pdJjFwx9oUs/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:47', '2014-12-03 23:44:47'),
(93, 2, 'Factbox - Australia batsman Phillip Hughes', 'REUTERS - Factbox on Australian cricketer Phillip Hughes, who will be buried in his home town of Macksville on Wednesday.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/UigC3XMruQg/story01.htm', '2014-12-03', 0, '2014-12-04', 1, '2014-12-03 23:44:47', '2014-12-03 23:44:47'),
(94, 2, 'Lebanon detains wife of Islamic State leader', 'BEIRUT (Reuters) - The Lebanese army detained a wife and daughter of Islamic State leader Abu Bakr al-Baghdadi as they crossed from Syria nine days ago, security officials said on Tuesday, in a move seen as likely to put pressure on the Islamist chief.\n  ', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/E2kIkaDPbGk/story01.htm', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:47', '2014-12-04 23:35:53'),
(95, 2, 'Don''t ban bouncers, says Sehwag', 'MUMBAI (Reuters) - Banning bouncers following the death of Australia''s Phillip Hughes would be unfair on bowlers because batsmen always have the option of ducking under short-pitched deliveries, former India opener Virender Sehwag said on Tuesday.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/WLTVawqm3dw/story01.htm', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:47', '2014-12-04 23:35:53'),
(96, 2, 'Supreme Court lets Sahara sell $424 million of local properties', 'MUMBAI (Reuters) - Sahara conglomerate on Tuesday won the Supreme Court''s approval to sell four of its domestic properties, which it said would raise 26.22 billion rupees ($424 million), crucial to secure the bail of its jailed chief.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/2ar1I3jN7PE/story01.htm', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:47', '2014-12-04 23:35:53'),
(97, 2, 'RBI leaves rates on hold, says could ease early 2015', 'MUMBAI (Reuters) - The Reserve Bank of India held interest rates steady as widely expected at a policy review on Tuesday, and said it could ease monetary policy early next year provided inflationary pressures do not reappear and the government controls th', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/I0gfCNgxAdE/story01.htm', '2014-12-02', 0, '2014-12-04', 2, '2014-12-03 23:44:47', '2014-12-04 23:35:53'),
(98, 2, 'Modi seeks to draw line under row over minister''s pro-Hindu comments', 'NEW DELHI (Reuters) - Prime Minister Narendra Modi sought on Thursday to draw a line under a row sparked by a ministerial colleague''s derogatory comments about non-Hindus, urging angry parliament deputies to accept her apology and move on.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/6KqDMtxulIQ/story01.htm', '2014-12-04', 0, '2014-12-04', 1, '2014-12-04 01:51:04', '2014-12-04 01:51:04'),
(99, 2, 'Islamic State cedes little ground despite air attacks', 'BAGHDAD/BEIRUT (Reuters) - They have made enemies across the globe and endured three months of U.S.-led air strikes, but Islamic State fighters have surrendered little of their self-declared caliphate to the broad sweep of forces arrayed against them.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/qdH_uFLjRsA/story01.htm', '2014-12-04', 0, '2014-12-04', 1, '2014-12-04 01:51:04', '2014-12-04 01:51:04'),
(100, 2, 'Protesters block NY streets after officer cleared in chokehold death', 'NEW YORK (Reuters) - Thousands of demonstrators disrupted New York City traffic into early Thursday after a grand jury decided not to bring charges against a white police officer in the chokehold death of an unarmed black man.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/YUitz74V4qk/story01.htm', '2014-12-04', 0, '2014-12-04', 1, '2014-12-04 01:51:04', '2014-12-04 01:51:04'),
(101, 2, 'Shaun Marsh called up as Australian eyes turn to Adelaide', 'SYDNEY (Reuters) - Australia have called batsman Shaun Marsh into their squad for the opening test of the four-match series against India, which was pushed back to next week after the death of Phillip Hughes.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/yjwCNoM6vr0/story01.htm', '2014-12-04', 0, '2014-12-04', 1, '2014-12-04 01:51:04', '2014-12-04 01:51:04'),
(102, 2, 'Ebola booster vaccine starts first trials in Oxford', 'LONDON (Reuters) - Scientists at Oxford University have launched the first clinical tests of a new Ebola vaccine approach, using a booster developed by Denmark''s Bavarian Nordic that may improve the effects of a shot from GlaxoSmithKline.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/gTsM3AvskHo/story01.htm', '2014-12-04', 0, '2014-12-04', 1, '2014-12-04 04:24:38', '2014-12-04 04:24:38'),
(103, 2, 'Hughes tragedy hangs over India warm-up match', 'REUTERS - The tragedy of Phillip Hughes''s shocking death hung like a dark cloud over the Gliderol Stadium on Thursday as India shifted their focus to cricket with a hastily arranged two-day practice match against a Cricket Australia XI in Adelaide.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/Lne478ArsVw/story01.htm', '2014-12-04', 0, '2014-12-04', 1, '2014-12-04 04:24:38', '2014-12-04 04:24:38'),
(104, 2, 'Ryan Harris says to play Adelaide, Marsh called up', 'SYDNEY (Reuters) - Australia fast bowler Ryan Harris has declared himself a certain starter for the first test against India but cannot say the same for all of his team mates a day after Phillip Hughes'' funeral.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/yjwCNoM6vr0/story01.htm', '2014-12-04', 0, '2014-12-04', 1, '2014-12-04 04:24:38', '2014-12-04 04:24:38'),
(105, 2, 'End of the road for Delhi''s old cars as India battles smog', 'NEW DELHI (Reuters) - The National Green Tribunal has banned all vehicles older than 15 years from the streets of the capital, New Delhi, in a bid to clean up air that one prominent study this year found to be the world''s dirtiest.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/AWvUWmLSvsk/story01.htm', '2014-12-04', 0, '2014-12-04', 1, '2014-12-04 04:24:38', '2014-12-04 04:24:38'),
(106, 2, 'Myanmar workers charged with murder of British tourists in Thailand', 'BANGKOK (Reuters) - Two Myanmar men were charged on Thursday with the murder of two British tourists in Thailand, public prosecutors said, the latest turn in a roller-coaster case that has been blighted by allegations of a bungled investigation and ill tr', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/S7M_WkLl3aI/story01.htm', '2014-12-04', 0, '2014-12-04', 1, '2014-12-04 04:24:38', '2014-12-04 04:24:38'),
(107, 1, 'VIDEO: Afghan security fears impact economy', 'As foreign troops prepare to leave Afghanistan, security fears are already impacting on the country''s economy, reports Mike Wooldridge from Kabul.', 'http://www.bbc.co.uk/news/world-asia-30325571#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:51', '2014-12-04 23:35:51'),
(108, 1, 'VIDEO: Toddler''s buggy rolls onto rail track', 'A toddler''s pushchair has rolled off a train platform and onto the tracks at a station in Melbourne, Australia, causing the 18-month old minor injuries.', 'http://www.bbc.co.uk/news/world-australia-30334122#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(109, 1, 'VIDEO: British ''Hero of Israel'' reburied', 'The ashes of John Henry Patterson are to be reburied in Israel after a state funeral attended by Prime Minister Benjamin Netanyahu.', 'http://www.bbc.co.uk/news/world-middle-east-30325189#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(110, 1, 'VIDEO: Obama: Americans feel deep unfairness', 'President Obama has said many Americans sense a "deep unfairness" within the US criminal justice system, following the death of a black man who was placed in an apparent chokehold by a white police officer.', 'http://www.bbc.co.uk/news/world-us-canada-30334610#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(111, 1, 'VIDEO: Cosby urged to allow court cases', 'The American comedian Bill Cosby has been urged to waive his rights under the statute of limitations, so the courts can hear new allegations of sexual assault.', 'http://www.bbc.co.uk/news/world-us-canada-30324388#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(112, 1, 'VIDEO: Thriving community to graveyard', 'As a key UN climate change conference continues, BBC News visits a Nepali village destroyed by a landslide and finds a lack of funds is delaying protection projects.', 'http://www.bbc.co.uk/news/world-asia-30325022#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(113, 1, 'VIDEO: Putin: We are facing hard times', 'President Putin uses his state of the nation address to warn of hard times and accuse Western governments of trying to create a new iron curtain.', 'http://www.bbc.co.uk/news/world-europe-30328561#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(114, 1, 'VIDEO: Are pomegranates key to Afghan future?', 'Daud Qarizadah reports on plans to replace poppies with pomegranates in Afghanistan''s agriculture industry.', 'http://www.bbc.co.uk/news/world-asia-30328723#sa-ns_mchannel=rss&ns_source=PublicRSS20-sa', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(115, 2, 'Abbott to decide whether to return to SCG after Hughes shock', 'MELBOURNE (Reuters) - Sean Abbott alone will decide whether he wants to return to first class cricket at the Sydney Cricket Ground on Tuesday, two weeks after his short-pitched delivery felled Phillip Hughes at the same wicket, according to his state team', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/tgfweJWt0eQ/story01.htm', '2014-12-05', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(116, 2, 'No place for 2011 hero Yuvraj in India''s World Cup squad', 'NEW DELHI (Reuters) - All-rounder Yuvraj Singh has been left out of the India squad which will defend the 50-over World Cup title he helped them win in 2011.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/AMOpgKcbFeg/story01.htm', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(117, 2, 'U.S., Britain pledge to support Afghanistan as combat troops withdraw', 'LONDON (Reuters) - The United States and Britain pledged on Thursday to support Afghanistan''s new unity government as foreign combat troops withdraw from the country after a 13-year involvement.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/hOAr8wTnVFw/story01.htm', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(118, 2, 'James Bond to return in "SPECTRE", cast and car unveiled', 'IVER HEATH, England (Reuters) - James Bond will take on a sinister organisation with links to his past in the next installment of the blockbuster spy series, which star director Sam Mendes said on Thursday would be called "SPECTRE".\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/CPsx9WWNsVk/story01.htm', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(119, 2, 'Pakistani children test positive for HIV after blood transfusions', 'ISLAMABAD (Reuters) - At least 10 Pakistani children who received blood transfusions have tested positive for HIV, officials said Thursday, with a leading medic predicting the discovery could be just the "tip of the iceberg".\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/c_ptvZXza2E/story01.htm', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(120, 2, 'Force India to use Toyota wind tunnel', 'LONDON (Reuters) - Force India will use Toyota''s wind tunnel in Germany for all their aerodynamic testing from next season, the British-based Formula One team announced on Thursday.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/t-PVMhSGIh4/story01.htm', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(121, 2, 'HIGHLIGHTS - Putin delivers keynote speech on economy, Ukraine', 'MOSCOW (Reuters) - Here are quotes from Russian President Vladimir Putin''s annual state of the union speech to members of parliament and other top officials in the Kremlin on Thursday.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/6fPDgSh9Koc/story01.htm', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52'),
(122, 2, 'Al Qaeda''s Yemen branch issues video purporting to show U.S. captive', 'DUBAI (Reuters) - Al Qaeda''s Yemen branch published a video purporting to show an American hostage and threatened to kill him if unspecified demands were not met.\n  \n', 'http://feeds.reuters.com/~r/reuters/INtopNews/~3/UHaVBxw04rs/story01.htm', '2014-12-04', 0, '2014-12-05', 1, '2014-12-04 23:35:52', '2014-12-04 23:35:52');

-- --------------------------------------------------------

--
-- Table structure for table `eboo_publication`
--

CREATE TABLE IF NOT EXISTS `eboo_publication` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `publication_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `eboo_publication`
--

INSERT INTO `eboo_publication` (`id`, `publication_name`, `publication_image`, `session_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 'FOX', 'images/FOX.png', '2014-12-02', 1, '2014-12-02 01:05:13', '2014-12-02 01:05:13');

-- --------------------------------------------------------

--
-- Table structure for table `eboo_rss`
--

CREATE TABLE IF NOT EXISTS `eboo_rss` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `publication_id` int(11) NOT NULL,
  `rss_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `eboo_rss`
--

INSERT INTO `eboo_rss` (`id`, `category_id`, `publication_id`, `rss_link`, `session_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'http://feeds.bbci.co.uk/news/video_and_audio/world/rss.xml?edition=uk', '2014-12-02', 1, '2014-12-02 01:05:59', '2014-12-02 01:05:59'),
(2, 1, 1, 'http://feeds.reuters.com/reuters/INtopNews', '2014-12-02', 1, '2014-12-02 01:07:48', '2014-12-02 01:07:48');

-- --------------------------------------------------------

--
-- Table structure for table `eboo_users`
--

CREATE TABLE IF NOT EXISTS `eboo_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pincode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hardware_info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `software_version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fb_handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tumblr_handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin_handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_usage` int(11) NOT NULL,
  `session_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `eboo_user_clicked_data`
--

CREATE TABLE IF NOT EXISTS `eboo_user_clicked_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `u_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `feeds_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `eboo_user_search`
--

CREATE TABLE IF NOT EXISTS `eboo_user_search` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `u_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `search_query` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `articles_clicked` int(11) NOT NULL,
  `session_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `eboo_user_usage`
--

CREATE TABLE IF NOT EXISTS `eboo_user_usage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `u_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_usage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `links_clicked` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_15_042556_create_eboo_admin_table', 1),
('2014_10_15_095039_create_eboo_category_table', 1),
('2014_10_16_074350_create_eboo_rss_table', 1),
('2014_10_16_095340_create_eboo_publication_table', 1),
('2014_10_17_072549_create_eboo_feeds_table', 1),
('2014_10_25_093504_create_eboo_admin_roles', 1),
('2014_10_25_101226_add_role_to_eboo_admin_table', 1),
('2014_10_30_045514_create_password_reminders_table', 1),
('2014_10_31_064155_create_eboo_users_table', 1),
('2014_10_31_070254_create_eboo_user_usage_table', 1),
('2014_10_31_072540_add_clickcount_to_eboo_feeds_table', 1),
('2014_10_31_073228_create_eboo_user_clicked_data_table', 1),
('2014_10_31_073959_create_eboo_user_search_table', 1),
('2014_12_05_050812_create_oauth_session_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_session`
--

CREATE TABLE IF NOT EXISTS `oauth_session` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `state` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `access_token` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `expiry` datetime NOT NULL,
  `type` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `server` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `creation` datetime NOT NULL,
  `access_token_secret` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `authorized` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `user` int(11) NOT NULL,
  `refresh_token` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `oauth_session_session_server_unique` (`session`,`server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_reminders`
--

CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_reminders_email_index` (`email`),
  KEY `password_reminders_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
