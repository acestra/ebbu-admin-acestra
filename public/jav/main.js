

$(document).ready(function() {
	var url = window.location;
	$('div.collapse ul.nav a').filter(function() {
		return this.href == url;
	}).parent().addClass('active');


	   $('#example').dataTable( {
        "order": [[ 0, "desc" ], [1,'asc']],
         "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "jav/swf/copy_csv_xls_pdf.swf",
            
        }

    } );


     $('#example1').dataTable( {
        "order": [[ 0, "desc" ], [1,'asc']],
         "dom": 'T<"clear">lfrtip',
        "tableTools": {
            "sSwfPath": "jav/swf/copy_csv_xls_pdf.swf"
        }
    } );

    $('#Trending').dataTable( {
        "order": [[ 4, "desc" ]]
    } );
    $('#twitter-feed').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "twittersync-ajax",
            "dataSrc": "data"
        },
        'aoColumnDefs': [{
           'bSortable': false,
           'aTargets': [1,2]
        }],
        "order": [[ 5, "desc" ]]
    } );

    $('#twitter-feed-old').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "ajax/twittersync-old",
            "dataSrc": "data"
        },
        'aoColumnDefs': [{
           'bSortable': false,
           'aTargets': [1,2]
        }],
        "order": [[ 4, "desc" ]]
    } );

    $('#Rss-Feed').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "feedsync-ajax",
            "dataSrc": "data"
        },
        'aoColumnDefs': [{
           'bSortable': false,
           'aTargets': [1,2]
        }],
        "order": [[ 5, "desc" ]]
    } );
    $('#Rss-Feed-Old').dataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "ajax/feedsync-old",
            "dataSrc": "data"
        },
        'aoColumnDefs': [{
           'bSortable': false,
           'aTargets': [1,2]
        }],
        "order": [[ 4, "desc" ]]
    } );

    $('#trending-user').dataTable( {
        "order": [[ 3, "desc" ]]
    } );

	var url = window.location.href;
	var host = window.location.host;
	if(url.indexOf('http://' + host + '/feedsync') != -1) {
		$('div.collapse-main ul.nav a').parents('li#feedsync').addClass('active');
	}

/* User Profile */

    var panels = $('.user-infos');
    var panelsButton = $('.dropdown-user');
    panels.hide();

    //Click dropdown
    panelsButton.click(function() {
        //get data-for attribute
        var dataFor = $(this).attr('data-for');
        var idFor = $(dataFor);

        //current button
        var currentButton = $(this);
        idFor.slideToggle(400, function() {
            //Completed slidetoggle
            if(idFor.is(':visible'))
            {
                currentButton.html('<i class="glyphicon glyphicon-chevron-up text-muted"></i>');
            }
            else
            {
                currentButton.html('<i class="glyphicon glyphicon-chevron-down text-muted"></i>');
            }
        })
    });


    $('[data-toggle="tooltip"]').tooltip();
  $("#change_logo").css('display','none');
      $("#change_logo_buttton").click(function(){
         //alert('hi');
            $("#change_logo").css('display','block');
            $("#change_logo_buttton_div").css('display','none');
            $("#image").css("display",'none')
      });
      var i = 1;
  $(".add_search_term").click(function(e){
      var cat_term_db = $('.cat_term_db').val();
      //alert(cat_term_db);

   if(cat_term_db != '')
   {
     var cat_term_db_count = $('.cat_term_db').val();
     var cat_term_db_var = parseInt(10) - parseInt(cat_term_db_count);
   }else{
     var cat_term_db_var = parseInt(9);
   }
   
   //alert(cat_term_db_var);
    e.preventDefault();
    if(i <= cat_term_db_var)
    {
      $(".new_add").append('<div class="form-group remove_more_div_'+i+'"><label class="col-sm-3 control-label" id="Search Term">New Search Term '+ i+'</label><div class="col-sm-8"><input type="text" class="form-control" name="search_term_ajax[]"/></div><div class="col-sm-1"><a class="remove_more" onClick="remove_more('+i+')">X</a></div></div>');
     }
         i++;
         });
   $( ".promote_expire" ).datepicker({
      dateFormat: 'yy-mm-dd' 
    });
});
 function remove_more(id)
  {
      $(".remove_more_div_"+id).remove(); 
  }

  function remove_more_ajax(id)
  {
    //alert(id);
      $.ajax({
                url: "../../remove_more_in_edit",
                type: "get",
                data :{
                   search_term_id:id, 
                },
                success: function(data){
                    
                  $("#remove_more_edit_div_"+data).css('display','none');
                      
                  
                       
                }
            });
  }